package mathhelper.twf.defaultcontent.defaultrulepacks

class DefaultRulePacks {
    companion object {
        val defaultRulePacks = StandardMathRulePacks.get() +
                DefaultCombinatoricsRulePacks.get() +
                DefaultComplexRulePacks.get() +
                LogicRulePacks.get() +
                SetRulePacks.get() +
                DefaultPhysicsRulePacks.get()

        val defaultRulePacksMap = defaultRulePacks.associateBy { it.code!! }

        fun map() = defaultRulePacksMap
    }
}
