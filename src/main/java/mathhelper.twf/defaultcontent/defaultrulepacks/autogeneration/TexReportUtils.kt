package mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration

import mathhelper.twf.api.structureStringToTexString
import mathhelper.twf.config.SolutionsStepITR
import mathhelper.twf.config.TaskITR
import mathhelper.twf.taskautogeneration.ExpressionUtils

class TexReportUtils {

    companion object {

        fun renderToTex(tasks: List<TaskITR>, reportType: ReportType): String = when (reportType) {
            ReportType.RESULT_ONLY -> resultOnlyReport(tasks)
            ReportType.RESULT_WITH_POSTPROCESSING -> resultWithPostProcessingReport(tasks)
            ReportType.RESULT_WITH_SUBSTITUTIONS -> resultWithSubstitutionsReport(tasks)
            ReportType.RESULT_WITH_SUBSTITUTIONS_AND_INTERMEDIATE_EXPRESSIONS -> fullReport(tasks)
        }

        private fun addDollars(str : String?) : String {
            return StringBuilder("\$").append(str).append("\$").toString()
        }

        private fun makeReplacements(report: String) : String {
            var result = report.replace("π", "\\pi")
            result = result.replace("ctg", "cot")
            result = result.replace("tg", "tan")
            return result
        }

        private fun resultWithPostProcessingReport(tasks: List<TaskITR>): String {
            val reportBuilder = StringBuilder()
            for (i in tasks.indices) {
                val task = tasks[i]
                val intermediateExpression1 = task.expressionTaskIntermediateData?.expressionBeforePostprocessPhase1?.toString() ?: ""
                val intermediateExpression2 = task.expressionTaskIntermediateData?.expressionBeforePostprocessPhase2?.toString() ?: ""
                reportBuilder.append("${i+1})\n\n")
                reportBuilder.append("original task: ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(task.originalExpressionTex)}}\n\n")
                reportBuilder.append("generated task (before postprocessing phase 1): ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(structureStringToTexString(intermediateExpression1))}}\n\n")
                reportBuilder.append("generated task (before postprocessing phase 2): ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(structureStringToTexString(intermediateExpression2))}}\n\n")
                reportBuilder.append("generated task (after \\ \\ postprocessing): ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(task.goalExpressionTex)}}\n\n")
            }
            var report = reportBuilder.toString()
            report = makeReplacements(report)
            return report
        }

        fun fullReport(tasks: List<TaskITR>): String {
            val reportBuilder = StringBuilder()
            for (i in tasks.indices) {
                val task = tasks[i]
                val appliedPostprocessSubstitutions = task.expressionTaskIntermediateData!!.appliedPostprocessSubstitutions
                val steps : List<SolutionsStepITR> = task.solutionsStepsTree?.values?.first() as List<SolutionsStepITR>
                val expressionChain = task.solutionPlainText?.split(" = ") ?: arrayListOf()
                val expressionChainTex = mutableListOf<String>()
                for ((j, step) in steps.withIndex()) {
                    val expressionState = expressionChain[j]
                    val structureString = ExpressionUtils.stringToGeneratedExpression(
                        expressionState
                    ).expressionNode.identifier
                    val rule = step.substitution.code
                    var color = if (step.substitution.isExtending) "red" else "orange"
                    val lastStepId = appliedPostprocessSubstitutions[step.substitution.code] ?: Int.MAX_VALUE
                    if (step.stepId >= lastStepId) {
                        color = "green"
                    }
                    expressionChainTex.add("\\textcolor{$color}{" + "[${ruleToTex(rule)}]" + "}" + " = " + "\\textcolor{black}{" + structureStringToTexString(structureString) + "}" + " = " + "\\newline")
                }
                task.goalExpressionTex?.let { expressionChainTex.add(it) }

                val solution = expressionChainTex.joinToString("$ \\newline = $", "\\texttt{$", "$}")

                reportBuilder.append("${i+1})\n\n")
                reportBuilder.append("original task: ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(task.originalExpressionTex)}}\n\n")
                reportBuilder.append("generated task: ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(task.goalExpressionTex)}}\n\n")
                reportBuilder.append("original task structure string: ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(task.originalExpressionStructureString)}}\n\n")
                reportBuilder.append("generated task structure string: ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(task.goalExpressionStructureString)}}\n\n")
                reportBuilder.append("solution: ")
                reportBuilder.append("${solution}\n\n")
            }

            var report = reportBuilder.toString()
            report = makeReplacements(report)

            return report
        }

        private fun resultOnlyReport(tasks: List<TaskITR>): String {
            val reportBuilder = StringBuilder()
            if (tasks.isEmpty()) {
                return reportBuilder.toString()
            }
            reportBuilder.append("\n\n original task: ")
            reportBuilder.append("\\textcolor{blue}{${addDollars(tasks[0].originalExpressionTex)}}\n\n")
            reportBuilder.append("\\bigskip")

            for (i in tasks.indices) {
                val task = tasks[i]
                reportBuilder.append("${i+1}) ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(task.goalExpressionTex)}}\n\n")
                reportBuilder.append("\\bigskip")
            }
            var report = reportBuilder.toString()
            report = makeReplacements(report)
            return report
        }

        fun resultWithSubstitutionsReport(tasks: List<TaskITR>): String {
            val reportBuilder = StringBuilder()

            for (i in tasks.indices) {
                val task = tasks[i]
                val appliedPostprocessSubstitutions = task.expressionTaskIntermediateData!!.appliedPostprocessSubstitutions
                val steps : List<SolutionsStepITR> = task.solutionsStepsTree?.values?.first() as List<SolutionsStepITR>
                val rulesTex = mutableListOf<String>()
                for (step in steps) {
                    val rule = step.substitution.code
                    var color = if (step.substitution.isExtending) "red" else "orange"
                    val lastStepId = appliedPostprocessSubstitutions[step.substitution.code] ?: Int.MAX_VALUE
                    if (step.stepId >= lastStepId) {
                        color = "green"
                    }
                    rulesTex.add("\\textcolor{$color}{${ruleToTex(rule)}}")
                }
                reportBuilder.append("${i+1})\n\n")
                reportBuilder.append("original task: ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(task.originalExpressionTex)}}\n\n")
                reportBuilder.append("generated task: ")
                reportBuilder.append("\\textcolor{blue}{${addDollars(task.goalExpressionTex)}}\n\n")
                // needed for debug
                // reportBuilder.append("\\textcolor{blue}{${addDollars(task.goalExpressionStructureString)}}\n\n")
                reportBuilder.append("applied substitutions: ${rulesTex.joinToString("$;\\ $", "\\texttt{$", "$} \\newline")}")
            }

            var report = reportBuilder.toString()
            report = makeReplacements(report)

            return report
        }

        fun ruleToTex(rule: String) : String {
            val ruleTex: String = if (!rule.contains("__to__")) {
                if (rule.contains("sophisticatedPostprocessing")) {
                    rule.split("sophisticatedPostprocessing")[0]
                }
                rule
            } else {
                val parts = rule.split("__to__")
                val left = parts[0]
                val right = parts[1]
                structureStringToTexString(left) + " \\rightarrow " + structureStringToTexString(right)
            }
            return ruleTex
        }
    }

}