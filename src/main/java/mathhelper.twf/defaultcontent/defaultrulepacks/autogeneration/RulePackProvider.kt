package mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration

import mathhelper.twf.config.RulePackITR
import mathhelper.twf.defaultcontent.defaultrulepacks.StandardMathRulePacks

class RulePackProvider {
    companion object {
        private fun <T> concatenate(vararg lists: List<T>): List<T> {
            return listOf(*lists).flatten()
        }

        fun getDefaultMathPacks() : List<RulePackITR> {
            val codes = listOf(
                "ArithmeticPositiveAddition",
                "ArithmeticAddition",
                "ArithmeticMultiplication",
                "ArithmeticDivision",
                "ArithmeticExponentiation",
                "ShortMultiplication"
            )
            return StandardMathRulePacks.get().filter { codes.contains(it.code) }
        }

        fun getTrigonometricPacks() : List<RulePackITR> {
            return TrigonometricRulePacks.get()
        }

        fun getBasicTrigonometricSubstitutions(): List<RulePackITR> {
            return concatenate(
                    getDefaultMathPacks(),
                    getTrigonometricPacks()
            )
        }
    }
}