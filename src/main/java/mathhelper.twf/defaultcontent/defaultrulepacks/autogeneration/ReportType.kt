package mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration

enum class ReportType {
    RESULT_ONLY,
    RESULT_WITH_POSTPROCESSING,
    RESULT_WITH_SUBSTITUTIONS,
    RESULT_WITH_SUBSTITUTIONS_AND_INTERMEDIATE_EXPRESSIONS
}
