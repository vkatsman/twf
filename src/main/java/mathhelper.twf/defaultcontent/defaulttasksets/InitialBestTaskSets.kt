package mathhelper.twf.defaultcontent.defaulttasksets

import mathhelper.twf.api.stringToStructureString
import mathhelper.twf.config.*
import mathhelper.twf.defaultcontent.TaskSetTagCode
import mathhelper.twf.defaultcontent.TaskTagCode.*
import mathhelper.twf.standartlibextensions.toCustomCodeSuffixPart

class InitialBestTaskSets {
    companion object {
        val beginFromSimpleComputationalTasksMix = listOf<TaskITR>(
                TaskITR(
                        nameEn = "Uncanceled Fraction",
                        nameRu = "Несокращенная дробь",
                        originalExpressionStructureString = "(/(96;16))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ArithmeticDivision")),
                        difficulty = 1.0,
                        tags = mutableSetOf(FRACTION.code)
                ),
                TaskITR(
                        nameEn = "Uncomputed Root",
                        nameRu = "Невычисленный корень",
                        originalExpressionStructureString = "(^(27;/(1;3)))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ArithmeticExponentiation")),
                        difficulty = 1.0,
                        tags = mutableSetOf()
                ),
                TaskITR(
                        nameEn = "Difference of Squares",
                        nameRu = "Разность квадратов",
                        originalExpressionStructureString = "(+(^(64;2);-(^(36;2))))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ShortMultiplication")),
                        difficulty = 2.0,
                        tags = mutableSetOf(SHORT_MULTIPLICATION.code)
                ),
                TaskITR(
                        nameEn = "Square of Difference",
                        nameRu = "Квадрат разности",
                        originalExpressionStructureString = "(+(^(23;2);-(*(2;23;3));9))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ShortMultiplication")),
                        difficulty = 2.0,
                        tags = mutableSetOf(SHORT_MULTIPLICATION.code)
                ),
                TaskITR(
                        nameEn = "Guess 1",
                        nameRu = "Догадайся 1",
                        originalExpressionStructureString = "(+(16;*(2;64);^(16;2)))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ShortMultiplication")),
                        difficulty = 2.0,
                        tags = mutableSetOf(SHORT_MULTIPLICATION.code)
                ),
                TaskITR(
                        nameEn = "Guess 2",
                        nameRu = "Догадайся 2",
                        originalExpressionStructureString = "(^(+(^(65;2);-(^(56;2)));0.5))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ShortMultiplication")),
                        difficulty = 2.0,
                        tags = mutableSetOf(SHORT_MULTIPLICATION.code)
                ),
                TaskITR(
                        nameEn = "Sum of Cubes",
                        nameRu = "Сумма кубов",
                        originalExpressionStructureString = "(+(^(8;3);^(12;3)))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ShortMultiplication")),
                        difficulty = 3.0,
                        tags = mutableSetOf(SHORT_MULTIPLICATION.code)
                ),
                TaskITR(
                        nameEn = "Difference of Cubes",
                        nameRu = "Разность кубов",
                        originalExpressionStructureString = "(+(^(8;3);-(^(6;3))))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ShortMultiplication")),
                        difficulty = 3.0,
                        tags = mutableSetOf(SHORT_MULTIPLICATION.code)
                ),
                TaskITR(
                        nameEn = "Cube of sum",
                        nameRu = "Куб суммы",
                        originalExpressionStructureString = "(+(8;*(3;32);*(3;2;64);^(8;3)))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ShortMultiplication")),
                        difficulty = 3.0,
                        tags = mutableSetOf(SHORT_MULTIPLICATION.code)
                ),
                TaskITR(
                        nameEn = "Guess 3",
                        nameRu = "Догадайся 3",
                        originalExpressionStructureString = "(+(*(+(4;53);+(16;-(*(4;53));^(53;2)));*(+(4;-(53));+(^(4;2);*(4;53);^(53;2)))))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ShortMultiplication")),
                        difficulty = 3.0,
                        tags = mutableSetOf(SHORT_MULTIPLICATION.code)
                ),
                TaskITR(
                        nameEn = "Guess 4",
                        nameRu = "Догадайся 4",
                        originalExpressionStructureString = "(^(+(^(26;3);1;*(27;+(-(+(^(26;2);-(26))))));/(1;3)))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "ShortMultiplication")),
                        difficulty = 3.0,
                        tags = mutableSetOf(SHORT_MULTIPLICATION.code)
                ),
                TaskITR(
                        nameEn = "Logarithm",
                        nameRu = "Логарифм",
                        originalExpressionStructureString = "(*(3;^(3;log(9;3))))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "Logarithm")),
                        difficulty = 2.0,
                        tags = mutableSetOf(SHORT_MULTIPLICATION.code)
                ),
                TaskITR(
                        nameEn = "Difference of logarithms",
                        nameRu = "Разность логарифмов",
                        originalExpressionStructureString = "(+(log(72;3);-(log(8;3))))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "Logarithm")),
                        difficulty = 3.0,
                        tags = mutableSetOf()
                ),
                TaskITR(
                        nameEn = "Quotient of logarithms",
                        nameRu = "Частное логарифмов",
                        originalExpressionStructureString = "(/(log(81;27);log(9;27)))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "Logarithm")),
                        difficulty = 3.0,
                        tags = mutableSetOf()
                ),
                TaskITR(
                        nameEn = "Guess 5",
                        nameRu = "Догадайся 5",
                        originalExpressionStructureString = "(/(log(36;3);+(2;log(4;3))))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "Logarithm")),
                        difficulty = 3.0,
                        tags = mutableSetOf()
                ),
                TaskITR(
                        nameEn = "Guess 6",
                        nameRu = "Догадайся 6",
                        originalExpressionStructureString = "(/(^(16;log(32;4));^(16;log(2;4))))",
                        goalPattern = "?:0:?:?N",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "Logarithm")),
                        difficulty = 3.0,
                        tags = mutableSetOf()
                )
        )


        val initialBestTasks = beginFromSimpleComputationalTasksMix


        val defaultInitialBestSets = listOf(
                TaskSetITR(
                        code = "BeginFromSimpleComputationalMix",
                        nameEn = "Computational Mix", nameRu = "Вычислительный микс",
                        descriptionShortEn = "Varied tasks for smart computation",
                        descriptionShortRu = "Разнообразные задачки на умное вычисление",
                        descriptionEn = "Varied tasks for smart computation in different math subjects",
                        descriptionRu = "Разнообразные задачки на умное вычисление в разных областях математики",
                        subjectType = "standard_math",
                        tags = mutableSetOf(TaskSetTagCode.MIX.code),
                        tasks = beginFromSimpleComputationalTasksMix.map { it.copy() }
                )
        )
    }
}