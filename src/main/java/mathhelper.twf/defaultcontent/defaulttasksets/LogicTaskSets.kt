package mathhelper.twf.defaultcontent.defaulttasksets

import mathhelper.twf.config.RulePackLinkITR
import mathhelper.twf.config.TaskITR
import mathhelper.twf.config.TaskSetITR
import mathhelper.twf.defaultcontent.TaskSetTagCode
import mathhelper.twf.defaultcontent.TaskTagCode.*

class LogicTaskSets {
    companion object {
        val logicBaseTrainSetTasks = listOf<TaskITR>(
                TaskITR(
                        originalExpressionStructureString = "(and(implic(b;a);implic(c;a)))",
                        goalExpressionStructureString = "(or(a;not(or(b;c))))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase")),
                        subjectType = "logic",
                        difficulty = 1.0,
                        stepsNumber = 2,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(a;implic(b;a)))",
                        goalExpressionStructureString = "(a)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase")),
                        subjectType = "logic",
                        difficulty = 2.5,
                        tags = mutableSetOf(LOGIC.code, TRICK.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(implic(implic(a;not(b));c))",
                        goalExpressionStructureString = "(or(and(a;b);c))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(not(implic(or(a;b);b));alleq(1;c)))",
                        goalExpressionStructureString = "(and(a;not(b);c))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(implic(a;b);implic(b;a)))",
                        goalExpressionStructureString = "(alleq(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(a;not(a));or(not(a);not(b));or(a;b);or(not(b);b)))",
                        goalExpressionStructureString = "(xor(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(xor(xor(and(a;b);a);b))",
                        goalExpressionStructureString = "(or(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 4.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(implic(and(a;b);or(not(c);b)))",
                        goalExpressionStructureString = "(implic(a;implic(b;implic(c;b))))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(a;b;c);implic(a;or(c;d));implic(c;or(b;d));not(b);not(d)))",
                        goalType = "simplification",
                        goalPattern = "?:0:?:?N",
                        otherGoalData = mapOf(
                                "hiddenGoalExpressions" to listOf("(0)")
                        ),
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(a;b);implic(not(c);not(a));implic(not(d);not(b));not(or(c;d));not(or(not(c);not(d)))))",
                        goalType = "simplification",
                        goalPattern = "?:0:?:?N",
                        otherGoalData = mapOf(
                                "hiddenGoalExpressions" to listOf("(0)")
                        ),
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 5.5,
                        tags = mutableSetOf(LOGIC.code)
                )
        )

        val logicRelativeComplementTrainSetTasks = listOf<TaskITR>(
                TaskITR(
                        originalExpressionStructureString = "(or(set-(not(A);B);C))",
                        goalExpressionStructureString = "(implic(or(A;B);C))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(set-(not(and(A;B));B))",
                        goalExpressionStructureString = "(not(B))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(set-(set-(A;B);C);or(not(B);C)))",
                        goalExpressionStructureString = "(implic(implic(A;or(B;C));implic(B;C)))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "RelativeComplement"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 4.5,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(implic(or(A;B);and(A;B)))",
                        goalExpressionStructureString = "(and(not(set-(A;B));not(set-(B;A))))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "RelativeComplement"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 3.5,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(set-(or(A;B);and(implic(A;B);implic(B;A))))",
                        goalExpressionStructureString = "(or(set-(A;B);set-(B;A)))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "RelativeComplement"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 4.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(implic(implic(set-(A;C);D);set-(D;B)))",
                        goalExpressionStructureString = "(or(set-(D;B);set-(A;or(C;D))))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "RelativeComplement"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 4.5,
                        tags = mutableSetOf(LOGIC.code)
                )
        )

        val logicPeculiarOperationsTasks = listOf<TaskITR>(
        )

        val logicNormalFormsTrainSetTasks = listOf<TaskITR>(
                TaskITR(
                        originalExpressionStructureString = "(implic(implic(A;not(B));C))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase")),
                        subjectType = "logic",
                        difficulty = 1.5,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(not(or(and(not(A);not(B));and(not(C);not(D)))))",
                        goalPattern = "and : (or : 2) : : : not",
                        goalType = "CNF",
                        goalNumberProperty = 2,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, CNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(not(or(A;B;C)))",
                        goalPattern = "or : (and : 3) : : : not",
                        goalType = "DNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 2.5,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(implic(and(implic(C;B);not(or(A;B;C)));C))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 3.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(not(or(and(A;B;or(and(B;C);not(A)));A));and(B;A)))",
                        goalPattern = "or : (and : 3) : : : not",
                        goalType = "DNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 3.5,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(implic(implic(implic(implic(A;B);C);implic(implic(B;C);A));implic(implic(C;A);B)))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 4.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(and(A;not(B);not(C));and(A;not(B);C);and(not(A);B;C)))",
                        goalPattern = "and : (or) : : : not",
                        goalType = "CNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;B;not(C));or(A;not(B);C);or(not(A);B;C)))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(and(A;not(B));and(C;not(D));and(not(A);B);and(not(C);D)))",
                        goalPattern = "and : (or) : : : not",
                        goalType = "CNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;not(B));or(C;not(D));or(not(A);B);or(not(C);D)))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(and(A;not(B);C;not(D));and(not(A);B;not(C);D)))",
                        goalPattern = "and : (or : 3) : : : not",
                        goalType = "CNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;not(B);C;not(D));or(not(A);B;not(C);D)))",
                        goalPattern = "or : (and : 3) : : : not",
                        goalType = "DNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(and(A;not(B));and(C;not(D));and(not(A);B);and(not(C);D)))",
                        goalPattern = "and : (or : 3) : : : not",
                        goalType = "CNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;not(B));or(C;not(D));or(not(A);B);or(not(C);D)))",
                        goalPattern = "or : (and : 3) : : : not",
                        goalType = "DNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                )
        )

        val logicNormalFormsTrainSetTasksSecondLevel = listOf<TaskITR>(
                TaskITR(
                        originalExpressionStructureString = "(and(implic(implic(implic(A;B);C);implic(implic(B;C);A));implic(implic(C;A);B)))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 4.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(D;and(A;not(B);not(C));and(A;not(B);C);and(not(A);B;C)))",
                        goalPattern = "and : (or) : : : not",
                        goalType = "CNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(D;or(A;B;not(C));or(A;not(B);C);or(not(A);B;C)))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(D;and(A;not(B));and(C;not(D));and(not(A);B);and(not(C);D)))",
                        goalPattern = "and : (or) : : : not",
                        goalType = "CNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(implic(D;B);or(A;not(B));or(C;not(D));or(not(A);B);or(not(C);D)))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(implic(D;B);and(A;not(B);C;not(D));and(not(A);B;not(C);D)))",
                        goalPattern = "and : (or : 3) : : : not",
                        goalType = "CNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(implic(D;F);implic(A;not(B);C;not(D));or(not(A);B;not(C);D)))",
                        goalPattern = "or : (and : 3) : : : not",
                        goalType = "DNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(implic(E;F);and(A;not(B));and(C;not(D));and(not(A);B);and(not(C);D)))",
                        goalPattern = "and : (or : 3) : : : not",
                        goalType = "CNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(implic(E;F);or(A;not(B));or(C;not(D));or(not(A);B);or(not(C);D)))",
                        goalPattern = "or : (and : 3) : : : not",
                        goalType = "DNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(implic(E;F);and(A;not(B);C);and(C;not(D);E);and(not(A);B;F);and(not(C);D;A)))",
                        goalPattern = "and : (or : 3) : : : not",
                        goalType = "CNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "LogicAbsorptionLaw",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                )
        )

        val logicResolutionTrainSetTasks = listOf<TaskITR>(
                TaskITR(
                        originalExpressionStructureString = "(and(A;B;implic(A;C);not(C)))",
                        goalExpressionStructureString = "(0)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicResolution")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        tags = mutableSetOf(LOGIC.code, RESOLUTION.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(not(A);implic(not(A);B);implic(B;C);not(C)))",
                        goalExpressionStructureString = "(0)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicResolution")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        tags = mutableSetOf(LOGIC.code, RESOLUTION.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;C);implic(D;C);implic(A;D);not(C)))",
                        goalExpressionStructureString = "(0)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicResolution")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        tags = mutableSetOf(LOGIC.code, RESOLUTION.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;B;C);implic(A;or(C;D));implic(C;or(B;D));not(B);not(D)))",
                        goalExpressionStructureString = "(0)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicResolution")),
                        subjectType = "logic",
                        difficulty = 4.0,
                        tags = mutableSetOf(LOGIC.code, RESOLUTION.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;B);implic(not(C);not(A));implic(not(D);not(B));not(or(C;D));not(or(not(C);not(D)))))",
                        goalExpressionStructureString = "(0)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicResolution")),
                        subjectType = "logic",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, RESOLUTION.code)
                )
        )

        val logicUsualCnfDnfMixCheckYourselfTasks = listOf<TaskITR>(
                TaskITR(
                        originalExpressionStructureString = "(not(or(A;B;C)))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 1.5,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, CNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(implic(and(a;b);or(c;b)))",
                        goalExpressionStructureString = "(implic(d;implic(b;implic(not(c);b))))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(not(and(A;implic(B;C))))",
                        goalPattern = "and : (or : 3) : : : not",
                        goalType = "CNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, CNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(and(A;not(B);C;not(D));and(not(A);B;not(C);D)))",
                        goalPattern = "and : (or) : : : not",
                        goalType = "CNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;not(B);C;not(D));or(not(A);B;not(C);D)))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(and(A;not(B);not(C));and(A;not(B);C);and(not(A);B;C)))",
                        goalPattern = "and : (or : 3) : : : not",
                        goalType = "CNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;B;not(C));or(A;not(B);C);or(not(A);B;C)))",
                        goalPattern = "or : (and : 3) : : : not",
                        goalType = "DNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                )
        )

        val logicMixCheckYourselfTasks = listOf<TaskITR>(
                TaskITR(
                        originalExpressionStructureString = "(set-(A;set-(A;B)))",
                        goalPattern = "and : (or) : : : not",
                        goalType = "CNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "RelativeComplement")),
                        subjectType = "logic",
                        difficulty = 1.5,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, CNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(not(and(A;set-(B;C))))",
                        goalPattern = "and : (or : 3) : : : not",
                        goalType = "CNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "RelativeComplement")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, CNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(and(A;not(B);C;not(D));and(not(A);B;not(C);D)))",
                        goalPattern = "and : (or) : : : not",
                        goalType = "CNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;not(B);C;not(D));or(not(A);B;not(C);D)))",
                        goalPattern = "or : (and) : : : not",
                        goalType = "DNF",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 5.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(not(set-(A;or(C;D)));implic(C;or(B;D));or(A;B;C);not(B);not(D)))",
                        goalExpressionStructureString = "(0)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicResolution")),
                        subjectType = "logic",
                        difficulty = 4.0,
                        tags = mutableSetOf(LOGIC.code, RESOLUTION.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(or(and(A;not(B);not(C));and(A;not(B);C);and(not(A);B;C)))",
                        goalPattern = "and : (or : 3) : : : not",
                        goalType = "CNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                ),
                TaskITR(
                        originalExpressionStructureString = "(and(or(A;B;not(C));or(A;not(B);C);or(not(A);B;C)))",
                        goalPattern = "or : (and : 3) : : : not",
                        goalType = "DNF",
                        goalNumberProperty = 3,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 6.0,
                        tags = mutableSetOf(LOGIC.code, NORMAL_FORMS.code, DNF.code)
                )
        )


        val substifyBooleanLogicGameTasks = listOf<TaskITR>(
                TaskITR(
                        nameEn = "Double  Negation",
                        nameRu = "Двойное отрицание",
                        descriptionEn = "Double  Negation, complementation",
                        descriptionRu = "Двойное отрицание, дополнительность",
                        originalExpressionStructureString = "(and(not(not(a));not(a)))",
                        goalExpressionStructureString = "(0)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNeg")),
                        subjectType = "logic",
                        difficulty = 1.0,
                        stepsNumber = 2,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Distributivity",
                        nameRu = "Дистрибутивность",
                        descriptionEn = "Distributivity, complementation, identity",
                        descriptionRu = "Дистрибутивность, дополнительность, идентичность",
                        originalExpressionStructureString = "(and(or(a;not(b));or(a;b)))",
                        goalExpressionStructureString = "(a)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNeg")),
                        subjectType = "logic",
                        difficulty = 1.0,
                        stepsNumber = 3,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "De Morgan's Law",
                        nameRu = "Закон де Моргана",
                        descriptionEn = "Distributivity, de Morgan's Law, commutativity",
                        descriptionRu = "Дистрибутивность, закон де Моргана, коммутативность",
                        originalExpressionStructureString = "(and(or(a;not(c));or(a;not(b))))",
                        goalExpressionStructureString = "(or(a;not(or(b;c))))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNeg")),
                        subjectType = "logic",
                        difficulty = 1.0,
                        stepsNumber = 3,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Multiselection",
                        nameRu = "Мультивыделение",
                        descriptionEn = "Associativity by long tap to select to 2 items, de Morgan's Law",
                        descriptionRu = "Ассоциативность по долгому нажатию для выделения 2 мест, закон де Моргана",
                        originalExpressionStructureString = "(or(a;not(b);c;not(d)))",
                        goalExpressionStructureString = "(or(a;c;not(and(b;d))))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNeg")),
                        subjectType = "logic",
                        difficulty = 1.0,
                        stepsNumber = 3,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Triple de Morgan's Law",
                        nameRu = "Закон де Моргана для трех",
                        descriptionEn = "Multiselection, multiple de Morgan's Law",
                        descriptionRu = "Мультивыделение, многоразовый закон де Моргана",
                        originalExpressionStructureString = "(not(and(a;b;c)))",
                        goalExpressionStructureString = "(or(not(a);not(b);not(c)))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNeg")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        stepsNumber = 3,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Absorption Law",
                        nameRu = "Закон поглощения",
                        descriptionEn = "Identity, distributivity, identity",
                        descriptionRu = "Идентичность, дистрибутивность, идентичность",
                        originalExpressionStructureString = "(or(a;and(a;b)))",
                        goalExpressionStructureString = "(a)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNeg")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        stepsNumber = 4, // 3 with "clever distributivity"
                        tags = mutableSetOf(LOGIC.code, TRICK.code)
                ),
                TaskITR(
                        nameEn = "Implication",
                        nameRu = "Импликация",
                        descriptionEn = "Implication, distributivity, de Morgan's Law, implication",
                        descriptionRu = "Импликация, дистрибутивность, закон де Моргана, импликация",
                        originalExpressionStructureString = "(and(implic(not(b);a);implic(c;a)))",
                        goalExpressionStructureString = "(or(a;not(implic(b;c))))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplic")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        stepsNumber = 5,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Implication of Negations",
                        nameRu = "Импликация отрицаний",
                        descriptionEn = "Implication, commutativity, implication",
                        descriptionRu = "Импликация, коммутативность, импликация",
                        originalExpressionStructureString = "(implic(not(b);not(a)))",
                        goalExpressionStructureString = "(implic(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplic")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        stepsNumber = 3,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Implication Absorption",
                        nameRu = "Импликационное поглощение",
                        descriptionEn = "Implication, identity, distributivity, identity",
                        descriptionRu = "Импликация, идентичность, дистрибутивность, идентичность",
                        originalExpressionStructureString = "(and(a;implic(b;a)))",
                        goalExpressionStructureString = "(a)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplic")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        stepsNumber = 5, // 4 with "clever distributivity"
                        tags = mutableSetOf(LOGIC.code, TRICK.code)
                ),
                TaskITR(
                        nameEn = "Implication Distributivity",
                        nameRu = "Дистрибутивность импликации",
                        descriptionEn = "Implication, distributivity, implication",
                        descriptionRu = "Импликация, дистрибутивность, импликация",
                        originalExpressionStructureString = "(and(implic(a;b);implic(a;c)))",
                        goalExpressionStructureString = "(implic(a;and(b;c)))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplic")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        stepsNumber = 4,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Or Implication Distributivity",
                        nameRu = "Дистрибутивность или от импликации",
                        descriptionEn = "Implication, identity, de Morgan's Law, implication",
                        descriptionRu = "Импликация, идентичность, закон де Моргана, импликация",
                        originalExpressionStructureString = "(or(implic(a;c);implic(b;c)))",
                        goalExpressionStructureString = "(implic(and(a;b);c))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplic")),
                        subjectType = "logic",
                        difficulty = 3.0,
                        stepsNumber = 5,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Simplification",
                        nameRu = "Упрощение",
                        descriptionEn = "No hints",
                        descriptionRu = "Без подсказок",
                        originalExpressionStructureString = "(and(or(a;b);implic(not(c);not(a));implic(not(d);not(b));not(or(c;d));not(or(not(c);not(d)))))",
                        goalExpressionStructureString = "(0)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplic")),
                        subjectType = "logic",
                        difficulty = 4.0,
                        stepsNumber = 10,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Simplification Hard",
                        nameRu = "Тяжелое упрощение",
                        descriptionEn = "No hints",
                        descriptionRu = "Без подсказок",
                        originalExpressionStructureString = "(and(or(a;b;c);implic(a;or(c;d));implic(c;or(b;d));not(b);not(d)))",
                        goalExpressionStructureString = "(0)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplic")),
                        subjectType = "logic",
                        difficulty = 5.0,
                        stepsNumber = 20,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Exclusive or (XOR)",
                        nameRu = "Исключающее или (XOR)",
                        descriptionEn = "XOR, identity",
                        descriptionRu = "XOR, идентичность",
                        originalExpressionStructureString = "(xor(or(xor(a;a);xor(b;xor(1;1)));1))",
                        goalExpressionStructureString = "(not(b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXor")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        stepsNumber = 5,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "XOR Absorption",
                        nameRu = "XOR Поглощение",
                        descriptionEn = "Multiselection, identity",
                        descriptionRu = "Мультивыделение, идентичность",
                        originalExpressionStructureString = "(xor(xor(a;b);b))",
                        goalExpressionStructureString = "(a)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXor")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        stepsNumber = 3,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "XOR Negation",
                        nameRu = "Отрицание XOR",
                        descriptionShortEn = "Knowing 'a⊕b = (¬a∧b)∨(a∧¬b)' relate to",
                        descriptionShortRu = "Зная 'a⊕b = (¬a∧b)∨(a∧¬b)' свести к",
                        originalExpressionStructureString = "(not(xor(a;b)))",
                        goalExpressionStructureString = "(xor(not(a);b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXor")),
                        subjectType = "logic",
                        difficulty = 4.0,
                        stepsNumber = 10,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "XOR Conjunction Definition",
                        nameRu = "Конъюктивное определение XOR",
                        descriptionShortEn = "Knowing 'a⊕b = (¬a∧b)∨(a∧¬b)' relate to",
                        descriptionShortRu = "Зная 'a⊕b = (¬a∧b)∨(a∧¬b)' свести к",
                        originalExpressionStructureString = "(and(or(not(a);not(b));or(a;b)))",
                        goalExpressionStructureString = "(xor(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXor")),
                        subjectType = "logic",
                        difficulty = 4.0,
                        stepsNumber = 10,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "XOR Distribution",
                        nameRu = "Дистрибутивность XOR",
                        originalExpressionStructureString = "(xor(and(a;b);and(a;c)))",
                        goalExpressionStructureString = "(and(a;xor(b;c)))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXorConjunctionDef")),
                        subjectType = "logic",
                        difficulty = 4.0,
                        stepsNumber = 10,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "XOR 2-CNF Definition",
                        nameRu = "Определение XOR через 2-КНФ",
                        descriptionEn = "No hints",
                        descriptionRu = "Без подсказок",
                        originalExpressionStructureString = "(and(or(a;not(a));or(not(a);not(b));or(a;b);or(not(b);b)))",
                        goalExpressionStructureString = "(xor(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXorAdvanced")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        stepsNumber = 3,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "XOR of And Simplification",
                        nameRu = "Упрощение XOR конъюнкции",
                        descriptionEn = "No hints",
                        descriptionRu = "Без подсказок",
                        originalExpressionStructureString = "(xor(xor(and(a;b);a);b))",
                        goalExpressionStructureString = "(or(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXorAdvanced")),
                        subjectType = "logic",
                        difficulty = 5.0,
                        stepsNumber = 20,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Implication Zhegalkin Polynom",
                        nameRu = "Импликация полином Жегалкина",
                        descriptionEn = "No hints",
                        descriptionRu = "Без подсказок",
                        originalExpressionStructureString = "(xor(a;and(a;b);1))",
                        goalExpressionStructureString = "(implic(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXorAdvanced")),
                        subjectType = "logic",
                        difficulty = 5.0,
                        stepsNumber = 20,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Equivalence",
                        nameRu = "Эквиваленция",
                        descriptionEn = "No hints",
                        descriptionRu = "Без подсказок",
                        originalExpressionStructureString = "(and(not(implic(or(alleq(0;not(a));b);b));alleq(1;c)))",
                        goalExpressionStructureString = "(and(a;not(b);c))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXorEquivalence")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        stepsNumber = 6,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Equivalence and Xor",
                        nameRu = "Эквиваленция и Xor",
                        descriptionEn = "No hints",
                        descriptionRu = "Без подсказок",
                        originalExpressionStructureString = "(not(alleq(a;b)))",
                        goalExpressionStructureString = "(xor(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXorEquivalence")),
                        subjectType = "logic",
                        difficulty = 4.0,
                        stepsNumber = 10,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Equivalence Xor Distribution",
                        nameRu = "Дистрибутивность эквиваленции и Xor",
                        descriptionEn = "No hints",
                        descriptionRu = "Без подсказок",
                        originalExpressionStructureString = "(xor(alleq(a;b);alleq(a;c)))",
                        goalExpressionStructureString = "(xor(b;c))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXorEquivalenceAdvanced")),
                        subjectType = "logic",
                        difficulty = 2.0,
                        stepsNumber = 3,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Equivalence Zhegalkin Polynom",
                        nameRu = "Эквиваленция полином Жегалкина",
                        descriptionEn = "No hints",
                        descriptionRu = "Без подсказок",
                        originalExpressionStructureString = "(xor(a;b;1))",
                        goalExpressionStructureString = "(alleq(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXorEquivalenceAdvanced")),
                        subjectType = "logic",
                        difficulty = 5.0,
                        stepsNumber = 20,
                        tags = mutableSetOf(LOGIC.code)
                ),
                TaskITR(
                        nameEn = "Simplification to CNF",
                        nameRu = "Упрощение к КНФ",
                        descriptionEn = "No hints",
                        descriptionRu = "Без подсказок",
                        originalExpressionStructureString = "(or(and(not(a);or(not(b);c;not(d)));and(a;or(b;not(c);d));and(not(b);not(c));and(not(b);d);and(c;b);and(c;d);and(not(d);b);and(not(d);not(c))))",
                        goalExpressionStructureString = "(and(or(a;not(b);c;not(d));or(not(a);b;not(c);d)))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicAndOrNegImplicXorEquivalenceAdvanced")),
                        subjectType = "logic",
                        difficulty = 7.0,
                        stepsNumber = 30,
                        tags = mutableSetOf(LOGIC.code)
                )
        //TODO: add normal form and other tasks when such explanations be added to application
        )

        val allLogicTasks = logicBaseTrainSetTasks +
                logicRelativeComplementTrainSetTasks +
                logicPeculiarOperationsTasks +
                logicNormalFormsTrainSetTasks +
                logicResolutionTrainSetTasks +
                logicMixCheckYourselfTasks +
                logicUsualCnfDnfMixCheckYourselfTasks +
                substifyBooleanLogicGameTasks

        val defaultLogicTaskSets = listOf(
                TaskSetITR(
                        code = "LogicBaseTrainSet",
                        nameEn = "[Train Set] Basic Boolean Logic", nameRu = "[Тренировка] Основы булевой логики",
                        descriptionShortEn = "Logic Expressions Transformations",
                        descriptionShortRu = "Преобразования логических выражений",
                        descriptionEn = "[Train Set] Conjunction, Disjunction, Implication, de Morgan's laws, Absorption",
                        descriptionRu = "[Тренировка] Конъюнкция, дизъюнкция, импликация, законы де Моргана, поглощение",
                        subjectType = "logic",
                        tags = mutableSetOf(TaskSetTagCode.TRAIN_SET.code, TaskSetTagCode.LOGIC.code),
                        tasks = logicBaseTrainSetTasks.map { it.copy() }
                ),
                TaskSetITR(
                        code = "LogicRelativeComplementTrainSet",
                        nameEn = "[Train Set] Logic Relative Complement", nameRu = "[Тренировка] Логическое дополнение",
                        descriptionShortEn = "Logic Expressions Transformations",
                        descriptionShortRu = "Преобразования логических выражений",
                        descriptionEn = "[Train Set] Relative Complement and Conjunction, Disjunction, Implication, de Morgan's laws, Absorption",
                        descriptionRu = "[Тренировка] Логическое дополнение и конъюнкция, дизъюнкция, импликация, законы де Моргана, поглощение",
                        subjectType = "logic",
                        tags = mutableSetOf(TaskSetTagCode.TRAIN_SET.code, TaskSetTagCode.LOGIC.code),
                        tasks = logicRelativeComplementTrainSetTasks.map { it.copy() }
                ),
                TaskSetITR(
                        code = "LogicPeculiarOperations",
                        nameEn = "Logic Peculiar Operations", nameRu = "Необычные логические операции",
                        descriptionShortEn = "Logic Expressions Transformations: Negation of Conjunction and Disjunction",
                        descriptionShortRu = "Преобразования логических выражений: штрих Шефферра и стрелка Пирса",
                        descriptionEn = "Negation of Conjunction, Negation of Disjunction and Other Logic Operations",
                        descriptionRu = "Отрицание конъюнкции, отриицание дизъюнкции и другие логиические операции",
                        subjectType = "logic",
                        tags = mutableSetOf(TaskSetTagCode.LOGIC.code, TaskSetTagCode.EXTRAORDINARY.code),
                        tasks = logicPeculiarOperationsTasks.map { it.copy() }
                ),
                TaskSetITR(
                        code = "LogicNormalFormsTrainSet",
                        nameEn = "[Train Set] Logic Normal Forms", nameRu = "[Тренировка] Нормальные формы",
                        descriptionShortEn = "Logic Expressions Transformations",
                        descriptionShortRu = "Преобразования логических выражений",
                        descriptionEn = "[Train Set] Normal Forms of Logic Expressions",
                        descriptionRu = "[Тренировка] Нормальные формы логических выражений",
                        subjectType = "logic",
                        tags = mutableSetOf(TaskSetTagCode.LOGIC.code, TaskSetTagCode.TRAIN_SET.code),
                        tasks = logicNormalFormsTrainSetTasks.map { it.copy() }
                ),
                TaskSetITR(
                        code = "LogicNormalFormsTrainSetSecondLevel",
                        nameEn = "[Train Set] Logic Normal Forms 2 Level", nameRu = "[Тренировка] Нормальные формы 2 уровень",
                        descriptionShortEn = "Logic Expressions Transformations",
                        descriptionShortRu = "Преобразования логических выражений",
                        descriptionEn = "[Train Set] Normal Forms of Logic Expressions",
                        descriptionRu = "[Тренировка] Нормальные формы логических выражений",
                        subjectType = "logic",
                        tags = mutableSetOf(TaskSetTagCode.LOGIC.code, TaskSetTagCode.TRAIN_SET.code),
                        tasks = logicNormalFormsTrainSetTasksSecondLevel.map { it.copy() }
                ),
                TaskSetITR(
                        code = "LogicResolutionTrainSet",
                        nameEn = "[Train Set] Resolution Method", nameRu = "[Тренировка] Метод резолюций",
                        descriptionShortEn = "Logic Expressions Transformations",
                        descriptionShortRu = "Преобразования логических выражений",
                        descriptionEn = "[Train Set] Resolution Method for Logic Expressions",
                        descriptionRu = "[Тренировка] Метод резолюций для логических выражений",
                        subjectType = "logic",
                        tags = mutableSetOf(TaskSetTagCode.LOGIC.code, TaskSetTagCode.TRAIN_SET.code, TaskSetTagCode.RESOLUTION.code),
                        tasks = logicResolutionTrainSetTasks.map { it.copy() }
                ),
                TaskSetITR(
                        code = "LogicMixCheckYourself",
                        nameEn = "[Check Yourself] Mix of Logic Transformations", nameRu = "[Проверь себя] Микс логических преобразований",
                        descriptionShortEn = "Logic Expressions Transformations",
                        descriptionShortRu = "Преобразования логических выражений",
                        descriptionEn = "[Check Yourself] Logic Expressions Transformations",
                        descriptionRu = "[Проверь себя] Преобразования логических выражений",
                        subjectType = "logic",
                        tags = mutableSetOf(TaskSetTagCode.LOGIC.code, TaskSetTagCode.CHECK_YOURSELF.code),
                        tasks = logicMixCheckYourselfTasks.map { it.copy() }
                ),
                TaskSetITR(
                        code = "LogicUsualCnfDnfMixCheckYourself",
                        nameEn = "[Check Yourself] Mix of Logic Usual CNF DNF Transformations", nameRu = "[Проверь себя] Микс логических преобразований к КНФ ДНФ",
                        descriptionShortEn = "Logic Expressions Transformations",
                        descriptionShortRu = "Преобразования логических выражений",
                        descriptionEn = "[Check Yourself] Logic Expressions Usual CNF DNF Transformations",
                        descriptionRu = "[Проверь себя] Преобразования логических выражени, в том числе к нормальным формам КНФ ДНФ",
                        subjectType = "logic",
                        tags = mutableSetOf(TaskSetTagCode.LOGIC.code, TaskSetTagCode.CHECK_YOURSELF.code),
                        tasks = logicUsualCnfDnfMixCheckYourselfTasks.map { it.copy() }
                ),
                TaskSetITR(
                        code = "SubstifyBooleanLogicGameTasks",
                        nameEn = "Substify Boolean Logic Tasks", nameRu = "Задачи на булеву логику из Substify",
                        descriptionShortEn = "Boolean Expressions Transformations",
                        descriptionShortRu = "Преобразования булевых выражений",
                        descriptionEn = "Conjunction, Disjunction, Negation, Implication, Xor, Equivalence",
                        descriptionRu = "Конъюнкция, дизъюнкция, отрицание, импликация, исключающее или, эквивалентность",
                        subjectType = "logic",
                        tags = mutableSetOf(TaskSetTagCode.LOGIC.code),
                        tasks = substifyBooleanLogicGameTasks.map { it.copy() }
                )
        )
    }
}