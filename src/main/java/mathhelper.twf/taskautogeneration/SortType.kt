package mathhelper.twf.taskautogeneration

enum class SortType(val code: String) {
    BY_RULE_TAG_USAGE("По количеству использованных тем"),
    BY_TASK_LENGTH("По длине выражения")
}
