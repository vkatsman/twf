package mathhelper.twf.taskautogeneration

import mathhelper.twf.api.*
import mathhelper.twf.baseoperations.BaseOperationsComputation.Companion.epsilon
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.SolutionsStepITR
import mathhelper.twf.config.TaskITR
import mathhelper.twf.expressiontree.*
import mathhelper.twf.logs.MessageType
import mathhelper.twf.logs.log
import mathhelper.twf.platformdependent.random
import mathhelper.twf.platformdependent.randomInt
import mathhelper.twf.taskautogeneration.rulepack.PostprocessorRulePack
import kotlin.math.ceil
import kotlin.math.min

data class GeneratorSettings(
    val expressionGenerationDirection: ExpressionGenerationDirection = ExpressionGenerationDirection.FINAL_TO_ORIGINAL,
    val targetWeight: Double = 5.0,
    val targetWeightWindowCoefficient: Double = 0.2,
    val taskStartGenerator: (compiledConfiguration: CompiledConfiguration) -> GeneratedExpression,
    val compiledConfiguration: CompiledConfiguration = CompiledConfiguration(),
    val expressionSubstitutions: List<ExpressionSubstitution> = compiledConfiguration.compiledExpressionTreeTransformationRules,
    val maxCountSelectedOfTasksOnIteration: Int = 10,
    val minWidthOfRulesApplicationsOnIteration: Int = 3,
    val maxWidthOfRulesApplicationsOnIteration: Int = 10,
    val minStepsCountInAutogeneration: Int = 5,
    val extendReduceFactor: Double = 0.8, // defines the moment when generator starts apply reducing substitutions instead of extending
    val extendingExpressionSubstitutions: List<ExpressionSubstitution> = expressionSubstitutions.filter { it.isExtending },
    val reducingExpressionSubstitutions: List<ExpressionSubstitution> = expressionSubstitutions.filter { !it.isExtending },
    val postprocessExpressionSubstitutions: List<ExpressionSubstitution> = expressionSubstitutionsFromRulePackITR(
        PostprocessorRulePack.get(), mapOf(), false),
    val mandatoryResultTransformations: List<ExpressionSubstitution> = listOf(),
    val newVariablesExpressionSubstitutions: Map<String, List<ExpressionSubstitution>> = mapOf(),
    val nodeIdsToTransformSelector: (ExpressionNode) -> List<Int> = { expressionNode -> expressionNode.selectRandomNodeIdsToTransform() },
    val sortType: SortType = SortType.BY_RULE_TAG_USAGE,
    val sortOrder: SortOrder = SortOrder.DESC,
    val maxTaskCount: Int = 50
)

fun generateTrigonometricTasks(
    settings: GeneratorSettings
): List<TaskITR> {
    val compiledConfiguration = settings.compiledConfiguration
    val expressionComparator = compiledConfiguration.factComparator.expressionComparator
    val taskStartGeneratedExpression = settings.taskStartGenerator.invoke(compiledConfiguration)

    log.addMessage({ "targetWeight = ${settings.targetWeight}"}, messageType = MessageType.TECHNICAL)

    val tagsChosen = settings.expressionSubstitutions
            .flatMap { it.tagsForTaskGenerator }
            .distinct()
            .filter { it != RuleTag.BASIC_MATH }
    log.addMessage({ "chosen tags: " + tagsChosen.joinToString(separator = ", ")}, messageType = MessageType.TECHNICAL)

    val initialTask = ExpressionTask(taskStartGeneratedExpression.expressionNode.clone())
    log.addMessage({ "initial task: " + initialTask.currentExpression.computeIdentifier()}, messageType = MessageType.TECHNICAL)

    var taskStart = initialTask.clone()
    taskStart = dragConstantsToTheLeft(taskStart)

    var allTasks = mutableListOf<ExpressionTask>()
    var currentTasks = mutableListOf<ExpressionTask>()
    currentTasks.add(taskStart)

    for (i in 1..5) {
        currentTasks.add(splitConstants(taskStart, compiledConfiguration, stepId = 0))
    }
    currentTasks = unify(currentTasks, compiledConfiguration)

    val currentTasksAfterSplittingConstants = currentTasks.toMutableList()
    log.addMessage({ "number of tasks after splitting constants: " + currentTasksAfterSplittingConstants.size}, messageType = MessageType.TECHNICAL)
    log.addMessage({ "tasks after splitting constants: " + currentTasksAfterSplittingConstants.map { it.currentExpression.computeIdentifier() }.joinToString(separator = ", \n")}, messageType = MessageType.TECHNICAL)

    var stepId = 1
    var generatorIterationCounter = 0

    val minWeight = (1 - settings.targetWeightWindowCoefficient) * settings.targetWeight
    val maxWeight = (1 + settings.targetWeightWindowCoefficient) * settings.targetWeight
    var taskMaxWeightPrev = -1.0
    var taskMaxWeight = 0.0

    while (taskMaxWeight < maxWeight) {
        if (taskMaxWeight <= taskMaxWeightPrev) {
            log.addMessage({ "ERROR: the algorithm is cycled (max weight is not updated), stepId = $stepId, " +
                    "maxWeight = $taskMaxWeight, target = $maxWeight"}, messageType = MessageType.TECHNICAL)
            break
        }

        var newTasks = mutableListOf<ExpressionTask>()
        val stepIdCopy = stepId
        log.addMessage({ "stepId = ${stepIdCopy}"}, messageType = MessageType.TECHNICAL)
        for (currentTask in currentTasks) {
            compiledConfiguration.setExpressionSubstitutions(
                    if (currentTask.usedSubstitutions.sumByDouble { it.difficultyInTaskAutoGeneration } < settings.targetWeight * settings.extendReduceFactor) {
                        settings.extendingExpressionSubstitutions
                    } else {
                        settings.reducingExpressionSubstitutions
                    })

            val numberOfTrials = settings.maxWidthOfRulesApplicationsOnIteration
            val tasksProducedFromThis = mutableListOf<ExpressionTask>()
            for (j in 1..numberOfTrials) {
                log.addMessage({"stepId = $stepIdCopy, trialNumber = $j"}, MessageType.TECHNICAL)
                tryToGenerateNewTasks(currentTask, tasksProducedFromThis, settings, stepId)
                generatorIterationCounter++
                newTasks.addAll(tasksProducedFromThis)
                if (j >= settings.minWidthOfRulesApplicationsOnIteration && tasksProducedFromThis.isNotEmpty()) {
                    break
                }
            }
        }
        newTasks = unify(newTasks, compiledConfiguration)

        val groupsByPreviousExpression = newTasks.groupBy { it.previousExpressions.lastOrNull()?.identifier }
        val maxCountPerGroup = if (groupsByPreviousExpression.isEmpty()) { 0 }
        else {
            ceil(settings.maxCountSelectedOfTasksOnIteration.toDouble() / groupsByPreviousExpression.size).toInt()
        }
        val newCurrentTasksAfterCut = mutableListOf<ExpressionTask>()
        for ((_, tasksInGroup) in groupsByPreviousExpression) {
            val sortedTasks = sortTasks(tasksInGroup, settings.sortType, settings.sortOrder, tagsChosen)
            newCurrentTasksAfterCut.addAll(sortedTasks.subList(0, min(maxCountPerGroup, tasksInGroup.size)))
        }

        log.addMessage({ "after cut: " + newCurrentTasksAfterCut.map { it.currentExpression.computeIdentifier() }.joinToString(separator = ", \n")}, messageType = MessageType.TECHNICAL)
        newTasks = newCurrentTasksAfterCut

        allTasks.addAll(newTasks)
        allTasks = allTasks.distinctBy { it.currentExpression.toString() }.toMutableList()

        currentTasks = newTasks
        taskMaxWeightPrev = taskMaxWeight
        taskMaxWeight = allTasks.map { it.usedSubstitutions.sumByDouble { rule -> rule.difficultyInTaskAutoGeneration } }.max() ?: 0.0

        log.addMessage({ "new tasks (step $stepId): " + newTasks.map { structureStringToString(it.currentExpression.computeIdentifier()) + " (from ${structureStringToString(it.previousExpressions.last().toString())})" }.joinToString(separator = ", \n")}, messageType = MessageType.TECHNICAL)

        stepId++

        if (newTasks.isEmpty()) {
            log.addMessage({"WARNING - layer newTasks is empty, finishing algorithm"}, messageType = MessageType.TECHNICAL)
            break
        }
    }

    allTasks.forEach {
        it.currentExpression.applyAllSubstitutions(settings.mandatoryResultTransformations)
        it.currentExpression = simplifyAndNormalizeExpression(it.currentExpression, compiledConfiguration)
    }

    // postprocessing
    allTasks = allTasks.map {
        postprocess(it, settings.postprocessExpressionSubstitutions, compiledConfiguration, stepId)
    }.toMutableList()

    var resultAllTasks = unify(allTasks, compiledConfiguration)

    resultAllTasks.forEach {
        it.requiredSubstitutions = it.usedSubstitutions.groupBy { it.code }.values.map { it.first() }.toMutableSet()
        val expressionsInSolution = mutableListOf<ExpressionNode>().apply {
            addAll(it.previousExpressions)
            add(it.currentExpression)
        }
        it.solution = expressionsInSolution.joinToString(" = ") { i -> expressionToString(i) }
    }

    val tasksBeforeFinalFilter = resultAllTasks
    log.addMessage({ "before final filter: " + tasksBeforeFinalFilter.map { it.currentExpression.computeIdentifier() }.joinToString(separator = ", \n")}, messageType = MessageType.TECHNICAL)

    resultAllTasks = resultAllTasks
        .filter { validateTask(it, taskStartGeneratedExpression, expressionComparator) }
        .filter { verifyAllPartsHasBeenTransformed(initialTask.currentExpression, it.currentExpression) }.toMutableList()

    val tasksWithNeededComplexity = resultAllTasks.filter { it -> it.usedSubstitutions.sumByDouble { it.difficultyInTaskAutoGeneration } in minWeight..maxWeight }.toMutableList()
    log.addMessage({ "with needed weight : " + tasksWithNeededComplexity.map { it.currentExpression.computeIdentifier() }.joinToString(separator = ", \n")}, messageType = MessageType.TECHNICAL)
    if (tasksWithNeededComplexity.isEmpty()) {
        log.addMessage({ "WARNING: no tasks with needed weight. Tasks with max weight will be chosen : " + tasksWithNeededComplexity.map { it.currentExpression.computeIdentifier() }.joinToString(separator = ", \n")}, messageType = MessageType.TECHNICAL)
        resultAllTasks = resultAllTasks.filter { it.usedSubstitutions.sumByDouble { rule -> rule.difficultyInTaskAutoGeneration } <= maxWeight }.toMutableList()
        resultAllTasks.sortByDescending { it.usedSubstitutions.sumByDouble { rule -> rule.difficultyInTaskAutoGeneration } }
        resultAllTasks = resultAllTasks.take(5).toMutableList()
    } else {
        resultAllTasks = tasksWithNeededComplexity
    }

    resultAllTasks.forEach {
        val isEqual = compiledConfiguration.factComparator.expressionComparator.fastProbabilityCheckOnIncorrectTransformation(it.startExpression, it.currentExpression)
        if (!isEqual) {
            println("${it.startExpression} != ${it.currentExpression}")
        }
    }

    resultAllTasks = sortTasks(resultAllTasks, settings.sortType, settings.sortOrder, tagsChosen)
    resultAllTasks = resultAllTasks.subList(0, min(resultAllTasks.size, settings.maxTaskCount))

    log.addMessage({ "after final filter: " + resultAllTasks.map { it.currentExpression.computeIdentifier() }.joinToString(separator = ", \n")}, messageType = MessageType.TECHNICAL)
    log.addMessage({ "iterations total: $generatorIterationCounter" }, MessageType.TECHNICAL)

    return resultAllTasks.map {
        TaskITR(
            code = taskStartGeneratedExpression.code,
            nameEn = taskStartGeneratedExpression.nameEn,
            nameRu = taskStartGeneratedExpression.nameRu,
            descriptionShortEn = taskStartGeneratedExpression.descriptionShortEn,
            descriptionShortRu = taskStartGeneratedExpression.descriptionShortRu,
            descriptionEn = taskStartGeneratedExpression.descriptionEn,
            descriptionRu = taskStartGeneratedExpression.descriptionRu,
            subjectType = taskStartGeneratedExpression.subjectType,
            tags = it.usedSubstitutions.flatMap { rule -> rule.tagsForTaskGenerator }.map { tag -> tag.name }.toMutableSet(),
            originalExpressionStructureString = it.startExpression.toString(),

            goalType = "expression",
            goalExpressionStructureString = it.currentExpression.toString(),
            goalPattern = "",

            rulePacks = listOf(),
            rules = listOf(),

            stepsNumber = it.previousExpressions.size,
            time = it.time,
            difficulty = it.usedSubstitutions.sumByDouble {rule -> rule.difficultyInTaskAutoGeneration},
            targetDifficulty = settings.targetWeight,

            solutionPlainText = it.solution,
            solutionsStepsTree = mapOf("data" to it.solutionsStepTree),
            hints = mapOf("data" to it.hints),

            expressionTaskIntermediateData = it.expressionTaskIntermediateData
        )
    }
}

fun sortTasks(tasks: List<ExpressionTask>, sortType: SortType,
              sortOrder: SortOrder, tagsChosen: List<RuleTag>): MutableList<ExpressionTask> {
    var tagUsageComparator = compareBy<ExpressionTask> { getTagUsage(it, tagsChosen) }
    var expressionLengthComparator = compareBy<ExpressionTask> { it.currentExpression.identifier.length }
    if (sortOrder == SortOrder.DESC) {
        tagUsageComparator = tagUsageComparator.reversed()
        expressionLengthComparator = expressionLengthComparator.reversed()
    }
    return when (sortType) {
        SortType.BY_RULE_TAG_USAGE -> tasks.sortedWith(tagUsageComparator
                .thenComparator { t1, t2 -> expressionLengthComparator.compare(t1, t2) }).toMutableList()
        SortType.BY_TASK_LENGTH -> tasks.sortedWith(expressionLengthComparator).toMutableList()
    }
}

fun tryToGenerateNewTasks(currentTask: ExpressionTask, newCurrentTasks: MutableList<ExpressionTask>,
                          settings: GeneratorSettings, stepId: Int) {
    val compiledConfiguration = settings.compiledConfiguration
    val currentExpression = currentTask.currentExpression
    currentExpression.computeNodeIdsAsNumbersInDirectTraversalAndDistancesToRoot()

    // 1. places, then substitutions
    placesThenSubstitutionsGenerator(currentTask, newCurrentTasks, compiledConfiguration, stepId)

    // 2. substitutions, then places
    substitutionsThenPlacesGenerator(currentTask, newCurrentTasks, settings, stepId)
}

fun placesThenSubstitutionsGenerator(currentTask: ExpressionTask, newCurrentTasks: MutableList<ExpressionTask>,
                                     compiledConfiguration: CompiledConfiguration, stepId: Int) {
    val currentExpression = currentTask.currentExpression
    val expressionComparator = compiledConfiguration.factComparator.expressionComparator
    val selectedNodeIds = currentExpression.selectRandomNodeIdsToTransform()
    for (nodeId in selectedNodeIds) {
        val applications = findApplicableSubstitutionsInSelectedPlace(currentExpression, listOf(nodeId).toTypedArray(),
                compiledConfiguration, withReadyApplicationResult = true)

        log.addMessage({
            "(places, then substitutions) stepId = ${stepId}, " +
                    "applications = ${applications.map {
                        "rule: ${it.expressionSubstitution.computeIdentifier(false)}, result: ${it.resultExpression.computeIdentifier()}"
                    }.joinToString(separator = ", \n")
                    }"
        }, messageType = MessageType.TECHNICAL)


        for (application in applications) {
            if (application.containsTrigonometricFunctionInResult()
                    && (application.originalExpressionChangingPart.insideExponent(expressionComparator) || application.originalExpressionChangingPart.insideTrigonometricFunction())) {
                log.addMessage({ "(places, then substitutions) stepId = ${stepId}, rule ${application.expressionSubstitution.computeIdentifier(false)} is failed (can't be applied inside exponent or trigonometric function)" }, messageType = MessageType.TECHNICAL)
                continue
            }
            if (currentTask.previousExpressions.any { compareExpressionNodes(it, application.resultExpression) }) {
                log.addMessage({ "(places, then substitutions) stepId = ${stepId}, rule ${application.expressionSubstitution.computeIdentifier(false)} is failed (leads to one of previous expressions)" }, messageType = MessageType.TECHNICAL)
                continue
            }

            val newTask = currentTask.clone()
            newTask.previousExpressions.add(newTask.currentExpression.clone())
            newTask.currentExpression = application.resultExpression
            newTask.usedSubstitutions.add(application.expressionSubstitution)

            newTask.solutionsStepTree.add(SolutionsStepITR(
                    newTask.currentExpression.toString(),
                    application.expressionSubstitution,
                    listOf(nodeId),
                    stepId,
                    newTask.solutionsStepTree.lastOrNull()?.stepId ?: -1))
            newTask.updateTagWeightMap(application.expressionSubstitution)
            newCurrentTasks.add(newTask)

            log.addMessage({
                "(places, then substitutions) " +
                        "stepId = ${stepId}, new task = ${newTask.currentExpression.computeIdentifier()}, " +
                        "used substitution = ${application.expressionSubstitution.computeIdentifier(false)}"
            }, messageType = MessageType.TECHNICAL)
        }
    }
}

fun substitutionsThenPlacesGenerator(currentTask: ExpressionTask, newCurrentTasks: MutableList<ExpressionTask>,
                                     settings: GeneratorSettings, stepId: Int) {
    val compiledConfiguration = settings.compiledConfiguration
    val expressionComparator = compiledConfiguration.factComparator.expressionComparator
    val currentExpression = currentTask.currentExpression
    val functionsInExpression = currentExpression.getContainedFunctions()

    val actualExpressionSubstitutions =  if (currentTask.usedSubstitutions.sumByDouble { it.difficultyInTaskAutoGeneration } < settings.targetWeight * settings.extendReduceFactor) {
        // запутываем исходное выражение
        settings.extendingExpressionSubstitutions
    } else {
        // упрощаем получившеся выражение
        settings.reducingExpressionSubstitutions
    }

    var appropriateSubstitutions = mutableListOf<ExpressionSubstitution>()
    for (substitution in actualExpressionSubstitutions) {
        if (substitution.isAppropriateToFunctions(functionsInExpression) && substitution.left.nodeType != NodeType.EMPTY) {
            if (substitution.findAllPossibleSubstitutionPlaces(currentExpression, expressionComparator).isNotEmpty()) {
                substitution.setStepIdForRight(stepId)
                appropriateSubstitutions.add(substitution)
            }
        }
    }
    val appropriateSubstitutionsWithNonZeroWeight = appropriateSubstitutions.filter { it.difficultyInTaskAutoGeneration > 0.0 }.toMutableList()
    if (appropriateSubstitutionsWithNonZeroWeight.isNotEmpty()) {
        appropriateSubstitutions = appropriateSubstitutionsWithNonZeroWeight
    }

    log.addMessage({
        "(substitutions, then places) stepId = ${stepId}, " +
                "appropriate substitutions: ${appropriateSubstitutions.map { "code = ${it.computeIdentifier(false)}, " +
                        "weightInTaskAutoGeneration = ${it.weightInTaskAutoGeneration}, weight = ${it.difficultyInTaskAutoGeneration}" }
                        .joinToString(separator = ", \n")}"
    }, messageType = MessageType.TECHNICAL)

    val appropriateSubstitutionWeight = appropriateSubstitutions.sumByDouble {
        val mainTag = it.tagsForTaskGenerator.first()
        val tagWeight = currentTask.tagWeightMap[mainTag] ?: 1.0
        it.weightInTaskAutoGeneration * tagWeight
    }
    if (appropriateSubstitutionWeight < epsilon) {
        return
    }

    val newTask = currentTask.clone()
    var selector = random(0.0, appropriateSubstitutionWeight)
    log.addMessage({"selector value = $selector (out of $appropriateSubstitutionWeight)"}, MessageType.TECHNICAL)
    var currentSubstitutionIndex = 0
    var currentSubstitution = appropriateSubstitutions[0]
    while (selector > currentSubstitution.weightInTaskAutoGeneration) {
        val mainTag = currentSubstitution.tagsForTaskGenerator.first()
        val tagWeight = currentTask.tagWeightMap[mainTag] ?: 1.0
        selector -= currentSubstitution.weightInTaskAutoGeneration * tagWeight
        currentSubstitutionIndex++
        currentSubstitution = appropriateSubstitutions[currentSubstitutionIndex]
    }

    val selectedSubstitution = appropriateSubstitutions[currentSubstitutionIndex]

    log.addMessage({
        "(substitutions, then places) stepId = ${stepId}, " +
                "selected substitution: ${selectedSubstitution.computeIdentifier(false)}"
    }, messageType = MessageType.TECHNICAL)

    var places = selectedSubstitution.findAllPossibleSubstitutionPlaces(newTask.currentExpression, expressionComparator)
    if (selectedSubstitution.containsTrigonometricFunctionInResult()) {
        places = places.filter { !it.originalValue.insideExponent(expressionComparator) && !it.originalValue.insideTrigonometricFunction() }.toMutableList()
        if (places.isEmpty()) {
            log.addMessage({
                "(substitutions, then places) stepId = ${stepId}, " +
                        "selected substitution: ${selectedSubstitution.computeIdentifier(false)} failed (inside exponent or trigonometric function)"
            }, messageType = MessageType.TECHNICAL)
            return
        }
    }
    if (places.size == 0) {
        log.addMessage({
            "(substitutions, then places) stepId = ${stepId}, " +
                    "selected substitution: ${selectedSubstitution.computeIdentifier(false)} failed (no places)"
        }, messageType = MessageType.TECHNICAL)
        return
    }
    val changedExpression = newTask.currentExpression.clone()
    newTask.previousExpressions.add(newTask.currentExpression.clone())
    newTask.usedSubstitutions.add(selectedSubstitution)
    val bitMask = (1 shl randomInt(0, places.size - 1))
    val changedNodeIds = selectedSubstitution.applySubstitutionByBitMask(places, bitMask)
    if (currentTask.previousExpressions.any { compareExpressionNodes(it, newTask.currentExpression) }) {
        log.addMessage({
            "(substitutions, then places) stepId = ${stepId}, " +
                    "task is failed (leads to one of previous nodes): ${newTask.currentExpression.computeIdentifier()}"
        }, messageType = MessageType.TECHNICAL)
        return
    }
    newTask.solutionsStepTree.add(
            SolutionsStepITR(
                    changedExpression.toString(),
                    selectedSubstitution,
                    changedNodeIds,
                    stepId,
                    newTask.solutionsStepTree.lastOrNull()?.stepId ?: -1
            )
    )
    newTask.updateTagWeightMap(selectedSubstitution)

    log.addMessage({
        "(substitutions, then places) " +
                "stepId = ${stepId}, new task = ${newTask.currentExpression.computeIdentifier()}, " +
                "used substitution = ${selectedSubstitution.computeIdentifier(false)}"
    }, messageType = MessageType.TECHNICAL)
    newCurrentTasks.add(newTask)
}

fun getTagUsage(task: ExpressionTask, tagsChosen: List<RuleTag>): Double {
    val usedTags = task.usedSubstitutions.flatMap { it.tagsForTaskGenerator }
            .distinct()
    return (usedTags.intersect(tagsChosen).size.toDouble()) / tagsChosen.size
}

fun unify(tasks: List<ExpressionTask>, compiledConfiguration: CompiledConfiguration) : MutableList<ExpressionTask> {
    val unifiedTasks = mutableListOf<ExpressionTask>()

    for (task in tasks) {
        var isNew = true
        for (unifiedTask in unifiedTasks) {
            if (compiledConfiguration.factComparator.expressionComparator
                    .compareAsIs(task.currentExpression, unifiedTask.currentExpression)) {
                isNew = false
                break
            }
        }
        if (isNew) {
            unifiedTasks.add(task)
        }
    }
    return unifiedTasks
}
