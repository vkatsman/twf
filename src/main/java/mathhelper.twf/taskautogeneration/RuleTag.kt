package mathhelper.twf.taskautogeneration

enum class RuleTag(val code: String, val readyForUseInProduction: Boolean = true) {

    // common math
    BASIC_MATH("Базовые правила математики"),

    // trigonometry
    TRIGONOMETRY_FUNCTIONS_DEFINITION("Определение основных тригонометрических функций (sin, cos, tg, ctg), их чётность/нечётность"),
    TRIGONOMETRY_STANDARD_ANGLES("Стандартные углы тригонометрической таблицы"),
    TRIGONOMETRY_BASIC_IDENTITY("Основное тригонометрическое тождество"),
    TRIGONOMETRY_PERIODIC("Периодичность тригонометрических функций"),
    TRIGONOMETRY_SHIFTING("Формулы приведения"),
    TRIGONOMETRY_INVERSE_FUNCTIONS("Аркфункции - определение"),
    TRIGONOMETRY_INVERSE_FUNCTIONS_PROPERTIES("Аркфункции - свойства"),
    TRIGONOMETRY_INVERSE_FUNCTIONS_ADVANCED("Аркфункции - продвинутые свойства"),
    TRIGONOMETRY_INVERSE_FUNCTIONS_STANDARD_ANGLES("Аркфункции - стандартные углы тригонометрической таблицы"),
    TRIGONOMETRY_SIN_COS_SUM_AND_DIFF_OF_ANGLES("Сумма и разность аргументов - sin, cos"),
    TRIGONOMETRY_TG_CTG_SUM_AND_DIFF_OF_ANGLES("Сумма и разность аргументов - tg, ctg"),
    TRIGONOMETRY_DOUBLE_ANGLES("Формулы двойного аргумента"),
    TRIGONOMETRY_TRIPLE_ANGLES("Формулы тройного аргумента"),
    TRIGONOMETRY_HALF_ANGLES("Формулы половинного аргумента"),
    TRIGONOMETRY_MULTI_ANGLES("Формулы аргумента nx"),
    TRIGONOMETRY_POWER_REDUCING("Формулы понижения степени"),
    TRIGONOMETRY_SUM_AND_DIFF_OF_FUNCTIONS("Преобразование суммы/разности тригонометрических функций в произведение"),
    TRIGONOMETRY_AUXILIARY_ARGUMENT("Формула вспомогательного аргумента", false),
    TRIGONOMETRY_WEIERSTRASS_SUBSTITUTION("Универсальная тригонометрическая подстановка"),
    TRIGONOMETRY_EULER_FORMULAS("Представление тригонометрических функций в комплексной форме", false),
    TRIGONOMETRY_HYPERBOLIC_FUNCTIONS("Гиперболические функции", false)
}
