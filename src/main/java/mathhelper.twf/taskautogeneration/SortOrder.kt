package mathhelper.twf.taskautogeneration

enum class SortOrder(val code: String) {
    ASC("По возрастанию"),
    DESC("По убыванию")
}
