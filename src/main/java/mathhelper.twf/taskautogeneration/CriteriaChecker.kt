package mathhelper.twf.taskautogeneration

import mathhelper.twf.api.structureStringToExpression
import mathhelper.twf.api.structureStringToString
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.TaskITR
import mathhelper.twf.expressiontree.ExpressionNode

val defectCountMap: MutableMap<DefectCriteria, Int> = mutableMapOf()

val compiledConfiguration = CompiledConfiguration()

fun checkTasksByAllCriteria(tasks: List<TaskITR>): Int {
    if (tasks.isEmpty()) {
        return 0
    }
    return tasks.mapNotNull { it.goalExpressionStructureString?.let { it1 -> structureStringToExpression(it1) } }
            .map { checkExpressionByAllCriteria(it) }
            .reduce { sum, x -> sum + x }
}

fun <K> increment(map: MutableMap<K, Int>, key: K) {
    if (map.containsKey(key)) {
        val prev: Int = map[key] ?: 0
        map[key] = prev + 1
    } else {
        map[key] = 1
    }
}

fun logDefectIncrement(criteria: DefectCriteria) {
    increment(defectCountMap, criteria)
}

fun checkExpressionByAllCriteria(node: ExpressionNode): Int {
    var defectValue = 0
    defectValue += computeDefectValueForMinusesInRow(node)
    defectValue += computeDefectValueForSyntaxSugar(node)
    defectValue += computeDefectValueForOverlayeredFraction(node)
    defectValue += computeDefectValueForUncomputedExpression(node)
    defectValue += computeDefectValueForNonReducedTerms(node)
    defectValue += computeDefectValueForNonReducedMultipliers(node)
    defectValue += computeDefectValueForRedundantBracketsForTerms(node)
    defectValue += computeDefectValueForRedundantBracketsForMultipliers(node)
    defectValue += computeDefectValueForFractionLengthRatioLimitExceeded(node)
    defectValue += computeDefectValueForNotLeftedConstants(node)
    defectValue += computeDefectValueForNotReducedFractionCriteria(node)
    return defectValue
}

fun computeDefectValueForNotReducedFractionCriteria(node: ExpressionNode, depth: Int = 0): Int {
    var result = 0
    for (child in node.children) {
        when (child.value) {
            "/" -> {
                val nominatorNodes = getMultipliers(child.children[0])
                val denominatorNodes = getMultipliers(child.children[1])
                val nodesMapOfNominator: Map<String, List<ExpressionNode>> = nominatorNodes.groupBy { getKeyPartForMultiplier(it).computeIdentifier() }
                val nodesMapOfDenominator: Map<String, List<ExpressionNode>> = denominatorNodes.groupBy { getKeyPartForMultiplier(it).computeIdentifier() }
                if (nodesMapOfNominator.isNotEmpty() && nodesMapOfDenominator.isNotEmpty()
                        && nodesMapOfDenominator.keys.intersect(nodesMapOfNominator.keys).isNotEmpty()) {
                    result += 1
                    logDefectIncrement(DefectCriteria.NON_REDUCED_FRACTION)
                } else {
                    val intsInNominator = retrieveNumbers(nominatorNodes)
                    val intsInDenominator = retrieveNumbers(denominatorNodes)
                    for (i1 in intsInNominator) {
                        for (i2 in intsInDenominator) {
                            if (findGCD(getModule(i1).value.toInt(), getModule(i2).value.toInt()) > 1) {
                                result += 1
                                logDefectIncrement(DefectCriteria.NON_REDUCED_FRACTION)
                                break
                            }
                        }
                    }
                }
                result += computeDefectValueForNotReducedFractionCriteria(child, depth = depth + 1)
            }
            else -> {
                result += computeDefectValueForNotReducedFractionCriteria(child, depth = depth + 1)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect NON_REDUCED_TERMS in expression ${structureStringToString(node.toString())}, score = $result")
    }
    return result
}

fun computeDefectValueForNotLeftedConstants(node: ExpressionNode, depth: Int = 0): Int {
    var result = 0
    for (child in node.children) {
        when (child.value) {
            "*" -> {
                val lastConstIndex = child.children.toTypedArray().indexOfLast { x -> x.isIntNumber() || (x.value == "-" && x.children.size == 1 && x.children[0].isIntNumber()) }
                val firstNonConstIndex = child.children.toTypedArray().indexOfFirst {x -> !(x.isIntNumber() || (x.value == "-" && x.children.size == 1 && x.children[0].isIntNumber()))}
                if (lastConstIndex > firstNonConstIndex) {
                    result += 1
                    logDefectIncrement(DefectCriteria.NOT_LEFTED_CONSTANTS)
                }
                result += computeDefectValueForNotLeftedConstants(child, depth = depth + 1)
            }
            else -> {
                result += computeDefectValueForNotLeftedConstants(child, depth = depth + 1)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect NOT_LEFTED_CONSTANTS in expression ${structureStringToString(node.toString())}, score = $result")
    }
    return result
}

fun computeDefectValueForFractionLengthRatioLimitExceeded(node: ExpressionNode, depth: Int = 0, r: Double = 20.0): Int {
    var result = 0
    for (child in node.children) {
        when (child.value) {
            "/" -> {
                val nominatorString = structureStringToString(child.children[0].identifier)
                val denominatorString = structureStringToString(child.children[1].identifier)
                val ratio = nominatorString.length.toDouble() / denominatorString.length
                if (ratio > r) {
                    result += 1
                    logDefectIncrement(DefectCriteria.NOMINATOR_TO_DENOMINATOR_FRACTION_LIMIT_EXCEEDED)
                }
                result += computeDefectValueForFractionLengthRatioLimitExceeded(child, depth = depth + 1)
            }
            else -> {
                result += computeDefectValueForFractionLengthRatioLimitExceeded(child, depth = depth + 1)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect NOMINATOR_TO_DENOMINATOR_FRACTION_LIMIT_EXCEEDED in expression ${structureStringToString(node.toString())}, score = $result")
    }
    return result
}

fun computeDefectValueForRedundantBracketsForMultipliers(node: ExpressionNode, depth: Int = 0): Int {
    var result = 0
    for (child in node.children) {
        when (child.value) {
            "*" -> {
                for (grandChild in child.children) {
                    if (grandChild.value == "*") {
                        result += 1
                        logDefectIncrement(DefectCriteria.REDUNDANT_BRACKETS_FOR_MULTIPLIERS)
                    }
                    result += computeDefectValueForRedundantBracketsForMultipliers(child, depth = depth + 1)
                }
            }
            else -> {
                result += computeDefectValueForRedundantBracketsForMultipliers(child, depth = depth + 1)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect REDUNDANT_BRACKETS_FOR_MULTIPLIERS in expression ${structureStringToString(node.toString())}, score = $result")
    }
    return result
}

fun computeDefectValueForRedundantBracketsForTerms(node: ExpressionNode, depth: Int = 0): Int {
    var result = 0
    for (child in node.children) {
        when (child.value) {
            "+" -> {
                for (grandChild in child.children) {
                    if (grandChild.value == "+") {
                        result += 1
                        logDefectIncrement(DefectCriteria.REDUNDANT_BRACKETS_FOR_TERMS)
                    }
                    result += computeDefectValueForRedundantBracketsForTerms(child, depth = depth + 1)
                }
            }
            else -> {
                result += computeDefectValueForRedundantBracketsForTerms(child, depth = depth + 1)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect REDUNDANT_BRACKETS_FOR_TERMS in expression ${structureStringToString(node.toString())}, score = $result")
    }
    return result
}

fun computeDefectValueForNonReducedMultipliers(node: ExpressionNode, depth: Int = 0): Int {
    var result = 0
    for (child in node.children) {
        when (child.value) {
            "*" -> {
                val nodesMap: Map<String, List<ExpressionNode>> = child.children.groupBy { getKeyPartForMultiplier(it).computeIdentifier() }
                if (nodesMap.any { it.value.size > 1 }) {
                    result += 1
                    logDefectIncrement(DefectCriteria.NON_REDUCED_MULTIPLIERS)
                }
                result += computeDefectValueForNonReducedMultipliers(child, depth = depth + 1)
            }
            else -> {
                result += computeDefectValueForNonReducedMultipliers(child, depth = depth + 1)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect NON_REDUCED_MULTIPLIERS in expression ${structureStringToString(node.toString())}, score = $result")
    }
    return result
}

fun computeDefectValueForNonReducedTerms(node: ExpressionNode, depth: Int = 0): Int {
    var result = 0
    for (child in node.children) {
        when (child.value) {
            "+" -> {
                val nodesMap: Map<String, List<ExpressionNode>> = child.children.groupBy { getKeyPartForTerm(it, compiledConfiguration).computeIdentifier() }
                if (nodesMap.any { it.value.size > 1 }) {
                    result += 1
                    logDefectIncrement(DefectCriteria.NON_REDUCED_TERMS)
                }
                result += computeDefectValueForNonReducedTerms(child, depth = depth + 1)
            }
            else -> {
                result += computeDefectValueForNonReducedTerms(child, depth = depth + 1)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect NON_REDUCED_TERMS in expression ${structureStringToString(node.toString())}, score = $result")
    }
    return result
}

fun computeDefectValueForUncomputedExpression(node: ExpressionNode, depth: Int = 0): Int {
    var result = 0
    for (child in node.children) {
        val numbers = getNumbers(child.children)
        when (child.value) {
            "+", "*", "^" -> {
                if (numbers.size >= 2) {
                    result += 1
                    logDefectIncrement(DefectCriteria.UNCOMPUTED_EXPRESSION)
                }
                result += computeDefectValueForUncomputedExpression(child, depth = depth + 1)
            }
            else -> {
                result += computeDefectValueForUncomputedExpression(child, depth = depth + 1)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect UNCOMPUTED_EXPRESSION in expression ${structureStringToString(node.toString())}, score = $result")
    }
    return result
}

fun getNumbers(nodes: List<ExpressionNode>) : MutableList<ExpressionNode> {
    return nodes
            .filter { it.isIntNumber()
                    || (it.value == "-" && it.children.size == 1 && it.children[0].isIntNumber())
            }.toMutableList()
}

fun computeDefectValueForOverlayeredFraction(node: ExpressionNode, depth: Int = 0, divisionsInARow: Int = 0): Int {
    var result = 0
    for (child in node.children) {
        when (child.value) {
            "*" -> {
                result += computeDefectValueForOverlayeredFraction(child, depth = depth + 1, divisionsInARow = divisionsInARow)
            }
            "+" -> {
                if (child.children.size == 1) {
                    result += computeDefectValueForOverlayeredFraction(child, depth = depth + 1, divisionsInARow = divisionsInARow)
                } else {
                    result += computeDefectValueForOverlayeredFraction(child, depth = depth + 1, divisionsInARow = 0)
                }
            }
            "-" -> {
                result += computeDefectValueForOverlayeredFraction(child, depth = depth + 1, divisionsInARow = divisionsInARow)
            }
            "/" -> {
                if (divisionsInARow > 0) {
                    result += 1
                    logDefectIncrement(DefectCriteria.OVERLAYERED_FRACTION)
                } else {
                    result += computeDefectValueForOverlayeredFraction(child, depth = depth + 1, divisionsInARow = divisionsInARow + 1)
                }
            }
            else -> {
                result += computeDefectValueForOverlayeredFraction(child, depth = depth + 1, divisionsInARow = 0)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect OVERLAYERED_FRACTION in expression ${structureStringToString(node.toString())}, score = $result")
    }
    return result
}

fun computeDefectValueForSyntaxSugar(node: ExpressionNode, depth: Int = 0): Int {
    var result = 0
    for (child in node.children) {
        when (child.value) {
            "+" -> {
                if (child.children.any { it.identifier == "0" || it.identifier == "(0)" }) {
                    result += 1
                    logDefectIncrement(DefectCriteria.SYNTAX_SUGAR)
                }
                result += computeDefectValueForSyntaxSugar(child, depth = depth + 1)
            }
            "*" -> {
                if (child.children.any { it.identifier == "1" || it.identifier == "(1)" || it.identifier == "0" || it.identifier == "(0)" }) {
                    result += 1
                    logDefectIncrement(DefectCriteria.SYNTAX_SUGAR)
                }
                result += computeDefectValueForSyntaxSugar(child, depth = depth + 1)
            }
            "^" -> {
                if (child.children.size == 2) {
                    val degree = child.children[1]
                    val base = child.children[0]
                    if (degree.identifier == "1" || degree.identifier == "0" || degree.identifier == "(1)" || degree.identifier == "(0)" || base.identifier == "1" || base.identifier == "(1)") {
                        result += 1
                        logDefectIncrement(DefectCriteria.SYNTAX_SUGAR)
                    }
                    result += computeDefectValueForSyntaxSugar(child, depth = depth + 1)
                }
            }
            "/" -> {
                val denominator = child.children[1]
                if (denominator.identifier == "1" || denominator.identifier == "(1)") {
                    result += 1
                    logDefectIncrement(DefectCriteria.SYNTAX_SUGAR)
                }
                result += computeDefectValueForSyntaxSugar(child, depth = depth + 1)
            }
            else -> {
                result += computeDefectValueForSyntaxSugar(child, depth = depth + 1)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect SYNTAX_SUGAR in expression ${structureStringToString(node.toString())}, score = $result")
    }
    return result
}

fun computeDefectValueForMinusesInRow(node: ExpressionNode, depth: Int = 0, minusesInARow: Int = 0): Int {
    var result = 0
    for (child in node.children) {
       when (child.value) {
            "+" -> {
                if (child.children.size <= 1) {
                    result += computeDefectValueForMinusesInRow(child, depth = depth + 1, minusesInARow = minusesInARow)
                } else {
                    result += computeDefectValueForMinusesInRow(child, depth = depth + 1, minusesInARow = 0)
                }
            }
            "-" -> {
                if (minusesInARow > 0) {
                    result += 1
                    logDefectIncrement(DefectCriteria.SEVERAL_UNARY_MINUSES_IN_A_ROW)
                } else {
                    result += computeDefectValueForMinusesInRow(child,  depth = depth + 1, minusesInARow = minusesInARow + 1)
                }
            }
            else -> {
                result += computeDefectValueForMinusesInRow(child,  depth = depth + 1, minusesInARow = 0)
            }
        }
    }
    if (result > 0 && depth == 0) {
        println("Defect SEVERAL_UNARY_MINUSES_IN_A_ROW in expression ${structureStringToString(node.toString())}, score = $result, structureString=$node")
    }
    return result
}



