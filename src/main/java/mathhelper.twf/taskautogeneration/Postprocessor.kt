package mathhelper.twf.taskautogeneration

import mathhelper.twf.api.structureStringToExpression
import mathhelper.twf.api.structureStringToString
import mathhelper.twf.api.structureStringToTexString
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.SolutionsStepITR
import mathhelper.twf.expressiontree.*
import mathhelper.twf.logs.MessageType
import mathhelper.twf.logs.log
import mathhelper.twf.platformdependent.abs
import mathhelper.twf.platformdependent.randomInt
import kotlin.math.max
import kotlin.math.min
import kotlin.math.pow

fun splitConstants(task: ExpressionTask, compiledConfiguration: CompiledConfiguration, stepId: Int): ExpressionTask {
    val normalizedTask = task.clone()
    normalizedTask.currentExpression = traverseAndSplitConstants(normalizedTask.currentExpression, compiledConfiguration)
    normalizedTask.currentExpression.identifier = normalizedTask.currentExpression.toString()
    logTransformation(task, normalizedTask, "splitConstants", stepId)
    return normalizedTask
}

fun traverseAndSplitConstants(node: ExpressionNode, compiledConfiguration: CompiledConfiguration): ExpressionNode {
    val updatedChildren = mutableListOf<ExpressionNode>()
    for (child in node.children) {
        val updatedChild = traverseAndSplitConstants(child, compiledConfiguration)
        updatedChildren.add(updatedChild)
        updatedChild.parent = node
    }
    node.children = updatedChildren
    if (node.isIntNumber() && node.value != "" && !node.insideExponent(compiledConfiguration.factComparator.expressionComparator)) {
        val constantValue = node.value.toInt()
        if (constantValue <= 1) {
            return node
        }
        val firstHalf = randomInt(1, max(1, constantValue))
        val secondHalf = constantValue - firstHalf
        return ExpressionNodeBuilder.buildNodeFromTerms(
                mutableListOf(ExpressionNodeBuilder.buildNodeFromConstant(firstHalf), ExpressionNodeBuilder.buildNodeFromConstant(secondHalf)),
                compiledConfiguration
        )
    }
    return node
}

fun verifyAllPartsHasBeenTransformed(initial: ExpressionNode, result: ExpressionNode): Boolean {
    val initialNode = getModule(initial)
    val resultNode = getModule(result)

    log.addMessage({ "verifyAllPartsHasBeenTransformed: initialNode = ${initialNode.computeIdentifier()}, " +
            "resultNode = ${resultNode.computeIdentifier()}"}, messageType = MessageType.TECHNICAL)

    if (resultNode.value == "+" || resultNode.value == "*") {
        val nodesShouldBeTransformed = getTermsOrMultipliers(initialNode)
        val actualNodes = getTermsOrMultipliers(resultNode)
        for (node in nodesShouldBeTransformed) {
            if (actualNodes.any { getModule(it).identifier == getModule(node).identifier }) {
                log.addMessage({ "verifyAllPartsHasBeenTransformed - task is dropped! (node is not transformed criteria):" +
                        " not transformed node = ${node.computeIdentifier()}"}, messageType = MessageType.TECHNICAL)
                return false
            }
        }
    } else if (initialNode.value == "/") {
        val initialNominator = initialNode.children[0]
        val initialDenominator = initialNode.children[1]
        if (resultNode.value == "/") {
            val resultNominator = resultNode.children[0]
            val resultDenominator = resultNode.children[1]
            val isValidationFailed = (getModule(initialNominator).identifier == getModule(resultNominator).identifier
                    || getModule(initialDenominator).identifier == getModule(resultDenominator).identifier)
            if (isValidationFailed) {
                log.addMessage({ "verifyAllPartsHasBeenTransformed - task is dropped! (fraction criteria): initialNode = ${initialNode.computeIdentifier()}, " +
                        "resultNode = ${resultNode.computeIdentifier()}"}, messageType = MessageType.TECHNICAL)
            }
            return isValidationFailed
        }
    }
    if (initialNode.identifier == resultNode.identifier) {
        log.addMessage({ "verifyAllPartsHasBeenTransformed - task is dropped! (the same identifiers criteria): initialNode = ${initialNode.computeIdentifier()}, " +
                "resultNode = ${resultNode.computeIdentifier()}"}, messageType = MessageType.TECHNICAL)
        return false
    }
    return  true
}

fun getTermsOrMultipliers(root: ExpressionNode): List<ExpressionNode> {
    if (root.value == "*" || root.value == "+") {
        return root.children
    }
    return mutableListOf(root)
}

fun ExpressionNode.containsError(): Boolean {
    var result = false
    for (child in this.children) {
        result = result && child.containsError()
    }
    return nodeType == NodeType.ERROR
}

fun validateTask(resultTask: ExpressionTask, taskStart: GeneratedExpression, expressionComparator: ExpressionComparator): Boolean {
    val initialExpression = taskStart.expressionNode
    val resultExpression = resultTask.currentExpression
    if (resultExpression.containsError()) {
        log.addMessage({ "resultExpression ${resultExpression.identifier} filtered (contains error)" }, messageType = MessageType.TECHNICAL)
        return false
    }
    val isValid = expressionComparator.fastProbabilityCheckOnIncorrectTransformation(initialExpression, resultExpression)
    if (!isValid) {
        val warningMessage = StringBuilder("Generated task is not equal to initial! \n")
                .append("Initial: ${initialExpression.identifier} \n")
                .append("Result: ${resultExpression.identifier}")

        log.addMessage({ warningMessage.toString() }, messageType = MessageType.TECHNICAL)
        return false
    }
    return true
}

/**
 * Used to transform generated task to normal form
 * For example, this postprocessor removes defects like 1/(1/a), -(-(a)), etc.
 *
 * @param task: task to transform
 * @param postprocessSubstitutions: substitutions that postprocessor will try to apply
 * See [mathhelper.twf.taskautogeneration.rulepack.PostprocessorRulePack]
 *
 * @return task with applied substitutions
 */
fun applySimplePostprocessorRules(task: ExpressionTask, postprocessSubstitutions: List<ExpressionSubstitution>,
                                  expressionComparator: ExpressionComparator, stepId: Int) : ExpressionTask {
    var currentTask = task.clone()

    var possibleSubstitutions =
            findPossibleSubstitutions(currentTask, postprocessSubstitutions, expressionComparator)
    while (possibleSubstitutions.isNotEmpty()) {
        for (substitution in postprocessSubstitutions) {
            val newTask = currentTask.clone()

            val places = substitution.findAllPossibleSubstitutionPlaces(newTask.currentExpression, expressionComparator)
            if (places.size == 0) {
                continue
            } else {
                val changedExpression = newTask.currentExpression.clone()
                newTask.previousExpressions.add(newTask.currentExpression.clone())
                newTask.usedSubstitutions.add(substitution)
                val bitMask = (1 shl places.size) - 1
                val changedNodeIds = substitution.applySubstitutionByBitMask(places, bitMask)
                newTask.expressionTaskIntermediateData.appliedPostprocessSubstitutions[substitution.code] = stepId
                newTask.solutionsStepTree.add(SolutionsStepITR(
                        changedExpression.toString(),
                        substitution,
                        changedNodeIds,
                        stepId,
                        newTask.solutionsStepTree.lastOrNull()?.stepId ?: -1))
            }
            currentTask = newTask
        }
        possibleSubstitutions =
                findPossibleSubstitutions(currentTask, postprocessSubstitutions, expressionComparator)
    }
    currentTask.expressionTaskIntermediateData.expressionBeforePostprocessPhase1 = task.currentExpression.clone()
    return currentTask
}

/**
 * Used to transform generated task to normal form,
 * but here are more complicated issues which can't be solved by simple rule substitution
 *
 * For example, this postprocessor can reduce fractions, can get rid of multilayer fractions etc.
 *
 * @param task: task to transform
 *
 * @return task in normalized form
 */
fun applySophisticatedPostprocessorRules(task: ExpressionTask, compiledConfiguration: CompiledConfiguration, stepId: Int): ExpressionTask {
    var prevTask = task

    var newTask = dragDescendantsUpper(task, "+", compiledConfiguration)
    logTransformation(prevTask, newTask, "dragTheDescendantsOfPlusUpper", stepId)

    prevTask = newTask
    newTask = dragDescendantsUpper(newTask, "*", compiledConfiguration)
    logTransformation(prevTask, newTask, "dragTheDescendantsOfMultiplicationUpper", stepId)

    prevTask = newTask
    newTask = getRidOfMultilayerFractions(newTask, compiledConfiguration)
    logTransformation(prevTask, newTask, "getRidOfMultilayerFractions", stepId)

    prevTask = newTask
    newTask = reduceFractions(newTask)
    logTransformation(prevTask, newTask, "reduceFractions", stepId)

    prevTask = newTask
    newTask = dragConstantsToTheLeft(newTask)
    logTransformation(prevTask, newTask, "dragConstantsToTheLeft", stepId)

    prevTask = newTask
    newTask = getRidOfDuplicatedNodes(newTask, compiledConfiguration)
    logTransformation(prevTask, newTask, "getRidOfDuplicatedNodes", stepId)

    prevTask = newTask
    newTask = getRidOfLongFractionIfNeeded(newTask, compiledConfiguration)
    logTransformation(prevTask, newTask, "getRidOfLongFractionIfNeeded", stepId)

    prevTask = newTask
    newTask = performComputationIfNeeded(newTask, compiledConfiguration)
    logTransformation(prevTask, newTask, "performComputationIfNeeded", stepId)

    return newTask
}

fun postprocess(task: ExpressionTask, postprocessSubstitutions: List<ExpressionSubstitution>,
                compiledConfiguration: CompiledConfiguration, stepId: Int): ExpressionTask {
    var prevTask: ExpressionTask
    var postprocessedTask = task
    var iterationsCount = 0
    do {
        prevTask = postprocessedTask
        postprocessedTask = applySophisticatedPostprocessorRules(prevTask, compiledConfiguration, stepId)
        postprocessedTask = applySimplePostprocessorRules(postprocessedTask, postprocessSubstitutions, compiledConfiguration.factComparator.expressionComparator, stepId)
        iterationsCount++
        if (iterationsCount > 3) { // TODO: fix this issue
            log.addMessage({ "WARNING: endless cycle" }, messageType = MessageType.TECHNICAL)
            log.addMessage({ "Prev task: ${prevTask.currentExpression.computeIdentifier()}" }, messageType = MessageType.TECHNICAL)
            log.addMessage({ "Postprocessed task: ${postprocessedTask.currentExpression.computeIdentifier()}" }, messageType = MessageType.TECHNICAL)
            return postprocessedTask
        }
    } while (prevTask.currentExpression.identifier != postprocessedTask.currentExpression.identifier)
    return postprocessedTask
}

fun performComputationIfNeeded(task: ExpressionTask, compiledConfiguration: CompiledConfiguration): ExpressionTask {
    val normalizedTask = task.clone()
    traverseAndPerformComputation(normalizedTask.currentExpression, compiledConfiguration)
    normalizedTask.currentExpression.identifier = normalizedTask.currentExpression.toString()
    return normalizedTask
}

fun traverseAndPerformComputation(inputNode: ExpressionNode, compiledConfiguration: CompiledConfiguration) {
    var node = inputNode
    if (node.nodeType == NodeType.FUNCTION && node.value == "+") {
        node = makeComputation(node, "+")
    }
    if (node.nodeType == NodeType.FUNCTION && node.value == "*") {
        node = makeComputation(node, "*")
    }
    for (child in node.children) {
        traverseAndPerformComputation(child, compiledConfiguration)
    }
}

fun retrieveNumbers(nodes: List<ExpressionNode>) : MutableList<ExpressionNode> {
    return nodes
            .map { computeExponentIfNeeded(it) }
            .filter { it.isIntNumber()
                    || (it.value == "-" && it.children.size == 1 && it.children[0].isIntNumber())
            }.toMutableList()
}

fun findGCD(n1: Int, n2: Int): Int {
    if (n2 != 0)
        return findGCD(n2, n1 % n2)
    else
        return n1
}

fun computeSum(numbers: List<ExpressionNode>) : Int {
    return numbers
            .map { if (it.value == "-") {-it.children[0].value.toInt()} else it.value.toInt() }
            .reduce { sum, x -> sum + x }
}

fun computeProd(numbers: List<ExpressionNode>) : Int {
    return numbers
            .map { if (it.value == "-") {-it.children[0].value.toInt()} else it.value.toInt() }
            .reduce { sum, x -> sum * x }
}

fun makeComputation(node: ExpressionNode, operation: String): ExpressionNode {
    val terms = node.children
    val numbers = retrieveNumbers(terms)
    val notNumbers = terms.filter { !numbers.contains(it) }
    if (numbers.size > 1) {
        when (operation) {
            "+" -> {
                val sum = computeSum(numbers)
                val sumNode = ExpressionNodeBuilder.buildNodeFromConstant(value = sum, parent = node)
                node.children = mutableListOf(sumNode)
                node.children.addAll(notNumbers)
            }
            "*" -> {
                val prod = computeProd(numbers)
                val prodNode = ExpressionNodeBuilder.buildNodeFromConstant(value = prod, parent = node)
                node.children = mutableListOf(prodNode)
                node.children.addAll(notNumbers)
            }
        }
    }
    if (node.children.size == 1) {
        return node.children[0]
    }
    return node
}

fun computeExponentIfNeeded(node: ExpressionNode): ExpressionNode {
    if (node.nodeType == NodeType.FUNCTION && node.value == "^"
            && node.children[0].isIntNumber() && node.children[1].isIntNumber()) {
        val base = node.children[0].value.toInt()
        val degree = node.children[1].value.toInt()
        val result = powInt(base, degree)
        return ExpressionNodeBuilder.buildNodeFromConstant(value = result, parent = node)
    }
    return node
}

fun powInt(base: Int, degree: Int) : Int {
    return base.toDouble().pow(degree.toDouble()).toInt()
}

fun traverseAndGetRidOfLongFractionIfNeeded(
        node: ExpressionNode,
        compiledConfiguration: CompiledConfiguration
): ExpressionNode {
    val updatedChildren = mutableListOf<ExpressionNode>()
    for (child in node.children) {
        val updatedChild = traverseAndGetRidOfLongFractionIfNeeded(child, compiledConfiguration)
        updatedChildren.add(updatedChild)
        updatedChild.parent = node
    }
    node.children = updatedChildren
    return getRidOfLongFraction(node, compiledConfiguration)
}

fun getRidOfLongFraction(node: ExpressionNode, compiledConfiguration: CompiledConfiguration,
                         nominatorToDenominatorRatioMax: Double = 20.0): ExpressionNode {
    if (node.nodeType == NodeType.FUNCTION && node.value == "/") {
        if (node.children[1].identifier.length == 0) {
            log.addMessage({"WARNING: denominator is empty"}, MessageType.TECHNICAL)
            return node
        }
        val nominatorString = structureStringToString(node.children[0].identifier)
        val denominatorString = structureStringToString(node.children[1].identifier)
        val ratio = nominatorString.length.toDouble() / denominatorString.length
        if (ratio > nominatorToDenominatorRatioMax) {
            val nominator = node.children[0]
            val denominator = node.children[1]
            val one = ExpressionNodeBuilder.buildNodeFromConstant(1)
            val oneDividedByDenominator = ExpressionNodeBuilder.buildNodeFromDividendAndDivisor(one, denominator, compiledConfiguration)
            return ExpressionNodeBuilder.buildNodeFromMultipliers(listOf(oneDividedByDenominator, nominator), compiledConfiguration)
        }
    }
    return node
}

fun logTransformation(prevTask: ExpressionTask, newTask: ExpressionTask, transformationName: String, stepId: Int) {
    if (prevTask.currentExpression.identifier != newTask.currentExpression.identifier) {
        val substitution = ExpressionSubstitution(
                left = prevTask.currentExpression,
                right = newTask.currentExpression,
                code = transformationName
        )

        newTask.expressionTaskIntermediateData.appliedPostprocessSubstitutions[substitution.code] = stepId
        newTask.solutionsStepTree.add(SolutionsStepITR(
                newTask.currentExpression.toString(),
                substitution,
                mutableListOf(),
                stepId,
                newTask.solutionsStepTree.lastOrNull()?.stepId ?: -1))
        newTask.previousExpressions.add(newTask.currentExpression.clone())
    }
}

fun getRidOfDuplicatedNodes(task: ExpressionTask, compiledConfiguration: CompiledConfiguration): ExpressionTask {
    var normalizedTask = task.clone()
    normalizedTask.currentExpression = traverseAndGetRidOfDuplicatedTerms(normalizedTask.currentExpression, compiledConfiguration)
    normalizedTask.currentExpression.identifier = normalizedTask.currentExpression.toString()

    normalizedTask = normalizedTask.clone()
    normalizedTask.currentExpression = traverseAndGetRidOfDuplicatedMultipliers(normalizedTask.currentExpression, compiledConfiguration)
    normalizedTask.currentExpression.identifier = normalizedTask.currentExpression.toString()

    return normalizedTask
}

fun traverseAndGetRidOfDuplicatedTerms(node: ExpressionNode, compiledConfiguration: CompiledConfiguration) : ExpressionNode {
    for (child in node.children) {
        traverseAndGetRidOfDuplicatedTerms(child, compiledConfiguration)
    }
    if (node.nodeType == NodeType.FUNCTION && node.value == "+") {
        val nodesMap: Map<String, List<ExpressionNode>> = node.children.groupBy { getKeyPartForTerm(it, compiledConfiguration).computeIdentifier() }
        if (nodesMap.all { it.value.size <= 1 }) {
            return node
        }
        val result = mutableListOf<ExpressionNode>()
        for ((id, nodes) in nodesMap) {
            val count = nodes
                    .map { getCountForTerm(it) }
                    .reduce { sum, x -> sum + x }
            if (count == 0) {
                continue
            }
            else if (count == 1) {
                val idNode = structureStringToExpression(id)
                result.add(if (idNode.children.size > 0) idNode.children[0] else idNode)
            } else if (count == -1) {
                val minusIdNode = structureStringToExpression("-($id)")
                result.add(if (minusIdNode.children.size > 0) minusIdNode.children[0] else minusIdNode)
            } else {
                val counterNode = ExpressionNodeBuilder.buildNodeFromConstant(count)
                val idNode = structureStringToExpression(id)
                val resultNode = if (idNode.children.size > 0) idNode.children[0] else idNode
                result.add(ExpressionNodeBuilder.buildNodeFromMultipliers(mutableListOf(counterNode, resultNode), compiledConfiguration))
            }
        }

        if (result.size == 0) {
            result.add(ExpressionNodeBuilder.buildNodeFromConstant(0))
            node.children = result
        }
        else if (result.size == 1) {
            val updatedNode = result[0]
            node.setNode(updatedNode)
        } else {
            node.children = result
        }
    }
    return node
}

fun traverseAndGetRidOfDuplicatedMultipliers(node: ExpressionNode, compiledConfiguration: CompiledConfiguration) : ExpressionNode {
    for (child in node.children) {
        traverseAndGetRidOfDuplicatedMultipliers(child, compiledConfiguration)
    }
    if (node.nodeType == NodeType.FUNCTION && node.value == "*") {
        val nodesMap: Map<String, List<ExpressionNode>> = node.children.groupBy { getKeyPartForMultiplier(it).identifier }
        if (nodesMap.all { it.value.size <= 1 }) {
            return node
        }
        val result = mutableListOf<ExpressionNode>()
        for ((id, nodes) in nodesMap) {
            if (nodes.all { it.isConstant() }) {
                result.addAll(nodes)
                continue
            }
            val degree = nodes
                    .map { getCountForMultiplier(it) }
                    .reduce { sum, x -> sum + x }
            if (degree == 1) {
                result.add(nodes[0])
            } else {
                val degreeExponent = ExpressionNodeBuilder.buildNodeFromConstant(degree)
                val idNode = structureStringToExpression(id)
                val degreeBase = if (idNode.children.size > 0) idNode.children[0] else idNode
                val exp = compiledConfiguration.createExpressionFunctionNode(
                        "^", -1,
                        children = listOf(degreeBase, degreeExponent)
                )
                result.add(exp)
            }
        }

        if (result.size == 1) {
            val updatedNode = result[0]
            node.children = updatedNode.children
            node.value = updatedNode.value
            node.identifier = updatedNode.identifier
            for (child in updatedNode.children) {
                child.parent = node
            }
        } else {
            node.children = result
        }
    }
    return node
}

fun getSign(node: ExpressionNode): Int {
    var countOfMinuses = 0
    var currentNode = node
    while ((currentNode.value == "" || currentNode.value == "+" || currentNode.value == "-")
            && currentNode.children.size == 1) {
        if (currentNode.value == "-") {
            countOfMinuses++
        }
        currentNode = currentNode.children[0]
    }
    return if (countOfMinuses % 2 == 0) 1 else -1
}

fun getCountForTerm(node: ExpressionNode): Int {
    val nodeWithSign = getModule(node)
    val nodeSign = getSign(node)
    if (nodeWithSign.nodeType == NodeType.FUNCTION && nodeWithSign.value == "*") {
        val numbers = retrieveNumbers(nodeWithSign.children)
        if (numbers.isEmpty()) {
            return 1
        }
        return nodeSign * computeProd(numbers)
    }
    return nodeSign
}

fun getCountForMultiplier(node: ExpressionNode): Int {
    if (node.nodeType == NodeType.FUNCTION && node.value == "^") {
        return computeSum(listOf(node.children[1]))
    }
    return 1
}

fun getKeyPartForTerm(node: ExpressionNode, compiledConfiguration: CompiledConfiguration) : ExpressionNode {
    val nodeWithoutSign = getModule(node)
    if (nodeWithoutSign.nodeType == NodeType.FUNCTION && nodeWithoutSign.value == "*") {
        val numbers = retrieveNumbers(nodeWithoutSign.children)
        val notNumbers = nodeWithoutSign.children
                .filter { !numbers.contains(it) }
                .toMutableList()
                .sortedWith(compareBy({!it.isConstant()}, {it.identifier}))
        if (notNumbers.size == 1) {
            return notNumbers[0]
        }
        return ExpressionNodeBuilder.buildNodeFromMultipliers(notNumbers, compiledConfiguration)
    }
    return nodeWithoutSign
}

fun getKeyPartForMultiplier(node: ExpressionNode) : ExpressionNode {
    if (node.nodeType == NodeType.FUNCTION && node.value == "^") {
        return node.children[0]
    }
    return node
}

fun getRidOfMultilayerFractions(task: ExpressionTask, compiledConfiguration: CompiledConfiguration): ExpressionTask {
    val normalizedTask = task.clone()
    normalizedTask.currentExpression = traverseAndLayFractions(normalizedTask.currentExpression, compiledConfiguration)
    normalizedTask.currentExpression.identifier = normalizedTask.currentExpression.toString()
    return normalizedTask
}

fun traverseAndLayFractions(node: ExpressionNode, compiledConfiguration: CompiledConfiguration): ExpressionNode {
    val updatedChildren = mutableListOf<ExpressionNode>()
    for (child in node.children) {
        val updatedChild = traverseAndLayFractions(child, compiledConfiguration)
        updatedChildren.add(updatedChild)
        updatedChild.parent = node
    }
    node.children = updatedChildren
    if (node.nodeType == NodeType.FUNCTION && node.value == "/") {
        val nominatorNodes = mutableListOf<ExpressionNode>()
        val denominatorNodes = mutableListOf<ExpressionNode>()

        layFractionRecursively(node, nominatorNodes, denominatorNodes)

        return ExpressionNodeBuilder.buildNodeFromDividendAndDivisor(
                nominator = ExpressionNodeBuilder.buildNodeFromMultipliers(nominatorNodes, compiledConfiguration),
                denominator = ExpressionNodeBuilder.buildNodeFromMultipliers(denominatorNodes, compiledConfiguration),
                compiledConfiguration = compiledConfiguration
        )
    }
    return node
}

fun layFractionRecursively(node: ExpressionNode,
                           nominatorNodes: MutableList<ExpressionNode>,
                           denominatorNodes: MutableList<ExpressionNode>,
                           isInsideNominator: Boolean = true) {
    if (node.nodeType == NodeType.FUNCTION && node.value == "/") {
        val children: List<ExpressionNode> = node.children
        layFractionRecursively(children[0], nominatorNodes, denominatorNodes, isInsideNominator)
        layFractionRecursively(children[1], nominatorNodes, denominatorNodes, !isInsideNominator)
    } else if (node.nodeType == NodeType.FUNCTION && (node.value == "*" || (node.value == "+" && node.children.size == 1) || node.value == "-")) {
        for (child in node.children) {
            layFractionRecursively(child, nominatorNodes, denominatorNodes, isInsideNominator)
        }
    } else {
        if (isInsideNominator) {
            nominatorNodes.add(node)
        } else {
            denominatorNodes.add(node)
        }
    }
}

fun dragDescendantsUpper(task: ExpressionTask, operation: String, compiledConfiguration: CompiledConfiguration): ExpressionTask {
    val normalizedTask = task.clone()
    normalizedTask.currentExpression = traverseAndDragDescendantsUpper(normalizedTask.currentExpression, operation, compiledConfiguration)
    normalizedTask.currentExpression.identifier = normalizedTask.currentExpression.toString()
    return normalizedTask
}

fun traverseAndDragDescendantsUpper(node: ExpressionNode, operation: String, compiledConfiguration: CompiledConfiguration): ExpressionNode {
    val updatedChildren = mutableListOf<ExpressionNode>()
    for (child in node.children) {
        val updatedChild = traverseAndDragDescendantsUpper(child, operation, compiledConfiguration)
        updatedChildren.add(updatedChild)
        updatedChild.parent = node
    }
    node.children = updatedChildren
    if (node.nodeType == NodeType.FUNCTION && node.value == operation) {
        val nodes: MutableList<ExpressionNode> = mutableListOf()
        collectNodesRecursively(node, operation, nodes)
        if (operation == "+") {
            return ExpressionNodeBuilder.buildNodeFromTerms(nodes, compiledConfiguration)
        } else if (operation == "*") {
            return ExpressionNodeBuilder.buildNodeFromMultipliers(nodes, compiledConfiguration)
        }
    }
    return node
}

fun collectNodesRecursively(node: ExpressionNode, operation: String, nodes: MutableList<ExpressionNode>) {
    if (node.nodeType == NodeType.FUNCTION && node.value == operation) {
        for (child in node.children) {
            collectNodesRecursively(child, operation, nodes)
        }
    } else {
        nodes.add(node)
    }
}

fun dragConstantsToTheLeft(task: ExpressionTask): ExpressionTask {
    val normalizedTask = task.clone()
    traverseAndSortChildrenAscendingByIdentifier(normalizedTask.currentExpression)
    normalizedTask.currentExpression.identifier = normalizedTask.currentExpression.toString()
    return normalizedTask
}

fun getRidOfLongFractionIfNeeded(task: ExpressionTask, compiledConfiguration: CompiledConfiguration): ExpressionTask {
    val normalizedTask = task.clone()
    normalizedTask.currentExpression = traverseAndGetRidOfLongFractionIfNeeded(normalizedTask.currentExpression, compiledConfiguration)
    normalizedTask.currentExpression.identifier = normalizedTask.currentExpression.toString()
    return normalizedTask
}

fun traverseAndSortChildrenAscendingByIdentifier(node: ExpressionNode) {
    for (child in node.children) {
        traverseAndSortChildrenAscendingByIdentifier(child)
    }
    if (node.nodeType == NodeType.FUNCTION && node.value == "*") {
        node.children.sortWith(compareBy({!it.isConstant()}, {getModule(it).identifier.length}, {it.identifier}))
    }
}

fun reduceFractions(task: ExpressionTask): ExpressionTask {
    val normalizedTask = task.clone()
    traverseAndReduceFractions(normalizedTask.currentExpression)
    normalizedTask.currentExpression.identifier = normalizedTask.currentExpression.toString()
    return normalizedTask
}

fun traverseAndReduceFractions(node: ExpressionNode) {
    for (child in node.children) {
        traverseAndReduceFractions(child)
    }
    if (node.nodeType == NodeType.FUNCTION && node.value == "/") {
        var nominatorNodes: MutableList<ExpressionNode> = getMultipliers(node.children[0])
        var denominatorNodes: MutableList<ExpressionNode> = getMultipliers(node.children[1])

        makeReduce(nominatorNodes, denominatorNodes)
        nominatorNodes = reduceOnes(nominatorNodes)
        denominatorNodes = reduceOnes(denominatorNodes)

        reduceIntegersInFraction(node, nominatorNodes, denominatorNodes)

        if (nominatorNodes.isEmpty()) {
            node.children[0] = ExpressionNodeBuilder.buildNodeFromConstant(value = 1, parent = node)
        } else if (nominatorNodes.size == 1) {
            node.children[0] = nominatorNodes[0]
        } else {
            node.children[0].children = nominatorNodes
        }

        if (denominatorNodes.isEmpty()) {
            node.children[1] = ExpressionNodeBuilder.buildNodeFromConstant(value = 1, parent = node)
        } else if (denominatorNodes.size == 1) {
            node.children[1] = denominatorNodes[0]
        } else {
            node.children[1].children = denominatorNodes
        }
    }
}

fun reduceIntegersInFraction(
        parentNode: ExpressionNode,
        nominatorNodes: MutableList<ExpressionNode>,
        denominatorNodes: MutableList<ExpressionNode>
) {
    val numbersInNominator = retrieveNumbers(nominatorNodes)
    val numbersInDenominator = retrieveNumbers(denominatorNodes)
    if (numbersInNominator.isEmpty() || numbersInDenominator.isEmpty()) {
        return
    }
    val nominatorProd = computeProd(numbersInNominator)
    if (nominatorProd == 0) {
        return
    }
    val denominatorProd = computeProd(numbersInDenominator)
    val gcd = findGCD(nominatorProd, denominatorProd)
    val nominatorResult = nominatorProd / gcd
    val denominatorResult = denominatorProd / gcd
    nominatorNodes.removeAll(numbersInNominator)
    denominatorNodes.removeAll(numbersInDenominator)
    nominatorNodes.add(0, ExpressionNodeBuilder.buildNodeFromConstant(value = nominatorResult, parent = parentNode))
    denominatorNodes.add(0, ExpressionNodeBuilder.buildNodeFromConstant(value = denominatorResult, parent = parentNode))
}

fun reduceOnes(nodes: MutableList<ExpressionNode>): MutableList<ExpressionNode> {
    if (nodes.size > 1) {
        return nodes.filter { it.identifier != "1" && it.identifier != "(1)" }.toMutableList()
    }
    return nodes
}

fun makeReduce(nominatorNodes: MutableList<ExpressionNode>,
               denominatorNodes: MutableList<ExpressionNode>) {
    val nominatorNodesCopy = nominatorNodes.toMutableList()
    val denominatorNodesCopy = denominatorNodes.toMutableList()
    for (node in nominatorNodesCopy) {
        val nodeToRemove = denominatorNodes.find { it.identifier == node.identifier }
        if (nodeToRemove != null) {
            denominatorNodes.remove(nodeToRemove)
        }
    }
    for (node in denominatorNodesCopy) {
        val nodeToRemove = nominatorNodes.find { it.identifier == node.identifier }
        if (nodeToRemove != null) {
            nominatorNodes.remove(nodeToRemove)
        }
    }
}

fun getMultipliers(node: ExpressionNode): MutableList<ExpressionNode> {
    if (node.nodeType == NodeType.FUNCTION && node.value == "*") {
        return node.children
    }
    return mutableListOf(node)
}

fun getModule(node: ExpressionNode): ExpressionNode {
    var currentNode = node
    while ((currentNode.value == "" || currentNode.value == "+" || currentNode.value == "-")
            && currentNode.children.size == 1) {
        currentNode = currentNode.children[0]
    }
    return currentNode
}

fun ExpressionNode.containsTrigonometricFunction() = this.containsFunctionBesides(ExpressionComparator.trigonometricAutoCheckingFunctionsSet)

fun ExpressionSubstitution.containsTrigonometricFunctionInResult() = this.right.containsTrigonometricFunction()

fun SubstitutionApplication.containsTrigonometricFunctionInResult() = this.expressionSubstitution.right.containsTrigonometricFunction()


fun ExpressionNode.insideExponent(expressionComparator: ExpressionComparator): Boolean {
    var parent = this.parent
    var prev = this
    while (parent != null) {
        if (parent.nodeType == NodeType.FUNCTION && parent.value == "^" && !expressionComparator.compareAsIs(parent.children[0], prev)) {
            return true
        }
        prev = parent
        parent = parent.parent
    }
    return false
}

fun ExpressionNode.insideTrigonometricFunction(): Boolean {
    var p = this.parent
    while (p != null) {
        if (p.nodeType == NodeType.FUNCTION && ExpressionComparator.trigonometricAutoCheckingFunctionsSet.contains(p.value)) {
            return true
        }
        p = p.parent
    }
    return false
}
