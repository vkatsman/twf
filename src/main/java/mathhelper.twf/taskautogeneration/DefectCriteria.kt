package mathhelper.twf.taskautogeneration

enum class DefectCriteria(val info: String) {
    SEVERAL_UNARY_MINUSES_IN_A_ROW("Несколько унарных минусов, идущих подряд"),
    SYNTAX_SUGAR("Синтаксический сахар: +0, *0, *1, /1, ^1"),
    OVERLAYERED_FRACTION("Избыточная этажность дроби"),
    UNCOMPUTED_EXPRESSION("Невычисленное арифметическое выражение"),
    NON_REDUCED_FRACTION("Несокращённая дробь"),
    NON_REDUCED_TERMS("Неприведённые слагаемые"),
    NON_REDUCED_MULTIPLIERS("Неприведённые множители"),
    REDUNDANT_BRACKETS_FOR_TERMS("Нераскрытые скобки при операции '+'"),
    REDUNDANT_BRACKETS_FOR_MULTIPLIERS("Нераскрытые скобки при операции '*'"),
    NOMINATOR_TO_DENOMINATOR_FRACTION_LIMIT_EXCEEDED("Символьная длина числителя больше символьной длины знаменателя в r раз"),
    NOT_LEFTED_CONSTANTS("Константы находятся не слева")
}
