package mathhelper.twf.api

import mathhelper.twf.config.RulePackITR
import mathhelper.twf.config.TaskITR
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.TexReportUtils.Companion.fullReport
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.TrigonometricRulePacks
import mathhelper.twf.logs.MessageType
import mathhelper.twf.logs.log
import mathhelper.twf.platformdependent.JsonParser
import mathhelper.twf.taskautogeneration.*


fun generateTasks(area: String,
                  startExpression: String,
                  rulepacks: Array<RulePackITR> = arrayOf(),
                  additionalParamsJsonString: String = "{}"): Array<TaskITR> {

    log.clear()

    if (area != "(Trigonometry)") {
        return arrayOf()
    }
    val parsedAdditionalParams = JsonParser.parseMap(additionalParamsJsonString)

    val tags: Array<RuleTag> = if (parsedAdditionalParams["tags"] == null) {
        RuleTag.values()
    } else {
        val tagsChosen = (parsedAdditionalParams["tags"] as List<String>)
                .map { RuleTag.valueOf(it) }
                .toMutableList()
        if (!tagsChosen.contains(RuleTag.BASIC_MATH)) {
            tagsChosen.add(RuleTag.BASIC_MATH)
        }
        tagsChosen.toTypedArray()
    }

    val settings = GeneratorSettings(
            targetWeight = mapTargetWeight((parsedAdditionalParams["complexity"] as String).toDouble()),
            maxCountSelectedOfTasksOnIteration = mapTargetGenerationDepth((parsedAdditionalParams["depth"] as String).toDouble()),
            expressionSubstitutions = ExpressionUtils.toExpressionSubstitutions(
                    rulepacks.ifEmpty { getDefaultRulePacks() }.toList(),
                    tags
            ),
            taskStartGenerator = { ExpressionUtils.structureStringToGeneratedExpression(startExpression) },
            sortType = SortType.valueOf(parsedAdditionalParams["sort"] as String),
            sortOrder = SortOrder.valueOf(parsedAdditionalParams["sortOrder"] as String)
    )

    val tasks = generateTrigonometricTasks(settings)

    log.addMessage({ fullReport(tasks) }, MessageType.USER)
    return tasks.toTypedArray()
}

fun getLogOfGeneration() = log.getLogInPlainText(messageType = MessageType.TECHNICAL)

fun getReportOfGeneration() = log.getLogInPlainText(messageType = MessageType.USER)

fun getAllTagsForGeneration(area: String): Array<RuleTag> {
    if (area != "(Trigonometry)") {
        return arrayOf()
    }
    return TrigonometricRulePacks.get()
            .flatMap { it.rules ?: emptyList() }
            .flatMap { it.tagsForTaskGenerator }
            .distinct()
            .filter { it.readyForUseInProduction }
            .filter { it != RuleTag.BASIC_MATH }
            .toTypedArray()
}

fun getAllSortTypesForGeneration(): Array<SortType> {
    return SortType.values()
}

fun getAllSortOrdersForGeneration(): Array<SortOrder> {
    return SortOrder.values()
}
