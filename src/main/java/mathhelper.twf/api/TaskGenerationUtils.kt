package mathhelper.twf.api

import mathhelper.twf.config.RulePackITR
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.RulePackProvider

fun getDefaultRulePacks(): Array<RulePackITR> = RulePackProvider.getBasicTrigonometricSubstitutions().toTypedArray()

fun mapTargetWeight(complexity: Double, minTargetWeight: Int = 1, maxTargetWeight: Int = 8): Double {
    return minTargetWeight + (complexity * (maxTargetWeight - minTargetWeight))
}

fun mapTargetGenerationDepth(depth: Double, minDepth: Int = 2, maxDepth: Int = 28): Int {
    return (minDepth + (depth * (maxDepth - minDepth))).toInt()
}
