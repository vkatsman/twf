package mathhelper.twf.api

import mathhelper.twf.config.*
import mathhelper.twf.defaultcontent.ExpressionFrontInput
import mathhelper.twf.defaultcontent.RuleFrontInput
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks
import mathhelper.twf.defaultcontent.toStructureStructureString
import mathhelper.twf.expressiontree.ExpressionSubstitution
import mathhelper.twf.expressiontree.NodeType
import mathhelper.twf.factstransformations.FactConstructorViewer
import mathhelper.twf.logs.log
import mathhelper.twf.mainpoints.*
import mathhelper.twf.platformdependent.JsonParser

fun createExpressionFrontInput(expression: String, format: String) = ExpressionFrontInput(expression, format)

fun createRuleITR(
        code: String? = null,
        nameEn: String? = null,
        nameRu: String? = null,
        descriptionShortEn: String? = null,
        descriptionShortRu: String? = null,
        descriptionEn: String? = null,
        descriptionRu: String? = null,

        left: ExpressionFrontInput? = null,
        right: ExpressionFrontInput? = null,
        priority: Int? = null,
        isExtending: Boolean? = null,
        matchJumbledAndNested: Boolean? = null,
        simpleAdditional: Boolean? = null,
        basedOnTaskContext: Boolean? = null,
        normalizationType: String? = null,
        weight: Double? = null,
        subjectType: String? = null
) = RuleITR(
        code, nameEn, nameRu, descriptionShortEn, descriptionShortRu, descriptionEn, descriptionRu,
        left?.toStructureStructureString(subjectType ?: "") ?: "",
        right?.toStructureStructureString(subjectType ?: "") ?: "",
        priority, isExtending, matchJumbledAndNested, simpleAdditional, basedOnTaskContext, normalizationType, weight
)

fun createRulePackLinkITR(
        namespaceCode: String? = null,
        rulePackCode: String? = null
) = RulePackLinkITR(
        namespaceCode, rulePackCode
)

fun createRulePackITR(
        code: String? = null,
        version: Int = 0,
        namespaceCode: String? = null,
        nameEn: String? = null,
        nameRu: String? = null,
        descriptionShortEn: String? = null,
        descriptionShortRu: String? = null,
        descriptionEn: String? = null,
        descriptionRu: String? = null,

        subjectType: String = "standard_math",
        rulePacks: List<RulePackLinkITR>? = null,
        rules: List<RuleITR>? = null,

        otherCheckSolutionData: String? = null, //parameters in task redefine parameters in this map
        otherAutoGenerationData: String? = null,
        otherData: String? = null
) = RulePackITR(
        code, version, namespaceCode, nameEn, nameRu, descriptionShortEn, descriptionShortRu, descriptionEn, descriptionRu,
        subjectType, rulePacks, rules,

        otherCheckSolutionData = if (otherCheckSolutionData != null) createOtherCheckSolutionData(otherCheckSolutionData) as Map<String, Any> else null,
        otherAutoGenerationData = if (otherAutoGenerationData != null) JsonParser.parseMap(otherAutoGenerationData) as Map<String, Any> else null,
        otherData = if (otherData != null) JsonParser.parseMap(otherData) as Map<String, Any> else null
)

fun createFunctionIdentifiersList (functionsList: List<Map<String, Any>>): List<FunctionIdentifierITR> {
    val functionIdentifiersList = mutableListOf<FunctionIdentifierITR>()
    for (function in functionsList) {
        val functionMap = function as Map<String, Any>
        val name = if (functionMap["name"] is String) functionMap["name"] as String else null
        val numberOfArguments = if (functionMap["numberOfArguments"] is String) functionMap["numberOfArguments"] as String else null
        val needRules = if (functionMap["needRules"] is String && functionMap["needRules"] != "false" && functionMap["needRules"] != "0" && functionMap["needRules"] != "") "1" else ""
        if (name != null && numberOfArguments != null) {
            functionIdentifiersList.add(FunctionIdentifierITR(name, numberOfArguments, needRules))
        }
    }
    return functionIdentifiersList
}

fun createOtherCheckSolutionData(otherCheckSolutionData: String): Map<String, Any>? {
    val jsonParsed = JsonParser.parseMap(otherCheckSolutionData) as Map<String, Any>
    if (jsonParsed["otherCheckSolutionData"] != null) {
        val jsonMap = jsonParsed["otherCheckSolutionData"] as Map<String, Any>
        val res = mutableMapOf<String, Any>()
        if (jsonMap[notChangesOnVariablesInComparisonFunctionJsonName] is List<*>) {
            res.put(notChangesOnVariablesInComparisonFunctionJsonName,
                    createFunctionIdentifiersList(jsonMap[notChangesOnVariablesInComparisonFunctionJsonName] as List<Map<String, Any>>))
        }
        if (jsonMap[notChangesOnVariablesInComparisonFunctionWithoutTransformationsJsonName] is List<*>) {
            res.put(notChangesOnVariablesInComparisonFunctionWithoutTransformationsJsonName,
                    createFunctionIdentifiersList(jsonMap[notChangesOnVariablesInComparisonFunctionWithoutTransformationsJsonName] as List<Map<String, Any>>))
        }
        return res
    }
    return null
}

fun createTaskITR(
        taskCreationType: String = "manual",

        code: String? = null,
        namespaceCode: String? = null,
        nameEn: String? = null,
        nameRu: String? = null,
        descriptionShortEn: String? = null,
        descriptionShortRu: String? = null,
        descriptionEn: String? = null,
        descriptionRu: String? = null,

        subjectType: String? = null,
        tags: Array<String>? = null,

        originalExpression: ExpressionFrontInput? = null,

        goalType: String? = null,
        goalExpression: ExpressionFrontInput? = null,
        goalPattern: String? = null,
        goalNumberProperty: Int? = null,
        otherGoalData: String? = null,

        rulePacks: Array<RulePackLinkITR>? = null,
        rules: Array<RuleITR>? = null,

        stepsNumber: Int? = null,
        time: Int? = null,
        difficulty: Double,

        solution: ExpressionFrontInput? = null,
        solutionsStepsTree: String? = null,

        interestingFacts: String? = null,
        nextRecommendedTasks: String? = null,

        hints: String? = null,
        otherCheckSolutionData: String? = null,

        countOfAutoGeneratedTasks: Int? = 0,
        otherAutoGenerationData: String? = null,

        otherAwardData: String? = null,
        otherData: String? = null
) = TaskITR(
        taskCreationType ?: "manual", code, 1, namespaceCode,
        nameEn, nameRu, descriptionShortEn, descriptionShortRu, descriptionEn, descriptionRu, subjectType,
        tags?.toMutableSet() ?: mutableSetOf<String>(),
        originalExpressionStructureString = originalExpression?.toStructureStructureString(subjectType ?: "") ?: "",

        goalType = goalType,
        goalExpressionStructureString = goalExpression?.toStructureStructureString(subjectType ?: "") ?: "",
        goalPattern = goalPattern,
        goalNumberProperty = goalNumberProperty,
        otherGoalData = if (otherGoalData != null) JsonParser.parseMap(otherGoalData) as Map<String, Any> else null,

        rulePacks = rulePacks?.toList(),
        rules = rules?.toList(),

        stepsNumber = stepsNumber,
        time = time,
        difficulty = difficulty,

        solutionPlainText = solution?.toStructureStructureString(subjectType ?: "") ?: "",
        solutionsStepsTree = if (solutionsStepsTree != null) JsonParser.parseMap(solutionsStepsTree) as Map<String, Any> else null,
        interestingFacts = if (interestingFacts != null) JsonParser.parseMap(interestingFacts) as Map<String, Any> else null,
        nextRecommendedTasks = if (nextRecommendedTasks != null) JsonParser.parseMap(nextRecommendedTasks) as Map<String, Any> else null,
        hints = if (hints != null) JsonParser.parseMap(hints) as Map<String, Any> else null,
        otherCheckSolutionData = if (otherCheckSolutionData != null) createOtherCheckSolutionData(otherCheckSolutionData) else null,
        countOfAutoGeneratedTasks = countOfAutoGeneratedTasks,
        otherAutoGenerationData = if (otherAutoGenerationData != null) JsonParser.parseMap(otherAutoGenerationData) as Map<String, Any> else null,
        otherAwardData = if (otherAwardData != null) JsonParser.parseMap(otherAwardData) as Map<String, Any> else null,
        otherData = if (otherData != null) JsonParser.parseMap(otherData) as Map<String, Any> else null
)

fun createCompiledConfigurationFromITR(
        taskITR: TaskITR? = null, //solving task, if null, actualRulePackCodes will be used
        rulePacksITR: Array<RulePackITR>? = null, //corresponding rule packs
        comparisonSettings: ComparisonSettings = ComparisonSettings(),
        actualRulePackCodes: Array<String>? = null
): CompiledConfiguration = internalCreateCompiledConfigurationFromITR(
        taskITR, actualRulePackCodes,
        rulePacksITR?.filter { it.code != null }?.associateBy { it.code!! } ?: DefaultRulePacks.map(),
        comparisonSettings
)

fun internalCreateCompiledConfigurationFromITR(
        taskITR: TaskITR? = null, //solving task, if null, actualRulePackCodes will be used
        actualRulePackCodes: Array<String>? = null,
        rulePacksMap: Map<String, RulePackITR> = DefaultRulePacks.map(), //corresponding rule packs
        comparisonSettings: ComparisonSettings = ComparisonSettings()
): CompiledConfiguration {

    log.addMessage({ "build verification settings" }, level = 0)

    val scopeFilter = mutableSetOf<String>()
    var subjectType = taskITR?.subjectType
    if (taskITR != null) {
        if (taskITR!!.subjectType in listOf("set", "logic")) {
            log.addMessage({ "'setTheory' is added to scopeFilter (got from task)" }, level = 1)
            scopeFilter.add("setTheory")
        }
    }

    val expressionSubstitutions = mutableListOf<ExpressionSubstitution>()
    taskITR?.rules?.forEach {
        expressionSubstitutions.add(expressionSubstitutionFromRuleITR(it))
    }
    log.addMessage({ "rulePacksMap: ${rulePacksMap.values.joinToString { "${it.code!!} : ${it.rules?.size} rules : ${it.rulePacks?.size} rulePacks" }}" }, level = 1)
    log.addMessage({ "taskRulePacks: ${taskITR?.rulePacks?.joinToString { "${it.rulePackCode!!}" }}" }, level = 1)
    val allRulePackCodes = mutableSetOf<String>()
    taskITR?.rulePacks?.forEach {
        val rulePack = rulePacksMap[it.rulePackCode]
        if (rulePack != null) {
            allRulePackCodes.addAll(getRulePackCodesFromTree(rulePack, rulePacksMap))
        }
    }
    actualRulePackCodes?.forEach { rulePackCode ->
        val rulePack = rulePacksMap[rulePackCode]
        if (rulePack != null) {
            allRulePackCodes.addAll(getRulePackCodesFromTree(rulePack, rulePacksMap))
        } else {
            log.addMessage({ "ERROR: rule pack '$rulePackCode' not found" }, level = 0)
        }
    }

    //collect all well-known functions and operations from all RulePacks
    val notChangesOnVariablesInComparisonFunctionSet = mutableSetOf<FunctionIdentifier>()
    val notChangesOnVariablesInComparisonFunctionWithoutTransformationsSet = mutableSetOf<FunctionIdentifier>()
    allRulePackCodes.forEach {
        val rulePack = rulePacksMap[it]
        if (rulePack != null) {
            rulePack.rules?.forEach {
                expressionSubstitutions.add(expressionSubstitutionFromRuleITR(it))
            }

            if (rulePack.otherCheckSolutionData != null && rulePack.otherCheckSolutionData[notChangesOnVariablesInComparisonFunctionJsonName] is List<*>) {
                notChangesOnVariablesInComparisonFunctionSet.addAll((rulePack.otherCheckSolutionData[notChangesOnVariablesInComparisonFunctionJsonName] as List<FunctionIdentifierITR>).map {it.toFunctionIdentifier()})
            }
            if (rulePack.otherCheckSolutionData != null && rulePack.otherCheckSolutionData[notChangesOnVariablesInComparisonFunctionWithoutTransformationsJsonName] is List<*>) {
                notChangesOnVariablesInComparisonFunctionWithoutTransformationsSet.addAll((rulePack.otherCheckSolutionData[notChangesOnVariablesInComparisonFunctionWithoutTransformationsJsonName] as List<FunctionIdentifierITR>).map {it.toFunctionIdentifier()})
            }

            if (taskITR == null) {
                if (subjectType == null) {
                    subjectType = rulePack.subjectType
                }
                if(rulePack.subjectType in listOf("set", "logic")) {
                    log.addMessage({ "'setTheory' is added to scopeFilter (got from rulePack '${rulePack.code}')" }, level = 1)
                    scopeFilter.add("setTheory")
                }
            }
        }
    }

    val functionConfiguration = FunctionConfiguration(scopeFilter)
    if (expressionSubstitutions.isNotEmpty() == true) {
        functionConfiguration.treeTransformationRules = mutableListOf()
        functionConfiguration.taskContextTreeTransformationRules = mutableListOf()
        log.addMessage({ "functionConfiguration rules cleaned" }, level = 1)
    }

    functionConfiguration.notChangesOnVariablesInComparisonFunction = notChangesOnVariablesInComparisonFunctionSet.toList()
    functionConfiguration.notChangesOnVariablesInComparisonFunctionWithoutTransformations = notChangesOnVariablesInComparisonFunctionWithoutTransformationsSet.toList()

    if (taskITR?.otherCheckSolutionData != null && taskITR.otherCheckSolutionData[notChangesOnVariablesInComparisonFunctionJsonName] is List<*>) {
        functionConfiguration.notChangesOnVariablesInComparisonFunction = (taskITR.otherCheckSolutionData[notChangesOnVariablesInComparisonFunctionJsonName] as List<FunctionIdentifierITR>).map {it.toFunctionIdentifier()}
    }
    if (taskITR?.otherCheckSolutionData != null && taskITR.otherCheckSolutionData[notChangesOnVariablesInComparisonFunctionWithoutTransformationsJsonName] is List<*>) {
        functionConfiguration.notChangesOnVariablesInComparisonFunctionWithoutTransformations = (taskITR.otherCheckSolutionData[notChangesOnVariablesInComparisonFunctionWithoutTransformationsJsonName] as List<FunctionIdentifierITR>).map {it.toFunctionIdentifier()}
    }

    log.addMessage({ "notChangesOnVariablesInComparisonFunction: ${functionConfiguration.notChangesOnVariablesInComparisonFunction.joinToString { it.getIdentifier() }}" }, level = 1)
    log.addMessage({ "notChangesOnVariablesInComparisonFunctionWithoutTransformations: ${functionConfiguration.notChangesOnVariablesInComparisonFunctionWithoutTransformations.joinToString { it.getIdentifier() }}" }, level = 1)

    val compiledConfiguration = CompiledConfiguration(
            functionConfiguration = functionConfiguration,
            subjectType = subjectType ?: "",
            comparisonSettings = comparisonSettings
    )

    if (expressionSubstitutions.isNotEmpty()) {
        compiledConfiguration.setExpressionSubstitutions(expressionSubstitutions)
    }

    allRulePackCodes.forEach {
        val rulePack = rulePacksMap[it]
        if (rulePack != null) {
            compiledConfiguration.setInfoFromAdditionalParams(rulePack.otherCheckSolutionData)
        }
    }
    if (taskITR?.otherCheckSolutionData != null) {
        compiledConfiguration.setInfoFromAdditionalParams(taskITR.otherCheckSolutionData)
    }

    return compiledConfiguration
}


fun checkSolutionInTexITR(
        originalTexSolution: String, //string with learner solution in Tex format
        taskITR: TaskITR, //solving task
        rulePacksITR: Array<RulePackITR>, //corresponding rule packs
        shortErrorDescription: String = "0", //make error message shorter and easier to understand: crop parsed steps from error description
        skipTrivialCheck: Boolean = false, //do not check completeness of transformations, only correctness,
        comparisonSettings: ComparisonSettings = ComparisonSettings(),
        inputCompiledConfiguration: CompiledConfiguration? = null // can be specified to make verification process faster
): TexVerificationResult {
    log.clear()

    val finalCompiledConfiguration = inputCompiledConfiguration ?: createCompiledConfigurationFromITR(taskITR, rulePacksITR, comparisonSettings)

    return checkFactsInTex(
            originalTexSolution,
            taskITR.originalExpressionStructureString ?: "",
            taskITR.goalExpressionStructureString ?: "",
            "",
            taskITR.goalPattern ?: "",
            "", // TODO: support inequalities in tasks
            "",
            shortErrorDescription = shortErrorDescription,
            skipTrivialCheck = skipTrivialCheck,
            compiledConfiguration = finalCompiledConfiguration,
            otherGoalData = taskITR.otherGoalData)
}


fun checkSolutionInTex(
        originalTexSolution: String, //string with learner solution in Tex format

        //// individual task parameters:
        startExpressionIdentifier: String = "", //Expression, from which learner need to start the transformations in structure string
        targetFactPattern: String = "", //Pattern that specify criteria that learner's answer must meet
        additionalFactsIdentifiers: String = "", ///Identifiers split by configSeparator - task condition facts should be here, that can be used as rules only for this task

        endExpressionIdentifier: String = "", //Expression, which learner need to deduce
        targetFactIdentifier: String = "", //Fact that learner need to deduce. It is more flexible than startExpressionIdentifier and endExpressionIdentifier, allow to specify inequality like '''EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{<=}{(/(2;sin(x)))}'''
        comparisonSign: String = "", //Comparison sign

        //// general configuration parameters
        //functions, which null-weight transformations allowed (if no other transformations), split by configSeparator
        //choose one of 2 api forms:
        wellKnownFunctions: List<FunctionIdentifier> = listOf(),
        wellKnownFunctionsString: String = "", //"${configSeparator}0$configSeparator${configSeparator}1$configSeparator+$configSeparator-1$configSeparator-$configSeparator-1$configSeparator*$configSeparator-1$configSeparator/$configSeparator-1$configSeparator^$configSeparator-1",

        //functions, which null-weight transformations allowed with any other transformations, split by configSeparator
        //choose one of 2 api forms:
        unlimitedWellKnownFunctions: List<FunctionIdentifier> = wellKnownFunctions,
        unlimitedWellKnownFunctionsString: String = wellKnownFunctionsString,

        //expression transformation rules
        //choose one of api forms:
        expressionTransformationRules: List<ExpressionSubstitution> = listOf(), //full list of expression transformations rules
        expressionTransformationRulesString: String = "", //""S(i, a, a, f(i))${configSeparator}f(a)${configSeparator}S(i, a, b, f(i))${configSeparator}S(i, a, b-1, f(i)) + f(b)", //function transformation rules, parts split by configSeparator; if it equals " " then expressions will be checked only by testing
        taskContextExpressionTransformationRules: String = "", //for expression transformation rules based on variables
        rulePacks: Array<String> = listOf<String>().toTypedArray(),

        maxExpressionTransformationWeight: String = "1.0",
        maxDistBetweenDiffSteps: String = "", //is it allowed to differentiate expression in one step
        scopeFilter: String = "", //subject scopes which user representation sings is used

        shortErrorDescription: String = "0", //make error message shorter and easier to understand: crop parsed steps from error description
        skipTrivialCheck: Boolean = false, //do not check completeness of transformations, only correctness
        otherGoalData: Map<String, Any>? = null, // may contains key="hiddenGoalExpressions" with list of possible goal expression values in structure string
        subjectType: String = ""
): TexVerificationResult {
    log.clear()
    val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(
            rulePacks,
            wellKnownFunctionsString,
            expressionTransformationRulesString,
            maxExpressionTransformationWeight,
            unlimitedWellKnownFunctionsString,
            taskContextExpressionTransformationRules,
            maxDistBetweenDiffSteps,
            scopeFilter,

            wellKnownFunctions,
            unlimitedWellKnownFunctions,
            expressionTransformationRules,
            subjectType)
    return checkFactsInTex(
            originalTexSolution,
            startExpressionIdentifier,
            endExpressionIdentifier,
            targetFactIdentifier,
            targetFactPattern,
            comparisonSign,
            additionalFactsIdentifiers,
            shortErrorDescription,
            skipTrivialCheck,
            compiledConfiguration,
            otherGoalData)
}


fun checkSolutionInTexWithCompiledConfiguration(
        originalTexSolution: String, //string with learner solution in Tex format
        compiledConfiguration: CompiledConfiguration,

        //// individual task parameters:
        startExpressionIdentifier: String = "", //Expression, from which learner need to start the transformations
        targetFactPattern: String = "", //Pattern that specify criteria that learner's answer must meet
        comparisonSign: String = "", //Comparison sign
        additionalFactsIdentifiers: String = "", ///Identifiers split by configSeparator - task condition facts should be here, that can be used as rules only for this task

        endExpressionIdentifier: String = "", //Expression, which learner need to deduce
        targetFactIdentifier: String = "", //Fact that learner need to deduce. It is more flexible than startExpressionIdentifier and endExpressionIdentifier, allow to specify inequality like '''EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{<=}{(/(2;sin(x)))}'''

        shortErrorDescription: String = "0", //make error message shorter and easier to understand: crop parsed steps from error description
        skipTrivialCheck: Boolean = false, //do not check completeness of transformations, only correctness
        otherGoalData: Map<String, Any>? = null // may contains key="hiddenGoalExpressions" with list of possible goal expression values in structure string
): TexVerificationResult {
    log.clear()
    return checkFactsInTex(
            originalTexSolution,
            startExpressionIdentifier,
            endExpressionIdentifier,
            targetFactIdentifier,
            targetFactPattern,
            comparisonSign,
            additionalFactsIdentifiers,
            shortErrorDescription,
            skipTrivialCheck,
            compiledConfiguration,
            otherGoalData)
}

/**
 * check with maximum possible transformation complexity
 */
fun checkChainCorrectnessInTex(originalTexSolution: String, subjectType: String = "") = checkSolutionInTex(
        originalTexSolution = originalTexSolution,
        startExpressionIdentifier = "",
        targetFactPattern = "",
        additionalFactsIdentifiers = "",
        endExpressionIdentifier = "",
        targetFactIdentifier = "",
        comparisonSign = "",
        wellKnownFunctions = listOf(),
        wellKnownFunctionsString = "",
        unlimitedWellKnownFunctions = listOf(),
        unlimitedWellKnownFunctionsString = "",
        expressionTransformationRules = listOf(),
        expressionTransformationRulesString = "",
        taskContextExpressionTransformationRules = "",
        rulePacks = arrayOf(),
        maxExpressionTransformationWeight = "",
        maxDistBetweenDiffSteps = "",
        scopeFilter = "",
        shortErrorDescription = "",
        skipTrivialCheck = true,
        otherGoalData = null,
        subjectType = subjectType
)