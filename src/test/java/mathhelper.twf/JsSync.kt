package mathhelper.twf

import org.junit.Ignore
import org.junit.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

class JsSync {
    @Test
    @Ignore
    fun sync () {
        syncSrc ()
    }

    fun syncSrc () {
        val javaMathhelpertwfRoot = File("./src/main/java/mathhelper.twf/")
        for (file in javaMathhelpertwfRoot.listFiles()) {
            if (file.name != "platformdependent") {
                file.copyRecursively(File("./../twf-js/src/mathhelper.twf/${file.name}/"), true)
            }
        }
    }
}