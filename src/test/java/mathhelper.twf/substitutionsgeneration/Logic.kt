package mathhelper.twf.substitutionsgeneration

import mathhelper.twf.api.*
import mathhelper.twf.expressiontree.*
import mathhelper.twf.substitutiontests.parseStringExpression
import kotlin.test.assertEquals
import org.junit.Test

class Logic {
    val generalCompiledConfiguration = createCompiledConfigurationFromITR(actualRulePackCodes = arrayOf("LogicZhegalkinNandNor"))
    val compiledConfigurationNoXorDistribution = createCompiledConfigurationFromITR(actualRulePackCodes = arrayOf("LogicBase"))
    val compiledConfigurationAbsorbtionLaw = createCompiledConfigurationFromITR(actualRulePackCodes = arrayOf("LogicAbsorptionLaw", "RelativeComplement"))

    @Test
    fun AndOrDistributionReduce() {
        val expression = structureStringToExpression("(implic(and(or(a;b);or(a;c);or(a;d);or(a;e));a))")
        assertEquals("  :  [0]\n" +
                "  implic  :  [1]\n" +
                "    and  :  [2]\n" +
                "      or  :  [3]\n" +
                "        a  :  [4]\n" +
                "        b  :  [5]\n" +
                "      or  :  [6]\n" +
                "        a  :  [7]\n" +
                "        c  :  [8]\n" +
                "      or  :  [9]\n" +
                "        a  :  [10]\n" +
                "        d  :  [11]\n" +
                "      or  :  [12]\n" +
                "        a  :  [13]\n" +
                "        e  :  [14]\n" +
                "    a  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,6,12).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, compiledConfigurationNoXorDistribution)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("and(or(a;b);or(a;c);or(a;e))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("and(or();or();or())", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("and(or(a;b);or(a;c);or(a;e))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("and(or(a;b);or(a;c);or(a;e))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("and(place_for_result();or(a;d))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("and(or(a;d))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, compiledConfigurationNoXorDistribution, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '((a|(b&c&e))&(a|d))->a'\n" +
                "expressionSubstitution.left: '(and(or(a;b);or(a;c);or(a;e)))'\n" +
                "expressionSubstitution.right: '(or(a;and(b;c;e)))'\n" +
                "originalExpression: '(implic(and(or(a;b);or(a;c);or(a;d);or(a;e));a))'\n" +
                "originalExpressionChangingPart: 'and(or(a;b);or(a;c);or(a;e))'\n" +
                "resultExpression: '(implic(and(or(a;and(b;c;e));or(a;d));a))'\n" +
                "resultExpressionChangingPart: 'or(a;and(b;c;e))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '10'\n" +
                "\n" +
                "result: '(((a|b)&(a|c)&(a|e))&(a|d))->a'\n" +
                "expressionSubstitution.left: '(and(or(a;b);or(a;c);or(a;e)))'\n" +
                "expressionSubstitution.right: '(and(and(or(a;b);or(a;c);or(a;e))))'\n" +
                "originalExpression: '(implic(and(or(a;b);or(a;c);or(a;d);or(a;e));a))'\n" +
                "originalExpressionChangingPart: 'and(or(a;b);or(a;c);or(a;e))'\n" +
                "resultExpression: '(implic(and(and(or(a;b);or(a;c);or(a;e));or(a;d));a))'\n" +
                "resultExpressionChangingPart: 'and(and(or(a;b);or(a;c);or(a;e)))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '40'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun AndXorDistributionReduce() { //no distribution should be  (a^b)&(a^c) != a^(b&c)
        val expression = structureStringToExpression("(implic(and(xor(a;b);xor(a;c);xor(a;d);xor(a;e));a))")
        assertEquals("  :  [0]\n" +
                "  implic  :  [1]\n" +
                "    and  :  [2]\n" +
                "      xor  :  [3]\n" +
                "        a  :  [4]\n" +
                "        b  :  [5]\n" +
                "      xor  :  [6]\n" +
                "        a  :  [7]\n" +
                "        c  :  [8]\n" +
                "      xor  :  [9]\n" +
                "        a  :  [10]\n" +
                "        d  :  [11]\n" +
                "      xor  :  [12]\n" +
                "        a  :  [13]\n" +
                "        e  :  [14]\n" +
                "    a  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,6,12).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("and(xor(a;b);xor(a;c);xor(a;e))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("and(xor();xor();xor())", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("and(xor(a;b);xor(a;c);xor(a;e))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("and(xor(a;b);xor(a;c);xor(a;e))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("and(place_for_result();xor(a;d))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("and(xor(a;d))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '(((a^b)&(a^c)&(a^e))&(a^d))->a'\n" +
                "expressionSubstitution.left: '(and(xor(a;b);xor(a;c);xor(a;e)))'\n" +
                "expressionSubstitution.right: '(and(and(xor(a;b);xor(a;c);xor(a;e))))'\n" +
                "originalExpression: '(implic(and(xor(a;b);xor(a;c);xor(a;d);xor(a;e));a))'\n" +
                "originalExpressionChangingPart: 'and(xor(a;b);xor(a;c);xor(a;e))'\n" +
                "resultExpression: '(implic(and(and(xor(a;b);xor(a;c);xor(a;e));xor(a;d));a))'\n" +
                "resultExpressionChangingPart: 'and(and(xor(a;b);xor(a;c);xor(a;e)))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '40'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun xorAndDistributionReduce() { // distribution should be  (a&b)^(a&c) != a&(b^c)
        val expression = structureStringToExpression("(implic(xor(and(a;b);and(a;c);and(a;d);and(a;e));a))")
        assertEquals("  :  [0]\n" +
                "  implic  :  [1]\n" +
                "    xor  :  [2]\n" +
                "      and  :  [3]\n" +
                "        a  :  [4]\n" +
                "        b  :  [5]\n" +
                "      and  :  [6]\n" +
                "        a  :  [7]\n" +
                "        c  :  [8]\n" +
                "      and  :  [9]\n" +
                "        a  :  [10]\n" +
                "        d  :  [11]\n" +
                "      and  :  [12]\n" +
                "        a  :  [13]\n" +
                "        e  :  [14]\n" +
                "    a  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,6,12).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("xor(and(a;b);and(a;c);and(a;e))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("xor(and();and();and())", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("xor(and(a;b);and(a;c);and(a;e))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("xor(and(a;b);and(a;c);and(a;e))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("xor(place_for_result();and(a;d))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("xor(and(a;d))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '((a&(b^c^e))^(a&d))->a'\n" +
                "expressionSubstitution.left: '(xor(and(a;b);and(a;c);and(a;e)))'\n" +
                "expressionSubstitution.right: '(and(a;xor(b;c;e)))'\n" +
                "originalExpression: '(implic(xor(and(a;b);and(a;c);and(a;d);and(a;e));a))'\n" +
                "originalExpressionChangingPart: 'xor(and(a;b);and(a;c);and(a;e))'\n" +
                "resultExpression: '(implic(xor(and(a;xor(b;c;e));and(a;d));a))'\n" +
                "resultExpressionChangingPart: 'and(a;xor(b;c;e))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '10'\n" +
                "\n" +
                "result: '(((a&b)^(a&c)^(a&e))^(a&d))->a'\n" +
                "expressionSubstitution.left: '(xor(and(a;b);and(a;c);and(a;e)))'\n" +
                "expressionSubstitution.right: '(xor(xor(and(a;b);and(a;c);and(a;e))))'\n" +
                "originalExpression: '(implic(xor(and(a;b);and(a;c);and(a;d);and(a;e));a))'\n" +
                "originalExpressionChangingPart: 'xor(and(a;b);and(a;c);and(a;e))'\n" +
                "resultExpression: '(implic(xor(xor(and(a;b);and(a;c);and(a;e));and(a;d));a))'\n" +
                "resultExpressionChangingPart: 'xor(xor(and(a;b);and(a;c);and(a;e)))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '40'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun xorOrDistributionReduceNoXorDistribution() { // distribution should be  (a|b)^(a|c) == a|(b^c)
        val expression = structureStringToExpression("(implic(xor(or(a;b);or(a;c);or(a;d);or(a;e));a))")
        assertEquals("  :  [0]\n" +
                "  implic  :  [1]\n" +
                "    xor  :  [2]\n" +
                "      or  :  [3]\n" +
                "        a  :  [4]\n" +
                "        b  :  [5]\n" +
                "      or  :  [6]\n" +
                "        a  :  [7]\n" +
                "        c  :  [8]\n" +
                "      or  :  [9]\n" +
                "        a  :  [10]\n" +
                "        d  :  [11]\n" +
                "      or  :  [12]\n" +
                "        a  :  [13]\n" +
                "        e  :  [14]\n" +
                "    a  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,6,12).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, compiledConfigurationNoXorDistribution)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("xor(or(a;b);or(a;c);or(a;e))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("xor(or();or();or())", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("xor(or(a;b);or(a;c);or(a;e))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("xor(or(a;b);or(a;c);or(a;e))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("xor(place_for_result();or(a;d))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("xor(or(a;d))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, compiledConfigurationNoXorDistribution, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '(((a|b)^(a|c)^(a|e))^(a|d))->a'\n" +
                "expressionSubstitution.left: '(xor(or(a;b);or(a;c);or(a;e)))'\n" +
                "expressionSubstitution.right: '(xor(xor(or(a;b);or(a;c);or(a;e))))'\n" +
                "originalExpression: '(implic(xor(or(a;b);or(a;c);or(a;d);or(a;e));a))'\n" +
                "originalExpressionChangingPart: 'xor(or(a;b);or(a;c);or(a;e))'\n" +
                "resultExpression: '(implic(xor(xor(or(a;b);or(a;c);or(a;e));or(a;d));a))'\n" +
                "resultExpressionChangingPart: 'xor(xor(or(a;b);or(a;c);or(a;e)))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '40'", result.joinToString(separator = "\n\n"))
    }


    @Test
    fun XorOrDistributionTwoArgs() {
        val expression = structureStringToExpression("(alleq(xor(or(a;b;c);or(a;c;d);or(a;d);e);a))")
        assertEquals("  :  [0]\n" +
                "  alleq  :  [1]\n" +
                "    xor  :  [2]\n" +
                "      or  :  [3]\n" +
                "        a  :  [4]\n" +
                "        b  :  [5]\n" +
                "        c  :  [6]\n" +
                "      or  :  [7]\n" +
                "        a  :  [8]\n" +
                "        c  :  [9]\n" +
                "        d  :  [10]\n" +
                "      or  :  [11]\n" +
                "        a  :  [12]\n" +
                "        d  :  [13]\n" +
                "      e  :  [14]\n" +
                "    a  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(6,12).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
// No or xor distribution
//                "result: '((a|((b|c)^d))^(a|c|d)^e)==a'\n" +
//                "expressionSubstitution.left: '(xor(or(a;b;c);or(a;d)))'\n" +
//                "expressionSubstitution.right: '(or(a;xor(or(b;c);d)))'\n" +
//                "originalExpression: '(alleq(xor(or(a;b;c);or(a;c;d);or(a;d);e);a))'\n" +
//                "originalExpressionChangingPart: 'xor(or(a;b;c);or(a;d))'\n" +
//                "resultExpression: '(alleq(xor(or(a;xor(or(b;c);d));or(a;c;d);e);a))'\n" +
//                "resultExpressionChangingPart: 'or(a;xor(or(b;c);d))'\n" +
//                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
//                "priority: '10'\n" +
//                "\n" +
                "result: '((!(a|b|c)^!(a|d))^(a|c|d)^e)==a'\n" +
                "expressionSubstitution.left: '(xor(a;b))'\n" +
                "expressionSubstitution.right: '(xor(not(a);not(b)))'\n" +
                "originalExpression: '(alleq(xor(or(a;b;c);or(a;c;d);or(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'xor(or(a;b;c);or(a;d))'\n" +
                "resultExpression: '(alleq(xor(xor(not(or(a;b;c));not(or(a;d)));or(a;c;d);e);a))'\n" +
                "resultExpressionChangingPart: 'xor(not(or(a;b;c));not(or(a;d)))'\n" +
                "substitutionType: '(xor(a;b))__to__(xor(not(a);not(b)))'\n" +
                "priority: '20'\n" +
                "\n" +
                "result: '(((a|b|c)^(a|d))^(a|c|d)^e)==a'\n" +
                "expressionSubstitution.left: '(xor(or(a;b;c);or(a;d)))'\n" +
                "expressionSubstitution.right: '(xor(xor(or(a;b;c);or(a;d))))'\n" +
                "originalExpression: '(alleq(xor(or(a;b;c);or(a;c;d);or(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'xor(or(a;b;c);or(a;d))'\n" +
                "resultExpression: '(alleq(xor(xor(or(a;b;c);or(a;d));or(a;c;d);e);a))'\n" +
                "resultExpressionChangingPart: 'xor(xor(or(a;b;c);or(a;d)))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '40'\n" +
                "\n" +
                "result: '(!((a|b|c)==(a|d))^(a|c|d)^e)==a'\n" +
                "expressionSubstitution.left: '(xor(a;b))'\n" +
                "expressionSubstitution.right: '(not(alleq(a;b)))'\n" +
                "originalExpression: '(alleq(xor(or(a;b;c);or(a;c;d);or(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'xor(or(a;b;c);or(a;d))'\n" +
                "resultExpression: '(alleq(xor(not(alleq(or(a;b;c);or(a;d)));or(a;c;d);e);a))'\n" +
                "resultExpressionChangingPart: 'not(alleq(or(a;b;c);or(a;d)))'\n" +
                "substitutionType: '(xor(a;b))__to__(not(alleq(a;b)))'\n" +
                "priority: '90'\n" +
                "\n" +
                "result: '((!(a|b|c)==(a|d))^(a|c|d)^e)==a'\n" +
                "expressionSubstitution.left: '(xor(a;b))'\n" +
                "expressionSubstitution.right: '(alleq(not(a);b))'\n" +
                "originalExpression: '(alleq(xor(or(a;b;c);or(a;c;d);or(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'xor(or(a;b;c);or(a;d))'\n" +
                "resultExpression: '(alleq(xor(alleq(not(or(a;b;c));or(a;d));or(a;c;d);e);a))'\n" +
                "resultExpressionChangingPart: 'alleq(not(or(a;b;c));or(a;d))'\n" +
                "substitutionType: '(xor(a;b))__to__(alleq(not(a);b))'\n" +
                "priority: '90'\n" +
                "\n" +
                "result: '((a|b|(c^a^a))^(a|c|d)^(a|d)^e)==a'\n" +
                "expressionSubstitution.left: '(c)'\n" +
                "expressionSubstitution.right: '(xor(c;a;a))'\n" +
                "originalExpression: '(alleq(xor(or(a;b;c);or(a;c;d);or(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'c'\n" +
                "resultExpression: '(alleq(xor(or(a;b;xor(c;a;a));or(a;c;d);or(a;d);e);a))'\n" +
                "resultExpressionChangingPart: 'xor(c;a;a)'\n" +
                "substitutionType: 'XorExtension'\n" +
                "priority: '92'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun OrXorDistributionOpenBrackets() {
        val expression = structureStringToExpression("(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))")
        assertEquals("  :  [0]\n" +
                "  alleq  :  [1]\n" +
                "    or  :  [2]\n" +
                "      xor  :  [3]\n" +
                "        a  :  [4]\n" +
                "        b  :  [5]\n" +
                "        c  :  [6]\n" +
                "      xor  :  [7]\n" +
                "        a  :  [8]\n" +
                "        c  :  [9]\n" +
                "        d  :  [10]\n" +
                "      xor  :  [11]\n" +
                "        a  :  [12]\n" +
                "        d  :  [13]\n" +
                "      e  :  [14]\n" +
                "    a  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(7,14).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '(!(!(a^c^d)&!e)|(a^b^c)|(a^d))==a'\n" +
                "expressionSubstitution.left: '(or(a;b))'\n" +
                "expressionSubstitution.right: '(not(and(not(a);not(b))))'\n" +
                "originalExpression: '(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'or(xor(a;c;d);e)'\n" +
                "resultExpression: '(alleq(or(not(and(not(xor(a;c;d));not(e)));xor(a;b;c);xor(a;d));a))'\n" +
                "resultExpressionChangingPart: 'not(and(not(xor(a;c;d));not(e)))'\n" +
                "substitutionType: '(or(a;b))__to__(not(and(not(a);not(b))))'\n" +
                "priority: '15'\n" +
// no or distribution
//                "\n" +
//                "result: '(((a|e)^(c|e)^(d|e))|(a^b^c)|(a^d))==a'\n" +
//                "expressionSubstitution.left: '(or(xor(a;c;d);e))'\n" +
//                "expressionSubstitution.right: '(xor(or(a;e);or(c;e);or(d;e)))'\n" +
//                "originalExpression: '(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
//                "originalExpressionChangingPart: 'or(xor(a;c;d);e)'\n" +
//                "resultExpression: '(alleq(or(xor(or(a;e);or(c;e);or(d;e));xor(a;b;c);xor(a;d));a))'\n" +
//                "resultExpressionChangingPart: 'xor(or(a;e);or(c;e);or(d;e))'\n" +
//                "substitutionType: 'OpeningBrackets'\n" +
//                "priority: '30'\n" +
                "\n" +
                "result: '(((a^c^d)^e^((a^c^d)&e))|(a^b^c)|(a^d))==a'\n" +
                "expressionSubstitution.left: '(or(a;b))'\n" +
                "expressionSubstitution.right: '(xor(a;b;and(a;b)))'\n" +
                "originalExpression: '(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'or(xor(a;c;d);e)'\n" +
                "resultExpression: '(alleq(or(xor(xor(a;c;d);e;and(xor(a;c;d);e));xor(a;b;c);xor(a;d));a))'\n" +
                "resultExpressionChangingPart: 'xor(xor(a;c;d);e;and(xor(a;c;d);e))'\n" +
                "substitutionType: '(or(a;b))__to__(xor(a;b;and(a;b)))'\n" +
                "priority: '34'\n" +
                "\n" +
                "result: '((a^b^c)|e|(a^d)|(a^c^d))==a'\n" +
                "expressionSubstitution.left: '(or(xor(a;b;c);xor(a;c;d);xor(a;d);e))'\n" +
                "expressionSubstitution.right: '(or(xor(a;b;c);e;xor(a;d);xor(a;c;d)))'\n" +
                "originalExpression: '(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'or(xor(a;b;c);xor(a;c;d);xor(a;d);e)'\n" +
                "resultExpression: '(alleq(or(xor(a;b;c);e;xor(a;d);xor(a;c;d));a))'\n" +
                "resultExpressionChangingPart: 'or(xor(a;b;c);e;xor(a;d);xor(a;c;d))'\n" +
                "substitutionType: 'Swap'\n" +
                "priority: '40'\n" +
                "\n" +
                "result: '(((a^c^d)|e)|(a^b^c)|(a^d))==a'\n" +
                "expressionSubstitution.left: '(or(xor(a;c;d);e))'\n" +
                "expressionSubstitution.right: '(or(or(xor(a;c;d);e)))'\n" +
                "originalExpression: '(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'or(xor(a;c;d);e)'\n" +
                "resultExpression: '(alleq(or(or(xor(a;c;d);e);xor(a;b;c);xor(a;d));a))'\n" +
                "resultExpressionChangingPart: 'or(or(xor(a;c;d);e))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '40'\n" +
                "\n" +
                "result: '((a^b^c)|((a^c^d)^e^e)|(a^d)|e)==a'\n" +
                "expressionSubstitution.left: '(xor(a;c;d))'\n" +
                "expressionSubstitution.right: '(xor(xor(a;c;d);e;e))'\n" +
                "originalExpression: '(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'xor(a;c;d)'\n" +
                "resultExpression: '(alleq(or(xor(a;b;c);xor(xor(a;c;d);e;e);xor(a;d);e);a))'\n" +
                "resultExpressionChangingPart: 'xor(xor(a;c;d);e;e)'\n" +
                "substitutionType: 'XorExtension'\n" +
                "priority: '92'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun OrXorDistributionOpenBracketsNoXorDistribution() {
        val expression = structureStringToExpression("(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))")
        assertEquals("  :  [0]\n" +
                "  alleq  :  [1]\n" +
                "    or  :  [2]\n" +
                "      xor  :  [3]\n" +
                "        a  :  [4]\n" +
                "        b  :  [5]\n" +
                "        c  :  [6]\n" +
                "      xor  :  [7]\n" +
                "        a  :  [8]\n" +
                "        c  :  [9]\n" +
                "        d  :  [10]\n" +
                "      xor  :  [11]\n" +
                "        a  :  [12]\n" +
                "        d  :  [13]\n" +
                "      e  :  [14]\n" +
                "    a  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(7,14).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, compiledConfigurationNoXorDistribution)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, compiledConfigurationNoXorDistribution, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '(!(!(a^c^d)&!e)|(a^b^c)|(a^d))==a'\n" +
                "expressionSubstitution.left: '(or(a;b))'\n" +
                "expressionSubstitution.right: '(not(and(not(a);not(b))))'\n" +
                "originalExpression: '(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'or(xor(a;c;d);e)'\n" +
                "resultExpression: '(alleq(or(not(and(not(xor(a;c;d));not(e)));xor(a;b;c);xor(a;d));a))'\n" +
                "resultExpressionChangingPart: 'not(and(not(xor(a;c;d));not(e)))'\n" +
                "substitutionType: '(or(a;b))__to__(not(and(not(a);not(b))))'\n" +
                "priority: '15'\n" +
                "\n" +
                "result: '((a^b^c)|e|(a^d)|(a^c^d))==a'\n" +
                "expressionSubstitution.left: '(or(xor(a;b;c);xor(a;c;d);xor(a;d);e))'\n" +
                "expressionSubstitution.right: '(or(xor(a;b;c);e;xor(a;d);xor(a;c;d)))'\n" +
                "originalExpression: '(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'or(xor(a;b;c);xor(a;c;d);xor(a;d);e)'\n" +
                "resultExpression: '(alleq(or(xor(a;b;c);e;xor(a;d);xor(a;c;d));a))'\n" +
                "resultExpressionChangingPart: 'or(xor(a;b;c);e;xor(a;d);xor(a;c;d))'\n" +
                "substitutionType: 'Swap'\n" +
                "priority: '40'\n" +
                "\n" +
                "result: '(((a^c^d)|e)|(a^b^c)|(a^d))==a'\n" +
                "expressionSubstitution.left: '(or(xor(a;c;d);e))'\n" +
                "expressionSubstitution.right: '(or(or(xor(a;c;d);e)))'\n" +
                "originalExpression: '(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'or(xor(a;c;d);e)'\n" +
                "resultExpression: '(alleq(or(or(xor(a;c;d);e);xor(a;b;c);xor(a;d));a))'\n" +
                "resultExpressionChangingPart: 'or(or(xor(a;c;d);e))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '40'\n" +
                "\n" +
                "result: '((a^b^c)|((a^c^d)^e^e)|(a^d)|e)==a'\n" +
                "expressionSubstitution.left: '(xor(a;c;d))'\n" +
                "expressionSubstitution.right: '(xor(xor(a;c;d);e;e))'\n" +
                "originalExpression: '(alleq(or(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'xor(a;c;d)'\n" +
                "resultExpression: '(alleq(or(xor(a;b;c);xor(xor(a;c;d);e;e);xor(a;d);e);a))'\n" +
                "resultExpressionChangingPart: 'xor(xor(a;c;d);e;e)'\n" +
                "substitutionType: 'XorExtension'\n" +
                "priority: '92'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun AndXorDistributionOpenBrackets() {
        val expression = structureStringToExpression("(alleq(and(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))")
        assertEquals("  :  [0]\n" +
                "  alleq  :  [1]\n" +
                "    and  :  [2]\n" +
                "      xor  :  [3]\n" +
                "        a  :  [4]\n" +
                "        b  :  [5]\n" +
                "        c  :  [6]\n" +
                "      xor  :  [7]\n" +
                "        a  :  [8]\n" +
                "        c  :  [9]\n" +
                "        d  :  [10]\n" +
                "      xor  :  [11]\n" +
                "        a  :  [12]\n" +
                "        d  :  [13]\n" +
                "      e  :  [14]\n" +
                "    a  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(7,3).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '!(!(a^c^d)|!(a^b^c))&(a^d)&e==a'\n" +
                "expressionSubstitution.left: '(and(a;b))'\n" +
                "expressionSubstitution.right: '(not(or(not(a);not(b))))'\n" +
                "originalExpression: '(alleq(and(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'and(xor(a;b;c);xor(a;c;d))'\n" +
                "resultExpression: '(alleq(and(not(or(not(xor(a;c;d));not(xor(a;b;c))));xor(a;d);e);a))'\n" +
                "resultExpressionChangingPart: 'not(or(not(xor(a;c;d));not(xor(a;b;c))))'\n" +
                "substitutionType: '(and(a;b))__to__(not(or(not(a);not(b))))'\n" +
                "priority: '15'\n" +
                "\n" +
                "result: '((a&a)^(a&b)^(a&c)^(c&a)^(c&b)^(c&c)^(d&a)^(d&b)^(d&c))&(a^d)&e==a'\n" +
                "expressionSubstitution.left: '(and(xor(a;c;d);xor(a;b;c)))'\n" +
                "expressionSubstitution.right: '(xor(and(a;a);and(a;b);and(a;c);and(c;a);and(c;b);and(c;c);and(d;a);and(d;b);and(d;c)))'\n" +
                "originalExpression: '(alleq(and(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'and(xor(a;c;d);xor(a;b;c))'\n" +
                "resultExpression: '(alleq(and(xor(and(a;a);and(a;b);and(a;c);and(c;a);and(c;b);and(c;c);and(d;a);and(d;b);and(d;c));xor(a;d);e);a))'\n" +
                "resultExpressionChangingPart: 'xor(and(a;a);and(a;b);and(a;c);and(c;a);and(c;b);and(c;c);and(d;a);and(d;b);and(d;c))'\n" +
                "substitutionType: 'OpeningBrackets'\n" +
                "priority: '30'\n" +
                "\n" +
                "result: '((a&(a^b^c))^(c&(a^b^c))^(d&(a^b^c)))&(a^d)&e==a'\n" +
                "expressionSubstitution.left: '(and(xor(a;c;d);xor(a;b;c)))'\n" +
                "expressionSubstitution.right: '(xor(and(a;xor(a;b;c));and(c;xor(a;b;c));and(d;xor(a;b;c))))'\n" +
                "originalExpression: '(alleq(and(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'and(xor(a;c;d);xor(a;b;c))'\n" +
                "resultExpression: '(alleq(and(xor(and(a;xor(a;b;c));and(c;xor(a;b;c));and(d;xor(a;b;c)));xor(a;d);e);a))'\n" +
                "resultExpressionChangingPart: 'xor(and(a;xor(a;b;c));and(c;xor(a;b;c));and(d;xor(a;b;c)))'\n" +
                "substitutionType: 'OpeningBrackets'\n" +
                "priority: '30'\n" +
                "\n" +
                "result: '(a^c^d)&(a^b^c)&(a^d)&e==a'\n" +
                "expressionSubstitution.left: '(and(xor(a;b;c);xor(a;c;d);xor(a;d);e))'\n" +
                "expressionSubstitution.right: '(and(xor(a;c;d);xor(a;b;c);xor(a;d);e))'\n" +
                "originalExpression: '(alleq(and(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'and(xor(a;b;c);xor(a;c;d);xor(a;d);e)'\n" +
                "resultExpression: '(alleq(and(xor(a;c;d);xor(a;b;c);xor(a;d);e);a))'\n" +
                "resultExpressionChangingPart: 'and(xor(a;c;d);xor(a;b;c);xor(a;d);e)'\n" +
                "substitutionType: 'Swap'\n" +
                "priority: '40'\n" +
                "\n" +
                "result: '((a^c^d)&(a^b^c))&(a^d)&e==a'\n" +
                "expressionSubstitution.left: '(and(xor(a;b;c);xor(a;c;d)))'\n" +
                "expressionSubstitution.right: '(and(and(xor(a;c;d);xor(a;b;c))))'\n" +
                "originalExpression: '(alleq(and(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'and(xor(a;b;c);xor(a;c;d))'\n" +
                "resultExpression: '(alleq(and(and(xor(a;c;d);xor(a;b;c));xor(a;d);e);a))'\n" +
                "resultExpressionChangingPart: 'and(and(xor(a;c;d);xor(a;b;c)))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '40'\n" +
                "\n" +
                "result: '((a^b^c)&(a^c^d))&(a^d)&e==a'\n" +
                "expressionSubstitution.left: '(and(xor(a;b;c);xor(a;c;d)))'\n" +
                "expressionSubstitution.right: '(and(and(xor(a;b;c);xor(a;c;d))))'\n" +
                "originalExpression: '(alleq(and(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'and(xor(a;b;c);xor(a;c;d))'\n" +
                "resultExpression: '(alleq(and(and(xor(a;b;c);xor(a;c;d));xor(a;d);e);a))'\n" +
                "resultExpressionChangingPart: 'and(and(xor(a;b;c);xor(a;c;d)))'\n" +
                "substitutionType: 'OriginalOrderExtraction'\n" +
                "priority: '41'\n" +
                "\n" +
                "result: '(a^b^c)&((a^c^d)^(a^b^c)^(a^b^c))&(a^d)&e==a'\n" +
                "expressionSubstitution.left: '(xor(a;c;d))'\n" +
                "expressionSubstitution.right: '(xor(xor(a;c;d);xor(a;b;c);xor(a;b;c)))'\n" +
                "originalExpression: '(alleq(and(xor(a;b;c);xor(a;c;d);xor(a;d);e);a))'\n" +
                "originalExpressionChangingPart: 'xor(a;c;d)'\n" +
                "resultExpression: '(alleq(and(xor(a;b;c);xor(xor(a;c;d);xor(a;b;c);xor(a;b;c));xor(a;d);e);a))'\n" +
                "resultExpressionChangingPart: 'xor(xor(a;c;d);xor(a;b;c);xor(a;b;c))'\n" +
                "substitutionType: 'XorExtension'\n" +
                "priority: '92'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun XorSimplification() {
        val expression = structureStringToExpression("(alleq(xor(a;a;a;b;a);a))")
        assertEquals("  :  [0]\n" +
                "  alleq  :  [1]\n" +
                "    xor  :  [2]\n" +
                "      a  :  [3]\n" +
                "      a  :  [4]\n" +
                "      a  :  [5]\n" +
                "      b  :  [6]\n" +
                "      a  :  [7]\n" +
                "    a  :  [8]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(7,3).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '(0^a^a^b)==a'\n" +
                "expressionSubstitution.left: '(xor(a;a))'\n" +
                "expressionSubstitution.right: '(0)'\n" +
                "originalExpression: '(alleq(xor(a;a;a;b;a);a))'\n" +
                "originalExpressionChangingPart: 'xor(a;a)'\n" +
                "resultExpression: '(alleq(xor(0;a;a;b);a))'\n" +
                "resultExpressionChangingPart: '0'\n" +
                "substitutionType: '(xor(a;a))__to__(0)'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: '((a&(1^1))^a^a^b)==a'\n" +
                "expressionSubstitution.left: '(xor(a;a))'\n" +
                "expressionSubstitution.right: '(and(a;xor(1;1)))'\n" +
                "originalExpression: '(alleq(xor(a;a;a;b;a);a))'\n" +
                "originalExpressionChangingPart: 'xor(a;a)'\n" +
                "resultExpression: '(alleq(xor(and(a;xor(1;1));a;a;b);a))'\n" +
                "resultExpressionChangingPart: 'and(a;xor(1;1))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '10'\n" +
                "\n" +
                "result: '((!a^!a)^a^a^b)==a'\n" +
                "expressionSubstitution.left: '(xor(a;b))'\n" +
                "expressionSubstitution.right: '(xor(not(a);not(b)))'\n" +
                "originalExpression: '(alleq(xor(a;a;a;b;a);a))'\n" +
                "originalExpressionChangingPart: 'xor(a;a)'\n" +
                "resultExpression: '(alleq(xor(xor(not(a);not(a));a;a;b);a))'\n" +
                "resultExpressionChangingPart: 'xor(not(a);not(a))'\n" +
                "substitutionType: '(xor(a;b))__to__(xor(not(a);not(b)))'\n" +
                "priority: '20'\n" +
                "\n" +
                "result: '((a^a)^a^a^b)==a'\n" +
                "expressionSubstitution.left: '(xor(a;a))'\n" +
                "expressionSubstitution.right: '(xor(xor(a;a)))'\n" +
                "originalExpression: '(alleq(xor(a;a;a;b;a);a))'\n" +
                "originalExpressionChangingPart: 'xor(a;a)'\n" +
                "resultExpression: '(alleq(xor(xor(a;a);a;a;b);a))'\n" +
                "resultExpressionChangingPart: 'xor(xor(a;a))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '40'\n" +
                "\n" +
                "result: '(!(a==a)^a^a^b)==a'\n" +
                "expressionSubstitution.left: '(xor(a;b))'\n" +
                "expressionSubstitution.right: '(not(alleq(a;b)))'\n" +
                "originalExpression: '(alleq(xor(a;a;a;b;a);a))'\n" +
                "originalExpressionChangingPart: 'xor(a;a)'\n" +
                "resultExpression: '(alleq(xor(not(alleq(a;a));a;a;b);a))'\n" +
                "resultExpressionChangingPart: 'not(alleq(a;a))'\n" +
                "substitutionType: '(xor(a;b))__to__(not(alleq(a;b)))'\n" +
                "priority: '90'\n" +
                "\n" +
                "result: '((!a==a)^a^a^b)==a'\n" +
                "expressionSubstitution.left: '(xor(a;b))'\n" +
                "expressionSubstitution.right: '(alleq(not(a);b))'\n" +
                "originalExpression: '(alleq(xor(a;a;a;b;a);a))'\n" +
                "originalExpressionChangingPart: 'xor(a;a)'\n" +
                "resultExpression: '(alleq(xor(alleq(not(a);a);a;a;b);a))'\n" +
                "resultExpressionChangingPart: 'alleq(not(a);a)'\n" +
                "substitutionType: '(xor(a;b))__to__(alleq(not(a);b))'\n" +
                "priority: '90'\n" +
                "\n" +
                "result: '(a^a^a^b^(a^a^a))==a'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(xor(a;a;a))'\n" +
                "originalExpression: '(alleq(xor(a;a;a;b;a);a))'\n" +
                "originalExpressionChangingPart: 'a'\n" +
                "resultExpression: '(alleq(xor(a;a;a;b;xor(a;a;a));a))'\n" +
                "resultExpressionChangingPart: 'xor(a;a;a)'\n" +
                "substitutionType: 'XorExtension'\n" +
                "priority: '92'", result.joinToString(separator = "\n\n"))
    }


    @Test
    fun testGenerateSubstitutionsBySelectedNodesAndItsForwardInverseExtension () {
        val expression = parseStringExpression("(a | 0) -> (1 & b)")
        assertEquals("  :  [0]\n" +
                "  implic  :  [1]\n" +
                "    or  :  [2]\n" +
                "      a  :  [3]\n" +
                "      0  :  [4]\n" +
                "    and  :  [5]\n" +
                "      1  :  [6]\n" +
                "      b  :  [7]\n", expression.toStringsWithNodeIds())

        val alphaSelectedTwoPlacesNodeIds = listOf(2,3).toTypedArray()
        val alphaMulDivTwoPlacesRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,alphaSelectedTwoPlacesNodeIds, generalCompiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.XOR, parseStringExpression("x&y")))
        assertEquals("", alphaMulDivTwoPlacesRes.joinToString (separator = "\n"))


        // LOGIC_ABSORPTION
        val logicAbsorptionSelectedNodeIds = listOf(2).toTypedArray()
        val logicAbsorptionRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,logicAbsorptionSelectedNodeIds, generalCompiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.LOGIC_ABSORPTION, parseStringExpression("x&y")))
        assertEquals("", logicAbsorptionRes.joinToString (separator = "\n"))

        val logicAbsorptionResAllowed = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,logicAbsorptionSelectedNodeIds, compiledConfigurationAbsorbtionLaw,
                ForwardInverseExtension(ForwardInverseExtensionType.LOGIC_ABSORPTION, parseStringExpression("x&y")))
        assertEquals("" +
                "result: '((a|0)&((a|0)|(x&y)))->(1&b)'\n" +
                "expressionSubstitution.left: '(or(a;0))'\n" +
                "expressionSubstitution.right: '(and(or(a;0);or(or(a;0);(and(x;y)))))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(or(a;0))'\n" +
                "resultExpression: '(implic(and(or(a;0);or(or(a;0);and(x;y)));and(1;b)))'\n" +
                "resultExpressionChangingPart: '(and(or(a;0);or(or(a;0);(and(x;y)))))'\n" +
                "substitutionType: 'OrAndExtension'\n" +
                "priority: '10'\n" +
                "result: '((a|0)|((a|0)&(x&y)))->(1&b)'\n" +
                "expressionSubstitution.left: '(or(a;0))'\n" +
                "expressionSubstitution.right: '(or(or(a;0);and(or(a;0);(and(x;y)))))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(or(a;0))'\n" +
                "resultExpression: '(implic(or(or(a;0);and(or(a;0);and(x;y)));and(1;b)))'\n" +
                "resultExpressionChangingPart: '(or(or(a;0);and(or(a;0);(and(x;y)))))'\n" +
                "substitutionType: 'OrAndExtension'\n" +
                "priority: '11'", logicAbsorptionResAllowed.joinToString (separator = "\n"))


        // XOR
        val xorSelectedNodeIds = listOf(2).toTypedArray()
        val xorRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,xorSelectedNodeIds, generalCompiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.XOR, parseStringExpression("x")))
        assertEquals("" +
                "result: '((a|0)^x^x)->(1&b)'\n" +
                "expressionSubstitution.left: '(or(a;0))'\n" +
                "expressionSubstitution.right: '(xor(or(a;0);(x);(x)))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(or(a;0))'\n" +
                "resultExpression: '(implic(xor(or(a;0);x;x);and(1;b)))'\n" +
                "resultExpressionChangingPart: '(xor(or(a;0);(x);(x)))'\n" +
                "substitutionType: 'XorExtension'\n" +
                "priority: '10'", xorRes.joinToString (separator = "\n"))

        val xor0SelectedNodeIds = listOf(4).toTypedArray()
        val xor0Res = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,xor0SelectedNodeIds, generalCompiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.XOR, parseStringExpression("x")))
        assertEquals("" +
                "result: '(a|(0^x^x))->(1&b)'\n" +
                "expressionSubstitution.left: '(0)'\n" +
                "expressionSubstitution.right: '(xor(0;(x);(x)))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(0)'\n" +
                "resultExpression: '(implic(or(a;xor(0;x;x));and(1;b)))'\n" +
                "resultExpressionChangingPart: '(xor(0;(x);(x)))'\n" +
                "substitutionType: 'XorExtension'\n" +
                "priority: '10'\n" +
                "\n" +
                "result: '(a|(x^x))->(1&b)'\n" +
                "expressionSubstitution.left: '(0)'\n" +
                "expressionSubstitution.right: '(xor((x);(x)))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(0)'\n" +
                "resultExpression: '(implic(or(a;xor(x;x));and(1;b)))'\n" +
                "resultExpressionChangingPart: '(xor((x);(x)))'\n" +
                "substitutionType: 'XorExtension'\n" +
                "priority: '10'", xor0Res.joinToString (separator = "\n\n"))

        // ZERO_TO
        val zeroWrongSelectedNodeIds = listOf(2).toTypedArray()
        val zeroWrongRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression, zeroWrongSelectedNodeIds, generalCompiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.ZERO_TO, parseStringExpression("x")))
        assertEquals("", zeroWrongRes.joinToString (separator = "\n\n"))

        val zeroSelectedNodeIds = listOf(4).toTypedArray()
        val zeroRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,zeroSelectedNodeIds, generalCompiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.ZERO_TO, parseStringExpression("x->y")))
        assertEquals("" +
                "result: '(a|(0&(x->y)))->(1&b)'\n" +
                "expressionSubstitution.left: '(0)'\n" +
                "expressionSubstitution.right: '(and(0;(implic(x;y))))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(0)'\n" +
                "resultExpression: '(implic(or(a;and(0;implic(x;y)));and(1;b)))'\n" +
                "resultExpressionChangingPart: '(and(0;(implic(x;y))))'\n" +
                "substitutionType: 'zeroAndExtension'\n" +
                "priority: '10'\n" +
                "\n" +
                "result: '(a|((x->y)^(x->y)))->(1&b)'\n" +
                "expressionSubstitution.left: '(0)'\n" +
                "expressionSubstitution.right: '(xor((implic(x;y));(implic(x;y))))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(0)'\n" +
                "resultExpression: '(implic(or(a;xor(implic(x;y);implic(x;y)));and(1;b)))'\n" +
                "resultExpressionChangingPart: '(xor((implic(x;y));(implic(x;y))))'\n" +
                "substitutionType: 'XorExtension'\n" +
                "priority: '10'", zeroRes.joinToString (separator = "\n\n"))

        val zeroWithSetMinusRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,zeroSelectedNodeIds, compiledConfigurationAbsorbtionLaw,
                ForwardInverseExtension(ForwardInverseExtensionType.ZERO_TO, parseStringExpression("x->y")))
        assertEquals("" +
                "result: '(a|(0&(x->y)))->(1&b)'\n" +
                "expressionSubstitution.left: '(0)'\n" +
                "expressionSubstitution.right: '(and(0;(implic(x;y))))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(0)'\n" +
                "resultExpression: '(implic(or(a;and(0;implic(x;y)));and(1;b)))'\n" +
                "resultExpressionChangingPart: '(and(0;(implic(x;y))))'\n" +
                "substitutionType: 'zeroAndExtension'\n" +
                "priority: '10'\n" +
                "\n" +
                "result: '(a|((x->y)^(x->y)))->(1&b)'\n" +
                "expressionSubstitution.left: '(0)'\n" +
                "expressionSubstitution.right: '(xor((implic(x;y));(implic(x;y))))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(0)'\n" +
                "resultExpression: '(implic(or(a;xor(implic(x;y);implic(x;y)));and(1;b)))'\n" +
                "resultExpressionChangingPart: '(xor((implic(x;y));(implic(x;y))))'\n" +
                "substitutionType: 'XorExtension'\n" +
                "priority: '10'\n" +
                "\n" +
                "result: '(a|((x->y)\\(x->y)))->(1&b)'\n" +
                "expressionSubstitution.left: '(0)'\n" +
                "expressionSubstitution.right: '(set-((implic(x;y));(implic(x;y))))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(0)'\n" +
                "resultExpression: '(implic(or(a;set-(implic(x;y);implic(x;y)));and(1;b)))'\n" +
                "resultExpressionChangingPart: '(set-((implic(x;y));(implic(x;y))))'\n" +
                "substitutionType: 'xSetMinusXExtension'\n" +
                "priority: '10'\n" +
                "\n" +
                "result: '(a|(0\\(x->y)))->(1&b)'\n" +
                "expressionSubstitution.left: '(0)'\n" +
                "expressionSubstitution.right: '(set-(0;(implic(x;y))))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(0)'\n" +
                "resultExpression: '(implic(or(a;set-(0;implic(x;y)));and(1;b)))'\n" +
                "resultExpressionChangingPart: '(set-(0;(implic(x;y))))'\n" +
                "substitutionType: 'zeroSetMinusXExtension'\n" +
                "priority: '10'", zeroWithSetMinusRes.joinToString (separator = "\n\n"))

        // ONE_TO
        val oneWrongSelectedNodeIds = listOf(4).toTypedArray()
        val oneWrongRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression, oneWrongSelectedNodeIds, generalCompiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.ONE_TO, parseStringExpression("x")))
        assertEquals("", oneWrongRes.joinToString (separator = "\n\n"))

        val oneSelectedNodeIds = listOf(6).toTypedArray()
        val oneRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,oneSelectedNodeIds, generalCompiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.ONE_TO, parseStringExpression("not(x)")))
        assertEquals("" +
                "result: '(a|0)->((1|!x)&b)'\n" +
                "expressionSubstitution.left: '(1)'\n" +
                "expressionSubstitution.right: '(or(1;(not(x))))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(1)'\n" +
                "resultExpression: '(implic(or(a;0);and(or(1;not(x));b)))'\n" +
                "resultExpressionChangingPart: '(or(1;(not(x))))'\n" +
                "substitutionType: 'oneOrExtension'\n" +
                "priority: '10'\n" +
                "\n" +
                "result: '(a|0)->((!x->!x)&b)'\n" +
                "expressionSubstitution.left: '(1)'\n" +
                "expressionSubstitution.right: '(implic((not(x));(not(x))))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(1)'\n" +
                "resultExpression: '(implic(or(a;0);and(implic(not(x);not(x));b)))'\n" +
                "resultExpressionChangingPart: '(implic((not(x));(not(x))))'\n" +
                "substitutionType: 'xImplicXExtension'\n" +
                "priority: '10'\n" +
                "\n" +
                "result: '(a|0)->((!x->1)&b)'\n" +
                "expressionSubstitution.left: '(1)'\n" +
                "expressionSubstitution.right: '(implic((not(x));1))'\n" +
                "originalExpression: '(implic(or(a;0);and(1;b)))'\n" +
                "originalExpressionChangingPart: '(1)'\n" +
                "resultExpression: '(implic(or(a;0);and(implic(not(x);1);b)))'\n" +
                "resultExpressionChangingPart: '(implic((not(x));1))'\n" +
                "substitutionType: 'xImplicOneExtension'\n" +
                "priority: '10'", oneRes.joinToString (separator = "\n\n"))

    }
}