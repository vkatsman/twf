package mathhelper.twf.substitutionsgeneration

import mathhelper.twf.api.createCompiledConfigurationFromExpressionSubstitutionsAndParams
import mathhelper.twf.api.expressionSubstitutionFromStructureStrings
import mathhelper.twf.api.findApplicableSubstitutionsInSelectedPlace
import mathhelper.twf.api.structureStringToExpression
import mathhelper.twf.expressiontree.SubstitutionSelectionData
import mathhelper.twf.expressiontree.TREE_COMPUTATION_DEFAULT
import mathhelper.twf.expressiontree.fillSubstitutionSelectionData
import kotlin.test.assertEquals
import org.junit.Test

class Physics {
    val generalCompiledConfiguration = createCompiledConfigurationFromExpressionSubstitutionsAndParams(
            listOf(
                    expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                    expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                    expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                    expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                    expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                    expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                    expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic", priority = 5),
                    expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                    expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                    expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                    expressionSubstitutionFromStructureStrings(code = "MinusInOutBrackets", priority = 61),
                    expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT),
                    expressionSubstitutionFromStructureStrings(code = "ZeroComputation"),

                    expressionSubstitutionFromStructureStrings("(a)", "(^(a;1))", simpleAdditional = true, isExtending = true),
                    expressionSubstitutionFromStructureStrings("(*(/(1;a);a))", "(1)"),
                    expressionSubstitutionFromStructureStrings("(/(Fₐ;*(B;l;sin(φ))))","(I)", true, true),
                    expressionSubstitutionFromStructureStrings("(/(Fₐ;*(I;l;sin(φ))))","(B)", true, true)
            ).toTypedArray(),
            mapOf(Pair("simpleComputationRuleParamsMaxCalcComplexity", "6"))
    )



    @Test
    fun extraRule() {
        val expression = structureStringToExpression("(*(/(Fₐ;*(I;l;sin(φ)));S;cos(φ)))")
        assertEquals("  :  [0]\n" +
                "  *  :  [1]\n" +
                "    /  :  [2]\n" +
                "      Fₐ  :  [3]\n" +
                "      *  :  [4]\n" +
                "        I  :  [5]\n" +
                "        l  :  [6]\n" +
                "        sin  :  [7]\n" +
                "          φ  :  [8]\n" +
                "    S  :  [9]\n" +
                "    cos  :  [10]\n" +
                "      φ  :  [11]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '(Fₐ/(I*l*sin(φ)))^1*S*cos(φ)'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(^(a;1))'\n" +
                "originalExpression: '(*(/(Fₐ;*(I;l;sin(φ)));S;cos(φ)))'\n" +
                "originalExpressionChangingPart: '/(Fₐ;*(I;l;sin(φ)))'\n" +
                "resultExpression: '(*(^(/(Fₐ;*(I;l;sin(φ)));1);S;cos(φ)))'\n" +
                "resultExpressionChangingPart: '^(/(Fₐ;*(I;l;sin(φ)));1)'\n" +
                "substitutionType: ''(a)'->'(^(a;1))''\n" +
                "priority: '50'\n" +
                "\n" +
                "result: 'B*S*cos(φ)'\n" +
                "expressionSubstitution.left: '(/(Fₐ;*(I;l;sin(φ))))'\n" +
                "expressionSubstitution.right: '(B)'\n" +
                "originalExpression: '(*(/(Fₐ;*(I;l;sin(φ)));S;cos(φ)))'\n" +
                "originalExpressionChangingPart: '/(Fₐ;*(I;l;sin(φ)))'\n" +
                "resultExpression: '(*(B;S;cos(φ)))'\n" +
                "resultExpressionChangingPart: 'B'\n" +
                "substitutionType: ''(/(Fₐ;*(I;l;sin(φ))))'->'(B)''\n" +
                "priority: '50'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun divisionAsMultiplicationBug() {
        val expression = structureStringToExpression("(*(/(φ;*(S;cos(φ)));S;cos(φ)))")
        assertEquals("  :  [0]\n" +
                "  *  :  [1]\n" +
                "    /  :  [2]\n" +
                "      φ  :  [3]\n" +
                "      *  :  [4]\n" +
                "        S  :  [5]\n" +
                "        cos  :  [6]\n" +
                "          φ  :  [7]\n" +
                "    S  :  [8]\n" +
                "    cos  :  [9]\n" +
                "      φ  :  [10]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(5,8).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("*(/(*(S));S)", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("*(/(*(S));S)", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("*(/(1;S);S)", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("*(/(1;S);S)", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("*(place_for_result();φ;/(1;cos(φ));cos(φ))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("*(/(φ;*(cos(φ)));cos(φ))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '1*(φ/cos(φ))*cos(φ)'\n" +
                "expressionSubstitution.left: '(*(/(1;a);a))'\n" +
                "expressionSubstitution.right: '(1)'\n" +
                "originalExpression: '(*(/(φ;*(S;cos(φ)));S;cos(φ)))'\n" +
                "originalExpressionChangingPart: '*(/(1;S);S)'\n" +
                "resultExpression: '(*(1;/(φ;cos(φ));cos(φ)))'\n" +
                "resultExpressionChangingPart: '1'\n" +
                "substitutionType: ''(*(/(1;a);a))'->'(1)''\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '(φ/((S+S-S)*cos(φ)))*S*cos(φ)'\n" +
                "expressionSubstitution.left: '(S)'\n" +
                "expressionSubstitution.right: '(+(S;S;-(S)))'\n" +
                "originalExpression: '(*(/(φ;*(S;cos(φ)));S;cos(φ)))'\n" +
                "originalExpressionChangingPart: 'S'\n" +
                "resultExpression: '(*(/(φ;*(+(S;S;-(S));cos(φ)));S;cos(φ)))'\n" +
                "resultExpressionChangingPart: '+(S;S;-(S))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'\n" +
                "\n" +
                "result: '(φ/(((S*S)/S)*cos(φ)))*S*cos(φ)'\n" +
                "expressionSubstitution.left: '(S)'\n" +
                "expressionSubstitution.right: '(/(*(S;S);S))'\n" +
                "originalExpression: '(*(/(φ;*(S;cos(φ)));S;cos(φ)))'\n" +
                "originalExpressionChangingPart: 'S'\n" +
                "resultExpression: '(*(/(φ;*(/(*(S;S);S);cos(φ)));S;cos(φ)))'\n" +
                "resultExpressionChangingPart: '/(*(S;S);S)'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'\n" +
                "\n" +
                "result: '((1/S)*S)*(φ/cos(φ))*cos(φ)'\n" +
                "expressionSubstitution.left: '(*(/(1;S);S))'\n" +
                "expressionSubstitution.right: '(*(*(/(1;S);S)))'\n" +
                "originalExpression: '(*(/(φ;*(S;cos(φ)));S;cos(φ)))'\n" +
                "originalExpressionChangingPart: '*(/(1;S);S)'\n" +
                "resultExpression: '(*(*(/(1;S);S);/(φ;cos(φ));cos(φ)))'\n" +
                "resultExpressionChangingPart: '*(*(/(1;S);S))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '90'", result.joinToString(separator = "\n\n"))
    }
}