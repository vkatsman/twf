package mathhelper.twf.substitutionsgeneration

import mathhelper.twf.api.createCompiledConfigurationFromExpressionSubstitutionsAndParams
import mathhelper.twf.api.expressionSubstitutionFromStructureStrings
import mathhelper.twf.api.findApplicableSubstitutionsInSelectedPlace
import mathhelper.twf.api.structureStringToExpression
import mathhelper.twf.expressiontree.SubstitutionSelectionData
import mathhelper.twf.expressiontree.TREE_COMPUTATION_DEFAULT
import mathhelper.twf.expressiontree.fillSubstitutionSelectionData
import kotlin.test.assertEquals
import org.junit.Test

class ShortMultiplication {
    val generalCompiledConfiguration = createCompiledConfigurationFromExpressionSubstitutionsAndParams(
            listOf(
                    expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                    expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                    expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                    expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                    expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                    expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                    expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic", priority = 5),
                    expressionSubstitutionFromStructureStrings(code = "TwoSidesArithmeticReduce", priority = 5),
                    expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                    expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                    expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                    expressionSubstitutionFromStructureStrings(code = "MinusInOutBrackets", priority = 61),
                    expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT),
                    expressionSubstitutionFromStructureStrings(code = "ZeroComputation"),

                    expressionSubstitutionFromStructureStrings("(a)", "(^(a;1))", simpleAdditional = true, isExtending = true)
            ).toTypedArray(),
            mapOf(Pair("simpleComputationRuleParamsMaxCalcComplexity", "6"))
    )

    @Test
    fun failingMultipleTest1() {
        val expression = structureStringToExpression("(+(-(*(*(^(4;/(1;2));^(6;/(1;2)));^(6;/(1;2))));*(*(^(6;/(1;2));^(9;/(1;2)));^(6;/(1;2)))))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    -  :  [2]\n" +
                "      *  :  [3]\n" +
                "        *  :  [4]\n" +
                "          ^  :  [5]\n" +
                "            4  :  [6]\n" +
                "            /  :  [7]\n" +
                "              1  :  [8]\n" +
                "              2  :  [9]\n" +
                "          ^  :  [10]\n" +
                "            6  :  [11]\n" +
                "            /  :  [12]\n" +
                "              1  :  [13]\n" +
                "              2  :  [14]\n" +
                "        ^  :  [15]\n" +
                "          6  :  [16]\n" +
                "          /  :  [17]\n" +
                "            1  :  [18]\n" +
                "            2  :  [19]\n" +
                "    *  :  [20]\n" +
                "      *  :  [21]\n" +
                "        ^  :  [22]\n" +
                "          6  :  [23]\n" +
                "          /  :  [24]\n" +
                "            1  :  [25]\n" +
                "            2  :  [26]\n" +
                "        ^  :  [27]\n" +
                "          9  :  [28]\n" +
                "          /  :  [29]\n" +
                "            1  :  [30]\n" +
                "            2  :  [31]\n" +
                "      ^  :  [32]\n" +
                "        6  :  [33]\n" +
                "        /  :  [34]\n" +
                "          1  :  [35]\n" +
                "          2  :  [36]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3, 11, 15).toTypedArray()
        val genRes = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true)
    }

    @Test
    fun failingMultipleTest2() {
        val expression = structureStringToExpression("(*(6;^(9;/(1;2))))")
        assertEquals("  :  [0]\n" +
                "  *  :  [1]\n" +
                "    6  :  [2]\n" +
                "    ^  :  [3]\n" +
                "      9  :  [4]\n" +
                "      /  :  [5]\n" +
                "        1  :  [6]\n" +
                "        2  :  [7]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,1).toTypedArray()
        val genRes = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true)
    }

    @Test
    fun failingMultipleTest3() {
        val expression = structureStringToExpression("(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))))")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    +  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        +  :  [4]\n" +
                "          a  :  [5]\n" +
                "          b  :  [6]\n" +
                "        3  :  [7]\n" +
                "      ^  :  [8]\n" +
                "        +  :  [9]\n" +
                "          a  :  [10]\n" +
                "          -  :  [11]\n" +
                "            b  :  [12]\n" +
                "        3  :  [13]\n" +
                "      -  :  [14]\n" +
                "        *  :  [15]\n" +
                "          3  :  [16]\n" +
                "          a  :  [17]\n" +
                "    +  :  [18]\n" +
                "      +  :  [19]\n" +
                "        *  :  [20]\n" +
                "          2  :  [21]\n" +
                "          ^  :  [22]\n" +
                "            a  :  [23]\n" +
                "            2  :  [24]\n" +
                "        *  :  [25]\n" +
                "          6  :  [26]\n" +
                "          ^  :  [27]\n" +
                "            b  :  [28]\n" +
                "            2  :  [29]\n" +
                "        -  :  [30]\n" +
                "          3  :  [31]\n" +
                "      -  :  [32]\n" +
                "        +  :  [33]\n" +
                "          -  :  [34]\n" +
                "            0  :  [35]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(34).toTypedArray()

            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("-(0)", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("-()", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("-(0)", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("-(0)", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())

            val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
            assertEquals("" +
                    "result: '((a+b)^3+(a-b)^3-3*a)/((2*a^2+6*b^2-3)+0)'\n" +
                    "expressionSubstitution.left: '(-(0))'\n" +
                    "expressionSubstitution.right: '(-0)'\n" +
                    "originalExpression: '(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))))'\n" +
                    "originalExpressionChangingPart: '-(0)'\n" +
                    "resultExpression: '(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(+(*(2;^(a;2));*(6;^(b;2));-(3));0)))'\n" +
                    "resultExpressionChangingPart: '-0'\n" +
                    "substitutionType: 'SimpleComputation'\n" +
                    "priority: '5'\n" +
                    "\n" +
                    "result: '((a+b)^3+(a-b)^3-3*a)/((2*a^2+6*b^2-3)-(-0)^1)'\n" +
                    "expressionSubstitution.left: '(a)'\n" +
                    "expressionSubstitution.right: '(^(a;1))'\n" +
                    "originalExpression: '(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))))'\n" +
                    "originalExpressionChangingPart: '-(0)'\n" +
                    "resultExpression: '(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(^(+(-(0));1)))))'\n" +
                    "resultExpressionChangingPart: '^(-(0);1)'\n" +
                    "substitutionType: ''(a)'->'(^(a;1))''\n" +
                    "priority: '50'", result.joinToString(separator = "\n\n"))
        }
        if (true) {
            val selectedNodeIds = listOf(19, 33).toTypedArray()

            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("+(+();-(+()))", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
            assertEquals("+(place_for_result())", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
            assertEquals("()", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

            val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
            assertEquals("" +
                    "result: '((a+b)^3+(a-b)^3-3*a)/(2*a^2+6*b^2-3-(-0))'\n" +
                    "expressionSubstitution.left: '(+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0)))))'\n" +
                    "expressionSubstitution.right: '(+(*(2;^(a;2));*(6;^(b;2));-(3);-(+(-(0)))))'\n" +
                    "originalExpression: '(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))))'\n" +
                    "originalExpressionChangingPart: '+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))'\n" +
                    "resultExpression: '(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(*(2;^(a;2));*(6;^(b;2));-(3);-(+(-(0))))))'\n" +
                    "resultExpressionChangingPart: '+(*(2;^(a;2));*(6;^(b;2));-(3);-(+(-(0))))'\n" +
                    "substitutionType: 'OpeningBrackets'\n" +
                    "priority: '50'\n" +
                    "\n" +
                    "result: '((a+b)^3+(a-b)^3-3*a)/(((2*a^2+6*b^2-3)-0-(-0))-(-0))'\n" +
                    "expressionSubstitution.left: '(+(*(2;^(a;2));*(6;^(b;2));-(3)))'\n" +
                    "expressionSubstitution.right: '(+(+(*(2;^(a;2));*(6;^(b;2));-(3));+(-(0));-(+(-(0)))))'\n" +
                    "originalExpression: '(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))))'\n" +
                    "originalExpressionChangingPart: '+(*(2;^(a;2));*(6;^(b;2));-(3))'\n" +
                    "resultExpression: '(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(0);-(+(-(0))));-(+(-(0))))))'\n" +
                    "resultExpressionChangingPart: '+(+(*(2;^(a;2));*(6;^(b;2));-(3));+(-(0));-(+(-(0))))'\n" +
                    "substitutionType: 'ComplicatingExtension'\n" +
                    "priority: '60'\n" +
                    "\n" +
                    "result: '((a+b)^3+(a-b)^3-3*a)/(-(-0)+(2*a^2+6*b^2-3))'\n" +
                    "expressionSubstitution.left: '(+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0)))))'\n" +
                    "expressionSubstitution.right: '(+(-(+(-(0)));+(*(2;^(a;2));*(6;^(b;2));-(3))))'\n" +
                    "originalExpression: '(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))))'\n" +
                    "originalExpressionChangingPart: '+(+(*(2;^(a;2));*(6;^(b;2));-(3));-(+(-(0))))'\n" +
                    "resultExpression: '(/(+(^(+(a;b);3);^(+(a;-(b));3);-(*(3;a)));+(-(+(-(0)));+(*(2;^(a;2));*(6;^(b;2));-(3)))))'\n" +
                    "resultExpressionChangingPart: '+(-(+(-(0)));+(*(2;^(a;2));*(6;^(b;2));-(3)))'\n" +
                    "substitutionType: 'ArgumentsPermutation'\n" +
                    "priority: '90'", result.joinToString(separator = "\n\n"))
        }
    }

    @Test
    fun failingTest4() {
        val expression = structureStringToExpression("(+(10;*((+(x;3));(+(x;-(3))));-(^(x;2))))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    10  :  [2]\n" +
                "    *  :  [3]\n" +
                "        :  [4]\n" +
                "        +  :  [5]\n" +
                "          x  :  [6]\n" +
                "          3  :  [7]\n" +
                "        :  [8]\n" +
                "        +  :  [9]\n" +
                "          x  :  [10]\n" +
                "          -  :  [11]\n" +
                "            3  :  [12]\n" +
                "    -  :  [13]\n" +
                "      ^  :  [14]\n" +
                "        x  :  [15]\n" +
                "        2  :  [16]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(8).toTypedArray()
        val genRes = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true)
    }

    @Test
    fun zeroComputationTest1() {
        val expression = structureStringToExpression("(and(a;b;1;c;1))")
        assertEquals("  :  [0]\n" +
                "  and  :  [1]\n" +
                "    a  :  [2]\n" +
                "    b  :  [3]\n" +
                "    1  :  [4]\n" +
                "    c  :  [5]\n" +
                "    1  :  [6]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,1).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true)
        assertEquals("" +
                "result: 'a&b&c'\n" +
                "expressionSubstitution.left: '(and(a;b;1;c;1))'\n" +
                "expressionSubstitution.right: '(and(a;b;c))'\n" +
                "originalExpression: '(and(a;b;1;c;1))'\n" +
                "originalExpressionChangingPart: '(and(a;b;1;c;1))'\n" +
                "resultExpression: '(and(a;b;c))'\n" +
                "resultExpressionChangingPart: '(and(a;b;c))'\n" +
                "substitutionType: 'ZeroComputation'\n" +
                "priority: '50'", result.filter { it.substitutionType == "ZeroComputation" }.joinToString(separator = "\n\n"))
    }

    @Test
    fun zeroComputationTest2() {
        val expression = structureStringToExpression("(+(-(0);-(b);0;c;-(0)))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    -  :  [2]\n" +
                "      0  :  [3]\n" +
                "    -  :  [4]\n" +
                "      b  :  [5]\n" +
                "    0  :  [6]\n" +
                "    c  :  [7]\n" +
                "    -  :  [8]\n" +
                "      0  :  [9]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,1).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true)
        assertEquals("" +
                "result: '-b+c'\n" +
                "expressionSubstitution.left: '(+(-(0);-(b);0;c;-(0)))'\n" +
                "expressionSubstitution.right: '(+(-(b);c))'\n" +
                "originalExpression: '(+(-(0);-(b);0;c;-(0)))'\n" +
                "originalExpressionChangingPart: '(+(-(0);-(b);0;c;-(0)))'\n" +
                "resultExpression: '(+(-(b);c))'\n" +
                "resultExpressionChangingPart: '(+(-(b);c))'\n" +
                "substitutionType: 'ZeroComputation'\n" +
                "priority: '50'", result.filter { it.substitutionType == "ZeroComputation" }.joinToString(separator = "\n\n"))
    }

    @Test
    fun zeroComputationTest3() {
        val expression = structureStringToExpression("(*(b;0;c))")
        assertEquals("  :  [0]\n" +
                "  *  :  [1]\n" +
                "    b  :  [2]\n" +
                "    0  :  [3]\n" +
                "    c  :  [4]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,1).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true)
        assertEquals("" +
                "result: '0'\n" +
                "expressionSubstitution.left: '(*(b;0;c))'\n" +
                "expressionSubstitution.right: '(0)'\n" +
                "originalExpression: '(*(b;0;c))'\n" +
                "originalExpressionChangingPart: '(*(b;0;c))'\n" +
                "resultExpression: '(0)'\n" +
                "resultExpressionChangingPart: '(0)'\n" +
                "substitutionType: 'ZeroComputation'\n" +
                "priority: '50'", result.filter { it.substitutionType == "ZeroComputation" }.joinToString(separator = "\n\n"))
    }

    @Test
    fun computationTest1() {
        val expression = structureStringToExpression("(^(9;/(1;2)))")
        val selectedNodeIds = listOf(1).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '3'\n" +
                "expressionSubstitution.left: '(^(9;/(1;2)))'\n" +
                "expressionSubstitution.right: '(3)'\n" +
                "originalExpression: '(^(9;/(1;2)))'\n" +
                "originalExpressionChangingPart: '^(9;/(1;2))'\n" +
                "resultExpression: '(3)'\n" +
                "resultExpressionChangingPart: '3'\n" +
                "substitutionType: 'SimpleComputation'\n" +
                "priority: '5'", result.filter { it.substitutionType == TREE_COMPUTATION_DEFAULT }.joinToString(separator = "\n\n"))
    }

    @Test
    fun computationTest2() {
        val expression = structureStringToExpression("(^(27;/(1;3)))")
        val selectedNodeIds = listOf(1).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '3'\n" +
                "expressionSubstitution.left: '(^(27;/(1;3)))'\n" +
                "expressionSubstitution.right: '(3)'\n" +
                "originalExpression: '(^(27;/(1;3)))'\n" +
                "originalExpressionChangingPart: '^(27;/(1;3))'\n" +
                "resultExpression: '(3)'\n" +
                "resultExpressionChangingPart: '3'\n" +
                "substitutionType: 'SimpleComputation'\n" +
                "priority: '5'", result.filter { it.substitutionType == TREE_COMPUTATION_DEFAULT }.joinToString(separator = "\n\n"))
    }

    @Test
    fun computationTest3() {
        val expression = structureStringToExpression("(+(2;3))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    2  :  [2]\n" +
                "    3  :  [3]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(1,2,3).toTypedArray()

        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("+(2;3)", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("+(2;3)", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("+(2;3)", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("+(2;3)", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("+(place_for_result())", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("()", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '5'\n" +
                "expressionSubstitution.left: '(+(2;3))'\n" +
                "expressionSubstitution.right: '(5)'\n" +
                "originalExpression: '(+(2;3))'\n" +
                "originalExpressionChangingPart: '+(2;3)'\n" +
                "resultExpression: '(5)'\n" +
                "resultExpressionChangingPart: '5'\n" +
                "substitutionType: 'SimpleComputation'\n" +
                "priority: '5'", result.filter { it.substitutionType == TREE_COMPUTATION_DEFAULT }.joinToString(separator = "\n\n"))
    }



    @Test
    fun noMultipliersSum() {
        val expression = structureStringToExpression("(+(+(a;a;a);1))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    +  :  [2]\n" +
                "      a  :  [3]\n" +
                "      a  :  [4]\n" +
                "      a  :  [5]\n" +
                "    1  :  [6]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,4,5).toTypedArray()

        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: 'a*(1+1+1)+1'\n" +
                "expressionSubstitution.left: '(+(a;a;a))'\n" +
                "expressionSubstitution.right: '(*(a;+(1;1;1)))'\n" +
                "originalExpression: '(+(+(a;a;a);1))'\n" +
                "originalExpressionChangingPart: '+(a;a;a)'\n" +
                "resultExpression: '(+(*(a;+(1;1;1));1))'\n" +
                "resultExpressionChangingPart: '*(a;+(1;1;1))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun zeroSumComputation() {
        val expression = structureStringToExpression("(+(+(0;0);1))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    +  :  [2]\n" +
                "      0  :  [3]\n" +
                "      0  :  [4]\n" +
                "    1  :  [5]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,4).toTypedArray()

        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '0+1'\n" +
                "expressionSubstitution.left: '(+(0;0))'\n" +
                "expressionSubstitution.right: '(0)'\n" +
                "originalExpression: '(+(+(0;0);1))'\n" +
                "originalExpressionChangingPart: '+(0;0)'\n" +
                "resultExpression: '(+(0;1))'\n" +
                "resultExpressionChangingPart: '0'\n" +
                "substitutionType: 'SimpleComputation'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: '0*(1+1)+1'\n" +
                "expressionSubstitution.left: '(+(0;0))'\n" +
                "expressionSubstitution.right: '(*(0;+(1;1)))'\n" +
                "originalExpression: '(+(+(0;0);1))'\n" +
                "originalExpressionChangingPart: '+(0;0)'\n" +
                "resultExpression: '(+(*(0;+(1;1));1))'\n" +
                "resultExpressionChangingPart: '*(0;+(1;1))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: '((0+0-0)+0)+1'\n" +
                "expressionSubstitution.left: '(0)'\n" +
                "expressionSubstitution.right: '(+(0;0;-(0)))'\n" +
                "originalExpression: '(+(+(0;0);1))'\n" +
                "originalExpressionChangingPart: '0'\n" +
                "resultExpression: '(+(+(+(0;0;-(0));0);1))'\n" +
                "resultExpressionChangingPart: '+(0;0;-(0))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun computationTest4() {
        val expression = structureStringToExpression("(/(*(96;9;512);*(36;24)))")
        val selectedNodeIds = listOf(3, 8).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '(4*9*512)/36'\n" +
                "expressionSubstitution.left: '(/(96;24))'\n" +
                "expressionSubstitution.right: '(4)'\n" +
                "originalExpression: '(/(*(96;9;512);*(36;24)))'\n" +
                "originalExpressionChangingPart: '/(*(96;9;512);*(36;24))'\n" +
                "resultExpression: '(/(*(4;9;512);36))'\n" +
                "resultExpressionChangingPart: '/(*(4;9;512);36)'\n" +
                "substitutionType: 'ReduceFraction'\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '((96*9*512)/(36*24))^1'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(^(a;1))'\n" +
                "originalExpression: '(/(*(96;9;512);*(36;24)))'\n" +
                "originalExpressionChangingPart: '/(*(96;9;512);*(36;24))'\n" +
                "resultExpression: '(^(/(*(96;9;512);*(36;24));1))'\n" +
                "resultExpressionChangingPart: '^(/(*(96;9;512);*(36;24));1)'\n" +
                "substitutionType: ''(a)'->'(^(a;1))''\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '((96+24-24)*9*512)/(36*24)'\n" +
                "expressionSubstitution.left: '(96)'\n" +
                "expressionSubstitution.right: '(+(96;24;-(24)))'\n" +
                "originalExpression: '(/(*(96;9;512);*(36;24)))'\n" +
                "originalExpressionChangingPart: '96'\n" +
                "resultExpression: '(/(*(+(96;24;-(24));9;512);*(36;24)))'\n" +
                "resultExpressionChangingPart: '+(96;24;-(24))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'\n" +
                "\n" +
                "result: '(((96*24)/24)*9*512)/(36*24)'\n" +
                "expressionSubstitution.left: '(96)'\n" +
                "expressionSubstitution.right: '(/(*(96;24);24))'\n" +
                "originalExpression: '(/(*(96;9;512);*(36;24)))'\n" +
                "originalExpressionChangingPart: '96'\n" +
                "resultExpression: '(/(*(/(*(96;24);24);9;512);*(36;24)))'\n" +
                "resultExpressionChangingPart: '/(*(96;24);24)'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'", result.joinToString(separator = "\n\n"))
    }


    @Test
    fun computationTest5() { // TODO: reproduce bug 5*(3*2-1) -> 5*(3*2)/5  and fix it
        val expression = structureStringToExpression("(*(5;+(*(3;2);-(1))))")
        assertEquals("  :  [0]\n" +
                "  *  :  [1]\n" +
                "    5  :  [2]\n" +
                "    +  :  [3]\n" +
                "      *  :  [4]\n" +
                "        3  :  [5]\n" +
                "        2  :  [6]\n" +
                "      -  :  [7]\n" +
                "        1  :  [8]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(1).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
    }


    @Test
    fun avoidDivisionOnZeroRules() {
        val expression = structureStringToExpression("(/(*(96;9;512);*(36;0)))")
        val selectedNodeIds = listOf(3, 8).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '((96*9*512)/(36*0))^1'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(^(a;1))'\n" +
                "originalExpression: '(/(*(96;9;512);*(36;0)))'\n" +
                "originalExpressionChangingPart: '/(*(96;9;512);*(36;0))'\n" +
                "resultExpression: '(^(/(*(96;9;512);*(36;0));1))'\n" +
                "resultExpressionChangingPart: '^(/(*(96;9;512);*(36;0));1)'\n" +
                "substitutionType: ''(a)'->'(^(a;1))''\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '((96+0-0)*9*512)/(36*0)'\n" +
                "expressionSubstitution.left: '(96)'\n" +
                "expressionSubstitution.right: '(+(96;0;-(0)))'\n" +
                "originalExpression: '(/(*(96;9;512);*(36;0)))'\n" +
                "originalExpressionChangingPart: '96'\n" +
                "resultExpression: '(/(*(+(96;0;-(0));9;512);*(36;0)))'\n" +
                "resultExpressionChangingPart: '+(96;0;-(0))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun avoidHiddenDivisionOnZeroRules() {
        val expression = structureStringToExpression("(/(*(96;+(^(x;2);-(^(x;2)));512);*(36;4)))")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    *  :  [2]\n" +
                "      96  :  [3]\n" +
                "      +  :  [4]\n" +
                "        ^  :  [5]\n" +
                "          x  :  [6]\n" +
                "          2  :  [7]\n" +
                "        -  :  [8]\n" +
                "          ^  :  [9]\n" +
                "            x  :  [10]\n" +
                "            2  :  [11]\n" +
                "      512  :  [12]\n" +
                "    *  :  [13]\n" +
                "      36  :  [14]\n" +
                "      4  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(14, 4).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '((96*(x^2-x^2)*512)/(36*4))^1'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(^(a;1))'\n" +
                "originalExpression: '(/(*(96;+(^(x;2);-(^(x;2)));512);*(36;4)))'\n" +
                "originalExpressionChangingPart: '/(*(96;+(^(x;2);-(^(x;2)));512);*(36;4))'\n" +
                "resultExpression: '(^(/(*(96;+(^(x;2);-(^(x;2)));512);*(36;4));1))'\n" +
                "resultExpressionChangingPart: '^(/(*(96;+(^(x;2);-(^(x;2)));512);*(36;4));1)'\n" +
                "substitutionType: ''(a)'->'(^(a;1))''\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '(96*(x^2-x^2)*512)/((36+(x^2-x^2)-(x^2-x^2))*4)'\n" +
                "expressionSubstitution.left: '(36)'\n" +
                "expressionSubstitution.right: '(+(36;+(^(x;2);-(^(x;2)));-(+(^(x;2);-(^(x;2))))))'\n" +
                "originalExpression: '(/(*(96;+(^(x;2);-(^(x;2)));512);*(36;4)))'\n" +
                "originalExpressionChangingPart: '36'\n" +
                "resultExpression: '(/(*(96;+(^(x;2);-(^(x;2)));512);*(+(36;+(^(x;2);-(^(x;2)));-(+(^(x;2);-(^(x;2)))));4)))'\n" +
                "resultExpressionChangingPart: '+(36;+(^(x;2);-(^(x;2)));-(+(^(x;2);-(^(x;2)))))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun avoidHiddenDivisionOnZeroRulesInCustomRules() {
        val expression = structureStringToExpression("(+(^(x;2);-(^(x;2))))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    ^  :  [2]\n" +
                "      x  :  [3]\n" +
                "      2  :  [4]\n" +
                "    -  :  [5]\n" +
                "      ^  :  [6]\n" +
                "        x  :  [7]\n" +
                "        2  :  [8]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(1).toTypedArray()
        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
            listOf(expressionSubstitutionFromStructureStrings("(a)", "(/(1;/(1;a)))", simpleAdditional = true, isExtending = true)).toTypedArray()
        ), withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("", result.joinToString(separator = "\n\n"))
    }


    @Test
    fun commutativeOperationExpansionTest() {
        val expression = structureStringToExpression("(*(6;*(a;/(1;2))))")
        assertEquals("  :  [0]\n" +
                "  *  :  [1]\n" +
                "    6  :  [2]\n" +
                "    *  :  [3]\n" +
                "      a  :  [4]\n" +
                "      /  :  [5]\n" +
                "        1  :  [6]\n" +
                "        2  :  [7]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(1,2).toTypedArray()
            val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
            assertEquals("" +
                    "result: '6*a*(1/2)'\n" +
                    "expressionSubstitution.left: '(*(6;*(a;/(1;2))))'\n" +
                    "expressionSubstitution.right: '(*(6;a;/(1;2)))'\n" +
                    "originalExpression: '(*(6;*(a;/(1;2))))'\n" +
                    "originalExpressionChangingPart: '*(6;*(a;/(1;2)))'\n" +
                    "resultExpression: '(*(6;a;/(1;2)))'\n" +
                    "resultExpressionChangingPart: '*(6;a;/(1;2))'\n" +
                    "substitutionType: 'OpeningBrackets'\n" +
                    "priority: '50'", result.filter { it.substitutionType == "OpeningBrackets" }.joinToString(separator = "\n\n"))
        }
        if (true) {
            val selectedNodeIds = listOf(1).toTypedArray()
            val result = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
            assertEquals("" +
                    "result: '6*a*(1/2)'\n" +
                    "expressionSubstitution.left: '(*(6;*(a;/(1;2))))'\n" +
                    "expressionSubstitution.right: '(*(6;a;/(1;2)))'\n" +
                    "originalExpression: '(*(6;*(a;/(1;2))))'\n" +
                    "originalExpressionChangingPart: '*(6;*(a;/(1;2)))'\n" +
                    "resultExpression: '(*(6;a;/(1;2)))'\n" +
                    "resultExpressionChangingPart: '*(6;a;/(1;2))'\n" +
                    "substitutionType: 'OpeningBrackets'\n" +
                    "priority: '50'\n" +
                    "\n" +
                    "result: '(6*(a*(1/2)))^1'\n" +
                    "expressionSubstitution.left: '(a)'\n" +
                    "expressionSubstitution.right: '(^(a;1))'\n" +
                    "originalExpression: '(*(6;*(a;/(1;2))))'\n" +
                    "originalExpressionChangingPart: '*(6;*(a;/(1;2)))'\n" +
                    "resultExpression: '(^(*(6;*(a;/(1;2)));1))'\n" +
                    "resultExpressionChangingPart: '^(*(6;*(a;/(1;2)));1)'\n" +
                    "substitutionType: ''(a)'->'(^(a;1))''\n" +
                    "priority: '50'", result.joinToString(separator = "\n\n"))
        }
    }



    @Test
    fun diffDivisionTest() {
        val expression = structureStringToExpression("(+(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))));2))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    +  :  [2]\n" +
                "      /  :  [3]\n" +
                "        ^  :  [4]\n" +
                "          +  :  [5]\n" +
                "            x  :  [6]\n" +
                "            -  :  [7]\n" +
                "              y  :  [8]\n" +
                "          2  :  [9]\n" +
                "        *  :  [10]\n" +
                "          4  :  [11]\n" +
                "          x  :  [12]\n" +
                "          y  :  [13]\n" +
                "      -  :  [14]\n" +
                "        /  :  [15]\n" +
                "          ^  :  [16]\n" +
                "            +  :  [17]\n" +
                "              x  :  [18]\n" +
                "              y  :  [19]\n" +
                "            2  :  [20]\n" +
                "          *  :  [21]\n" +
                "            4  :  [22]\n" +
                "            x  :  [23]\n" +
                "            y  :  [24]\n" +
                "    2  :  [25]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2).toTypedArray()

        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("+()", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals(null, substitutionSelectionData.notSelectedSubtreeTopArguments)

        val result = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals("" +
                "result: '((x-y)^2-(x+y)^2)/(4*x*y)+2'\n" +
                "expressionSubstitution.left: '(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y)))))'\n" +
                "expressionSubstitution.right: '(/(+(^(+(x;-(y));2);-(^(+(x;y);2)));*(4;x;y)))'\n" +
                "originalExpression: '(+(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))));2))'\n" +
                "originalExpressionChangingPart: '+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))))'\n" +
                "resultExpression: '(+(/(+(^(+(x;-(y));2);-(^(+(x;y);2)));*(4;x;y));2))'\n" +
                "resultExpressionChangingPart: '/(+(^(+(x;-(y));2);-(^(+(x;y);2)));*(4;x;y))'\n" +
                "substitutionType: 'ReduceArithmetic'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: '(x-y)^2/(4*x*y)-(x+y)^2/(4*x*y)+2'\n" +
                "expressionSubstitution.left: '(+(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))));2))'\n" +
                "expressionSubstitution.right: '(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y)));2))'\n" +
                "originalExpression: '(+(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))));2))'\n" +
                "originalExpressionChangingPart: '+(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))));2)'\n" +
                "resultExpression: '(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y)));2))'\n" +
                "resultExpressionChangingPart: '+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y)));2)'\n" +
                "substitutionType: 'ParentBracketsExpansion'\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '((x-y)^2/(4*x*y)-(x+y)^2/(4*x*y))^1+2'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(^(a;1))'\n" +
                "originalExpression: '(+(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))));2))'\n" +
                "originalExpressionChangingPart: '+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))))'\n" +
                "resultExpression: '(+(^(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))));1);2))'\n" +
                "resultExpressionChangingPart: '^(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))));1)'\n" +
                "substitutionType: ''(a)'->'(^(a;1))''\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '-(-(x-y)^2/(4*x*y)+(x+y)^2/(4*x*y))+2'\n" +
                "expressionSubstitution.left: '(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y)))))'\n" +
                "expressionSubstitution.right: '(+(-(+(-(/(^(+(x;-(y));2);*(4;x;y)));/(^(+(x;y);2);*(4;x;y))))))'\n" +
                "originalExpression: '(+(+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))));2))'\n" +
                "originalExpressionChangingPart: '+(/(^(+(x;-(y));2);*(4;x;y));-(/(^(+(x;y);2);*(4;x;y))))'\n" +
                "resultExpression: '(+(-(+(-(/(^(+(x;-(y));2);*(4;x;y)));/(^(+(x;y);2);*(4;x;y))));2))'\n" +
                "resultExpressionChangingPart: '+(-(+(-(/(^(+(x;-(y));2);*(4;x;y)));/(^(+(x;y);2);*(4;x;y)))))'\n" +
                "substitutionType: 'MinusFromBrackets'\n" +
                "priority: '61'", result.joinToString(separator = "\n\n"))
    }
}