package mathhelper.twf.api

import org.junit.Test
import kotlin.test.assertEquals

class SolutionVerificationTest {
    @Test
    fun checkSolutionInTexDemo() {
        val wellKnownFunctionsString = ";;;0;;;;;;1;;;+;;;-1;;;-;;;-1;;;*;;;-1;;;/;;;-1;;;^;;;-1"
        val startExpressionTex = "\\sin^2\\left(\\frac{3\\cdot \\pi }{2}+x\\right)\\cdot \\left(\\left(\\cos \\left(x\\right)+\\sin \\left(x\\right)\\right)^2-\\sin \\left(2\\cdot x\\right)+tg^2\\left(x\\right)\\right)"// "\\sin ^2\\left(\\frac{3\\cdot \\pi }{2}+x\\right)\\cdot \\left(\\left(\\cos \\left(x\\right)+\\sin \\left(x\\right)\\right)^2-\\sin \\left(2\\cdot x\\right)+tg^2\\left(x\\right)\\right)"
        val startExpressionSS = expressionToStructureString(stringToExpression(startExpressionTex, format = "Tex"))
        assertEquals("(*(^(sin(+(/(*(3;π);2);x));2);+(^(+(cos(x);sin(x));2);-(sin(*(2;x)));^(tg(x);2))))", startExpressionSS)
        val res = checkSolutionInTex(
                "\\textcolor{purple}{\\sin ^2\\left(\\frac{3\\cdot \\pi }{2}+x\\right)\\cdot \\left(\\left(\\cos \\left(x\\right)+\\sin \\left(x\\right)\\right)^2-\\sin \\left(2\\cdot x\\right)+tg^2\\left(x\\right)\\right)=1}",
                startExpressionIdentifier = startExpressionSS,
                targetFactPattern = "",
                endExpressionIdentifier="(1)",
                comparisonSign = "=",
                wellKnownFunctionsString = wellKnownFunctionsString,
                unlimitedWellKnownFunctionsString = wellKnownFunctionsString,
                rulePacks = arrayOf("Trigonometry")
        )

        assertEquals("\\textcolor{purple}{\\sin ^2\\left(\\frac{3\\cdot \\pi }{2}+x\\right)\\cdot \\left(\\left(\\cos \\left(x\\right)+\\sin \\left(x\\right)\\right)^2-\\sin \\left(2\\cdot x\\right)+tg^2\\left(x\\right)\\right)\\textcolor{red}{=}1}", res.validatedSolution)
        assertEquals("Error: Unclear transformation between '(*(^(sin(+(/(*(3;π);2);x));2);+(^(+(cos(x);sin(x));2);-(sin(*(2;x)));^(tg(x);2))))' and '(1)' ", res.errorMessage)
    }
}