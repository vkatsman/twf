package mathhelper.twf.api

import mathhelper.twf.config.ComparisonType
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks.Companion.defaultRulePacks
import mathhelper.twf.expressiontree.ExpressionSubstitution
import mathhelper.twf.expressiontree.NodeType
import mathhelper.twf.expressiontree.addRootNodeToExpression
import org.junit.Test

import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class CompiledConfigurationBySubjectAPIKtTest {

    @Test
    fun testCorrectionOfDefinedRules() {
        val compiledConfiguration = createConfigurationFromRulePacksAndParams()

        val allNotHardcodedRules = mutableListOf<ExpressionSubstitution>()
        for (rulePack in defaultRulePacks) {
            if (rulePack.rules != null) {
                for (rule in rulePack.rules!!) {
                    if (rule.leftStructureString?.isNotBlank() == true && rule.leftStructureString?.isNotBlank() == true) {
                        allNotHardcodedRules.add(expressionSubstitutionFromRuleITR(rule))
                    }
                }
            }
        }
        assertTrue(allNotHardcodedRules.size > 913)

        var incorrectRulesCount = 0
        for (rule in allNotHardcodedRules) {
            if (rule.left.nodeType == NodeType.ERROR || rule.right.nodeType == NodeType.ERROR) {
                incorrectRulesCount++
                println("Syntax error while parsing '${rule.left}' -> '${rule.right}'")
            } else if (rule.left.nodeType != NodeType.EMPTY || rule.right.nodeType != NodeType.EMPTY) {
                if (!rule.basedOnTaskContext) {
                    if (!compiledConfiguration.factComparator.expressionComparator.fastProbabilityCheckOnIncorrectTransformation(addRootNodeToExpression(rule.left), addRootNodeToExpression(rule.right), ComparisonType.EQUAL)) {
                        incorrectRulesCount++
                        println("'${rule.left}' != '${rule.right}'")
                    }
                }
            }
        }
        assertTrue(incorrectRulesCount <= 25)
    }

    @Test
    fun createRuleCombinationPackConfiguration() {
        val compiledConfiguration = createConfigurationFromRulePacksAndParams(actualRulePackCodesBySubject(arrayOf("Combinatorics", "Logic", "LogicAbsorption", "LogicResolution", "PhysicsSimpleMoving", "PhysicsCircleMoving", "PhysicsNuclear", "PhysicsMolecular", "PhysicsElectrodynamics")))
        assertEquals(565, compiledConfiguration.compiledExpressionTreeTransformationRules.size)
        assertEquals(7, compiledConfiguration.compiledExpressionSimpleAdditionalTreeTransformationRules.size)
    }

    @Test
    fun createLogarithmRulePackConfiguration() {
        val compiledConfiguration = createConfigurationFromRulePacksAndParams(listOf("Logarithm").toTypedArray())
        assertEquals(169, compiledConfiguration.compiledExpressionTreeTransformationRules.size)
        assertEquals(7, compiledConfiguration.compiledExpressionSimpleAdditionalTreeTransformationRules.size)
    }
}