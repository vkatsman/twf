package mathhelper.twf.api

import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.expressiontree.ExpressionSubstitution
import mathhelper.twf.expressiontree.ForwardInverseExtension
import mathhelper.twf.expressiontree.ForwardInverseExtensionType
import mathhelper.twf.expressiontree.TREE_COMPUTATION_DEFAULT
import org.junit.Ignore
import org.junit.Test

import kotlin.test.assertEquals
import mathhelper.twf.substitutiontests.parseStringExpression

internal class ExpressionSubstitutionsAPIKtTest {

    @Test
    @Ignore
    fun createExpressionSubstitutionsCompiledConfigurationSample (){
        val expressionSubstitutions = listOf(
                expressionSubstitutionFromStructureStrings("(+(^(a;2);-(^(b;2))))","(*(+(a;-(b));+(a;b)))", matchJumbledAndNested = true, priority = 40, code = "Short Multiplication", nameRu = "Сокращенное Умножение"),
                expressionSubstitutionFromStructureStrings("(x)","(^(x;1))", simpleAdditional= true, isExtending = true, priority = 40),
                expressionSubstitutionFromStructureStrings("(sin(a))","(0.4)", basedOnTaskContext = true, priority = 10),
                expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic", priority = 5),
                expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                expressionSubstitutionFromStructureStrings(code = "MinusInOutBrackets", priority = 61)
        ).toTypedArray()

        // creation
        val compiledConfigurationWithoutAdditionalParams: CompiledConfiguration = createCompiledConfigurationFromExpressionSubstitutionsAndParams(expressionSubstitutions)
        val compiledConfigurationWithAdditionalParams: CompiledConfiguration = createCompiledConfigurationFromExpressionSubstitutionsAndParams(expressionSubstitutions,
                mapOf(Pair("simpleComputationRuleParamsMaxCalcComplexity", "5"))
        )

        // using
        val expression = parseStringExpression("((sin(x))^2+x+(sin(x))^2+(cos(x))^2)^2")
        val selectedNodeIds = listOf(3,12).toTypedArray()
        val foundSubstitutionsDefaultMaxCalcComplexity = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, compiledConfigurationWithoutAdditionalParams, withReadyApplicationResult = true)
        val foundSubstitutionsMaxCalcComplexity5 = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, compiledConfigurationWithAdditionalParams, withReadyApplicationResult = true)
    }

    @Test
    @Ignore
    fun findApplicableSubstitutionsInSelectedPlaceSample() {
        val expression = parseStringExpression("((sin(x))^2+x+(sin(x))^2+(cos(x))^2)^2")
        assertEquals("  :  [0]\n" +
                "  ^  :  [1]\n" +
                "    +  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        sin  :  [4]\n" +
                "          x  :  [5]\n" +
                "        2  :  [6]\n" +
                "      x  :  [7]\n" +
                "      ^  :  [8]\n" +
                "        sin  :  [9]\n" +
                "          x  :  [10]\n" +
                "        2  :  [11]\n" +
                "      ^  :  [12]\n" +
                "        cos  :  [13]\n" +
                "          x  :  [14]\n" +
                "        2  :  [15]\n" +
                "    2  :  [16]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,12).toTypedArray()
        val compiledConfiguration = CompiledConfiguration().apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(sin(x))))", "(sin(+(-(x))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(sin(x))", "(+(-(sin(+(-(x))))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(cos(x))))", "(cos(+(π;-(x))))"))

            compiledExpressionSimpleAdditionalTreeTransformationRules.clear()
            compiledExpressionSimpleAdditionalTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(x)", "(^(x;1))"))
        }
        val res = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, compiledConfiguration, withReadyApplicationResult = true)
        res.firstOrNull()?.expressionSubstitution // substitution rule
        res.firstOrNull()?.resultExpression // result expression after one of substitutions
    }

    @Test
    fun findApplicableSubstitutionsInSelectedNumberSample() {
        val expression = parseStringExpression("((sin(x))^2+11.11+(cos(x))^2)^2")
        assertEquals("  :  [0]\n" +
                "  ^  :  [1]\n" +
                "    +  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        sin  :  [4]\n" +
                "          x  :  [5]\n" +
                "        2  :  [6]\n" +
                "      11.11  :  [7]\n" +
                "      ^  :  [8]\n" +
                "        cos  :  [9]\n" +
                "          x  :  [10]\n" +
                "        2  :  [11]\n" +
                "    2  :  [12]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(7).toTypedArray()
        val res = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds,createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT),
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization")
                ).toTypedArray()
        ), withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        assertEquals(3, res.size)
        assertEquals(3, res.filter { it.substitutionType == "NumberTransformation" }.size)
        assertEquals("(11.11)", res[0].expressionSubstitution.left.toString())
        assertEquals("(11.11)", res[1].expressionSubstitution.left.toString())
        assertEquals("(11.11)", res[2].expressionSubstitution.left.toString())
        assertEquals("(/(1111;100))", res[0].expressionSubstitution.right.toString())
        assertEquals("(+(10.11;1))", res[1].expressionSubstitution.right.toString())
        assertEquals("(+(12.11;-(1)))", res[2].expressionSubstitution.right.toString())

        assertEquals("11.11", res[0].originalExpressionChangingPart.toString())
        assertEquals("(^(+(^(sin(x);2);/(1111;100);^(cos(x);2));2))", res[0].resultExpression.toString())
        assertEquals("/(1111;100)", res[0].resultExpressionChangingPart.toString())

        assertEquals("11.11", res[1].originalExpressionChangingPart.toString())
        assertEquals("(^(+(^(sin(x);2);+(10.11;1);^(cos(x);2));2))", res[1].resultExpression.toString())
        assertEquals("+(10.11;1)", res[1].resultExpressionChangingPart.toString())

        assertEquals("11.11", res[2].originalExpressionChangingPart.toString())
        assertEquals("(^(+(^(sin(x);2);+(12.11;-(1));^(cos(x);2));2))", res[2].resultExpression.toString())
        assertEquals("+(12.11;-(1))", res[2].resultExpressionChangingPart.toString())
    }

    @Test
    fun findApplicableSubstitutionsByTwoPlacesSample() {
        val expression = parseStringExpression("((sin(x))^2+11.11+(cos(x))^2)^2")
        assertEquals("  :  [0]\n" +
                "  ^  :  [1]\n" +
                "    +  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        sin  :  [4]\n" +
                "          x  :  [5]\n" +
                "        2  :  [6]\n" +
                "      11.11  :  [7]\n" +
                "      ^  :  [8]\n" +
                "        cos  :  [9]\n" +
                "          x  :  [10]\n" +
                "        2  :  [11]\n" +
                "    2  :  [12]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(4,8).toTypedArray()
        val res = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds,createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ), withFullExpressionChangingPart = false)
        assertEquals(3, res.size)
        assertEquals(2, res.filter { it.substitutionType == "ComplicatingExtension" }.size)
        assertEquals("(sin(x))", res[0].expressionSubstitution.left.toString())
        assertEquals("(+(sin(x);^(cos(x);2);-(^(cos(x);2))))", res[0].expressionSubstitution.right.toString())
        assertEquals("sin(x)", res[0].originalExpressionChangingPart.toString())
        assertEquals("(^(+(^(+(sin(x);^(cos(x);2);-(^(cos(x);2)));2);11.11;^(cos(x);2));2))", res[0].resultExpression.toString())
        assertEquals("+(sin(x);^(cos(x);2);-(^(cos(x);2)))", res[0].resultExpressionChangingPart.toString())

        assertEquals("(sin(x))", res[1].expressionSubstitution.left.toString())
        assertEquals("(/(*(sin(x);^(cos(x);2));^(cos(x);2)))", res[1].expressionSubstitution.right.toString())
        assertEquals("sin(x)", res[1].originalExpressionChangingPart.toString())
        assertEquals("(^(+(^(/(*(sin(x);^(cos(x);2));^(cos(x);2));2);11.11;^(cos(x);2));2))", res[1].resultExpression.toString())
        assertEquals("/(*(sin(x);^(cos(x);2));^(cos(x);2))", res[1].resultExpressionChangingPart.toString())

        assertEquals("(+(^(sin(x);2);^(cos(x);2)))", res[2].expressionSubstitution.left.toString())
        assertEquals("(+(+(^(sin(x);2);^(cos(x);2))))", res[2].expressionSubstitution.right.toString())
        assertEquals("+(^(sin(x);2);^(cos(x);2))", res[2].originalExpressionChangingPart.toString())
        assertEquals("(^(+(+(^(sin(x);2);^(cos(x);2));11.11);2))", res[2].resultExpression.toString())
        assertEquals("+(+(^(sin(x);2);^(cos(x);2)))", res[2].resultExpressionChangingPart.toString())
    }

    @Test
    fun findApplicableSubstitutionsByTwoPlacesOnSameLevelSample() {
        val expression = parseStringExpression("(((sin(x))^2+11.11)+(cos(x))^2)^2")
        assertEquals("  :  [0]\n" +
                "  ^  :  [1]\n" +
                "    +  :  [2]\n" +
                "      +  :  [3]\n" +
                "        ^  :  [4]\n" +
                "          sin  :  [5]\n" +
                "            x  :  [6]\n" +
                "          2  :  [7]\n" +
                "        11.11  :  [8]\n" +
                "      ^  :  [9]\n" +
                "        cos  :  [10]\n" +
                "          x  :  [11]\n" +
                "        2  :  [12]\n" +
                "    2  :  [13]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(4,9).toTypedArray()
        val res = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds,createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ), withReadyApplicationResult = false)
        assertEquals("" +
                "result: '(sin(x)^2+11.11+cos(x)^2)^2'\n" +
                "expressionSubstitution.left: '(+(+(^(sin(x);2);11.11);^(cos(x);2)))'\n" +
                "expressionSubstitution.right: '(+(^(sin(x);2);11.11;^(cos(x);2)))'\n" +
                "originalExpression: '(^(+(+(^(sin(x);2);11.11);^(cos(x);2));2))'\n" +
                "originalExpressionChangingPart: '(+(+(^(sin(x);2);11.11);^(cos(x);2)))'\n" +
                "resultExpression: '(^(+(^(sin(x);2);11.11;^(cos(x);2));2))'\n" +
                "resultExpressionChangingPart: '(+(^(sin(x);2);11.11;^(cos(x);2)))'\n" +
                "substitutionType: 'OpeningBrackets'\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '((cos(x)^2+11.11)+sin(x)^2)^2'\n" +
                "expressionSubstitution.left: '(+(+(^(sin(x);2);11.11);^(cos(x);2)))'\n" +
                "expressionSubstitution.right: '(+(+(^(cos(x);2);11.11);^(sin(x);2)))'\n" +
                "originalExpression: '(^(+(+(^(sin(x);2);11.11);^(cos(x);2));2))'\n" +
                "originalExpressionChangingPart: '(+(+(^(sin(x);2);11.11);^(cos(x);2)))'\n" +
                "resultExpression: '(^(+(+(^(cos(x);2);11.11);^(sin(x);2));2))'\n" +
                "resultExpressionChangingPart: '(+(+(^(cos(x);2);11.11);^(sin(x);2)))'\n" +
                "substitutionType: 'Swap'\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '(((sin(x)^2+cos(x)^2-cos(x)^2)+11.11)+cos(x)^2)^2'\n" +
                "expressionSubstitution.left: '(^(sin(x);2))'\n" +
                "expressionSubstitution.right: '(+(^(sin(x);2);^(cos(x);2);-(^(cos(x);2))))'\n" +
                "originalExpression: '(^(+(+(^(sin(x);2);11.11);^(cos(x);2));2))'\n" +
                "originalExpressionChangingPart: '(^(sin(x);2))'\n" +
                "resultExpression: '(^(+(+(+(^(sin(x);2);^(cos(x);2);-(^(cos(x);2)));11.11);^(cos(x);2));2))'\n" +
                "resultExpressionChangingPart: '(+(^(sin(x);2);^(cos(x);2);-(^(cos(x);2))))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'\n" +
                "\n" +
                "result: '(((sin(x)^2*cos(x)^2)/cos(x)^2+11.11)+cos(x)^2)^2'\n" +
                "expressionSubstitution.left: '(^(sin(x);2))'\n" +
                "expressionSubstitution.right: '(/(*(^(sin(x);2);^(cos(x);2));^(cos(x);2)))'\n" +
                "originalExpression: '(^(+(+(^(sin(x);2);11.11);^(cos(x);2));2))'\n" +
                "originalExpressionChangingPart: '(^(sin(x);2))'\n" +
                "resultExpression: '(^(+(+(/(*(^(sin(x);2);^(cos(x);2));^(cos(x);2));11.11);^(cos(x);2));2))'\n" +
                "resultExpressionChangingPart: '(/(*(^(sin(x);2);^(cos(x);2));^(cos(x);2)))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'\n" +
                "\n" +
                "result: '((sin(x)^2+cos(x)^2)+11.11)^2'\n" +
                "expressionSubstitution.left: '(+(^(sin(x);2);^(cos(x);2)))'\n" +
                "expressionSubstitution.right: '(+(+(^(sin(x);2);^(cos(x);2))))'\n" +
                "originalExpression: '(^(+(+(^(sin(x);2);11.11);^(cos(x);2));2))'\n" +
                "originalExpressionChangingPart: '(+(^(sin(x);2);^(cos(x);2)))'\n" +
                "resultExpression: '(^(+(+(^(sin(x);2);^(cos(x);2));11.11);2))'\n" +
                "resultExpressionChangingPart: '(+(+(^(sin(x);2);^(cos(x);2))))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '90'", res.joinToString(separator = "\n\n"))
    }

    @Test
    fun applySwapSubstitutionInSelectedPlacesImpossible() {
        val expression = parseStringExpression("(((sin(x))^2+11.11)+(cos(x))^2)^2")
        assertEquals("  :  [0]\n" +
                "  ^  :  [1]\n" +
                "    +  :  [2]\n" +
                "      +  :  [3]\n" +
                "        ^  :  [4]\n" +
                "          sin  :  [5]\n" +
                "            x  :  [6]\n" +
                "          2  :  [7]\n" +
                "        11.11  :  [8]\n" +
                "      ^  :  [9]\n" +
                "        cos  :  [10]\n" +
                "          x  :  [11]\n" +
                "        2  :  [12]\n" +
                "    2  :  [13]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(4,9).toTypedArray()
        val res = applySubstitutionInSelectedPlace(expression, selectedNodeIds, expressionSubstitutionFromStructureStrings("(+(+(^(sin(x);2);11.11);^(cos(x);2)))", "(+(+(^(cos(x);2);11.11);^(sin(x);2)))"),
                CompiledConfiguration())
        assertEquals(null, res)
    }

    @Test
    fun applySubstitutionInSelectedPlacesSample() {
        val expression = parseStringExpression("((sin(x))^2+x+(sin(x))^2+(cos(x))^2)^2")
        assertEquals("  :  [0]\n" +
                "  ^  :  [1]\n" +
                "    +  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        sin  :  [4]\n" +
                "          x  :  [5]\n" +
                "        2  :  [6]\n" +
                "      x  :  [7]\n" +
                "      ^  :  [8]\n" +
                "        sin  :  [9]\n" +
                "          x  :  [10]\n" +
                "        2  :  [11]\n" +
                "      ^  :  [12]\n" +
                "        cos  :  [13]\n" +
                "          x  :  [14]\n" +
                "        2  :  [15]\n" +
                "    2  :  [16]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3,12).toTypedArray()
        val res = applySubstitutionInSelectedPlace(expression, selectedNodeIds, expressionSubstitutionFromStructureStrings("(+(^(sin(a);2);^(cos(a);2)))", "(1)"),
                CompiledConfiguration())
        assertEquals("(^(+(1;x;^(sin(x);2));2))", res.toString())
    }

    @Test
    fun applySubstitutionInSelectedPlaceTopNodeSample() {
        val expression = parseStringExpression("((sin(x))^2+(cos(x))^2)^2")
        assertEquals("  :  [0]\n" +
                "  ^  :  [1]\n" +
                "    +  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        sin  :  [4]\n" +
                "          x  :  [5]\n" +
                "        2  :  [6]\n" +
                "      ^  :  [7]\n" +
                "        cos  :  [8]\n" +
                "          x  :  [9]\n" +
                "        2  :  [10]\n" +
                "    2  :  [11]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2).toTypedArray()
        val res = applySubstitutionInSelectedPlace(expression, selectedNodeIds, expressionSubstitutionFromStructureStrings("(+(^(sin(a);2);^(cos(a);2)))", "(1)"),
                CompiledConfiguration())
        assertEquals("(^(1;2))", res.toString())
    }

    @Test
    fun applySubstitutionInSelectedOrderNodeSample() {
        val expression = parseStringExpression("a^2+c-2*a*b+b^2")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    ^  :  [2]\n" +
                "      a  :  [3]\n" +
                "      2  :  [4]\n" +
                "    c  :  [5]\n" +
                "    -  :  [6]\n" +
                "      *  :  [7]\n" +
                "        2  :  [8]\n" +
                "        a  :  [9]\n" +
                "        b  :  [10]\n" +
                "    ^  :  [11]\n" +
                "      b  :  [12]\n" +
                "      2  :  [13]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(11, 6, 2).toTypedArray()
        val res = applySubstitutionInSelectedPlace(expression, selectedNodeIds, expressionSubstitutionFromStructureStrings("(+(^(a;2);-(*(2;a;b));^(b;2)))", "(^(+(a;-(b));2))", matchJumbledAndNested = true),
                CompiledConfiguration())
        assertEquals("(+(^(+(b;-(a));2);c))", res.toString())
    }

    @Test
    fun generateAndApplySubstitutionInSelectedMinusComputationNode() {
        val expression = parseStringExpression("-36*(-1/4)*1")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    -  :  [2]\n" +
                "      *  :  [3]\n" +
                "        36  :  [4]\n" +
                "        +  :  [5]\n" +
                "          -  :  [6]\n" +
                "            /  :  [7]\n" +
                "              1  :  [8]\n" +
                "              4  :  [9]\n" +
                "        1  :  [10]\n", expression.toStringsWithNodeIds())
        assertEquals("(+(-(*(36;+(-(/(1;4)));1))))", expression.toString())
        val selectedNodeIds = listOf(3).toTypedArray()
        val genRes = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds,createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"), expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ), withReadyApplicationResult = false).filter { it.substitutionType == TREE_COMPUTATION_DEFAULT }
        assertEquals(1, genRes.size)
        assertEquals("(*(36;+(-(/(1;4)));1))", genRes[0].expressionSubstitution.left.toString())
        assertEquals("(+(-(9)))", genRes[0].expressionSubstitution.right.toString())
        assertEquals("(*(36;+(-(/(1;4)));1))", genRes[0].originalExpressionChangingPart.toString())
        assertEquals("(+(-(+(-(9)))))", genRes[0].resultExpression.toString())
        assertEquals("(+(-(9)))", genRes[0].resultExpressionChangingPart.toString())
        val res = applySubstitutionInSelectedPlace(expression, selectedNodeIds, expressionSubstitutionFromStructureStrings(genRes[0].expressionSubstitution.left.toString(), genRes[0].expressionSubstitution.right.toString()),
                CompiledConfiguration())
        assertEquals("(+(-(+(-(9)))))", res.toString())
    }

    @Test
    fun generateAndApplySubstitutionInSelectedMinusComputationNodeWithVariables() {
        val expression = parseStringExpression("-36*a*((a*b)/(c*d))*(-1/4)*1")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    -  :  [2]\n" +
                "      *  :  [3]\n" +
                "        36  :  [4]\n" +
                "        a  :  [5]\n" +
                "        /  :  [6]\n" +
                "          *  :  [7]\n" +
                "            a  :  [8]\n" +
                "            b  :  [9]\n" +
                "          *  :  [10]\n" +
                "            c  :  [11]\n" +
                "            d  :  [12]\n" +
                "        +  :  [13]\n" +
                "          -  :  [14]\n" +
                "            /  :  [15]\n" +
                "              1  :  [16]\n" +
                "              4  :  [17]\n" +
                "        1  :  [18]\n", expression.toStringsWithNodeIds())
        assertEquals("(+(-(*(36;a;/(*(a;b);*(c;d));+(-(/(1;4)));1))))", expression.toString())
        val selectedNodeIds = listOf(4,15,18).toTypedArray()
        val genRes = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds,createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"), expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ), withReadyApplicationResult = false).filter { it.substitutionType == TREE_COMPUTATION_DEFAULT }
        assertEquals(1, genRes.size)
        assertEquals("(*(36;+(-(/(1;4)));1))", genRes[0].expressionSubstitution.left.toString())
        assertEquals("(+(-(9)))", genRes[0].expressionSubstitution.right.toString())
        assertEquals("(*(36;+(-(/(1;4)));1))", genRes[0].originalExpressionChangingPart.toString())
        assertEquals("(+(-(*(+(-(9));a;/(*(a;b);*(c;d))))))", genRes[0].resultExpression.toString())
        assertEquals("(+(-(9)))", genRes[0].resultExpressionChangingPart.toString())
        val res = applySubstitutionInSelectedPlace(expression, selectedNodeIds, expressionSubstitutionFromStructureStrings(genRes[0].expressionSubstitution.left.toString(), genRes[0].expressionSubstitution.right.toString()),
                CompiledConfiguration())
        assertEquals("(+(-(*(+(-(9));a;/(*(a;b);*(c;d))))))", res.toString())
    }

    @Test
    fun AmulBplus4rulesSearch() {
        val expression = parseStringExpression("4 + A * B")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    4  :  [2]\n" +
                "    *  :  [3]\n" +
                "      A  :  [4]\n" +
                "      B  :  [5]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(4,5).toTypedArray()
        val genRes = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap")).toTypedArray()
        ), withReadyApplicationResult = true)
        assertEquals(3, genRes.size)
        assertEquals("(*(A;B))", genRes[0].expressionSubstitution.left.toString())
        assertEquals("(*(B;A))", genRes[0].expressionSubstitution.right.toString())
        assertEquals("(*(A;B))", genRes[0].originalExpressionChangingPart.toString())
        assertEquals("(+(4;*(B;A)))", genRes[0].resultExpression.toString())
        assertEquals("(*(B;A))", genRes[0].resultExpressionChangingPart.toString())

        assertEquals("(A)", genRes[1].expressionSubstitution.left.toString())
        assertEquals("(+(A;B;-(B)))", genRes[1].expressionSubstitution.right.toString())
        assertEquals("(A)", genRes[1].originalExpressionChangingPart.toString())
        assertEquals("(+(4;*(+(A;B;-(B));B)))", genRes[1].resultExpression.toString())
        assertEquals("(+(A;B;-(B)))", genRes[1].resultExpressionChangingPart.toString())

        assertEquals("(A)", genRes[2].expressionSubstitution.left.toString())
        assertEquals("(/(*(A;B);B))", genRes[2].expressionSubstitution.right.toString())
        assertEquals("(A)", genRes[2].originalExpressionChangingPart.toString())
        assertEquals("(+(4;*(/(*(A;B);B);B)))", genRes[2].resultExpression.toString())
        assertEquals("(/(*(A;B);B))", genRes[2].resultExpressionChangingPart.toString())
    }

    @Test
    fun AmulBplus4rulesSearchParentSelected() {
        val expression = parseStringExpression("4 + A * B")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    4  :  [2]\n" +
                "    *  :  [3]\n" +
                "      A  :  [4]\n" +
                "      B  :  [5]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3).toTypedArray()
        val genRes = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds,CompiledConfiguration(), withReadyApplicationResult = true)
        assertEquals(0, genRes.size)
    }

    @Test
    fun checkAndAddNewVariableReplacementTest() {
        val compiledConfiguration = CompiledConfiguration()
        assertEquals("EXISTING_VARIABLE", checkAndAddNewVariableReplacement("AA", "(+(a;b))", "(+(AA;b))", compiledConfiguration)!!.code)
        assertEquals("INCORRECT_VARIABLE_NAME", checkAndAddNewVariableReplacement("56", "(+(x;-(x)))", "(+(A;b))", compiledConfiguration)!!.code)
        assertEquals("VARIABLE_VALUE_IS_ZERO", checkAndAddNewVariableReplacement("α", "(+(x;-(x)))", "(+(A;b;αα))", compiledConfiguration)!!.code)

        assertEquals(null, checkAndAddNewVariableReplacement("α", "(+(x;-(y)))", "(+(A;b))", compiledConfiguration))
        assertEquals("EXISTING_VARIABLE", checkAndAddNewVariableReplacement("α", "(+(α;-(+(x;-(y)))))", "(+(A;b))", compiledConfiguration)!!.code)
        assertEquals("VARIABLE_VALUE_IS_ZERO", checkAndAddNewVariableReplacement("αα", "(+(α;-(+(x;-(y)))))", "(+(A;b))", compiledConfiguration)!!.code)
        assertEquals(null, checkAndAddNewVariableReplacement("t", "(+(x;-(z)))", "(+(A;b))", compiledConfiguration))

        val expression = parseStringExpression("x - y + α * t")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    x  :  [2]\n" +
                "    -  :  [3]\n" +
                "      y  :  [4]\n" +
                "    *  :  [5]\n" +
                "      α  :  [6]\n" +
                "      t  :  [7]\n", expression.toStringsWithNodeIds())

        val alphaSelectedNodeIds = listOf(2,3).toTypedArray()
        val alphaRes = findApplicableSubstitutionsInSelectedPlace(expression,alphaSelectedNodeIds, compiledConfiguration, withReadyApplicationResult = true)
        assertEquals("result: 'α+α*t'\n" +
                "expressionSubstitution.left: '(+(x;-(y)))'\n" +
                "expressionSubstitution.right: '(α)'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(+(x;-(y)))'\n" +
                "resultExpression: '(+(α;*(α;t)))'\n" +
                "resultExpressionChangingPart: '(α)'\n" +
                "substitutionType: ''\n" +
                "priority: '50'", alphaRes.joinToString (separator = "\n"))

        val tSelectedNodeIds = listOf(7).toTypedArray()
        val tRes = findApplicableSubstitutionsInSelectedPlace(expression,tSelectedNodeIds, compiledConfiguration, withReadyApplicationResult = true)
        assertEquals("result: 'x-y+α*(x-z)'\n" +
                "expressionSubstitution.left: '(t)'\n" +
                "expressionSubstitution.right: '(+(x;-(z)))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(t)'\n" +
                "resultExpression: '(+(x;-(y);*(α;+(x;-(z)))))'\n" +
                "resultExpressionChangingPart: '(+(x;-(z)))'\n" +
                "substitutionType: ''\n" +
                "priority: '50'", tRes.joinToString (separator = "\n"))
    }

    @Test
    fun testGenerateSubstitutionsBySelectedNodesAndItsForwardInverseExtension () {
        val compiledConfiguration = createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "SetComplicatingExtension"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension"),
                        expressionSubstitutionFromStructureStrings(code = "PowRootComplicatingExtension"),
                        expressionSubstitutionFromStructureStrings(code = "LogExpComplicatingExtension")
                ).toTypedArray())
        assertEquals(null, checkAndAddNewVariableReplacement("α", "(+(x;-(y)))", "(+(A;b))", compiledConfiguration))

        val expression = parseStringExpression("x - y + α * t")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    x  :  [2]\n" +
                "    -  :  [3]\n" +
                "      y  :  [4]\n" +
                "    *  :  [5]\n" +
                "      α  :  [6]\n" +
                "      t  :  [7]\n", expression.toStringsWithNodeIds())

        val expressionWithNegativePart = parseStringExpression("x - y + α * t + (α - x + y - 1)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    x  :  [2]\n" +
                "    -  :  [3]\n" +
                "      y  :  [4]\n" +
                "    *  :  [5]\n" +
                "      α  :  [6]\n" +
                "      t  :  [7]\n" +
                "    +  :  [8]\n" +
                "      α  :  [9]\n" +
                "      -  :  [10]\n" +
                "        x  :  [11]\n" +
                "      y  :  [12]\n" +
                "      -  :  [13]\n" +
                "        1  :  [14]\n", expressionWithNegativePart.toStringsWithNodeIds())

        val alphaSelectedTwoPlacesNodeIds = listOf(2,3).toTypedArray()
        val alphaMulDivTwoPlacesRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,alphaSelectedTwoPlacesNodeIds, compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.MULTIPLY_DIVIDE, parseStringExpression("α - x + y")))
        assertEquals("", alphaMulDivTwoPlacesRes.joinToString (separator = "\n"))


        // MULTIPLY_DIVIDE
        val alphaSelectedNodeIds = listOf(2).toTypedArray()
        val alphaMulDivZeroRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,alphaSelectedNodeIds, compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.MULTIPLY_DIVIDE, parseStringExpression("α - x + y")))
        assertEquals("", alphaMulDivZeroRes.joinToString (separator = "\n"))

        val alphaMulDivNotZeroRes = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,alphaSelectedNodeIds, compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.MULTIPLY_DIVIDE, parseStringExpression("α - x")))
        assertEquals("result: '(x*(α-x))/(α-x)-y+α*t'\n" +
                "expressionSubstitution.left: '(x)'\n" +
                "expressionSubstitution.right: '(/(*(x;(+(α;-(x))));(+(α;-(x)))))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(x)'\n" +
                "resultExpression: '(+(/(*(x;+(α;-(x)));+(α;-(x)));-(y);*(α;t)))'\n" +
                "resultExpressionChangingPart: '(/(*(x;(+(α;-(x))));(+(α;-(x)))))'\n" +
                "substitutionType: 'MulDivComplicatingExtension'\n" +
                "priority: '10'", alphaMulDivNotZeroRes.joinToString (separator = "\n"))

        // ADD_SUBTRACT
        val alphaAddSub = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,alphaSelectedNodeIds, compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.ADD_SUBTRACT, parseStringExpression("2")))
        assertEquals("result: '(x+2-2)-y+α*t'\n" +
                "expressionSubstitution.left: '(x)'\n" +
                "expressionSubstitution.right: '(+(x;(2);-((2))))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(x)'\n" +
                "resultExpression: '(+(+(x;2;-(2));-(y);*(α;t)))'\n" +
                "resultExpressionChangingPart: '(+(x;(2);-((2))))'\n" +
                "substitutionType: 'PlusMinusComplicatingExtension'\n" +
                "priority: '10'\n" +
                "result: '(x-2+2)-y+α*t'\n" +
                "expressionSubstitution.left: '(x)'\n" +
                "expressionSubstitution.right: '(+(x;-((2));(2)))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(x)'\n" +
                "resultExpression: '(+(+(x;-(2);2);-(y);*(α;t)))'\n" +
                "resultExpressionChangingPart: '(+(x;-((2));(2)))'\n" +
                "substitutionType: 'MinusPlusComplicatingExtension'\n" +
                "priority: '11'", alphaAddSub.joinToString (separator = "\n"))


        // POW_ROOT
        val alphaPowRootFromNegative = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expressionWithNegativePart, arrayOf(8), compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.POW_ROOT, parseStringExpression("4-2")))
        assertEquals("", alphaPowRootFromNegative.joinToString (separator = "\n"))

        val alphaPowRootFromNegativeInt = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expressionWithNegativePart, arrayOf(8), compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.POW_ROOT, parseStringExpression("4")))
        assertEquals("result: 'x-y+α*t+((α-x+y-1)^4)^(1/4)'\n" +
                "expressionSubstitution.left: '(+(α;-(x);y;-(1)))'\n" +
                "expressionSubstitution.right: '(^(^(+(α;-(x);y;-(1));(4));/(1;(4))))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t);+(α;-(x);y;-(1))))'\n" +
                "originalExpressionChangingPart: '(+(α;-(x);y;-(1)))'\n" +
                "resultExpression: '(+(x;-(y);*(α;t);^(^(+(α;-(x);y;-(1));4);/(1;4))))'\n" +
                "resultExpressionChangingPart: '(^(^(+(α;-(x);y;-(1));(4));/(1;(4))))'\n" +
                "substitutionType: 'PowRootExtension'\n" +
                "priority: '9'", alphaPowRootFromNegativeInt.joinToString (separator = "\n"))

        val alphaPowRootFromNegativeOdd = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expressionWithNegativePart, arrayOf(8), compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.POW_ROOT, parseStringExpression("7")))
        assertEquals("result: 'x-y+α*t+((α-x+y-1)^7)^(1/7)'\n" +
                "expressionSubstitution.left: '(+(α;-(x);y;-(1)))'\n" +
                "expressionSubstitution.right: '(^(^(+(α;-(x);y;-(1));(7));/(1;(7))))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t);+(α;-(x);y;-(1))))'\n" +
                "originalExpressionChangingPart: '(+(α;-(x);y;-(1)))'\n" +
                "resultExpression: '(+(x;-(y);*(α;t);^(^(+(α;-(x);y;-(1));7);/(1;7))))'\n" +
                "resultExpressionChangingPart: '(^(^(+(α;-(x);y;-(1));(7));/(1;(7))))'\n" +
                "substitutionType: 'PowRootExtension'\n" +
                "priority: '9'\n" +
                "result: 'x-y+α*t+((α-x+y-1)^(1/7))^7'\n" +
                "expressionSubstitution.left: '(+(α;-(x);y;-(1)))'\n" +
                "expressionSubstitution.right: '(^(^(+(α;-(x);y;-(1));/(1;(7)));(7)))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t);+(α;-(x);y;-(1))))'\n" +
                "originalExpressionChangingPart: '(+(α;-(x);y;-(1)))'\n" +
                "resultExpression: '(+(x;-(y);*(α;t);^(^(+(α;-(x);y;-(1));/(1;7));7)))'\n" +
                "resultExpressionChangingPart: '(^(^(+(α;-(x);y;-(1));/(1;(7)));(7)))'\n" +
                "substitutionType: 'rootPowExtension'\n" +
                "priority: '10'", alphaPowRootFromNegativeOdd.joinToString (separator = "\n"))

        val alphaPowRoot = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression, alphaSelectedNodeIds, compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.POW_ROOT, parseStringExpression("0.5")))
        assertEquals("result: '(x^0.5)^(1/0.5)-y+α*t'\n" +
                "expressionSubstitution.left: '(x)'\n" +
                "expressionSubstitution.right: '(^(^(x;(0.5));/(1;(0.5))))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(x)'\n" +
                "resultExpression: '(+(^(^(x;0.5);/(1;0.5));-(y);*(α;t)))'\n" +
                "resultExpressionChangingPart: '(^(^(x;(0.5));/(1;(0.5))))'\n" +
                "substitutionType: 'PowRootExtension'\n" +
                "priority: '9'\n" +
                "result: '(x^(1/0.5))^0.5-y+α*t'\n" +
                "expressionSubstitution.left: '(x)'\n" +
                "expressionSubstitution.right: '(^(^(x;/(1;(0.5)));(0.5)))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(x)'\n" +
                "resultExpression: '(+(^(^(x;/(1;0.5));0.5);-(y);*(α;t)))'\n" +
                "resultExpressionChangingPart: '(^(^(x;/(1;(0.5)));(0.5)))'\n" +
                "substitutionType: 'rootPowExtension'\n" +
                "priority: '10'", alphaPowRoot.joinToString (separator = "\n"))


        // EXPONENTIATE_LOGARITHM
        val alphaLogExpBase1 = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,alphaSelectedNodeIds, compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.EXPONENTIATE_LOGARITHM, parseStringExpression("3-2")))
        assertEquals("", alphaLogExpBase1.joinToString (separator = "\n"))

        val alphaLogExpBaseUnknown = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,alphaSelectedNodeIds, compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.EXPONENTIATE_LOGARITHM, parseStringExpression("a-b")))
        assertEquals("result: 'log((a-b)^x,a-b)-y+α*t'\n" +
                "expressionSubstitution.left: '(x)'\n" +
                "expressionSubstitution.right: '(log(^((+(a;-(b)));x);(+(a;-(b)))))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(x)'\n" +
                "resultExpression: '(+(log(^(+(a;-(b));x);+(a;-(b)));-(y);*(α;t)))'\n" +
                "resultExpressionChangingPart: '(log(^((+(a;-(b)));x);(+(a;-(b)))))'\n" +
                "substitutionType: 'logExpExtension'\n" +
                "priority: '9'\n" +
                "result: 'log(x,a-b)^(a-b)-y+α*t'\n" +
                "expressionSubstitution.left: '(x)'\n" +
                "expressionSubstitution.right: '(^(log(x;(+(a;-(b))));(+(a;-(b)))))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(x)'\n" +
                "resultExpression: '(+(^(log(x;+(a;-(b)));+(a;-(b)));-(y);*(α;t)))'\n" +
                "resultExpressionChangingPart: '(^(log(x;(+(a;-(b))));(+(a;-(b)))))'\n" +
                "substitutionType: 'expLogExtension'\n" +
                "priority: '10'", alphaLogExpBaseUnknown.joinToString (separator = "\n"))

        val alphaLogExp = generateSubstitutionsBySelectedNodesAndItsForwardInverseExtension(expression,alphaSelectedNodeIds, compiledConfiguration,
                ForwardInverseExtension(ForwardInverseExtensionType.EXPONENTIATE_LOGARITHM, parseStringExpression("2")))
        assertEquals("result: 'log(2^x,2)-y+α*t'\n" +
                "expressionSubstitution.left: '(x)'\n" +
                "expressionSubstitution.right: '(log(^((2);x);(2)))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(x)'\n" +
                "resultExpression: '(+(log(^(2;x);2);-(y);*(α;t)))'\n" +
                "resultExpressionChangingPart: '(log(^((2);x);(2)))'\n" +
                "substitutionType: 'logExpExtension'\n" +
                "priority: '9'\n" +
                "result: 'log(x,2)^2-y+α*t'\n" +
                "expressionSubstitution.left: '(x)'\n" +
                "expressionSubstitution.right: '(^(log(x;(2));(2)))'\n" +
                "originalExpression: '(+(x;-(y);*(α;t)))'\n" +
                "originalExpressionChangingPart: '(x)'\n" +
                "resultExpression: '(+(^(log(x;2);2);-(y);*(α;t)))'\n" +
                "resultExpressionChangingPart: '(^(log(x;(2));(2)))'\n" +
                "substitutionType: 'expLogExtension'\n" +
                "priority: '10'", alphaLogExp.joinToString (separator = "\n"))
    }
}