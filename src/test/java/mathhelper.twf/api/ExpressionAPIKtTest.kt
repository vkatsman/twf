package mathhelper.twf.api

import mathhelper.twf.config.FunctionConfiguration
import mathhelper.twf.config.PI_STRING_USUAL
import mathhelper.twf.expressiontree.ExpressionNode
import mathhelper.twf.expressiontree.ExpressionTreeParser
import mathhelper.twf.expressiontree.NodeType
import org.junit.Test
import org.junit.Ignore

import kotlin.test.assertEquals

class ExpressionAPIKtTest {
    @Test
    fun expressionNormalizationTest() {
        val string = "(+(*(+(^(sin(+(+(*(8;y);*(6;y);-(*(4;y));*(y);-(*(2;y)));t;-(+(r;-(+(-(r);8));-(+(*(-(g));5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(*(+(2));-(+(4;-(+(t;-(b)))))))));-(u)))));2);-(cos(x))))))"
        val expression = structureStringToExpression(string)
        normalizeExpressionToUsualForm(expression)
        assertEquals("(+(^(sin(+(+(*(8;y);*(6;y);-(*(4;y));y;-(*(2;y)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(2;-(+(4;-(+(t;-(b)))))))));-(u)))));2);-(cos(x))))", expression.toString())
    }

    @Test
    fun piInStringToStructureString() {
        val structureString = stringToStructureString("(1+tg(pi/24)*tg(13*pi/24)) / (tg(13*pi/24) - tg(pi/24))")
        assertEquals("(/(+(1;*(tg(/($PI_STRING_USUAL;24));tg(/(*(13;$PI_STRING_USUAL);24))));+(tg(/(*(13;$PI_STRING_USUAL);24));-(tg(/($PI_STRING_USUAL;24))))))", structureString)
    }

    @Test
    fun minusZeroNormalizationTest() {
        val string = "(+(1;-0))"
        val expression = ExpressionNode(NodeType.FUNCTION, "")
        expression.addChild(ExpressionNode(NodeType.FUNCTION, "+"))
        expression.children.last().addChild(ExpressionNode(NodeType.VARIABLE, "1"))
        expression.children.last().addChild(ExpressionNode(NodeType.VARIABLE, "-0"))
        normalizeExpressionToUsualForm(expression)
        assertEquals("(+(1;+(-(0))))", expression.toString())
    }

    @Test
    fun expressionErrorTest() {
        val string = "a-b/c)"
        val expression = stringToExpression((string))
        assertEquals(NodeType.ERROR, expression.nodeType)
        assertEquals("Unexpected ')'", expression.value)
        assertEquals(5, expression.startPosition)
    }

    @Test
    fun expressionTexNotClosedErrorTest() {
        val string = "\\\\frac{a}{b"
        val expression = stringToExpression((string))
        assertEquals(NodeType.ERROR, expression.nodeType)
        assertEquals("closing bracket missing", expression.value)
    }

    @Test
    fun sqrtLogTest() {
        val string = "sqrt(a)/sqrt(a/b)"
        val expression = stringToExpression((string))
        val texString = expressionToTexString(expression)
        assertEquals("\\frac{\\sqrt{a}}{\\sqrt{\\frac{a}{b}}}", texString)
    }

    @Test
    fun defaultLogicStartExpressionToTexTest() {
        val string = "(and(a;or(a;b)))"
        val expression = structureStringToExpression(string)
        val texString = expressionToTexString(expression)
        assertEquals("a \\land \\left(a \\lor b\\right)", texString)
    }

    @Test
    fun sinSqrExpressionToTexTest() {
        val string = "(^(sin(x);cos(x)))"
        val expression = structureStringToExpression(string)
        val texString = expressionToTexString(expression)
        assertEquals("\\sin ^{\\cos\\left(x\\right)} \\left(x\\right)", texString)
    }

    @Test
    fun expressionAndStructureStringSetTheoryTest() {
        val string = "!x \\\\ y"
        val expression = stringToExpression((string), "setTheory")
        val structureString = expressionToStructureString(expression)
        assertEquals("(set-(not(x);y))", structureString)
        val newExpresion = structureStringToExpression(structureString)
        assertEquals("!x\\\\y", expressionToString(newExpresion, 2))
        assertEquals("¬x∖y", expressionToUnicodeString(newExpresion, 2))
        val texString = expressionToTexString(expression)
        assertEquals(" \\neg x \\setminus y", texString)
    }

    @Test
    fun expressionToStringMinusTest() {
        val string = "-(a-b/c)"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("-(a-b/c)", userString)
        val texString = expressionToTexString(expression)
        assertEquals(" - \\left(a - \\frac{b}{c}\\right)", texString)
    }

    @Test
    fun expressionToTexFractionNoExtraBracketsTest() {
        val string = "(a-b/c)/(a-t)"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("(a-b/c)/(a-t)", userString)
        val texString = expressionToTexString(expression)
        assertEquals("\\frac{a - \\frac{b}{c}}{a - t}", texString) // not '\frac{\left(a-\frac{b}{c}\right)}{\left(a-t\right)}'
    }

    @Test
    fun doubleBracketDegreeTest() {
        val string = "((+(-a^3)))+1"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("((-a^3))+1", userString)
        val texString = expressionToTexString(expression)
        assertEquals("\\left(\\left( - {a^{3}}\\right)\\right) + 1", texString)
    }

    @Test
    fun doubleBracketOfPlusDegreeTest() {
        val string = "((+(-a^(3+b))))+1"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("((-a^(3+b)))+1", userString)
        val texString = expressionToTexString(expression)
        assertEquals("\\left(\\left( - {a^{3 + b}}\\right)\\right) + 1", texString)
    }

    @Test
    fun vladEmptyNodeMinusPriorityBug() {
        val string = "a+b-(c+d)"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("(+(a;b;-(+(c;d))))", expression.toString())
        assertEquals("a+b-(c+d)", userString)

        val structureString = "+((*(^(+(a;b;c;d;e;f;g);2.0);100.0));-(+(x;y;z;t;w;v;h;10.0)))"
        val structureExpression = structureStringToExpression(structureString)
        assertEquals("(a+b+c+d+e+f+g)^2.0*100.0-(x+y+z+t+w+v+h+10.0)", expressionToString(structureExpression))

        val structureStringBug = "+((*(^(+(a;b;c;d;e;f;g);2.0);100.0));-((+(x;y;z;t;w;v;h;10.0))))"
        val structureExpressionBug = structureStringToExpression(structureStringBug)
//        assertEquals("(a+b+c+d+e+f+g)^2.0*100.0-(x+y+z+t+w+v+h+10.0)", expressionToString(structureExpressionBug))   // TODO: handle empty node problem
    }

    @Test
    fun allowOnlyOneSymbolVariablesTest() {
        val string = "a²-33ab+b²"
        val expression = stringToExpression(string, functionConfiguration = FunctionConfiguration(
                allowOnlyOneSymbolVariables = true
        ))
        assertEquals("(+(^(a;2);-(*(33;a;b));^(b;2)))", expression.toString())
    }

    @Test
    fun notAllowOnlyOneSymbolVariablesTest() {
        val string = "a²-3ab+b²"
        val expression = stringToExpression(string, functionConfiguration = FunctionConfiguration(
                allowOnlyOneSymbolVariables = false
        ))
        assertEquals("(+(^(a;2);-(*(3;ab));^(b;2)))", expression.toString())
    }

    @Test
    fun multipleDegreeTest() {
        val string = "(a+a*b)^b^c^(d+tg(x))^7^y"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("(a+a*b)^b^c^(d+tg(x))^7^y", userString)
        val texString = expressionToTexString(expression)
        assertEquals("{\\left(a + a \\cdot b\\right)^{b^{c^{d + tg\\left(x\\right)^{7^{y}}}}}}", texString)
    }

    @Test
    fun multipleDegreeTestTexSin() {
        val string = "(a+a*b)^b^c^(d+sin(x))^7^y"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("(a+a*b)^b^c^(d+sin(x))^7^y", userString)
        val texString = expressionToTexString(expression)
        assertEquals("{\\left(a + a \\cdot b\\right)^{b^{c^{d + \\sin\\left(x\\right)^{7^{y}}}}}}", texString)
    }

    @Test
    fun multipleDegreeLogTest() {
        val string = "(a+log(a*b,b*a))^b^c^(d+sin(x+8)-log(b,a))^7^y"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("(a+log(a*b,b*a))^b^c^(d+sin(x+8)-log(b,a))^7^y", userString)
        val texString = expressionToTexString(expression)
        assertEquals("{\\left(a + \\log _{b \\cdot a}{a \\cdot b}\\right)^{b^{c^{d + \\sin\\left(x + 8\\right) - \\log _{a}{b}^{7^{y}}}}}}", texString)
    }

    @Test
    fun texLogBracketsTest() {
        val string = "log(a*b,b*a)*4"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("log(a*b,b*a)*4", userString)
        val texString = expressionToTexString(expression)
        assertEquals("\\left(\\log _{b \\cdot a}{a \\cdot b}\\right) \\cdot 4", texString)
    }

    @Test
    fun expressionToStringBracketsTest() {
        val string = "(cos(x)*cos(y)-cos(x+y))/(cos(x)*cos(-y)-sin(x)*sin(-y)-sin(x)*sin(y))"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("(cos(x)*cos(y)-cos(x+y))/(cos(x)*cos(-y)-sin(x)*sin(-y)-sin(x)*sin(y))", userString)
        val texString = expressionToTexString(expression)
        assertEquals("\\frac{\\cos\\left(x\\right) \\cdot \\cos\\left(y\\right) - \\cos\\left(x + y\\right)}{\\cos\\left(x\\right) \\cdot \\cos\\left( - y\\right) - \\sin\\left(x\\right) \\cdot \\sin\\left( - y\\right) - \\sin\\left(x\\right) \\cdot \\sin\\left(y\\right)}", texString)
    }

    @Test
    fun expressionReduceExtraSignsTest() {
//        val string = "(+(+(-(^(a;3)))))"
        val string = "(+(+(+(sin(+(x)));+(^(a;3));log(+(a);+(+(b)));+(+(+(-(^(a;3))))))))"
        val expression = structureStringToExpression((string))
        expression.reduceExtraSigns(setOf("+"), setOf("-"))
        assertEquals("(+(+(sin(x));+(^(a;3));log(a;b);+(+(-(^(a;3))))))", expression.toString())
        val userString = expressionToString(expression)
        assertEquals("(sin(x))+(a^3)+log(a,b)+((-a^3))", userString)
        val texString = expressionToTexString(expression)
        assertEquals("\\left(\\sin\\left(x\\right)\\right) + \\left({a^{3}}\\right) + \\log _{b}{a} + \\left(\\left( - {a^{3}}\\right)\\right)", texString)
    }

    @Test
    fun emptyOrBlankStructureStringTest() {
        val string = ""
        val expression = structureStringToExpression(string)
        assertEquals(NodeType.ERROR, expression.nodeType)
        assertEquals("No expression found()", expression.toString())
    }

    @Test
    fun emptyOrBlankStringTest() {
        val string = ""
        val expression = stringToExpression(string)
        assertEquals(NodeType.ERROR, expression.nodeType)
        assertEquals("No expression found()", expression.toString())
    }

    @Test
    fun expressionNormilizeSubstructionsTest() {
        val string = "(+(+(sin(-(x));-(-(-(^(a;3))));-(-(^(a;3))))))"
        val expression = structureStringToExpression((string))
        expression.normalizeSubtructions(FunctionConfiguration())
        assertEquals("(+(+(sin(+(-(x)));-(+(-(+(-(^(a;3))))));-(+(-(^(a;3)))))))", expression.toString())
    }

    @Test
    fun expressionToStringSetTheoryTest() {
        val string = "!x \\\\ y"
        val expression = stringToExpression((string), "setTheory")
        val userString = expressionToString(expression, 2)
        assertEquals("!x\\\\y", userString)
    }

    @Test
    fun expressionToStringTest() {
        val string = "sin(2*x) + sin(x+sin(y))"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("sin(2*x)+sin(x+sin(y))", userString)
    }

    @Test
    fun structureStringToExpressionErrorsTest () {
        val correctWithoutExtraBrackets = structureStringToExpression("+(^(a;3);+(^(sin;3)))")
        assertEquals(NodeType.FUNCTION, correctWithoutExtraBrackets.nodeType)
        assertEquals("(+(^(a;3);+(^(sin;3))))", correctWithoutExtraBrackets.toString())

        val wrongOperationSimple = structureStringToExpression("(^+^(a;3))")
        assertEquals(NodeType.ERROR, wrongOperationSimple.nodeType)
        assertEquals("Unknown function: '^+^' with '2' arguments", wrongOperationSimple.value)
        assertEquals(1, wrongOperationSimple.startPosition)

        val wrongOperation = structureStringToExpression("(+(^+^(a;3);+(^(a;3))))")
        assertEquals(NodeType.ERROR, wrongOperation.nodeType)
        assertEquals("Unknown function: '^+^' with '2' arguments", wrongOperation.value)
        assertEquals(3, wrongOperation.startPosition)

        val wrongVariableSymbol = structureStringToExpression("(+(^(a;3);+(^(:;3))))")
        assertEquals(NodeType.ERROR, wrongVariableSymbol.nodeType)
        assertEquals("Wrong variable symbol ':'", wrongVariableSymbol.value)
        assertEquals(14, wrongVariableSymbol.startPosition)

        val extraOpenBracketSimple = structureStringToExpression("(((^(a;3))")
        assertEquals(NodeType.ERROR, extraOpenBracketSimple.nodeType)
        assertEquals("closing bracket missing", extraOpenBracketSimple.value)
        assertEquals(8, extraOpenBracketSimple.startPosition)

        val extraCloseBracket = structureStringToExpression("(+(^(ab));35))")
        assertEquals(NodeType.ERROR, extraCloseBracket.nodeType)
        assertEquals("Unexpected ')'", extraCloseBracket.value)
        assertEquals(12, extraCloseBracket.startPosition)

        val extraLotsBrackets = structureStringToExpression("+(^(a)));3)))))")
        assertEquals(NodeType.ERROR, extraLotsBrackets.nodeType)
        assertEquals("Unexpected ')'", extraLotsBrackets.value)
        assertEquals(7, extraLotsBrackets.startPosition)

        val extraOpenBracket = structureStringToExpression("(+(^(ab;35))")
        assertEquals(NodeType.ERROR, extraOpenBracket.nodeType)
        assertEquals("closing bracket missing", extraOpenBracket.value)
        assertEquals(10, extraOpenBracket.startPosition)
    }

    @Test
    fun structureStringToExpressionBracketsDeepTest() {
        val exprOneBracket = structureStringToExpression("(+(^(a;3);+(^(a;3))))")
        val exprTwoBracket = structureStringToExpression("(+(a;((b))))")
        assertEquals("(+(a;b))", structureStringToExpression("(+(a;b))").toString())
        assertEquals("(+(^(a;3);+(^(a;3))))", exprOneBracket.toString())
        assertEquals("(+(a;((b))))", exprTwoBracket.toString())

        assertEquals("(tg(*(2;x)))", structureStringToExpression("(tg(*(2;x)))").toString())

        val subst = expressionSubstitutionFromStructureStrings("(+(a;(b)))", "(+((b);a))")
        assertEquals("(+(a;(b)))", subst.left.toString())

        val placesOneBracket = findSubstitutionPlacesInExpression(exprOneBracket, subst)
        assertEquals(0, placesOneBracket.size)
        val placesTwoBracket = findSubstitutionPlacesInExpression(exprTwoBracket, subst)
        assertEquals(1, placesTwoBracket.size)
    }

    @Test
    fun stringToStructureStringTest() {
        assertEquals("(+(^(a;3);*(3;^(a;2);b);*(3;a;b);^(b;3)))", stringToStructureString("a^3 + 3*a^2*b + 3*a*b + b^3"))
        assertEquals("(+(-(+(a;-(/(b;c))))))", stringToStructureString("-(a-b/c)"))
        assertEquals("(or(set-(not(x);y);z))", stringToStructureString("(!x \\\\ y) | z", scope = "setTheory"))
        assertEquals("(+(-(/(1;a));-(/(1;*(c;cos(z))))))", stringToStructureString("-1/a - 1/c*cos(z)"))
        assertEquals("(and(or(A;not(B));or(A;not(C))))", stringToStructureString("(A | !B) & (A | !C)", scope = "setTheory"))
    }

    @Test
    fun expressionFactorialToStringTest() {
        val string = "y!!*n!"
        val expression = stringToExpression((string))
        val userString = expressionToString(expression)
        assertEquals("y!!*n!", userString)
        val texString = expressionToTexString(expression)
        assertEquals("y !!  \\cdot n ! ", texString)
    }

    @Test
    fun expressionSetNandNor() {
        val string = "alleq(0,!x,!((x↑v)&nor(z,b,0)))"
        val expression = stringToExpression((string), scope = "setTheory")
        val userString = expressionToString(expression)
        assertEquals("0==!x==!((x↑v)&(z↓b↓0))", userString)
        assertEquals("0≡¬x≡¬((x↑v)∧(z↓b↓0))", expressionToUnicodeString(expression))
        val texString = expressionToTexString(expression)
        assertEquals("0 \\equiv  \\neg x \\equiv  \\neg \\left(\\left(x ↑ v\\right) \\land \\left(z ↓ b ↓ 0\\right)\\right)", texString)
    }

    @Test
    fun expressionSetNotToStringTest() {
        val string = "!(!y)&n!"
        val expression = stringToExpression((string), scope = "setTheory")
        val userString = expressionToString(expression)
        assertEquals("!(!y)&n!", userString)
        assertEquals("¬(¬y)∧n!", expressionToUnicodeString(expression))
        val texString = expressionToTexString(expression)
        assertEquals(" \\neg  \\neg y \\land n ! ", texString)
    }

    @Test
    fun compareWithoutSubstitutionsTestCorrect (){
        val result = compareWithoutSubstitutions(
                "a + b * c + 4*a - b + sin(x)",
                "sin(x) + 5*a + b * (c - 1)"
        )
        assertEquals(true, result)
    }

    @Test
    fun compareWithoutSubstitutionsTestWrong (){
        val result = compareWithoutSubstitutions(
                "a + b * c + 4*a - b + sin(x) + a",
                "sin(x) + 5*a + b * (c - 1)",
                ""
        )
        assertEquals(false, result)
    }

    @Test
    fun compare3KNF (){
        val result = compareByPattern("(a|!b|c)&(!a|!b|x)", "and : (or : 3) : : : not", "setTheory")
        assertEquals(true, result)
    }

    @Test
    fun compareKNF (){
        val result = compareByPattern("(a|!b|c)&(!a|x)", "and : (or) : : : not", "setTheory")
        assertEquals(true, result)
    }

    @Test
    fun compareWrong3KNF (){
        val result = compareByPattern("(a|!b|c)&(!a|x)", "and : (or : 3) : : : not", "setTheory")
        assertEquals(false, result)
    }

    @Test
    fun compareWrongKNF (){
        val result = compareByPattern("(a|!b|c)&!(!a|x)", "and : (or) : : : not", "setTheory")
        assertEquals(false, result)
    }

    @Test
    fun compareDNFInsteadOfKNF (){
        val result = compareByPattern("(a&!b&c)|(a&x)", "and : (or) : : : not", "setTheory")
        assertEquals(false, result)
    }

    @Test
    @Ignore //TODO: investigate, why it's fails (wrong result for acos)
    fun compareTestTrigonometryAsinCorrect (){
        val result = compareWithoutSubstitutions(
                "asin(1)-acos(x)",
                "asin(x)",
                ""
        )
        assertEquals(true, result)
    }

    @Test
    fun compareTestTrigonometrySinCorrect (){
        val result = compareWithoutSubstitutions(
                "sin(pi/2)",
                "-sin(3*pi/2)",
                "", "+;-;*;/;sin"
        )
        assertEquals(true, result)
    }

    @Test
    fun compareTestTrigonometrySinUncorrectBecauseOfForbiddenSin (){
        val result = compareWithoutSubstitutions(
                "sin(pi/2)",
                "-sin(3*pi/2)",
                "", "+;-;*;/"
        )
        assertEquals(false, result)
    }

    @Test
    fun substitutionApplicationTest (){
        val expression = stringToExpression("x")
        val substitution = expressionSubstitutionFromStrings(
                "x",
                "tg(x)"
        )
        val matchedPlaces = findSubstitutionPlacesInExpression(expression, substitution)
        assertEquals(1, matchedPlaces.size)
        val applicationResult = applySubstitution(expression, substitution, matchedPlaces)
        assertEquals("tg(x)", expressionToString(applicationResult))
    }

    @Test
    fun testSubstitutionApplicationWithNodeIds (){
        val expression = stringToExpression("x+y/z")
        assertEquals("  :  [-1; 5; 0]\n" +
                "  +  :  [-1; 5; 1]\n" +
                "    x  :  [0; 1; 2]\n" +
                "    /  :  [-1; 5; 3]\n" +
                "      y  :  [2; 3; 4]\n" +
                "      z  :  [4; 5; 5]\n", expression.toStringsWithPositions())
        val substitution = expressionSubstitutionFromStrings(
                "a/b",
                "a*b/(b*b)"
        )
        val matchedPlaces = findSubstitutionPlacesInExpression(expression, substitution)
        assertEquals(1, matchedPlaces.size)
        val applicationResult = applySubstitution(expression, substitution, matchedPlaces)
        assertEquals("x+(y*z)/(z*z)", expressionToString(applicationResult))
        assertEquals("  :  [-1; 5; 0]\n" +
                "  +  :  [-1; 5; 1]\n" +
                "    x  :  [0; 1; 2]\n" +
                "    /  :  [-1; 9; 3]\n" +
                "      *  :  [-1; 3; 4]\n" +
                "        y  :  [2; 3; 5]\n" +
                "        z  :  [4; 5; 6]\n" +
                "      *  :  [4; 9; 7]\n" +
                "        z  :  [4; 5; 8]\n" +
                "        z  :  [4; 5; 9]\n", expression.toStringsWithPositions())
    }

    @Test
    fun testStructureStringSubstitutionApplicationWithNodeIds (){
        val expression = structureStringToExpression("(+(x;/(y;z)))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    x  :  [2]\n" +
                "    /  :  [3]\n" +
                "      y  :  [4]\n" +
                "      z  :  [5]\n", expression.toStringsWithNodeIds())
        val substitution = expressionSubstitutionFromStructureStrings(
                "(/(a;b))",
                "(/(*(a;b);*(b;b)))"
        )
        val matchedPlaces = findSubstitutionPlacesInExpression(expression, substitution)
        assertEquals(1, matchedPlaces.size)
        val applicationResult = applySubstitution(expression, substitution, matchedPlaces)
        assertEquals("x+(y*z)/(z*z)", expressionToString(applicationResult))
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    x  :  [2]\n" +
                "    /  :  [3]\n" +
                "      *  :  [4]\n" +
                "        y  :  [5]\n" +
                "        z  :  [6]\n" +
                "      *  :  [7]\n" +
                "        z  :  [8]\n" +
                "        z  :  [9]\n", expression.toStringsWithNodeIds())
    }

    @Test
    fun testSubstitutionApplicationWithNodeIdsForDeepAppliedSubstitution (){
        val expression = stringToExpression("(!(!(!(a|b))))", scope = "setTheory")
        assertEquals("  :  [0; 14; 0]\n" +
                "  not  :  [1; 11; 1]\n" +
                "    not  :  [3; 11; 2]\n" +
                "      not  :  [5; 11; 3]\n" +
                "        or  :  [6; 11; 4]\n" +
                "          a  :  [7; 8; 5]\n" +
                "          b  :  [9; 10; 6]\n", expression.toStringsWithPositions())
        val substitution = expressionSubstitutionFromStrings(
                "a",
                "a&a"
        )
        val matchedPlaces = findSubstitutionPlacesInExpression(expression, substitution)
        assertEquals(6, matchedPlaces.size)
        val applicationResult = applySubstitution(expression, substitution, listOf(matchedPlaces[1]))
        assertEquals("!(!(!(a|(b&b))))", expressionToString(applicationResult))
        assertEquals("  :  [0; 14; 0]\n" +
                "  not  :  [1; 11; 1]\n" +
                "    not  :  [3; 11; 2]\n" +
                "      not  :  [5; 11; 3]\n" +
                "        or  :  [6; 11; 4]\n" +
                "          a  :  [7; 8; 5]\n" +
                "          and  :  [0; 3; 6]\n" +
                "            b  :  [9; 10; 7]\n" +
                "            b  :  [9; 10; 8]\n", expression.toStringsWithPositions())
    }

    @Test
    fun findSubstitutionPlacesInExpressionJSONTest(){
        val result = findSubstitutionPlacesCoordinatesInExpressionJSON(
                "sin(2*x) + sin(x+sin(y))",
                "sin(x)",
                "sqrt(1 - (cos(x))^2)"
        )
        assertEquals("{\"substitutionPlaces\":[{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"24\",\"startPosition\":\"0\",\"endPosition\":\"8\"},{\"parentStartPosition\":\"14\",\"parentEndPosition\":\"24\",\"startPosition\":\"17\",\"endPosition\":\"23\"},{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"24\",\"startPosition\":\"11\",\"endPosition\":\"24\"}]}", result)
    }

    @Test
    fun applySubstitutionTest(){
        val result = applyExpressionBySubstitutionPlaceCoordinates(
                "sin(2*x) + sin(x+sin(y))",
                "sin(x)",
                "sqrt(1 - (cos(x))^2)",
                14,
                24,
                17,
                23
        )
        assertEquals("sin(2*x)+sin(x+sqrt(1-cos(y)^2))", result)
    }

    @Test
    fun findSubstitutionPlacesInExpressionJSONTest_BugFromIlya(){
        val result = findSubstitutionPlacesCoordinatesInExpressionJSON(
                "((cos(x)*cos(y))-cos(x+y))/(cos(x-y)-(sin(x)*sin(y)))",
                "cos(x+y)",
                "cos(x)*cos(y)-sin(x)*sin(y)"
        )
        assertEquals("{\"substitutionPlaces\":[{\"parentStartPosition\":\"16\",\"parentEndPosition\":\"25\",\"startPosition\":\"17\",\"endPosition\":\"25\"},{\"parentStartPosition\":\"27\",\"parentEndPosition\":\"53\",\"startPosition\":\"28\",\"endPosition\":\"36\"}]}", result)
    }

    @Test
    fun findSubstitutionPlacesInExpressionJSONTest_BugFromAlex(){
        val result = findSubstitutionPlacesCoordinatesInExpressionJSON(
                "(A|B)&(C->0)",
                "!A&!B",
                "!(A|B)",
                scope = "setTheory"
        )
        assertEquals("{\"substitutionPlaces\":[]}", result)
    }

    @Test
    fun findSubstitutionPlacesInExpressionJSONTest_Zero_Universum(){
        val result = findSubstitutionPlacesCoordinatesInExpressionJSON(
                "(A|B)&(C&0)",
                "A&0",
                "0",
                scope = "setTheory"
        )
        assertEquals("{\"substitutionPlaces\":[{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"11\",\"startPosition\":\"6\",\"endPosition\":\"11\"}]}", result)

        val applicationResult = applyExpressionBySubstitutionPlaceCoordinates(
                "(A|B)&(C&0)",
                "A&0",
                "0",
                0,
                11,
                6,
                11,
                scope = "setTheory"
        )
        assertEquals("(A|B)&0", applicationResult)
    }

    @Test
    fun findSubstitutionPlacesInExpressionJSONTest_OnePlace(){
        val result = findSubstitutionPlacesCoordinatesInExpressionJSON(
                "!A&!B",
                "!A&!B",
                "!(A|B)",
                scope = "setTheory"
        )
        assertEquals("{\"substitutionPlaces\":[{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"5\",\"startPosition\":\"0\",\"endPosition\":\"5\"}]}", result)
    }

    @Test
    fun applyExpressionByStructureStringsSubstitutionPlaceCoordinates() {
        val result = applyExpressionByStructureStringsSubstitutionPlaceCoordinates("!(!(!(!(!A)))&!B)",
                "(and(A;not(B)))", "(set-(A;B))",
                0, 17,
                1, 17, "setTheory")
        assertEquals("!(!(!(!(!A)))\\B)", result)
    }

    @Test
    fun applySubstitutionPlacesInExpressionJSONTest(){
        val result = findSubstitutionPlacesCoordinatesInExpressionJSON(
                "A\\\\(B\\\\C)",
                "A",
                "A&A",
                scope = "setTheory"
        )
        assertEquals("{\"substitutionPlaces\":[{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"9\",\"startPosition\":\"0\",\"endPosition\":\"1\"},{\"parentStartPosition\":\"3\",\"parentEndPosition\":\"9\",\"startPosition\":\"4\",\"endPosition\":\"5\"},{\"parentStartPosition\":\"3\",\"parentEndPosition\":\"9\",\"startPosition\":\"7\",\"endPosition\":\"8\"},{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"9\",\"startPosition\":\"3\",\"endPosition\":\"9\"},{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"9\",\"startPosition\":\"0\",\"endPosition\":\"9\"}]}",
                result)

        val applicationResult = applyExpressionBySubstitutionPlaceCoordinates(
                "A\\\\(B\\\\C)",
                "A",
                "A&A",
                0,
                9,
                0,
                9,
                scope = "setTheory"
        )
        assertEquals("(A\\(B\\C))&(A\\(B\\C))", applicationResult)
    }

    @Test
    fun applySubstitutionTest_BugFromIlya(){
        val result = applyExpressionBySubstitutionPlaceCoordinates(
                "((cos(x)*cos(y))-cos(x+y))/(cos(x-y)-(sin(x)*sin(y)))",
                "cos(x+y)",
                "cos(x)*cos(y)-sin(x)*sin(y)",
                27,
                53,
                28,
                36
        )
        assertEquals("(cos(x)*cos(y)-cos(x+y))/((cos(x)*cos(-y)-sin(x)*sin(-y))-sin(x)*sin(y))", result)
    }

    @Test
    fun decodeExpressionInUrlTest() {
        val ss = decodeUrlSymbols("(*(8;%5E(cos(x);4)))")
        assertEquals("(*(8;^(cos(x);4)))", ss)
    }

    @Test
    fun texStringNormalizationTestStatement() {
        val res = texStringNormalization("tg\\left(\\frac{pi}{4}\\right)=tg\\left(\\frac{pi\\cdot 2}{4\\cdot 2+x-x}\\right)")
        assertEquals("\\tg\\left(\\frac{pi}{4}\\right)=\\tg\\left(\\frac{pi\\cdot 2}{4\\cdot 2+x-x}\\right)", res)
    }

    @Test
    fun testTexTrigonometry() {
        val origString = "2 \\cdot sin\\left(x\\right) \\cdot \\left( - cos\\left(x + π\\right)\\right) + \\frac{1}{1 + \\ctg ^{2} \\left(x\\right)} + cos ^{2} \\left(x\\right) - sin\\left(2 \\cdot x\\right)"
        val root = stringToExpression(origString, format = "Tex")
        assertEquals("(+(*(2;sin(x);+(-(cos(+(x;π)))));/(1;+(1;^(ctg(x);2)));^(cos(x);2);-(sin(*(2;x)))))", root.toString())
    }

    @Test
    fun testTexTrigonometrySqr() {
        val origString = "\\frac{1}{\\sin^2\\left(x\\right)+\\sin\\left(2\\cdot x-\\frac{3\\cdot\\pi}{2}\\right)}-tg^2\\left(x+\\pi\\right)"
        val root = stringToExpression(origString, format = "Tex")
        assertEquals("(+(/(1;+(^(sin(x);2);sin(+(*(2;x);-(/(*(3;π);2))))));-(^(tg(+(x;π));2))))", root.toString())
    }

    @Test
    fun testTexTrigonometryTg() {
        val origString = "tg^{\\sin \\left(x\\right)}\\left(x\\right)"
        val root = stringToExpression(origString, format = "Tex")
        assertEquals("(^(tg(x);sin(x)))", root.toString())
    }
}