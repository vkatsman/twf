package mathhelper.twf.defaultcontent

import mathhelper.twf.api.compareWithoutSubstitutions
import mathhelper.twf.api.structureStringToExpression
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.DebugOutputMessages
import mathhelper.twf.config.FunctionConfiguration
import mathhelper.twf.defaultcontent.defaulttasksets.SetTaskSets
import mathhelper.twf.expressiontree.ExpressionStructureConditionConstructor
import mathhelper.twf.expressiontree.NodeType
import mathhelper.twf.expressiontree.checkExpressionStructure
import org.junit.Test
import kotlin.test.assertTrue

internal class SetTaskSetsTest {

    val debugOutputMessages = DebugOutputMessages(
            expressionProbabilityComparisonFalseDetailsPrintln = true
    )
    val expressionStructureConditionConstructor = ExpressionStructureConditionConstructor(CompiledConfiguration(functionConfiguration = FunctionConfiguration()))

    @Test
    fun allSetTasksCorrectness() {
        for (task in SetTaskSets.allSetTasks) {
            val originalExpression = structureStringToExpression(task.originalExpressionStructureString ?: "")
            mathhelper.twf.assert(originalExpression.nodeType == NodeType.FUNCTION && originalExpression.value == "",
                    { "Error: originalExpressionStructureString '${task.originalExpressionStructureString}' is incorrect '${originalExpression.value}' " })

            if (!task.goalExpressionStructureString.isNullOrBlank()) {
                val goalExpression = structureStringToExpression(task.goalExpressionStructureString!!)
                mathhelper.twf.assert(goalExpression.nodeType == NodeType.FUNCTION && goalExpression.value == "",
                        { "Error: originalExpressionStructureString '${task.goalExpressionStructureString}' is incorrect '${goalExpression.value}' " })

                mathhelper.twf.assert(task.rules?.isNotEmpty() == true || compareWithoutSubstitutions(originalExpression, goalExpression, notChangesOnVariablesFunction = setOf(), debugOutputMessages = debugOutputMessages),
                        { "Error: '${task.originalExpressionStructureString}' != '${task.goalExpressionStructureString}'" })
            }

            val hiddenGoalExpressionsStructureStrings = task.otherGoalData?.get("hiddenGoalExpressions") as List<String>?
            if (hiddenGoalExpressionsStructureStrings != null) {
                for (hiddenGoalExpressionsStructureString in hiddenGoalExpressionsStructureStrings) {
                    val hiddenGoalExpression = structureStringToExpression(hiddenGoalExpressionsStructureString)
                    mathhelper.twf.assert(hiddenGoalExpression.nodeType == NodeType.FUNCTION && hiddenGoalExpression.value == "",
                            { "Error: originalExpressionStructureString '${hiddenGoalExpressionsStructureString}' is incorrect '${hiddenGoalExpression.value}' " })

                    mathhelper.twf.assert(task.rules?.isNotEmpty() == true || compareWithoutSubstitutions(originalExpression, hiddenGoalExpression, notChangesOnVariablesFunction = setOf(), debugOutputMessages = debugOutputMessages),
                            { "Error: '${task.originalExpressionStructureString}' != '${hiddenGoalExpressionsStructureString}'" })

                    if (task.goalPattern != null) {
                        val node = expressionStructureConditionConstructor.parse(task.goalPattern!!)
                        if(!checkExpressionStructure(hiddenGoalExpression, node)) {
                            println("Warning: goalPattern '${task.goalPattern}' does not match hiddenGoalExpression '${hiddenGoalExpressionsStructureString}'")
                        }
                    }
                }
            }
        }
    }
}