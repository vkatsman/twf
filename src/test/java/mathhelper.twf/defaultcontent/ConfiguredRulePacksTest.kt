package mathhelper.twf.defaultcontent

import mathhelper.twf.api.compareWithoutSubstitutions
import mathhelper.twf.api.getAllExtendedAlgebraSubstitutions
import mathhelper.twf.expressiontree.NodeType
import mathhelper.twf.api.*
import org.junit.Test
import kotlin.test.assertTrue
import mathhelper.twf.assert
import mathhelper.twf.config.DebugOutputMessages
import mathhelper.twf.config.FunctionConfiguration
import mathhelper.twf.config.TreeTransformationRule
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultCombinatoricsRulePacks
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks
import kotlin.test.assertEquals

class ConfiguredRulePacksTest {
    @Test
    fun testAllExtendedAlgebraSubstitutionsCorrectness() {
        val allRules = getAllExtendedAlgebraSubstitutions()
        var countOfInEqualRules = 0
        for (rule in allRules) {
            assert(rule.left.value == "",
                    { "Error: rule.left.value is not empty top in rule '${rule.code}' '${rule.left}' ->  '${rule.right}' " })
            assert(rule.right.value == "",
                    { "Error: rule.right.value is not empty top in rule '${rule.code}' '${rule.left}' ->  '${rule.right}' " })

            if (rule.left.nodeType != NodeType.FUNCTION || rule.right.nodeType != NodeType.FUNCTION) {
                assert(rule.code.isNotBlank(),
                        { "Error: rule.code is blank in rule '${rule.code}' '${rule.left}' ->  '${rule.right}' " })
            } else {
                if (!compareWithoutSubstitutions(rule.left, rule.right, notChangesOnVariablesFunction = setOf())) {
                    countOfInEqualRules ++
                    println("Warning: '${rule.left}' != '${rule.right}'")
                }
            }
        }
        println(countOfInEqualRules)
        assert(countOfInEqualRules <= 50, { "too high countOfInEqualRules = '$countOfInEqualRules" })
    }

    @Test
    fun testCombinatoricSubstitutionsCorrectness() {
        val allRulePacks = DefaultCombinatoricsRulePacks.defaultCombinatoricsRulePacks
        var countOfInEqualRules = 0
        for (rulePack in allRulePacks) {
            val allRules = rulePack.rules!!
            for (ruleITR in allRules) {
                if (ruleITR.leftStructureString!!.isBlank() && ruleITR.rightStructureString!!.isBlank()) {
                    continue
                }
                val rule = expressionSubstitutionFromRuleITR(ruleITR)
                assert(rule.left.value == "",
                        { "Error: rule.left.value is not empty top in rule '${rule.code}' '${rule.left}' ->  '${rule.right}' " })
                assert(rule.right.value == "",
                        { "Error: rule.right.value is not empty top in rule '${rule.code}' '${rule.left}' ->  '${rule.right}' " })

                if (rule.left.nodeType != NodeType.FUNCTION || rule.right.nodeType != NodeType.FUNCTION) {
                    assert(rule.code.isNotBlank(),
                            { "Error: rule.code is blank in rule '${rule.code}' '${rule.left}' ->  '${rule.right}' " })
                } else {
                    if (!compareWithoutSubstitutions(rule.left, rule.right, notChangesOnVariablesFunction = setOf(),
//                                    debugOutputMessages = DebugOutputMessages(expressionProbabilityComparisonFalseDetailsPrintln = true),
                                    functionConfiguration = FunctionConfiguration(setOf(""), setOf()).apply {
                                        treeTransformationRules = mutableListOf<TreeTransformationRule>(
                                                TreeTransformationRule("U(m,n)", "m^n", true),
                                                TreeTransformationRule("A(m,n)", "m! / (m - n)!", true),
                                                TreeTransformationRule("P(n)", "n!", true),
                                                TreeTransformationRule("P(n)", "A(n,n)", true),
                                                TreeTransformationRule("C(m,n)", "m! / (m - n)! / n!", true),
                                                TreeTransformationRule("V(m,n)", "(m + n - 1)! / (m - 1)! / n!", true),
                                                TreeTransformationRule("C(n)", "(2*n)! / (n!)^2 / (n+1)", true),

                                                TreeTransformationRule("sqrt(x)", "x^0.5", true),
                                                TreeTransformationRule("root(x,p)", "x^(1/p)", true),
                                                TreeTransformationRule("log(a,b)", "ln(a) / ln(b)", true),
                                                TreeTransformationRule("sec(x)", "1 / cos(x)", true),
                                                TreeTransformationRule("csc(x)", "1 / sin(x)", true),
                                                TreeTransformationRule("n!", "P(i,1,n,i)", true),
                                                TreeTransformationRule("d(expr)", "d(expr,x)", true)
                                        )
                                    })) {
                        countOfInEqualRules ++
                        println("Warning: '${rule.left}' != '${rule.right}'")
                    }
                }
            }
        }
        println(countOfInEqualRules)
        assert(countOfInEqualRules <= 50, { "too high countOfInEqualRules = '$countOfInEqualRules" })
    }
}