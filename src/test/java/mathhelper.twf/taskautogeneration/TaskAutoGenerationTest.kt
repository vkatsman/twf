package mathhelper.twf.taskautogeneration

import mathhelper.twf.*
import mathhelper.twf.api.getTrigonometrySubstitutions
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.RuleITR
import mathhelper.twf.config.TaskITR
import mathhelper.twf.config.TaskSetITR
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultCombinatoricsRulePacks
import mathhelper.twf.defaultcontent.defaultrulepacks.StandardMathRulePacks
import mathhelper.twf.defaultcontent.defaultrulepacks.LogicRulePacks
import mathhelper.twf.defaultcontent.defaulttasksets.CombinatoricTaskSets
import mathhelper.twf.defaultcontent.defaulttasksets.InitialBestTaskSets
import mathhelper.twf.defaultcontent.defaulttasksets.LogicTaskSets
import mathhelper.twf.defaultcontent.defaulttasksets.LogicTaskSets.Companion.defaultLogicTaskSets
import mathhelper.twf.defaultcontent.defaulttasksets.TriginometryTaskSets.Companion.defaultTrigonometryTaskSets
import mathhelper.twf.defaultcontent.defaulttasksets.TriginometryTaskSets.Companion.shortMultiplicationTrigonometryTasks
import mathhelper.twf.defaultcontent.toTaskSetFrontInput
import mathhelper.twf.platformdependent.JsonParser
import org.junit.Ignore
import org.junit.Test
import kotlin.test.assertEquals


class TaskAutoGenerationTest {
    @Test
    @Ignore
    fun printLogicTaskSetWithRulePacksInITR () {
        val namespace = "global"
        val outputDirectoryPath = "/Users/vkatsman/Documents/global/tasks/"
        printTaskSetWithRulePacksInITR(namespace, outputDirectoryPath, LogicTaskSets.defaultLogicTaskSets.filter { it.code == "SubstifyBooleanLogicGameTasks" }.first(), LogicRulePacks.get())
    }

    @Test
    @Ignore
    fun printInitialBestSetsWithRulePacksInITR () {
        val namespace = "global"
        val outputDirectoryPath = "/Users/vkatsman/Documents/global/tasks/"
        printTaskSetWithRulePacksInITR(namespace, outputDirectoryPath, InitialBestTaskSets.defaultInitialBestSets.first(), StandardMathRulePacks.get())
        printJsonTaskSetsInFrontInput(namespace, outputDirectoryPath, InitialBestTaskSets.defaultInitialBestSets)
    }



    @Test
    @Ignore
    fun printActualRulePacksInITR () {
        val namespace = "global"
        val outputDirectoryPath = "/Users/vkatsman/Documents/global/rules/"
        printJsonRulePacksInITR(namespace, outputDirectoryPath, LogicRulePacks.get())
    }

    @Test
    @Ignore
    fun printActualLogicTaskSetsInITR () {
        val namespace = "global"
        val outputDirectoryPath = "/Users/vkatsman/Documents/global/tasks/"
        printJsonTaskSetsInITR(namespace, outputDirectoryPath, LogicTaskSets.defaultLogicTaskSets)
    }

    @Test
    @Ignore
    fun printActualRulePacksInFrontInput () {
        //val namespace = "fanovikov_polytech_spring_2022"
        val namespace = "fanovikov_polytech"
        val outputDirectoryPath = "/Users/vkatsman/Documents/fanovikov_polytech/rules_2022_05_14/"
        printJsonRulePacksInFrontInput(namespace, outputDirectoryPath, DefaultCombinatoricsRulePacks.get()) // defaultRulePacks)
    }

    @Test
    @Ignore
    fun printActualTaskSetsInITR() {
        val namespace = "fanovikov_polytech"
        val outputDirectoryPath = "/Users/vkatsman/Documents/fanovikov_polytech/tasks_2022_05_20/"
        printJsonTaskSetsInFrontInput(namespace, outputDirectoryPath, CombinatoricTaskSets.defaultCombinatoricsTaskSets) //defaultTaskSets)
    }

    @Test
    @Ignore
    fun printActualTaskSetsInFrontInput() {
        val namespace = "fanovikov_polytech_spring_2022"
        val outputDirectoryPath = "/Users/vkatsman/Documents/fanovikov_polytech/tasks/"
        printJsonTaskSetsInFrontInput(namespace, outputDirectoryPath, LogicTaskSets.defaultLogicTaskSets) //defaultTaskSets)
    }

    @Test
    @Ignore
    fun printTrigonometryTaskSets() {
        val namespace = "global_test"
        val outputDirectoryPath = "/Users/vkatsman/Documents/MathHelperTaskSets/"
        printJsonTaskSetsInFrontInput(namespace, outputDirectoryPath, defaultTrigonometryTaskSets)
    }

    @Test
    @Ignore
    fun printLogicTaskSets() {
        val namespace = "global_test"
        val outputDirectoryPath = "/Users/vkatsman/Documents/MathHelperTaskSets/"
        printJsonTaskSetsInFrontInput(namespace, outputDirectoryPath, defaultLogicTaskSets)
    }


    @Test
    @Ignore
    fun shortMultiplicationTasksGeneration() {
        val compiledConfiguration = CompiledConfiguration()
        val tasks = generateExpressionTransformationTasks(
                ExpressionTaskGeneratorSettings(
                        ExpressionGenerationDirection.ORIGINAL_TO_FINAL,
                        6,
                        5,
                        { trigonometryShortMultiplicationStartExpressionGeneration(compiledConfiguration) },
                        compiledConfiguration,
                        getTrigonometrySubstitutions()
                )
        )

        tasks.forEach {
            println("${it.originalExpressionStructureString} -> ${it.goalExpressionStructureString}")
            println("${it.solutionPlainText}")
            println("${it.hints}\n")
        }
    }

    @Test
    @Ignore
    fun printShortMultiplicationTrigonometryTaskSet() {
        val taskSetITR = TaskSetITR(
                code = "TrigonometryShortMultiplication",
                tasks = shortMultiplicationTrigonometryTasks)

        val namespace = "global_test"
        val outputDirectoryPath = "~/Documents/MathHelperTaskSets/"
        printJsonTaskSets(namespace, outputDirectoryPath, listOf(taskSetITR))
    }

    @Test
    fun testJsonOtherGoalDataDeserialiser () {
        val otherGoalData = "{\"otherGoalData\": {\n" +
                "    \"hiddenGoalExpressions\": [\n" +
                "      \"(0)\"\n" +
                "    ]\n" +
                "  }}"

        val jsonObject = JsonParser.parseMap(otherGoalData)
        assertEquals("{otherGoalData={hiddenGoalExpressions=[(0)]}}", jsonObject.toString())
        val otherGoalDataMap = (jsonObject as Map<String, Any>).get("otherGoalData")
        assertEquals("{hiddenGoalExpressions=[(0)]}", otherGoalDataMap.toString())
        val hiddenGoalExpressions = (otherGoalDataMap as Map<String, Any>).get("hiddenGoalExpressions") as List<String>
        assertEquals("[(0)]", hiddenGoalExpressions.toString())
        assertEquals("(0)", hiddenGoalExpressions.first().toString())
    }

    @Test
    @Ignore
    fun testAutoCompletionTaskITR() {
        val testTasks = listOf<TaskITR>(
                TaskITR(
                        originalExpressionStructureString = "(/(*(+(2;*(2;sin(x)));+(1;-(sin(x))));*(+(1;cos(x));+(2;-(*(2;cos(x)))))))",
                        goalType = "expression",
                        goalExpressionStructureString = "(4)",
                        rules = listOf(
                                RuleITR(leftStructureString = "(ctg(x))", rightStructureString = "(2)", basedOnTaskContext = true, matchJumbledAndNested = false, simpleAdditional = false, isExtending = false, priority = 2, code = "", normalizationType = "ORIGINAL")
                        ),
                        tags = mutableSetOf("shortMultiplication"),
                        difficulty = 1.0
                ),
                TaskITR(
                        originalExpressionStructureString = "(/(*(+(2;*(2;sin(x)));+(1;-(sin(x))));*(+(1;cos(x));+(2;-(*(2;cos(x)))))))",
                        goalType = "computation",
                        goalPattern = "+:0-1(-:1):?:?R",
                        tags = mutableSetOf("shortMultiplication"),
                        difficulty = 1.0
                ),
                TaskITR(
                        originalExpressionStructureString = "(/(+(sin(x);-(sin(*(3;x)));-(sin(*(5;x)));sin(*(7;x)));+(cos(x);-(cos(*(3;x)));cos(*(5;x));-(cos(*(7;x))))))", //TODO
                        goalType = "simplification",
                        goalPattern = "tg,* : (* : 2)",
                        tags = mutableSetOf(),
                        difficulty = 1.0
                )
        )

        val defaultTrigonometryTaskSets = listOf(
                TaskSetITR(
                        code = "TrigonometryShortMultiplication",
                        nameEn = "Short Multiplication in Trigonometry", nameRu = "Сокращенное умножение в тригонометрии",
                        descriptionShortEn = "New approach to formulas for short multiplication",
                        descriptionShortRu = "Выводи формулы сокращенного умножения по новому",
                        descriptionEn = "Derive short multiplication formulas using trigonometric transformations",
                        descriptionRu = "Выводи формулы сокращенного умножения с использованием тригонометрических преобразований",
                        subjectType = "standard_math",
                        tasks = testTasks
                )
        )

        val namespace = "global_test"

        val gsonWriter = getGsonWriter()
        val result = StringBuilder()
        for (taskSet in defaultTrigonometryTaskSets) {
            val modifiedTaskSet = taskSet.cloneBesidesMaps(newNamespaceCode = namespace).toTaskSetFrontInput()
            result.append(gsonWriter.toJson(modifiedTaskSet))
        }
        assertEquals("" +
                "{\n" +
                "  \"code\": \"global_test__TrigonometryShortMultiplication\",\n" +
                "  \"version\": 0,\n" +
                "  \"namespaceCode\": \"global_test\",\n" +
                "  \"nameEn\": \"Short Multiplication in Trigonometry\",\n" +
                "  \"nameRu\": \"Сокращенное умножение в тригонометрии\",\n" +
                "  \"descriptionShortEn\": \"New approach to formulas for short multiplication\",\n" +
                "  \"descriptionShortRu\": \"Выводи формулы сокращенного умножения по новому\",\n" +
                "  \"descriptionEn\": \"Derive short multiplication formulas using trigonometric transformations\",\n" +
                "  \"descriptionRu\": \"Выводи формулы сокращенного умножения с использованием тригонометрических преобразований\",\n" +
                "  \"subjectType\": \"standard_math\",\n" +
                "  \"tasks\": [\n" +
                "    {\n" +
                "      \"taskCreationType\": \"manual\",\n" +
                "      \"code\": \"global_test__TrigonometryShortMultiplication__0_Level 0 Proof\",\n" +
                "      \"version\": 0,\n" +
                "      \"namespaceCode\": \"global_test\",\n" +
                "      \"nameEn\": \"Level 0 Level 0 Proof\",\n" +
                "      \"nameRu\": \"Уровень 0 Уровень 0 Доказательство\",\n" +
                "      \"descriptionShortEn\": \"Relate to if 'ctg(x) = 2'\",\n" +
                "      \"descriptionShortRu\": \"Свести к если 'ctg(x) = 2'\",\n" +
                "      \"descriptionEn\": \"Prove '((2+2*sin(x))*(1-sin(x)))/((1+cos(x))*(2-2*cos(x))) = 4' if 'ctg(x) = 2'\",\n" +
                "      \"descriptionRu\": \"Доказать '((2+2*sin(x))*(1-sin(x)))/((1+cos(x))*(2-2*cos(x))) = 4', если 'ctg(x) = 2'\",\n" +
                "      \"subjectType\": \"standard_math\",\n" +
                "      \"originalExpression\": {\n" +
                "        \"expression\": \"(/(*(+(2;*(2;sin(x)));+(1;-(sin(x))));*(+(1;cos(x));+(2;-(*(2;cos(x)))))))\",\n" +
                "        \"format\": \"STRUCTURE_STRING\"\n" +
                "      },\n" +
                "      \"goalType\": \"expression\",\n" +
                "      \"goalExpression\": {\n" +
                "        \"expression\": \"(4)\",\n" +
                "        \"format\": \"STRUCTURE_STRING\"\n" +
                "      },\n" +
                "      \"rules\": [\n" +
                "        {\n" +
                "          \"code\": \"(ctg(x))__to__(2)\",\n" +
                "          \"left\": {\n" +
                "            \"expression\": \"(ctg(x))\",\n" +
                "            \"format\": \"STRUCTURE_STRING\"\n" +
                "          },\n" +
                "          \"right\": {\n" +
                "            \"expression\": \"(2)\",\n" +
                "            \"format\": \"STRUCTURE_STRING\"\n" +
                "          },\n" +
                "          \"priority\": 2,\n" +
                "          \"isExtending\": false,\n" +
                "          \"matchJumbledAndNested\": false,\n" +
                "          \"simpleAdditional\": false,\n" +
                "          \"basedOnTaskContext\": true,\n" +
                "          \"normalizationType\": \"ORIGINAL\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"taskCreationType\": \"manual\",\n" +
                "      \"code\": \"global_test__TrigonometryShortMultiplication__1_Level 1 Computation\",\n" +
                "      \"version\": 0,\n" +
                "      \"namespaceCode\": \"global_test\",\n" +
                "      \"nameEn\": \"Level 1 Level 1 Computation\",\n" +
                "      \"nameRu\": \"Уровень 1 Уровень 1 Вычисление\",\n" +
                "      \"descriptionShortEn\": \"Compute\",\n" +
                "      \"descriptionShortRu\": \"Вычислить\",\n" +
                "      \"descriptionEn\": \"Compute '((2+2*sin(x))*(1-sin(x)))/((1+cos(x))*(2-2*cos(x)))'\",\n" +
                "      \"descriptionRu\": \"Вычислить '((2+2*sin(x))*(1-sin(x)))/((1+cos(x))*(2-2*cos(x)))'\",\n" +
                "      \"subjectType\": \"standard_math\",\n" +
                "      \"originalExpression\": {\n" +
                "        \"expression\": \"(/(*(+(2;*(2;sin(x)));+(1;-(sin(x))));*(+(1;cos(x));+(2;-(*(2;cos(x)))))))\",\n" +
                "        \"format\": \"STRUCTURE_STRING\"\n" +
                "      },\n" +
                "      \"goalType\": \"computation\",\n" +
                "      \"goalPattern\": \"+:0-1(-:1):?:?R\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"taskCreationType\": \"manual\",\n" +
                "      \"code\": \"global_test__TrigonometryShortMultiplication__2_Level 2 Simplification\",\n" +
                "      \"version\": 0,\n" +
                "      \"namespaceCode\": \"global_test\",\n" +
                "      \"nameEn\": \"Level 2 Level 2 Simplification\",\n" +
                "      \"nameRu\": \"Уровень 2 Уровень 2 Упрощение\",\n" +
                "      \"descriptionShortEn\": \"Simplify\",\n" +
                "      \"descriptionShortRu\": \"Упростить\",\n" +
                "      \"descriptionEn\": \"Simplify '(sin(x)-sin(3*x)-sin(5*x)+sin(7*x))/(cos(x)-cos(3*x)+cos(5*x)-cos(7*x))'\",\n" +
                "      \"descriptionRu\": \"Упростить '(sin(x)-sin(3*x)-sin(5*x)+sin(7*x))/(cos(x)-cos(3*x)+cos(5*x)-cos(7*x))'\",\n" +
                "      \"subjectType\": \"standard_math\",\n" +
                "      \"originalExpression\": {\n" +
                "        \"expression\": \"(/(+(sin(x);-(sin(*(3;x)));-(sin(*(5;x)));sin(*(7;x)));+(cos(x);-(cos(*(3;x)));cos(*(5;x));-(cos(*(7;x))))))\",\n" +
                "        \"format\": \"STRUCTURE_STRING\"\n" +
                "      },\n" +
                "      \"goalType\": \"simplification\",\n" +
                "      \"goalPattern\": \"tg,* : (* : 2)\"\n" +
                "    }\n" +
                "  ]\n" +
                "}" +
                "", result.toString())
    }

//    @Test
//    fun oneOriginalToFinalTest() {
//        val task = generation(
//                ExpressionGenerationDirection.ORIGINAL_TO_FINAL,
//                expressionSubstitutions = mutableListOf<ExpressionSubstitution>(
//                        ExpressionSubstitution(structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"), structureStringToExpression("1"))
//                ),
//                stepsCount = 1,
//                expressionsForGeneration = mutableListOf(structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"))
//        )
//        val result = structureStringToExpression("1")
//        assertTrue { compareExpressionNodes(result, task.finalExpression) }
//        assertEquals(1, task.requiredSubstitutions.size)
//        /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}\n")
//        for (substitution in task.requiredSubstitutions) {
//            println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}\n")
//        }*/
//    }
//
//    //have to fix 1
//    @Test
//    @Ignore
//    fun oneOriginalToFinalInvTest() {
//        val task = generation(
//                ExpressionGenerationDirection.ORIGINAL_TO_FINAL,
//                expressionSubstitutions = mutableListOf<ExpressionSubstitution>(
//                        ExpressionSubstitution(structureStringToExpression("(1)"), structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"))
//                ),
//                stepsCount = 1,
//                expressionsForGeneration = mutableListOf(structureStringToExpression("(1)"))
//        )
//        val result = structureStringToExpression("+(^(sin(x);2);^(cos(x);2))")
//        assertTrue { compareExpressionNodes(result, task.finalExpression) }
//        /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}\n")
//        for (substitution in task.requiredSubstitutions) {
//            println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}\n")
//        }*/
//    }
//
//    //have to fix 1
//    @Test
//    @Ignore
//    fun oneFinalToOriginalTest() {
//        val task = generation(
//                ExpressionGenerationDirection.FINAL_TO_ORIGINAL,
//                expressionSubstitutions =
//                mutableListOf<ExpressionSubstitution>(
//                        ExpressionSubstitution(structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"), structureStringToExpression("1"))
//                ),
//                stepsCount = 1,
//                expressionsForGeneration = mutableListOf(structureStringToExpression("(1)"))
//        )
//        val result = structureStringToExpression("+(^(sin(x);2);^(cos(x);2))")
//        assertTrue { compareExpressionNodes(result, task.originalExpression) }
//        /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}\n")
//        for (substitution in task.requiredSubstitutions) {
//            println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}\n")
//        }*/
//    }
//
//    @Test
//    fun oneFinalToOriginalInvTest() {
//        val task = generation(
//                ExpressionGenerationDirection.FINAL_TO_ORIGINAL,
//                expressionSubstitutions = mutableListOf<ExpressionSubstitution>(
//                        ExpressionSubstitution(structureStringToExpression("(1)"), structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"))
//                ),
//                stepsCount = 1,
//                expressionsForGeneration = mutableListOf(structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"))
//        )
//        val result = structureStringToExpression("1")
//        assertTrue { compareExpressionNodes(result, task.originalExpression) }
//        /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}\n")
//        for (substitution in task.requiredSubstitutions) {
//            println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}\n")
//        }*/
//    }
//
//    @Test
//    fun oneFinalToOriginalBothTest() {
//        val task = generation(
//                ExpressionGenerationDirection.FINAL_TO_ORIGINAL,
//                expressionSubstitutions = mutableListOf<ExpressionSubstitution>(
//                        ExpressionSubstitution(structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"), structureStringToExpression("1")),
//                        ExpressionSubstitution(structureStringToExpression("(1)"), structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"))
//                ),
//                stepsCount = 5,
//                expressionsForGeneration = mutableListOf(structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"))
//        )
//        val result = structureStringToExpression("1")
//        assertTrue { compareExpressionNodes(result, task.originalExpression) }
//        /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}\n")
//        for (substitution in task.requiredSubstitutions) {
//            println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}")
//        }
//        println()
//        for (substitution in task.allSubstitutions) {
//            println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}")
//        }*/
//    }
//
//    @Test //failed
//    @Ignore
//    fun workingWithConstTest() {
//        val task = generation(
//                ExpressionGenerationDirection.ORIGINAL_TO_FINAL,
//                expressionSubstitutions = mutableListOf<ExpressionSubstitution>(
//                        ExpressionSubstitution(structureStringToExpression("(+(^(sin(x);2);^(cos(x);2)))"), structureStringToExpression("1")),
//                        ExpressionSubstitution(structureStringToExpression("(1)"), structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"))
//                ),
//                stepsCount = 1,
//                expressionsForGeneration = mutableListOf(structureStringToExpression("2"))
//        )
//        println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}\n")
//    }
//
//    @Ignore
//    @Test
//    fun allFunctionsOriginalToFinalTest1() {
//        for (i in 1..10) {
//            val task = generation(
//                    ExpressionGenerationDirection.ORIGINAL_TO_FINAL,
//                    getTrigonometrySubstitutions(),
//                    stepsCount = 5,
//                    expressionsForGeneration = mutableListOf(structureStringToExpression(
//                            "(+(^(sin(x);2);^(cos(x);2)))"))
//            )
//            //println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}\n")
//        }
//    }
//
//    @Ignore
//    @Test
//    fun allFunctionsOriginalToFinalTest() {
//        for (i in 1..10) {
//            val task = generation(
//                    ExpressionGenerationDirection.ORIGINAL_TO_FINAL,
//                    getTrigonometrySubstitutions(),
//                    stepsCount = randomInt(1, 5),
//                    expressionsForGeneration = mutableListOf(structureStringToExpression(
//                            "(+(/(1;+(1;^(tg(x);2)));/(1;+(1;^(ctg(x);2)))))"))
//            )
//            //println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}\n")
//        }
//    }
//
//
//    @Test
//    fun allFunctionsFinalToOriginalOneStepTest() {
//        val allSubstitutions = getTrigonometrySubstitutions()
//        for (i in 1..10) {
//            val task = generation(
//                    ExpressionGenerationDirection.FINAL_TO_ORIGINAL,
//                    allSubstitutions,
//                    stepsCount = 1,
//                    expressionsForGeneration = mutableListOf(structureStringToExpression(
//                            "(+(^(sin(x);2);^(cos(x);2)))"))
//            )
//            assertTrue{task.requiredSubstitutions.size <= 1}
//            /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}")
//            for (substitution in task.requiredSubstitutions) {
//                println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}")
//            }
//            println()*/
//        }
//    }
//
//    @Test
//    @Ignore
//    fun allFunctionsFinalToOriginalTwoStepsTest() {
//        val allSubstitutions = getTrigonometrySubstitutions()
//        for (i in 1..10) {
//            val task = generation(
//                    ExpressionGenerationDirection.FINAL_TO_ORIGINAL,
//                    allSubstitutions,
//                    stepsCount = 2,
//                    expressionsForGeneration = mutableListOf(structureStringToExpression(
//                            "+(^(sin(x);2);^(cos(x);2))"))
//            )
//            assertTrue{task.requiredSubstitutions.size <= 2}
//            /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}")
//            for (substitution in task.requiredSubstitutions) {
//                println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}")
//            }
//            println()*/
//        }
//    }
//
//    @Test
//    @Ignore
//    fun allFunctionsFinalToOriginalThreeStepsTest() {
//        val allSubstitutions = getTrigonometrySubstitutions()
//        for (i in 1..10) {
//            val task = generation(
//                    ExpressionGenerationDirection.FINAL_TO_ORIGINAL,
//                    allSubstitutions,
//                    stepsCount = 3,
//                    expressionsForGeneration = mutableListOf(structureStringToExpression(
//                            "+(^(sin(x);2);^(cos(x);2))"))
//            )
//            assertTrue{task.requiredSubstitutions.size <= 3}
//            /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}")
//            for (substitution in task.requiredSubstitutions) {
//                println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}")
//            }
//            println()*/
//        }
//    }
//
//    @Test
//    fun allFunctionsFinalToOriginalFiveStepsTest() {
//        val allSubstitutions = getTrigonometrySubstitutions()
//        for (i in 1..10) {
//            val task = generation(
//                    ExpressionGenerationDirection.FINAL_TO_ORIGINAL,
//                    allSubstitutions,
//                    stepsCount = 5,
//                    expressionsForGeneration = mutableListOf(structureStringToExpression(
//                            "+(^(sin(x);2);^(cos(x);2))"))
//            )
//            assertTrue{task.requiredSubstitutions.size <= 5}
//            /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}")
//            for (substitution in task.requiredSubstitutions) {
//                println("${expressionToStructureString(substitution.left)} -> ${expressionToStructureString(substitution.right)}")
//            }
//            println()*/
//        }
//    }
//
//    @Test
//    fun notGoBackTestOnePlace() {
//        val task = generation(
//                ExpressionGenerationDirection.FINAL_TO_ORIGINAL,
//                mutableListOf<ExpressionSubstitution>(
//                        ExpressionSubstitution(structureStringToExpression("^(sin(x);2)"), structureStringToExpression("+(1;-(^(cos(x);2)))")),
//                        ExpressionSubstitution(structureStringToExpression("+(1;-(^(cos(x);2)))"), structureStringToExpression("^(sin(x);2)"))
//                ),
//                stepsCount = 4,
//                expressionsForGeneration = mutableListOf(structureStringToExpression("+(^(sin(x);2);^(cos(x);2))"))
//        )
//        val result = structureStringToExpression("(+(+(1;-(^(cos(x);2)));^(cos(x);2)))")
//        assertTrue{ compareExpressionNodes(result, task.originalExpression) }
//        assertEquals(1, task.requiredSubstitutions.size)
//        /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}\n")
//        for (substitution in task.requiredSubstitutions) {
//            println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}\n")
//        }*/
//    }
//
//    @Test
//    fun notGoBackTest() {
//        val task = generation(
//                ExpressionGenerationDirection.FINAL_TO_ORIGINAL,
//                mutableListOf<ExpressionSubstitution>(
//                        ExpressionSubstitution(structureStringToExpression("(^(sin(x);2))"), structureStringToExpression("(+(1;-(^(cos(x);2))))")),
//                        ExpressionSubstitution(structureStringToExpression("(+(1;-(^(cos(x);2))))"), structureStringToExpression("(^(sin(x);2))"))
//                ),
//                stepsCount = 10,
//                expressionsForGeneration = mutableListOf(structureStringToExpression("(+(+(^(sin(x);2);^(sin(x);2));^(cos(x);2)))"))
//        )
//        val result = structureStringToExpression("(+(+(+(1;-(^(cos(x);2)));+(1;-(^(cos(x);2))));^(cos(x);2)))")
//        assertTrue{ compareExpressionNodes(result, task.originalExpression) }
//        /*println("${expressionToString(task.originalExpression)} -> ${expressionToString(task.finalExpression)}\n")
//        for (substitution in task.requiredSubstitutions) {
//            println("${expressionToString(substitution.left)} -> ${expressionToString(substitution.right)}\n")
//        }*/
//    }
//
//    @Test
//    @Ignore
//    fun test1() {
//        val allSubstitutions = getTrigonometrySubstitutions()
//        val tasks = generateExpressionTaskFromFinalExpressions(
//                    allSubstitutions,
//                    5,
//                    mutableListOf(structureStringToExpression(
//                            "+(^(sin(x);2);^(cos(x);2))"))
//        )
//        for (task in tasks) {
//            println("${expressionToString(task.originalExpression)}, ${task.originalExpression.badStructureFine()}  -> ${expressionToString(task.finalExpression)},  ${task.finalExpression.badStructureFine()}\n")
//        }
//    }

}