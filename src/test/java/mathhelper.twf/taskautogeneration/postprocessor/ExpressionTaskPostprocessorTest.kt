package mathhelper.twf.taskautogeneration.postprocessor

import mathhelper.twf.api.expressionSubstitutionsFromRulePackITR
import mathhelper.twf.api.structureStringToExpression
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.expressiontree.ExpressionNode
import mathhelper.twf.expressiontree.ExpressionSubstitution
import mathhelper.twf.taskautogeneration.*
import mathhelper.twf.taskautogeneration.ExpressionUtils
import mathhelper.twf.taskautogeneration.rulepack.PostprocessorRulePack
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ExpressionTaskPostprocessorTest {
    val compiledConfiguration = CompiledConfiguration()
    val expressionComparator = compiledConfiguration.factComparator.expressionComparator

    companion object {
        @JvmStatic
        fun expressionsToPostprocess() = listOf(
            Arguments.of("-(-a)", "(a)"),
            Arguments.of("a/1", "(a)"),
            Arguments.of("a/(-1)", "(+(-(a)))"),
            Arguments.of("a*1", "(a)"),
            Arguments.of("1*a", "(a)"),
            Arguments.of("a*a", "(^(a;2))"),
            Arguments.of("a/a", "(1)"),
            Arguments.of("1/(1/a)", "(a)"),
            Arguments.of("(1/a)*a", "(1)"),
            Arguments.of("(-a)^2", "(^(a;2))"),
            Arguments.of("(-a)^(2*n)", "(^(a;*(2;n)))"),
            Arguments.of("(a^2)^0.5", "(a)"),
            Arguments.of("(a^0.5)^2", "(a)"),
            Arguments.of("(-a)*(-a)", "(^(a;2))"),
            Arguments.of("(a)*(-a)", "(+(-(^(a;2))))"),
            Arguments.of("(-a)*(a)", "(+(-(^(a;2))))"),
            Arguments.of("(-a)*(b)", "(+(-(*(a;b))))"),
            Arguments.of("(a)*(-b)", "(+(-(*(a;b))))"),
            Arguments.of("(-a)/(b)", "(+(-(/(a;b))))"),
            Arguments.of("(a)/(-b)", "(+(-(/(a;b))))"),
            Arguments.of("(1)/(b/a)", "(/(a;b))"),
            Arguments.of("(1/(a^(-1)))", "(a)"),
            Arguments.of("(1/(a^(-b)))", "(^(a;b))"),
            Arguments.of("(a^1)", "(a)"),
            Arguments.of("(a+a)", "(*(2;a))"),
            Arguments.of("(a^(-1))", "(/(1;a))"),
            Arguments.of("(a/(1/b))", "(*(a;b))"),
            Arguments.of("a^(-2)", "(/(1;^(a;2)))"),
            Arguments.of("a|a", "(a)"),
            Arguments.of("a&a", "(a)"),
            Arguments.of("or(a)", "(a)"),
            Arguments.of("and(a)", "(a)"),
            Arguments.of("\\neg(\\neg(a|b))", "(or(a;b))"),
            Arguments.of("\\neg(\\neg(b|b))", "(b)"),
            Arguments.of("1/tg(a)", "(ctg(a))"),
            Arguments.of("1/ctg(a)", "(tg(a))"),
            Arguments.of("sin(a)/cos(a)", "(tg(a))"),
            Arguments.of("cos(a)/sin(a)", "(ctg(a))"),
            Arguments.of("a-a", "(0)"),
            Arguments.of("-a+a", "(0)"),
            Arguments.of("a+0", "(a)"),
            Arguments.of("(a/b/c)", "(/(*(a;c);b))"),
            Arguments.of("(a+b)+c", "(+(a;b;c))"),
            Arguments.of("(a+b)-c", "(+(a;b;-(c)))"),
            Arguments.of("(a-b)+c", "(+(a;-(b);c))"),
            Arguments.of("(a/(b/c)^2)", "(/(*(a;^(c;2));^(b;2)))"),
            Arguments.of("(a/0.5)", "(*(2;a))"),
            Arguments.of("(1+a^2/0.5)", "(+(1;*(2;^(a;2))))"),
            Arguments.of("(1+a^2*0.5)", "(+(1;*(^(a;2);0.5)))"),
            Arguments.of("(0/a+1/a)", "(/(1;a))"),
            Arguments.of("(0*a+1*a)", "(a)"),
            Arguments.of("(0*a+1*a)", "(a)"),
            Arguments.of("(ctg(x)*ctg(x)*2)", "(*(^(ctg(x);2);2))"),
            Arguments.of("(2*ctg(x)*ctg(x))", "(*(2;^(ctg(x);2)))"),
            Arguments.of("(1+cos(x)^(-2))", "(+(1;/(1;^(cos(x);2))))"),
            Arguments.of("(2*(a/2))", "(a)"),
            Arguments.of("(3*(a/3))", "(a)")
        )

        @JvmStatic
        fun expressionsForMethodGetRidOfMultilayerFractions() = listOf(
            Arguments.of(
                "(/(/(pi;2);2))",
                "(/(pi;*(2;2)))"
            ),
            Arguments.of(
                "(cos(/(/(pi;2);2)))",
                "(cos(/(pi;*(2;2))))"
            ),
            Arguments.of(
                "(/(/(a;b);c))",
                "(/(a;*(b;c)))"
            ),
            Arguments.of(
                "(/(a;/(b;c)))",
                "(/(*(a;c);b))"
            ),
            Arguments.of(
                "(/(a;/(b;+(c;d))))",
                "(/(*(a;+(c;d));b))"
            ),
            Arguments.of(
                "(+(/(a;/(b;+(c;d)));/(e;/(f;+(g;h)))))",
                "(+(/(*(a;+(c;d));b);/(*(e;+(g;h));f)))"
            ),
            Arguments.of(
                "(/(a;+(/(b;+(c;d));e)))",
                "(/(a;+(/(b;+(c;d));e)))"
            ),
            Arguments.of(
                "(/(/(a;f);+(/(b;+(c;d));e)))",
                "(/(a;*(f;+(/(b;+(c;d));e))))"
            ),
            Arguments.of(
                "(/(*(a;d;/(e;f));*(b;c;/(g;h))))",
                "(/(*(a;d;e;h);*(f;b;c;g)))"
            ),
            Arguments.of(
                "(/(/(a;b);/(c;d)))",
                "(/(*(a;d);*(b;c)))"
            ),
            Arguments.of(
                "(/(/(/(a;b);/(c;d));/(e;f)))",
                "(/(*(a;d;f);*(b;c;e)))"
            ),
            Arguments.of(
                "(/(/(+(*(3;cos(x));cos(*(3;x)));*(4;(^(cos(x);3))));^(cos(*(2;x));2)))",
                "(/(+(*(3;cos(x));cos(*(3;x)));*(4;(^(cos(x);3));^(cos(*(2;x));2))))"
            ),
            Arguments.of(
                "(/(*(2;tg(x));+(/(/(/(*(2;tg(x));+(1;^(tg(x);2)));*(2;cos(x);sin(x)));^(sin(x);2));+(-(^(ctg(x);2)));+(-(^(tg(x);2))))))",
                "(/(*(2;tg(x));+(/(*(2;tg(x));*(+(1;^(tg(x);2));2;cos(x);sin(x);^(sin(x);2)));+(-(^(ctg(x);2)));+(-(^(tg(x);2))))))"
            )
        )

        @JvmStatic
        fun expressionsForMethodDragTheDescendantsOfPlusUpper() = listOf(
            /*Arguments.of(
                "(+(a;+(-(+(+(b;c);+(-(d)))))))",
                "(+(a;+(-(b));+(-(c));d))"
            ),*/ // TODO: somehow fix this
            Arguments.of(
                "(+(+(a;b);c))",
                "(+(a;b;c))"
            ),
            Arguments.of(
                "(+(+(a;b);+(-(c))))",
                "(+(a;b;-(c)))"
            ),
            Arguments.of(
                "(+(+(a;b);+(c;d)))",
                "(+(a;b;c;d))"
            ),
            Arguments.of(
                "(+(+(a;b);e;+(c;d)))",
                "(+(a;b;e;c;d))"
            ),
            Arguments.of(
                "(*((+(+(1;^(tg(x);2));+(-(^(tg(x);2)));^(ctg(x);2)));^(sin(x);2)))",
                "(*((+(1;^(tg(x);2);-(^(tg(x);2));^(ctg(x);2)));^(sin(x);2)))"
            )
        )

        @JvmStatic
        fun expressionsForMethodDragTheDescendantsOfMultUpper() = listOf(
            Arguments.of(
                "(*(*(a;b);c))",
                "(*(a;b;c))"
            ),
            Arguments.of(
                "(*(3;*(a;b)))",
                "(*(3;a;b))"
            ),
            Arguments.of(
                "(*(/(1;2);*(a;b)))",
                "(*(/(1;2);a;b))"
            )
        )

        @JvmStatic
        fun expressionsForMethodDragConstantsToTheLeft() = listOf(
            Arguments.of(
                "(*(a;b;+(-(2))))",
                "(*(+(-(2));a;b))"
            ),
            Arguments.of(
                "(*(/(1;^(sin(a);2));2))",
                "(*(2;/(1;^(sin(a);2))))"
            ),
            Arguments.of(
                "(*(*(+(1;^(ctg(x);2));^(sin(x);2));2))",
                "(*(2;*(^(sin(x);2);+(1;^(ctg(x);2)))))"
            ),
            Arguments.of(
                "(*(a;2))",
                "(*(2;a))"
            ),
            Arguments.of(
                "(*(a;2;b))",
                "(*(2;a;b))"
            ),
            Arguments.of(
                "(+(*(a;2);*(b;4)))",
                "(+(*(2;a);*(4;b)))"
            ),
            Arguments.of(
                "(+(1;a;*(a;5)))",
                "(+(1;a;*(5;a)))"
            )
        )

        @JvmStatic
        fun expressionsForMethodReduceFractions() = listOf(
            Arguments.of(
               "(/(*(a;b);*(a;a)))",
               "(/(b;a))"
            ),
            Arguments.of(
                "(/(+(*(a;b);1);*(c;b;a;+(*(a;b);1))))",
                "(/(1;*(c;b;a)))"
            ),
            Arguments.of(
                "(/(*(a;b);*(b;c)))",
                "(/(a;c))"
            ),
            Arguments.of(
                "(/(*(a;b;d;e);*(b;e;c)))",
                "(/(*(a;d);c))"
            ),
            Arguments.of(
                "(/(*(a;b);*(b;a)))",
                "(/(1;1))"
            ),
            Arguments.of(
                "(/(*(a;b;c);*(b;a)))",
                "(/(c;1))"
            ),
            Arguments.of(
                "(/(*(a;b);*(c;b;a)))",
                "(/(1;c))"
            ),
            Arguments.of(
                "(/(+(*(a;b);1);*(c;b;a)))",
                "(/(+(*(a;b);1);*(c;b;a)))"
            ),
            Arguments.of(
                "(/(*(a;b;1);*(b;c)))",
                "(/(a;c))"
            ),
            Arguments.of(
                "(ctg(/(*(10;x);4)))",
                "(ctg(/(*(5;x);2)))"
            )
        )

        @JvmStatic
        fun expressionsForMethodGetRidOfDuplicatedMultipliers() = listOf(
            Arguments.of(
                "(*(cos(x);cos(x)))",
                "(^(cos(x);2))"
            ),
            Arguments.of(
                "(*(cos(x);cos(x);cos(x)))",
                "(^(cos(x);3))"
            ),
            Arguments.of(
                "(*(sin(x);cos(x);cos(x);cos(x)))",
                "(*(sin(x);^(cos(x);3)))"
            ),
            Arguments.of(
                "(+(1;*(sin(x);cos(x);cos(x);cos(x))))",
                "(+(1;*(sin(x);^(cos(x);3))))"
            ),
            Arguments.of(
                "(/(*(a;a;a;a);*(b;b;b;b;b)))",
                "(/(^(a;4);^(b;5)))"
            ),
            Arguments.of(
                "(/(*(a;a;a;a;c);*(b;b;b;b;b;d)))",
                "(/(*(^(a;4);c);*(^(b;5);d)))"
            ),
            Arguments.of(
                "(/(+(*(+(sin(x);+(-(cos(x)));+(sin(x);+(-(cos(x))));1);+(*(+(sin(x);+(-(cos(x)));+(sin(x);+(-(cos(x))));/(tg(x);+(*(tg(x);tg(x));2)))))",
                "(/(+(^(+(sin(x);+(-(cos(x)));2);1);+(^(+(sin(x);+(-(cos(x)));2);/(tg(x);+(^(tg(x);2));2)))))"
            ),
            Arguments.of(
                "(*(ctg(a);ctg(a);tan(a)))",
                "(*(^(ctg(a);2);tan(a)))"
            )
        )

        @JvmStatic
        fun expressionsForMethodVerifyAllPartsHasBeenTransformed() = listOf(
            Arguments.of("(/(1;cos(x)))", "(+(/(1;cos(x));(1)))", false),
            Arguments.of("*(tg(*(3;x));ctg(x));", "*(tg(*(3;x));/(1;sin(x));+(*(2;^(cos(/(x;2));2));-(*(tg(x);ctg(x)))))", false),
            Arguments.of("(3)", "(+(tg(x);3))", false),
            Arguments.of("(3)", "(+(tg(x);2;1))", true),
            Arguments.of("(ctg(*(3;x)))", "(*(*(1;2);1;ctg(*(3;x));+(1;2;3;^(sin(x);2);^(cos(x);2))))", false)
            )

        @JvmStatic
        fun expressionsForMethodMakeComputation() = listOf(
            Arguments.of("(+(2;1))", "3", "+"),
            Arguments.of("(*(2;1))", "2", "*")
        )

        @JvmStatic
        fun expressionsForMethodGetRidOfDuplicatedNodes() = listOf(
                Arguments.of("(+(1;-(^(sin(x);2));-(1);^(sin(x);2);-(-(sin(+(/(π;2);-(*(2;x))))))))", "(sin(+(/(π;2);-(*(2;x)))))", "+"),
                Arguments.of("(+(a;b;-(b);-(a)))", "(+(0))", "+"),
                Arguments.of("(+(*(2;a);-(*(3;a));a))", "(+(0))", "+"),
                Arguments.of("(+(*(2;a);-(*(3;a))))", "(-(a))", "+"),
                Arguments.of("(+(a;-(1);-(^(ctg(x);2));^(ctg(x);2)))",  "(+(a;-(1)))", "+"),
                Arguments.of("(+(a;a;+(-(a))))", "(a)", "+"),
                Arguments.of("(+(a;a;1;+(-(a))))", "(+(a;1))", "+"),
                Arguments.of("(+(a;a))", "(*(2;a))", "+"),
                Arguments.of("(+(a;a;a))", "(*(3;a))", "+"),
                Arguments.of("(+(a;b;a;a;+(-(b))))", "(*(3;a))", "+"),
                Arguments.of("(+(a;+(-(a))))", "(+(0))", "+"),
                Arguments.of("(+(a;1;2;3;+(-(a))))", "(+(1;2;3))", "+"),
                Arguments.of("(+(a;a;+(-(*(2;a)))))", "(+(0))", "+"),
                Arguments.of("(+(+(-(b));b;a;1;2;3;+(-(a))))", "(+(1;2;3))", "+"),
                Arguments.of("(+(+(-(b));b;a;1;2;3;b;+(-(a))))", "(+(b;1;2;3))", "+"),
                Arguments.of("(+(tg(a);-(+(ctg(a)))))", "(+(tg(a);-(+(ctg(a)))))", "+"),
                Arguments.of("(+(^(cos(x);2);cos(*(2;x));^(cos(x);2)))", "(+(*(2;^(cos(x);2));cos(*(2;x))))", "+"),
                Arguments.of("(tg(+(x;π;π;π)))", "(tg(+(x;*(3;π))))", "+"),
                Arguments.of("(+(a;a))", "(*(2;a))", "+"),
                Arguments.of("(+(a;a;a))", "(*(3;a))", "+"),
                Arguments.of("(+(a;*(4;a;b);a;*(2;a;b)))", "(+(*(2;a);*(6;*(a;b))))", "+"),
                Arguments.of("(+(a;a;a;*(2;a)))", "(*(5;a))", "+"),
                Arguments.of("(+(*(3;a);a;a;*(2;a)))", "(*(7;a))", "+"),
                Arguments.of("(+(*(3;a);a;a;*(2;a);b))", "(+(*(7;a);b))", "+"),
                Arguments.of("(*(a;a))", "(^(a;2))", "*"),
                Arguments.of("(*(a;a;a))", "(^(a;3))", "*"),
                Arguments.of("(*(a;a;a;^(a;2)))", "(^(a;5))", "*"),
                Arguments.of("(*(a;b;a;a;^(a;2)))", "(*(^(a;5);b))", "*"),
                Arguments.of("(*(2;cos(x);cos(x)))", "(*(2;^(cos(x);2)))", "*")
        )
    }


    @ParameterizedTest
    @MethodSource("expressionsToPostprocess")
    fun whenPostProcessExpression_resultIsAsExpected(startExpressionString: String, expectedResultExpressionString: String) {
        // GIVEN
        val startExpression = ExpressionUtils.stringToGeneratedExpression(startExpressionString)
        val task = ExpressionTask(startExpression.expressionNode.clone())

        // WHEN
        val postprocessSubstitutions = expressionSubstitutionsFromRulePackITR(PostprocessorRulePack.get(), mapOf(), false)
        val resultTask = postProcess(task, postprocessSubstitutions)

        // THEN
        assertEquals(expectedResultExpressionString, resultTask.currentExpression.toString())
    }

    private fun postProcess(task: ExpressionTask, postProcessSubstitutions: List<ExpressionSubstitution>) : ExpressionTask {
        return postprocessGeneratedTask(task, postProcessSubstitutions,
            CompiledConfiguration().factComparator.expressionComparator, 1)
    }

    @ParameterizedTest
    @MethodSource("expressionsForMethodGetRidOfMultilayerFractions")
    fun testGetRidOfMultilayerFractions(initialExpressionString: String, expectedResultExpressionString: String) {
        // GIVEN
        val initialNode : ExpressionNode = structureStringToExpression(initialExpressionString)
        val expectedNode : ExpressionNode = structureStringToExpression(expectedResultExpressionString)

        // WHEN
        val resultNode : ExpressionNode = getRidOfMultilayerFractions(ExpressionTask(initialNode), CompiledConfiguration()).currentExpression

        // THEN
        assertTrue(
            expressionComparator.compareWithTreeTransformationRules(
                resultNode,
                expectedNode,
                compiledConfiguration.compiledExpressionTreeTransformationRules
            )
        )
        assertEquals(expectedNode.identifier, resultNode.identifier)
    }

    @ParameterizedTest
    @MethodSource("expressionsForMethodDragTheDescendantsOfPlusUpper")
    fun testDragTheDescendantsOfPlusUpper(initialExpressionString: String, expectedResultExpressionString: String) {
        // GIVEN
        val initialNode : ExpressionNode = structureStringToExpression(initialExpressionString)
        val expectedNode : ExpressionNode = structureStringToExpression(expectedResultExpressionString)

        // WHEN
        val resultNode : ExpressionNode = dragDescendantsUpper(ExpressionTask(initialNode), "+", CompiledConfiguration()).currentExpression

        // THEN
        assertTrue(
            expressionComparator.compareWithTreeTransformationRules(
                resultNode,
                expectedNode,
                compiledConfiguration.compiledExpressionTreeTransformationRules
            )
        )
        assertEquals(expectedNode.identifier, resultNode.identifier)
    }

    @ParameterizedTest
    @MethodSource("expressionsForMethodDragTheDescendantsOfMultUpper")
    fun testDragTheDescendantsOfMultUpper(initialExpressionString: String, expectedResultExpressionString: String) {
        // GIVEN
        val initialNode : ExpressionNode = structureStringToExpression(initialExpressionString)
        val expectedNode : ExpressionNode = structureStringToExpression(expectedResultExpressionString)

        // WHEN
        val resultNode : ExpressionNode = dragDescendantsUpper(ExpressionTask(initialNode), "*", CompiledConfiguration()).currentExpression

        // THEN
        /*assertTrue(
            expressionComparator.compareWithTreeTransformationRules(
                resultNode,
                expectedNode,
                compiledConfiguration.compiledExpressionTreeTransformationRules
            )
        )*/
        assertEquals(expectedNode.identifier, resultNode.identifier)
    }

    @ParameterizedTest
    @MethodSource("expressionsForMethodDragConstantsToTheLeft")
    fun testDragConstantsToTheLeft(initialExpressionString: String, expectedResultExpressionString: String) {
        // GIVEN
        val initialNode : ExpressionNode = structureStringToExpression(initialExpressionString)
        val expectedNode : ExpressionNode = structureStringToExpression(expectedResultExpressionString)

        // WHEN
        val resultNode : ExpressionNode = dragConstantsToTheLeft(ExpressionTask(initialNode)).currentExpression

        // THEN
        assertTrue(
            expressionComparator.compareWithTreeTransformationRules(
                resultNode,
                expectedNode,
                compiledConfiguration.compiledExpressionTreeTransformationRules
            )
        )
        assertEquals(expectedNode.identifier, resultNode.identifier)
    }

    @ParameterizedTest
    @MethodSource("expressionsForMethodReduceFractions")
    fun testReduceFractions(initialExpressionString: String, expectedResultExpressionString: String) {
        // GIVEN
        val initialNode : ExpressionNode = structureStringToExpression(initialExpressionString)
        val expectedNode : ExpressionNode = structureStringToExpression(expectedResultExpressionString)

        // WHEN
        val resultNode : ExpressionNode = reduceFractions(ExpressionTask(initialNode)).currentExpression

        // THEN
        assertEquals(expectedNode.identifier, resultNode.identifier)
    }

    @ParameterizedTest
    @MethodSource("expressionsForMethodGetRidOfDuplicatedMultipliers")
    fun testGetRidOfDuplicatedMultipliers(initialExpressionString: String, expectedResultExpressionString: String) {
        // GIVEN
        val initialNode : ExpressionNode = structureStringToExpression(initialExpressionString)
        val expectedNode : ExpressionNode = structureStringToExpression(expectedResultExpressionString)

        // WHEN
        val resultNode : ExpressionNode = getRidOfDuplicatedNodes(ExpressionTask(initialNode), CompiledConfiguration()).currentExpression

        // THEN
        assertTrue(
            expressionComparator.compareWithTreeTransformationRules(
                resultNode,
                expectedNode,
                compiledConfiguration.compiledExpressionTreeTransformationRules
            )
        )
        assertEquals(expectedNode.identifier, resultNode.identifier)
    }

    @ParameterizedTest
    @MethodSource("expressionsForMethodGetRidOfDuplicatedNodes")
    fun testGetRidOfDuplicatedNodes(initialExpressionString: String, expectedResultExpressionString: String) {
        // GIVEN
        val initialNode : ExpressionNode = structureStringToExpression(initialExpressionString)
        val expectedNode : ExpressionNode = structureStringToExpression(expectedResultExpressionString)

        // WHEN
        val resultNode : ExpressionNode = getRidOfDuplicatedNodes(ExpressionTask(initialNode), CompiledConfiguration()).currentExpression

        // THEN
        assertEquals(expectedNode.identifier, resultNode.identifier)
    }

    @ParameterizedTest
    @MethodSource("expressionsForMethodVerifyAllPartsHasBeenTransformed")
    fun testVerifyAllPartsHasBeenTransformed(initialNodeString: String, resultNodeString: String, correctAnswer: Boolean) {
        // GIVEN
        val initialNode : ExpressionNode = structureStringToExpression(initialNodeString)
        val expectedNode : ExpressionNode = structureStringToExpression(resultNodeString)

        // WHEN
        val answer = verifyAllPartsHasBeenTransformed(initialNode, expectedNode)

        // THEN
        assertEquals(answer, correctAnswer)
    }

    @ParameterizedTest
    @MethodSource("expressionsForMethodMakeComputation")
    fun testMakeComputation(initialNodeString: String, resultNodeString: String, operation: String) {
        // GIVEN
        val initialNode : ExpressionNode = structureStringToExpression(initialNodeString)

        // WHEN
        val node = makeComputation(initialNode.children[0], operation)

        // THEN
        assertEquals(resultNodeString, node.identifier)
    }
}
