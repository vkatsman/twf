package mathhelper.twf.taskautogeneration.tasksets

import mathhelper.twf.api.structureStringToExpression
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.ReportType
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.RulePackProvider
import mathhelper.twf.logs.MessageType
import mathhelper.twf.taskautogeneration.utils.StartExpressionsSets
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertTrue
import mathhelper.twf.logs.log
import mathhelper.twf.platformdependent.JsonParser
import mathhelper.twf.platformdependent.JsonParser.Companion.toJson
import mathhelper.twf.taskautogeneration.*
import org.junit.Ignore
import kotlin.test.assertEquals

class TrigonometricTaskSetGeneration : GeneratorTests() {

    /**
     * How to use task autogeneration reports
     *
     * 1. Define your start expressions. For example:
     * [mathhelper.twf.taskautogeneration.utils.StartExpressionsSets.Companion.trigonometricExpressions1]
     *
     * 2. Define substitutions (List<ExpressionSubstitution>) which will be used by generator
     *
     * 3. Choose report type: [mathhelper.twf.taskautogeneration.report.ReportType]
     *
     * 4. Write test. Example: [mathhelper.twf.taskautogeneration.tasksets.TrigonometricTaskSetGeneration.trigonometricGeneration_5]
     *    It's recommended to mark test with @Disabled because test produces file
     *
     * 5. Run test
     *
     * 6. Copy generated file content and go to https://www.overleaf.com/
     *    You may use OverLeaf template: https://disk.yandex.ru/d/h515In_cWFS4yA
     *    If you use template just copy file content to report.tex
     *
     * 7. Compile and get PDF report
     */

    companion object {
        @JvmStatic
        fun startExpressionsAndTags() = listOf(
                Arguments.of(
                        "(π)", arrayOf(RuleTag.TRIGONOMETRY_INVERSE_FUNCTIONS_STANDARD_ANGLES.toString())),
                Arguments.of(
                        "(^(cos(x);2))", arrayOf(RuleTag.TRIGONOMETRY_POWER_REDUCING.toString())),
                Arguments.of(
                        "(^(cos(x);2))", arrayOf(RuleTag.TRIGONOMETRY_DOUBLE_ANGLES.toString(), RuleTag.TRIGONOMETRY_FUNCTIONS_DEFINITION.toString())),
                Arguments.of(
                        "(^(cos(x);2))", arrayOf(RuleTag.TRIGONOMETRY_DOUBLE_ANGLES.toString())),
                Arguments.of(
                        "(sin(x))", arrayOf(RuleTag.TRIGONOMETRY_PERIODIC.toString())),
                Arguments.of(
                        "(sin(x))", arrayOf(RuleTag.TRIGONOMETRY_FUNCTIONS_DEFINITION.toString())),
                Arguments.of(
                        "(1)", arrayOf(RuleTag.TRIGONOMETRY_STANDARD_ANGLES.toString())),
                Arguments.of(
                        "(1)", arrayOf(RuleTag.TRIGONOMETRY_BASIC_IDENTITY.toString())),
                Arguments.of(
                        "(sin(x))", arrayOf(RuleTag.TRIGONOMETRY_SHIFTING.toString())),
                // TODO: uncomment after parser will be fixed
                /*Arguments.of(
                        "(arcsin(x))", arrayOf(RuleTag.TRIGONOMETRY_INVERSE_FUNCTIONS.toString())),
                Arguments.of(
                        "(arcsin(x))", arrayOf(RuleTag.TRIGONOMETRY_INVERSE_FUNCTIONS_PROPERTIES.toString())),
                Arguments.of(
                        "(arcsin(x))", arrayOf(RuleTag.TRIGONOMETRY_INVERSE_FUNCTIONS_ADVANCED.toString())),
                 */
                Arguments.of(
                        "(sin(+(π;x)))", arrayOf(RuleTag.TRIGONOMETRY_SIN_COS_SUM_AND_DIFF_OF_ANGLES.toString())),
                Arguments.of(
                        "(tg(+(π;x)))", arrayOf(RuleTag.TRIGONOMETRY_TG_CTG_SUM_AND_DIFF_OF_ANGLES.toString())),
                Arguments.of(
                        "(cos(*(2;x)))", arrayOf(RuleTag.TRIGONOMETRY_DOUBLE_ANGLES.toString())),
                Arguments.of(
                        "(cos(*(3;x)))", arrayOf(RuleTag.TRIGONOMETRY_TRIPLE_ANGLES.toString())),
                Arguments.of(
                        "(cos(*(333;x)))", arrayOf(RuleTag.TRIGONOMETRY_MULTI_ANGLES.toString())),
                Arguments.of(
                        "(+(sin(x);sin(π)))", arrayOf(RuleTag.TRIGONOMETRY_SUM_AND_DIFF_OF_FUNCTIONS.toString())),
                Arguments.of(
                        "(*(sin(x);sin(π)))", arrayOf(RuleTag.TRIGONOMETRY_SUM_AND_DIFF_OF_FUNCTIONS.toString())),
                Arguments.of(
                        "(+(sin(x);cos(x)))", arrayOf(RuleTag.TRIGONOMETRY_WEIERSTRASS_SUBSTITUTION.toString()))
        )
    }

    @Test
    @Disabled
    fun trigonometricGenerationApiCall() {
        val startExpression = "(cos(+(x;/(π;4))))"
        val tags = arrayOf(
                RuleTag.TRIGONOMETRY_BASIC_IDENTITY.toString(),
                RuleTag.TRIGONOMETRY_SIN_COS_SUM_AND_DIFF_OF_ANGLES.toString(),
                RuleTag.TRIGONOMETRY_STANDARD_ANGLES.toString()
        )
        val complexity = 1.0
        val additionalParamsJsonString = toJson(mapOf(
                "complexity" to complexity.toString(),
                "tags" to tags,
                "sort" to SortType.BY_RULE_TAG_USAGE.toString(),
                "sortOrder" to SortOrder.DESC.toString()
        ))
        val tasks = mathhelper.twf.api.generateTasks(
                area = "(Trigonometry)",
                startExpression = startExpression,
                additionalParamsJsonString = additionalParamsJsonString
        )
        print("Generated tasks total: ${tasks.size}")
        //println(log.getLogInPlainText(MessageType.TECHNICAL))
    }

    @ParameterizedTest
    @MethodSource("startExpressionsAndTags")
    @Disabled
    fun checkThatGenerationResultIsNotEmpty(startExpression: String, tags: Array<String>) {
        val additionalParamsJsonString = toJson(mapOf(
                "complexity" to 0.1.toString(),
                "tags" to tags,
                "sort" to SortType.BY_RULE_TAG_USAGE.toString(),
                "sortOrder" to SortOrder.DESC.toString()
        ))
        val tasks = mathhelper.twf.api.generateTasks(
                area = "(Trigonometry)",
                startExpression = startExpression,
                additionalParamsJsonString = additionalParamsJsonString
        )
        println(log.getLogInPlainText(MessageType.TECHNICAL))
        assertTrue(tasks.isNotEmpty())
    }

    @Test
    @Disabled
    fun trigonometricGeneration_5() {
        generateReport(
            fileName = "trigonometricExpressions5.tex",
            substitutions = ExpressionUtils.toExpressionSubstitutions(
                RulePackProvider.getBasicTrigonometricSubstitutions()
            ),
            startExpressions = StartExpressionsSets.trigonometricExpressions5(),
            reportType = ReportType.RESULT_ONLY,
            taskGenerator = { startExpression, substitution ->
                generateTasks_testVersion(
                    startExpression = startExpression,
                    substitutions = substitution,
                    targetWeight = 8.0
                )
            }
        )
    }

    @Test
    fun settingsMapToJson() {
        val tags = arrayOf(
                RuleTag.TRIGONOMETRY_DOUBLE_ANGLES.toString(),
                RuleTag.TRIGONOMETRY_BASIC_IDENTITY.toString(),
                RuleTag.TRIGONOMETRY_SHIFTING.toString()
        )
        val additionalParamsJsonString = toJson(mapOf(
                "complexity" to "0.4",
                "tags" to tags,
                "sort" to SortType.BY_RULE_TAG_USAGE.toString(),
                "sortOrder" to SortOrder.DESC.toString()
        ))
        assertEquals("{\"complexity\": \"0.4\", \"tags\": [\"TRIGONOMETRY_DOUBLE_ANGLES\", \"TRIGONOMETRY_BASIC_IDENTITY\", \"TRIGONOMETRY_SHIFTING\"], \"sort\": \"BY_RULE_TAG_USAGE\", \"sortOrder\": \"DESC\"}",
                additionalParamsJsonString)

        val parsedAdditionalParams = JsonParser.parseMap(additionalParamsJsonString)
        assertEquals(tags.toList(), parsedAdditionalParams["tags"] as List<String>)
        assertEquals("0.4", parsedAdditionalParams["complexity"])
    }

    @Test
    fun checkThatGenerateTasksResultIsNotEmpty() {
        val additionalParamsJsonString = "{\"complexity\":\"0.4\",\"depth\":\"0.5\",\"tags\":[\"TRIGONOMETRY_BASIC_IDENTITY\",\"TRIGONOMETRY_SHIFTING\",\"TRIGONOMETRY_DOUBLE_ANGLES\"],\"sort\":\"BY_RULE_TAG_USAGE\",\"sortOrder\":\"DESC\"}"
        val tasks = mathhelper.twf.api.generateTasks(
                area = "(Trigonometry)",
                startExpression = "(1)",
                additionalParamsJsonString = additionalParamsJsonString
        )
        println(log.getLogInPlainText(MessageType.TECHNICAL))
        assertTrue(tasks.isNotEmpty())
    }

    @Test
    fun whenSimplifyAndNormalizeExpression_arcfunctionIsNotComputed() {
        val originalExpression = "(asin(1))"
        var expressionToTransform = structureStringToExpression(originalExpression)
        expressionToTransform = simplifyAndNormalizeExpression(expressionToTransform, CompiledConfiguration(), 5)
        assertEquals(originalExpression, expressionToTransform.computeIdentifier())
    }

    @Test
    fun whenSimplifyAndNormalizeExpression_expressionIsNotComputed() {
        val originalExpression = "(*(3;asin(/(^(3;0.5);2))))"
        var expressionToTransform = structureStringToExpression(originalExpression)
        expressionToTransform = simplifyAndNormalizeExpression(expressionToTransform, CompiledConfiguration(), 5)
        assertEquals(originalExpression, expressionToTransform.computeIdentifier())
    }
}