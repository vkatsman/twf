package mathhelper.twf.taskautogeneration.tasksets

import mathhelper.twf.TestSuiteBase
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.TaskITR
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.ReportType
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.RulePackProvider
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.TexReportUtils
import mathhelper.twf.expressiontree.ExpressionSubstitution
import mathhelper.twf.platformdependent.JsonParser.Companion.toJson
import mathhelper.twf.taskautogeneration.*
import mathhelper.twf.taskautogeneration.report.*
import mathhelper.twf.taskautogeneration.ExpressionUtils
import mathhelper.twf.taskautogeneration.utils.StartExpressionsSets
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.io.File

open class GeneratorTests : TestSuiteBase() {

    fun generateTasks_testVersion(
        startExpression: String,
        targetWeight: Double = 3.0,
        substitutions: List<ExpressionSubstitution>
    ) = generateTrigonometricTasks(
        GeneratorSettings(
            targetWeight = targetWeight,
            taskStartGenerator = { ExpressionUtils.stringToGeneratedExpression(startExpression) },
            compiledConfiguration = CompiledConfiguration(),
            expressionSubstitutions = substitutions
        )
    )

    fun generateTasks(
        startExpression: String,
        goalStepsCount: Int = 3,
        goalDifferentRulesCount: Int = 5,
        substitutions: List<ExpressionSubstitution>
    ) = generateExpressionTransformationTasks(
        ExpressionTaskGeneratorSettings(
            ExpressionGenerationDirection.ORIGINAL_TO_FINAL,
            goalStepsCount,
            goalDifferentRulesCount,
            { ExpressionUtils.stringToGeneratedExpression(startExpression) },
            CompiledConfiguration(),
            substitutions
        )
    )

    fun generateReport(
            fileName: String, startExpressions: List<String>, substitutions: List<ExpressionSubstitution>,
            reportType: ReportType = ReportType.RESULT_WITH_SUBSTITUTIONS_AND_INTERMEDIATE_EXPRESSIONS,
            aggregator: (GroupingContext.() -> List<TaskGroup>)? = null,
            taskGenerator: (startExpression: String, substitutions: List<ExpressionSubstitution>) -> List<TaskITR> = { startExpr, expressionSubstitutions ->
            generateTasks(
                startExpression = startExpr,
                substitutions = expressionSubstitutions,
                goalStepsCount = 5,
                goalDifferentRulesCount = 3
            )
        }
    ) {
        FileUtils.deleteFileIfExists(fileName)
        FileUtils.appendLegend(fileName)

        if (aggregator != null) {
            val allTasks = mutableListOf<TaskITR>()
            for (startExpression in startExpressions) {
                val tasks = taskGenerator(startExpression, substitutions)
                allTasks.addAll(tasks)
                println("generated ${tasks.size} tasks for start expression $startExpression")
            }

            val groups = allTasks.createReportGroups {
                aggregator()
            }

            for (group in groups) {
                val groupTitle = "\n\n\\section{${group.title}}\n\n"
                File(fileName).appendText(groupTitle + TexReportUtils.renderToTex(group.tasks, reportType))
            }
        } else {
            for (startExpression in startExpressions) {
                val tasks = taskGenerator(startExpression, substitutions)
                println("generated ${tasks.size} tasks for start expression $startExpression")

                FileUtils.generateReport(fileName, tasks, reportType)
            }
        }
    }

    fun generateReport(
            fileName: String,
            startExpressions: List<String>,
            complexity: Double = 0.4,
            tags: Array<String>,
            reportType: ReportType = ReportType.RESULT_WITH_SUBSTITUTIONS_AND_INTERMEDIATE_EXPRESSIONS
    ) {
        FileUtils.deleteFileIfExists(fileName)
        FileUtils.appendLegend(fileName)

        val additionalParamsJsonString = toJson(mapOf(
                "complexity" to complexity.toString(),
                "tags" to tags,
                "sort" to SortType.BY_RULE_TAG_USAGE.toString(),
                "sortOrder" to SortOrder.DESC.toString()
        ))

        for (startExpression in startExpressions) {
            val tasks = mathhelper.twf.api.generateTasks(
                    area = "(Trigonometry)",
                    startExpression = startExpression,
                    additionalParamsJsonString = additionalParamsJsonString
            )
            println("generated ${tasks.size} tasks for start expression $startExpression")

            FileUtils.generateReport(fileName, tasks.toList(), reportType)
        }
    }
}
