package mathhelper.twf.taskautogeneration

import mathhelper.twf.api.*
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks
import mathhelper.twf.expressiontree.ExpressionNode
import mathhelper.twf.expressiontree.TREE_COMPUTATION_DEFAULT
import mathhelper.twf.substitutiontests.parseStringExpression
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class ExpressionNodeTests {

    @Test
    fun findVariablesTest() {
        val expr = stringToExpression("x&x&y&y&y&z&a&b|b&c")
        val count = mapOf("x" to 0, "y" to 0, "z" to 0).toMutableMap()
        expr.countNumberOfVariablesInMap(count)
        assertEquals(2, count["x"])
        assertEquals(3, count["y"])
        assertEquals(1, count["z"])
        assertEquals(3, count.values.size)
    }

    @Test
    fun checkLastAppliedSubstitutionInGeneratedTasksForAbnormalSubstitutions() {
        val expression = parseStringExpression("((sin(x))^2+11.11+(cos(x))^2)^2")
        val selectedNodeIds = listOf(7).toTypedArray()
        val res = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds,
            createCompiledConfigurationFromExpressionSubstitutionsAndParams(
            listOf(
                expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"),
                expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT),
                expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                expressionSubstitutionFromStructureStrings(code = "PowFactorization")
            ).toTypedArray()
        ), withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        res[0].resultExpression.setStepIdForAllNodes(11)
        assertEquals(11, res[0].resultExpression.children[0].lastModifiedStepId)
    }

    private fun ExpressionNode.checkLastModifiedStepIdForAllChildren(stepId: Int) {
        assertEquals(stepId, this.lastModifiedStepId)
        for (child in this.children) {
            child.checkLastModifiedStepIdForAllChildren(stepId)
        }
    }

    @Test
    fun checkLastAppliedSubstitutionInGeneratedTasksForNormalSubstitutions() {
        val stepId = 10
        val rule = DefaultRulePacks.defaultRulePacksMap["ArithmeticMultiplication"]?.rules?.
        filter { it.leftStructureString == "(*(a;0))" && it.rightStructureString == "(0)" }?.get(0)
        assertNotNull(rule)
        val substitution = expressionSubstitutionFromRuleITR(rule)
        val root = parseStringExpression("x*0")
        substitution.setStepIdForRight(stepId)
        val resultExpression = substitution.checkAndApply(root.children[0])
        assertNotNull(resultExpression)
        assertEquals("0", resultExpression.toString())
        resultExpression.checkLastModifiedStepIdForAllChildren(stepId)
        assertEquals("(*(a;0))", substitution.left.toString())
    }
}