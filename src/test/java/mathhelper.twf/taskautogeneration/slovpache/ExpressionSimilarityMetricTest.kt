package mathhelper.twf.taskautogeneration.slovpache

import mathhelper.twf.api.stringToExpression
import mathhelper.twf.taskautogeneration.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class ExpressionSimilarityMetricTest {
    val metric = CompoundSimilarityMetric(listOf(
        disparityMatrixMetric,
        variableOnlyInLeft,
        variableOnlyInRight
    ))

    // d(x, y) = 0 <=> x = y
    @Test
    fun identityOfIndiscernibles() {
        val expr1 = stringToExpression("x*y*z")
        val expr2 = stringToExpression("a+b")

        assertEquals(.0, metric(expr1, expr1))
        assertEquals(.0, metric(expr2, expr2))
        assertNotEquals(.0, metric(expr1, expr2))
    }

    // d(x, y) = d(y, x)
    @ParameterizedTest
    @MethodSource("metricSymmetryTestData")
    fun metricSymmetry(leftExpr: String, rightExpr: String) {
        val expr1 = stringToExpression(leftExpr)
        val expr2 = stringToExpression(rightExpr)

        val leftRightComparison = metric(expr1, expr2)
        val rightLeftComparison = metric(expr2, expr1)

        assertEquals(leftRightComparison, rightLeftComparison)
    }

    @ParameterizedTest
    @MethodSource("triangleInequalityTestData")
    fun triangleInequality(xExprStr: String, yExprStr: String, zExprStr: String) {
        val xExpr = stringToExpression(xExprStr)
        val yExpr = stringToExpression(yExprStr)
        val zExpr = stringToExpression(zExprStr)

        val xyComparison = metric(xExpr, yExpr)
        val xzComparison = metric(xExpr, zExpr)
        val yzComparison = metric(yExpr, zExpr)

        assertTrue { xyComparison <= xzComparison + yzComparison }
    }

    @Test
    fun sameCommutativeExpression() {
        val expr1 = stringToExpression("x*y*z")
        val expr2 = stringToExpression("z*x*y")

        assertEquals(.0, metric(expr1, expr2))
    }

    @Test
    fun notSimilarExpressions() {
        val expr1 = stringToExpression("x+y")
        val expr2 = stringToExpression("x*y")

        assertEquals(1.0, metric(expr1, expr2))
    }

    @Test
    fun associativeExpressionSimilarity() {
        val expr1 = stringToExpression("x+y")
        val expr2 = stringToExpression("x*y")

        assertEquals(1.0, metric(expr1, expr2))
    }

    @ParameterizedTest
    @MethodSource("metricSymmetryTestData")
    fun expressionSimilarity(leftExpr: String, rightExpr: String, similarity: Double) {
        val expr1 = stringToExpression(leftExpr)
        val expr2 = stringToExpression(rightExpr)

        val comp = metric(expr1, expr2)

        assertEquals(comp, similarity)
    }

    companion object {
        @JvmStatic
        fun metricSymmetryTestData() = listOf(
            Arguments.of("a * (b + c) / pi + log(g)", "x * 0 + 8", 10.0),
            Arguments.of("4x", "12y", 3.0),
            Arguments.of("64", "0", 1.0),
            Arguments.of("7^(x+1)", "x+1", 1.0),
            Arguments.of("f(pi) + x^2+15y-7z", "g(pi) + x^2+15y-7z", 4.0),
            Arguments.of("(m+4)/(n^2 - 1)*(n^2 + 4n - 12) + (m+1+4)/((k + 1)^2 - 1)", "12", 12.5)
        )

        @JvmStatic
        fun triangleInequalityTestData() = listOf(
            Arguments.of("a * (b + c) / pi + log(g)", "x * 0 + 8", "0"),
            Arguments.of("4x", "12y", "12z"),
            Arguments.of("f(pi) + x^2+15y-7z", "g(pi) + x^2+15y-7z", "16 + 24x - sin(pi/12)")
        )
    }
}