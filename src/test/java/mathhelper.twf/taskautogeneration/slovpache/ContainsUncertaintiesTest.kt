package mathhelper.twf.taskautogeneration.slovpache

import mathhelper.twf.api.stringToExpression
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.expressiondomain.containsUncertainties
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals

class ContainsUncertaintiesTest {
    @ParameterizedTest
    @MethodSource("testData")
    fun containsUncertainties(expressionString: String, containsUncertainties: Boolean) {
        val compiledConfiguration = CompiledConfiguration()
        val comparator = compiledConfiguration.factComparator.expressionComparator

        val expr = stringToExpression(expressionString)

        assertEquals(containsUncertainties, expr.containsUncertainties(comparator))
    }

    companion object {
        @JvmStatic
        fun testData() = listOf(
            Arguments.of("log(a,a) * log(a,b)", false),
            Arguments.of("x", false),
            Arguments.of("0", false),
            Arguments.of("1 / 0", true),
            Arguments.of("1 / x", false),
            Arguments.of("0 / 0", true),
            Arguments.of("1 + 1", false),
            Arguments.of("64", false),
            Arguments.of("7^(x+1)", false),
            Arguments.of("1 ^ 0", false),
            Arguments.of("(1 - 7) ^ 0", false),
            Arguments.of("0 / 123", false),
            Arguments.of("0 * 1 / 0", true),
            Arguments.of("1 / 0 / 0", true),
            Arguments.of("0 ^ 0", true),
            Arguments.of("log(0)", true),
            Arguments.of("log(-3)", true),
            Arguments.of("log(4, 2)", false),
            Arguments.of("log(10, 10)", false),
            Arguments.of("(x^2)/(y^3)", false),
            Arguments.of("log(a, b)", false),
            Arguments.of("log(a, b) / log(c, d)", false),
            Arguments.of("1 / log(b, 1) + log(a, b)", true)
        )
    }
}