package mathhelper.twf.taskautogeneration.slovpache

// Test expressions for task auto generation
val baselineExpressions = listOf(
    "1",
    "x + y",
    "log(x+y, 10) - log(x*y, 100)",
    "log(a, b)",
    "log((1-x)(1+x), b) + a * log(1-x, b^2)",
    "log(sqrt(1-x))",
    "log(n-3,a)+log(n+2,a)",
    "log(a*(b^log(b*a, b)), b)",
    "2*log(a, b) + 1"
)