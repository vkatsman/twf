package mathhelper.twf.taskautogeneration.slovpache

import mathhelper.twf.api.expressionSubstitutionsFromRulePackITR
import mathhelper.twf.api.stringToExpression
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.RuleITR
import mathhelper.twf.expressiontree.ExpressionSubstitution
import mathhelper.twf.taskautogeneration.*
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.ReportType
import mathhelper.twf.taskautogeneration.report.bySubstitutionGroups
import mathhelper.twf.taskautogeneration.rulepack.LogarithmsRulePack
import mathhelper.twf.taskautogeneration.rulepack.LogarithmsStandardMathRulePacks
import mathhelper.twf.taskautogeneration.tasksets.GeneratorTests
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

class LogarithmTaskGenerationITest : GeneratorTests() {
    val compiledConfiguration = CompiledConfiguration()

    @Test
    @Disabled
    fun generateLogarithmTasks() {
        val stdSubs =
            LogarithmsStandardMathRulePacks.get().flatMap { expressionSubstitutionsFromRulePackITR(it, mapOf(), false) }
        val substitutions =
            expressionSubstitutionsFromRulePackITR(LogarithmsRulePack.logarithmsRulePack, mapOf(), false) +
            stdSubs

        val compoundMetric = CompoundSimilarityMetric(
            listOf(
                DisparityMatrixMetric(3),
                variableOnlyInLeft,
                variableOnlyInRight
            )
        )
        val resultMetric = LambdaExpressionSimilarityMetric { l, r ->
            simplifiedDisparityMetric(l, r) / r.badStructureFine()
        }

        val compiledConfiguration = CompiledConfiguration(
            similarityMetric = resultMetric
        )

        val ruleGroups = mapOf(
            "From single number" to LogarithmsRulePack.extendingFromSingleNumber,
            "From simple logarithm" to LogarithmsRulePack.extendingFromSimpleLogarithm,
            "From fraction" to LogarithmsRulePack.extendingFromFraction,
            "Extending in context" to LogarithmsRulePack.extendingInLogarithmContext
        )

        val substitutionGroups = matchSubstitutions(substitutions, ruleGroups)

        val chains = mapOf(
            "(log(a;b))__to__(log(/(*(a;c);c);b))" to substitutions.filter { it.left.toString() == "(a)" }
        )

        generateReport(
            fileName = "logarithmicExpressions.tex",
            reportType = ReportType.RESULT_ONLY,
            startExpressions = baselineExpressions,
            substitutions = substitutions,
            aggregator = {
                +bySubstitutionGroups(substitutionGroups)
            }
        ) { startExpr, exprSubstitutions ->
            ExpressionGenerationKtTest.randomModifyExpression(
                stringToExpression(startExpr),
                exprSubstitutions,
                difficultyCoef = 7.6,
                varianceCoef = 7.0,
                compiledConfiguration = compiledConfiguration,
                substitutionChains = chains
            )
        }
    }

    private fun matchSubstitutions(
        substitutions: List<ExpressionSubstitution>,
        ruleGroups: Map<String, List<RuleITR>>
    ): Map<String, Set<ExpressionSubstitution>> = ruleGroups.map {
        val ruleCodes = it.value.map { rule -> rule.code }.toSet()
        val filteredSubstitutions = substitutions.filter { subs -> subs.code in ruleCodes }.toSet()

        return@map Pair(it.key, filteredSubstitutions)
    }.toMap()
}