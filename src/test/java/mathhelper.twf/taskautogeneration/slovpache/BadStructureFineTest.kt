package mathhelper.twf.taskautogeneration.slovpache

import mathhelper.twf.TestSuiteBase
import mathhelper.twf.api.getLogarithmSubstitutions
import mathhelper.twf.api.stringToExpression
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.taskautogeneration.*
import org.junit.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals

/**
 * Demo to show deterministic tests with random
 */
class BadStructureFineTest: TestSuiteBase() {
    @Test
    fun logarithmTaskGenerationTest() {
        val expression = stringToExpression("x+y")
        val compiledConfiguration = CompiledConfiguration()

        val tasks = generateExpressionTransformationTasks(
            ExpressionTaskGeneratorSettings(
                ExpressionGenerationDirection.ORIGINAL_TO_FINAL,
                3,
                3,
                { GeneratedExpression(expression) },
                compiledConfiguration,
                getLogarithmSubstitutions()
            )
        )

        tasks.forEach {
            println("${it.originalExpressionStructureString} -> ${it.goalExpressionStructureString}")
            println("${it.solutionPlainText}")
            println("${it.hints}\n")
            println("${stringToExpression(it.goalExpressionPlainText!!).badStructureFine()}")
        }
    }

    companion object {
        @JvmStatic
        fun expressionsToCheck() = listOf(
            Arguments.of("1/a", 1),
            Arguments.of("a+b+c-d*4", 0),
            Arguments.of("1/(a+(b/c)) - 4/(7/(5/8))", 3), // парсер читает 4/7/5/8 как /(4;7;5;8)
            Arguments.of("(a^2+b^2)/(1/6*c^4 - sin(pi/2))", 1)
        )
    }

    @ParameterizedTest
    @MethodSource("expressionsToCheck")
    fun getMaxFractionLevelTest(rawExpression: String, expectedMaxFractionLevel: Int) {
        val expression = stringToExpression(rawExpression)
        assertEquals(expectedMaxFractionLevel, expression.getMaxFractionLevel())
    }
}