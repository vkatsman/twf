package mathhelper.twf.taskautogeneration.qualitytests

import mathhelper.twf.api.getDefaultRulePacks
import mathhelper.twf.api.mapTargetGenerationDepth
import mathhelper.twf.api.mapTargetWeight
import mathhelper.twf.config.TaskITR
import mathhelper.twf.taskautogeneration.*
import mathhelper.twf.taskautogeneration.utils.StartExpressionsSets
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals

class CriteriaTest {

    val defectCountMapsNewAlgorithm: MutableList<MutableMap<DefectCriteria, Int>> = mutableListOf()
    val defectCountMapsOldAlgorithm: MutableList<MutableMap<DefectCriteria, Int>> = mutableListOf()

    private fun getAllStartExpressions(): List<String> = StartExpressionsSets.trigonometricExpressions100().distinct()

    @Test
    @Disabled("Takes too much time")
    fun checkGenerationByCriteriaDefects() {
        val allStartExpressions = getAllStartExpressions()

        val N = 1
        var totalGenerated = 0
        for (iteration in 1..N) {
            println("ITERATION $iteration / $N")
            val tasksGeneratedByNewAlgorithm = mutableListOf<TaskITR>()
            val tasksGeneratedByOldAlgorithm = mutableListOf<TaskITR>()

            for ((i, startExpression) in allStartExpressions.withIndex()) {
                val newAlgorithmResult = generateTasksFromStartExpression(startExpression)
                val oldAlgorithmResult = generateTasksWithOldAlgorithmFromStartExpression(startExpression)
                //oldAlgorithmResult.forEach { println(it.goalExpressionPlainText) }
                println("${i+1} / ${allStartExpressions.size}; expr: $startExpression; NEW: ${newAlgorithmResult.size} tasks; OLD: ${oldAlgorithmResult.size}")
                val countToCompare = if (newAlgorithmResult.size < oldAlgorithmResult.size) newAlgorithmResult.size else oldAlgorithmResult.size
                totalGenerated += countToCompare
                tasksGeneratedByNewAlgorithm.addAll(newAlgorithmResult.subList(0, countToCompare))
                tasksGeneratedByOldAlgorithm.addAll(oldAlgorithmResult.subList(0, countToCompare))
            }
            val totalDefectsNewAlgorithm = checkTasksByAllCriteria(tasksGeneratedByNewAlgorithm)
            defectCountMapsNewAlgorithm.add(defectCountMap.toMutableMap())
            defectCountMap.entries
                    .forEach{ println("${it.key.info}: ${it.value}") }
            defectCountMap.clear()

            val totalDefectsOldAlgorithm = checkTasksByAllCriteria(tasksGeneratedByOldAlgorithm)
            defectCountMapsOldAlgorithm.add(defectCountMap.toMutableMap())
            defectCountMap.entries
                    .forEach{ println("${it.key.info}: ${it.value}") }
            defectCountMap.clear()

            val resultMapForNewAlgorithm: MutableMap<DefectCriteria, Double> = collectResultMap(defectCountMapsNewAlgorithm, iteration)
            val resultMapForOldAlgorithm: MutableMap<DefectCriteria, Double> = collectResultMap(defectCountMapsOldAlgorithm, iteration)
            println("--------------------------------------------------------------------------------------")
            println("RESULT FOR NEW ALGORITHM (AVERAGED BY $iteration ITERATIONS) \n")
            println("NEW Algorithm: AVG defect score for total ${tasksGeneratedByNewAlgorithm.size} tasks: ${totalDefectsNewAlgorithm.toDouble() / tasksGeneratedByNewAlgorithm.size} \n")
            printResult(resultMapForNewAlgorithm)
            println("--------------------------------------------------------------------------------------")
            println("RESULT FOR OLD ALGORITHM (AVERAGED BY $iteration ITERATIONS) \n")
            println("OLD Algorithm: AVG defect score for total ${tasksGeneratedByOldAlgorithm.size} tasks: ${totalDefectsOldAlgorithm.toDouble() / tasksGeneratedByOldAlgorithm.size} \n")
            printResult(resultMapForOldAlgorithm)
            println("--------------------------------------------------------------------------------------")
            println("AVERAGE OF GENERATED TASKS ($iteration ITERATIONS): ${totalGenerated.toDouble() / iteration}\n")
        }
    }

    private fun printResult(map: MutableMap<DefectCriteria, Double>) {
        for (criteria in DefectCriteria.values()) {
            if (map.containsKey(criteria)) {
                println("${criteria.info}: ${map[criteria]}")
            } else {
                println("${criteria.info}: 0")
            }
        }
    }

    fun <K> incrementByValue(map: MutableMap<K, Double>, key: K, valueToAdd: Double) {
        if (map.computeIfPresent(key, { _, v -> v + valueToAdd }) == null) {
            map[key] = valueToAdd
        }
    }

    private fun collectResultMap(defectCountMaps: MutableList<MutableMap<DefectCriteria, Int>>, N: Int): MutableMap<DefectCriteria, Double> {
        val resultMap: MutableMap<DefectCriteria, Double> = mutableMapOf()
        for (map in defectCountMaps) {
            for (e in map.entries) {
                incrementByValue(resultMap, e.key, e.value.toDouble() / N)
            }
        }
        return resultMap
    }

    @ParameterizedTest
    @MethodSource("minusesInARowCriteriaData")
    fun testComputeDefectValueForMinusesInRow(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForMinusesInRow(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }

    @ParameterizedTest
    @MethodSource("overlayeredFractionCriteriaData")
    fun testComputeDefectValueForOverlayeredFractionCriteria(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForOverlayeredFraction(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }

    @ParameterizedTest
    @MethodSource("syntaxSugarCriteriaData")
    fun testComputeDefectValueForSyntaxSugarCriteria(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForSyntaxSugar(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }

    @ParameterizedTest
    @MethodSource("uncomputedExpressionCriteriaData")
    fun testComputeDefectValueForUncomputedExpressionCriteria(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForUncomputedExpression(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }

    @ParameterizedTest
    @MethodSource("notReducedTermsCriteriaData")
    fun testComputeDefectValueForNotReducedTermsCriteria(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForNonReducedTerms(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }

    @ParameterizedTest
    @MethodSource("notReducedMultCriteriaData")
    fun testComputeDefectValueForNotReducedMultCriteria(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForNonReducedMultipliers(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }

    @ParameterizedTest
    @MethodSource("notLeftedConstantsCriteriaData")
    fun testComputeDefectValueForNotLeftedConstantsCriteria(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForNotLeftedConstants(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }

    @ParameterizedTest
    @MethodSource("notReducedFractionCriteriaData")
    fun testComputeDefectValueForNotReducedFractionCriteria(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForNotReducedFractionCriteria(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }

    @ParameterizedTest
    @MethodSource("fractionLimitRatioCriteriaData")
    fun testComputeDefectValueForFractionLimitRatioCriteriaData(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForFractionLengthRatioLimitExceeded(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }

    @ParameterizedTest
    @MethodSource("bracketsForPlusCriteriaData")
    fun testComputeDefectValueForBracketsForPlusCriteriaData(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForRedundantBracketsForTerms(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }

    @ParameterizedTest
    @MethodSource("bracketsForMultCriteriaData")
    fun testComputeDefectValueForBracketsForMultCriteriaData(stringExpression: String, expectedDefectScore: Int) {
        val actualDefectScore = computeDefectValueForRedundantBracketsForMultipliers(ExpressionUtils.stringToExpressionNode(stringExpression))
        assertEquals(expectedDefectScore, actualDefectScore)
    }


    private fun generateTasksFromStartExpression(startExpression: String): List<TaskITR> {
        val settings = GeneratorSettings(
                targetWeight = mapTargetWeight(0.5),
                maxCountSelectedOfTasksOnIteration = mapTargetGenerationDepth(0.5),
                expressionSubstitutions = ExpressionUtils.toExpressionSubstitutions(
                        getDefaultRulePacks().toList(),
                        RuleTag.values()
                ),
                taskStartGenerator = { ExpressionUtils.stringToGeneratedExpression(startExpression) }
        )
        val tasks = generateTrigonometricTasks(settings)
        return tasks
    }

    private fun generateTasksWithOldAlgorithmFromStartExpression(startExpression: String): List<TaskITR> {
        val codes = listOf(
                "ArithmeticPositiveAddition",
                "ArithmeticAddition",
                "ArithmeticMultiplication",
                "ArithmeticDivision",
                "ArithmeticExponentiation",
                "ShortMultiplication",
                "ShortMultiplication",
                "BasicTrigonometricDefinitionsIdentity",
                "TrigonometrySinCosSumReduction",
                 "Trigonometry",
                "AdvancedTrigonometry"
        )
        val settings = ExpressionTaskGeneratorSettingsOld(
                goalStepsCount = 5,
                //maxCountSelectedOfTasksOnIteration = mapTargetGenerationDepth(0.25),
                expressionSubstitutions = ExpressionUtils.toExpressionSubstitutions(
                        DefaultStandardMathRulePacksOlc.get().filter { codes.contains(it.code) },
                        RuleTag.values()
                ),
                taskStartGenerator = { ExpressionUtils.stringToGeneratedExpression(startExpression) }
        )
        val tasks = generateExpressionTransformationTasksOld(settings)
        return tasks
    }

    companion object {
        @JvmStatic
        fun minusesInARowCriteriaData() = listOf(
                Arguments.of("(ctg(π/4)-sin(π/2)^2)^cos(π/3)*tg((-(-sin(π/2)^2)-sin(π/2)^2+π)/4)^(-(-cos(π/3)))", 2),
                Arguments.of("(-(-(tg(π/4)-sin(2*x)^2)))^cos(π/3)", 1),
                Arguments.of("sin(-(-(-(2*x)^tg(π/4))))", 1),
                Arguments.of("-(a-1)", 0),
                Arguments.of("-(-(-(a)))-(-(-(-(b))))", 2),
                Arguments.of("-(-(-(a)))", 1),
                Arguments.of("-(-a)", 1),
                Arguments.of("a", 0),
                Arguments.of("-(-a)-(-(a))", 2),
                Arguments.of("a-a-a-a", 0),
                Arguments.of("a-a-a-(-a)", 1),
                Arguments.of("-(-sin(x)^2)+(-(-cos(x)^2))", 2),
                Arguments.of("-(-sin(x)^2)+(-(-cos(x)^2))-a-(-b)", 3),
                Arguments.of("a-a", 0),
                Arguments.of("-a", 0)
        )

        @JvmStatic
        fun overlayeredFractionCriteriaData() = listOf(
                Arguments.of("1/a", 0),
                Arguments.of("1/((1+1)/a)", 1),
                Arguments.of("1/((1+2)/a)+1/((2+1)/((3+1)/b))", 2),
                Arguments.of("1/((1+1)/(cos^2(x)))+1/((1+3)/((1+x)/(tg^2(x))))", 2),
                Arguments.of("1/(-a)", 0),
                Arguments.of("1/a+(3+9)/((1+1)/b)", 1)
        )

        @JvmStatic
        fun syntaxSugarCriteriaData() = listOf(
                Arguments.of("π/(4*sin(π/4)^2)-acos(sin((sin(2*x)+sin(0))/2))", 0),
                Arguments.of("2*atg(sqrt((1-cos(-tg(0)))/(1+cos(-tg(0)))))", 0),
                Arguments.of("cos(0)", 0),
                Arguments.of("1+cos(0)", 0),
                Arguments.of("a", 0),
                Arguments.of("a*1", 1),
                Arguments.of("a/1+1*a", 2),
                Arguments.of("0*a+1*a", 2),
                Arguments.of("a^1", 1),
                Arguments.of("1*a+0", 2)
        )

        @JvmStatic
        fun uncomputedExpressionCriteriaData() = listOf(
                Arguments.of("1+1", 1),
                Arguments.of("1", 0),
                Arguments.of("(1+1)+(2+2)", 2),
                Arguments.of("2^2", 1),
                Arguments.of("2*2", 1)
        )

        @JvmStatic
        fun notReducedTermsCriteriaData() = listOf(
                Arguments.of("a+a", 1),
                Arguments.of("a+2*a", 1),
                Arguments.of("a", 0),
                Arguments.of("a+2*a+(2*b+3*b)", 2)
        )

        @JvmStatic
        fun notReducedMultCriteriaData() = listOf(
                Arguments.of("a*a", 1),
                Arguments.of("a*a^2", 1),
                Arguments.of("a", 0),
                Arguments.of("a*a^2+(b^2*b^3)", 2)
        )

        @JvmStatic
        fun fractionLimitRatioCriteriaData() = listOf(
                Arguments.of("(sin(x)+cos(x)+tg(x)+ctg(x)+131+2808+a^2+b^2+c^76-(-1))/2", 1),
                Arguments.of("1/2", 0)
        )

        @JvmStatic
        fun bracketsForPlusCriteriaData() = listOf(
                Arguments.of("(a+b)+c", 1),
                Arguments.of("((a+b)+c)*((a+d)+e)", 2),
                Arguments.of("a+b+c", 0)
        )

        @JvmStatic
        fun bracketsForMultCriteriaData() = listOf(
                Arguments.of("(a*b)*c", 1),
                Arguments.of("((a*b)*c)+((a*d)*e)", 2),
                Arguments.of("a*b*c", 0)
        )

        @JvmStatic
        fun notLeftedConstantsCriteriaData() = listOf(
                Arguments.of("2*b*c", 0),
                Arguments.of("b*2*c", 1),
                Arguments.of("π*b*2", 1),
                Arguments.of("b*2*c+d*5*c", 2)
        )

        @JvmStatic
        fun notReducedFractionCriteriaData() = listOf(
                Arguments.of("(cos(x)*14)/(sin(x)*7)+(4*sin(x))/2", 2),
                Arguments.of("(cos(x)^2)/cos(x)", 1),
                Arguments.of("4/2", 1),
                Arguments.of("1/2", 0),
                Arguments.of("cos^2(x)/cos^2(x)", 1),
                Arguments.of("cos^2(x)/cos^3(x)", 1),
                Arguments.of("cos(x)/sin(x)", 0),
                Arguments.of("(cos(x)*14)/(sin(x)*7)", 1)
        )
    }
}