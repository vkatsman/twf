package mathhelper.twf.taskautogeneration.utils

class StartExpressionsSets {

    companion object {
        @JvmStatic
        fun trigonometricExpressions1() = listOf(
            "cos(x)^2",
            "sin(x)^2-ctg(x)^2",
            "(ctg(x)+cos(x))^2",
            "tg(x)^3-ctg(x)^3",
            "sin(x)^2-sin(y)^2",
            "(cos(x)-cos(y))^3",
            "sin(x)^3+cos(y)^3",
            "sin(x)^3-tg(y)^3",
            "(cos(x)-sin(x))^3"
        )

        fun trigonometricExpressions2() = listOf(
            "1",
            "1+1+1",
            "sin(x)^2-1",
            "sin(x)*cos(x)",
            "2*cos(x)-sin(x)",
            "sin(x^2)",
            "1-tg(x)",
            "2*sin(x)+3*tg(x)",
            "tg(x)",
            "1/(cos(x)^2)",
            "1/(sin(x)^2)",
            "ctg(2*x)",
            "cos(2*x)",
            "sin(x)^4"
        )

        fun trigonometricExpressions3() = listOf(
            "1",
            "0",
            "cos(x)^2",
            "tg(2*x)"
        )

        fun trigonometricExpressions4() = listOf(
            "x^1",
            "cos(x)^1",
            "cos(x)^3",
            "cos(x)^2"
        )

        fun trigonometricExpressions5() = listOf(
            "cos(2*x)",
            "sin(x)^2",
            "2",
            "3",
            "9",
            "7",
            "1/cos(x)",
            "0",
            "cos(x)",
            "1",
            "1/2",
            "1/4",
            "3/4",
            "ctg(2*x)",
            "sin(2*x)+cos(2*x)",
            "cos(x)-cos(7*x)",
            "cos(x)-cos(6*x)",
            "(sin(x)+1)*(sin(x)-3)",
            "(sin(x)-cos(x))*(sin(x)+5*cos(x))",
            "(cos(x)-sin(x))*(1+1/2*cos(x))",
            "sin(x)+cos(x)+1",
            "4*sin(3*x)",
            "1+2*cos(2*x)",
            "sin(x)+cos(x)",
            "tg(x/2)^2",
            "ctg(3*x)",
            "ctg(2*x)",
            "tg(3*x)*ctg(x)",
            "cos(x)^3-1/2",
            "tg(x+1)^2",
            "sin(2*x)+cos(x)^2-sin(x+pi/3)",
            "1+sin(x)*cos(2*x)",
            "tg(2*x)-1",
            "ctg(x)^2-2",
            "tg(x)^3+4"
        )

        fun trigonometricExpressions100() = listOf(
                "cos(2*x)", "sin(x)^2", "2", "3", "9", "7", "1/cos(x)", "0", "cos(x)", "1", "1/2", "1/4", "3/4",
                "ctg(2*x)", "sin(2*x)+cos(2*x)", "cos(x)-cos(7*x)", "cos(x)-cos(6*x)", "(sin(x)+1)*(sin(x)-3)",
                "(sin(x)-cos(x))*(sin(x)+5*cos(x))", "(cos(x)-sin(x))*(1+1/2*cos(x))", "sin(x)+cos(x)+1",
                "4*sin(3*x)", "1+2*cos(2*x)", "sin(x)+cos(x)", "tg(x/2)^2", "ctg(3*x)", "ctg(2*x)", "tg(3*x)*ctg(x)", "cos(x)^3-1/2",
                "tg(x+1)^2", "sin(2*x)+cos(x)^2-sin(x+pi/3)", "1+sin(x)*cos(2*x)", "tg(2*x)-1", "ctg(x)^2-2", "tg(x)^3+4",
                "π", "π/2", "tg(x)", "ctg(x)", "cos(π/2+x)", "sin(π/2+x)", "sin(3*x+4*x)", "sin(77*x)", "cos(π/2)", "cos(π+2*x)",
                "sqrt(2)", "sqrt(2)/2", "sqrt(3)/3", "sqrt(3)", "sin(x)*cos(x)", "9*tg(x)^2", "sin(x/2)", "2*sin(x)+tg(x)",
                "1/2+sin(x)^2", "1/2+cos(x)^2", "2/7", "ctg(3*x)", "sin(2*x)*cos(2*x)", "x^2+π^2", "2*π", "-1", "-2", "-3",
                "-sin(x)^2", "-sin(x)^3", "sin(x)^3", "sin(x)^4", "tg(x/2)+ctg(x/2)", "1/(2*sin(x))", "1-2*x", "2-tg^2(x)",
                "tg(7*x)", "ctg(8*x)", "2/3", "242", "-ctg(x+π)", "π-2", "3*π", "2-cos(2*x)", "sin(x)*cos(2*x)+1", "3-2/sin(x)",
                "sin(x)/2+1", "1-2*ctg(x)+sin(x)", "1+1+1", "sin(x)^2-1", "sin(x)*cos(x)", "2*cos(x)-sin(x)", "sin(x^2)", "1-tg(x)",
                "2*sin(x)+3*tg(x)", "tg(x)", "1/(cos(x)^2)", "1/(sin(x)^2)", "ctg(2*x)", "cos(2*x)", "sin(x)^4", "cos(x)^5",
                "sin(x)^2-ctg(x)^2", "(ctg(x)+cos(x))^2", "tg(x)^3-ctg(x)^3", "sin(x)^2-sin(y)^2", "(cos(x)-cos(y))^3", "sin(x)^3+cos(y)^3",
                "sin(x)^3-tg(y)^3", "(cos(x)-sin(x))^3", "91/x", "1*2*sin(x)"
        )
    }
}