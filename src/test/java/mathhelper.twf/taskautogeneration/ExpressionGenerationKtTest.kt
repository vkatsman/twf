package mathhelper.twf.taskautogeneration

import mathhelper.twf.api.*
import mathhelper.twf.baseoperations.ComputeExpressionVariableType
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.TaskITR
import mathhelper.twf.expressiontree.ExpressionNode
import mathhelper.twf.expressiontree.ExpressionSubstitution
import mathhelper.twf.substitutiontests.parseStringExpression
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks.Companion.defaultRulePacksMap
import mathhelper.twf.expressiontree.TREE_COMPUTATION_DEFAULT
import mathhelper.twf.taskautogeneration.report.FileUtils.Companion.generateReport
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.ReportType
import org.junit.Ignore
import org.junit.Test
import org.junit.jupiter.api.DisplayName
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class ExpressionGenerationKtTest {

    @Test
    @Ignore
    fun trigonometryShortMultiplicationStartExpressionGenerationTest() {
        val expressionsSet = mutableSetOf<ExpressionNode>()
        println(trigonometryShortMultiplicationStartExpressionGeneration(CompiledConfiguration()))
    }

    @Test
    @Ignore
    fun generateTrigonometryCompletions() {
        val tasks = randomModifyExpression(stringToExpression("tg(x)*(sin(x)-cos(pi/2-x))"),
                getArithmeticDivisionSubstitutions() +
                getBasicTrigonometricDefinitionsIdentitySubstitutions(false) +
                        getTrigonometrySinCosSumReductionSubstitutions(false).apply { forEach { it.weightInTaskAutoGeneration = 100.0 } }
                        .filter {
                    !it.left.getContainedFunctions().contains("^") && !it.right.getContainedFunctions().contains("^")
                }
                , 7.6, 7.0
//                , listOf(
//                        expressionSubstitutionFromStructureStrings("(tg(x))", "(/(sin(x);cos(x)))"),
//                        expressionSubstitutionFromStructureStrings("(ctg(x))", "(/(cos(x);sin(x)))")
//                )
                , computeExpressionVariableType = ComputeExpressionVariableType.ALL_TO_NUMBERS_OR_PI_OR_E_ADDITIVELY_GROUPED
        )

        tasks.forEach {
            println("${structureStringToString(it.goalExpressionStructureString!!)}   ->   '${it.goalExpressionStructureString}'")
            println("${it.solutionPlainText}")
        }
    }

    @Test
    @Ignore
    @DisplayName("Example of how to use taskautogeneration reports")
    fun generateTrigonometryCompletions_withReportAndWithPostProcessing() {
        val tasks = randomModifyExpression(stringToExpression("1-tg(x)^2"),
            getArithmeticDivisionSubstitutions() +
                    getBasicTrigonometricDefinitionsIdentitySubstitutions(false) +
                    getTrigonometrySinCosSumReductionSubstitutions(false).apply { forEach { it.weightInTaskAutoGeneration = 100.0 } }
                        .filter {
                            !it.left.getContainedFunctions().contains("^") && !it.right.getContainedFunctions().contains("^")
                        }
            , 7.6, 7.0
            , computeExpressionVariableType = ComputeExpressionVariableType.ALL_TO_NUMBERS_OR_PI_OR_E_ADDITIVELY_GROUPED
        )

        tasks.forEach {
            println("${structureStringToString(it.goalExpressionStructureString!!)}   ->   '${it.goalExpressionStructureString}'")
            println("${it.solutionPlainText}")
        }

        val fileName = "generateTrigonometryCompletions.tex"
        generateReport(fileName, tasks, ReportType.RESULT_WITH_SUBSTITUTIONS)
    }

    @Test
    @Ignore
    fun generateLogicCompletions() {
        val tasks =
            randomModifyExpression(stringToExpression("a&b"),
                getAllLogicSubstitutions(),
                7.6, 7.0) +
            randomModifyExpression(stringToExpression("a|b"),
                getAllLogicSubstitutions(),
                7.6, 7.0)
        tasks.forEach {
            println("${structureStringToString(it.goalExpressionStructureString!!)}  ->  '${it.goalExpressionStructureString}'")
            println("${it.solutionPlainText}")
        }
    }

    @Test
    @Ignore
    fun generateLogicCompletionsWithNewVariables() {
        val tasks =
            randomModifyExpression(stringToExpression("a&b"),
                getAllLogicSubstitutions(),
                10.6, 7.0,
                newVariablesSubstitutions = getLogicNewVariablesSubstitutions(numberOfVariables = 2),
                nodeIdsToTransformSelector = {expressionNode -> expressionNode.selectNodeIdsToTransformByLastStepId() }) +
            randomModifyExpression(stringToExpression("a|b"),
                getAllLogicSubstitutions(),
                10.6, 7.0,
                newVariablesSubstitutions = getLogicNewVariablesSubstitutions(numberOfVariables = 2),
                nodeIdsToTransformSelector = {expressionNode -> expressionNode.selectNodeIdsToTransformByLastStepId() })
        tasks.forEach {
            println("${structureStringToString(it.goalExpressionStructureString!!)}  ->  '${it.goalExpressionStructureString}'")
            println("${it.solutionPlainText}")
        }
    }

    companion object {
        fun randomModifyExpression(
            expression: ExpressionNode, substitutions: List<ExpressionSubstitution>,
            difficultyCoef: Double = 5.0, varianceCoef: Double = 5.0,
            mandatoryResultTransformations: List<ExpressionSubstitution> = listOf(),
            computeExpressionVariableType: ComputeExpressionVariableType = ComputeExpressionVariableType.ALL_TO_NUMBERS,
            newVariablesSubstitutions: Map<String, List<ExpressionSubstitution>> = mapOf(),
            compiledConfiguration: CompiledConfiguration = CompiledConfiguration(),
            nodeIdsToTransformSelector: (ExpressionNode) -> List<Int> = { expressionNode -> expressionNode.selectRandomNodeIdsToTransform() },
            extendingSubstitutionsFilter: (ExpressionSubstitution) -> Boolean = { it -> it.priority!! >= 20 },
            reducingSubstitutionsFilter: (ExpressionSubstitution) -> Boolean = { it -> it.priority!! <= 30 },
            substitutionChains: Map<String, List<ExpressionSubstitution>> = mapOf()
        ): List<TaskITR> {
            compiledConfiguration.factComparator.expressionComparator.baseOperationsDefinitions.computeExpressionVariableType =
                computeExpressionVariableType
            return generateExpressionTransformationTasks(
                ExpressionTaskGeneratorSettings(
                    ExpressionGenerationDirection.ORIGINAL_TO_FINAL,
                    difficultyCoef.toInt(),
                    (difficultyCoef * 0.75).toInt(),
                    { GeneratedExpression(expression) },
                    compiledConfiguration,
                    substitutions,
                    maxCountSelectedOfTasksOnIteration = varianceCoef.toInt() * 3,
                    widthOfRulesApplicationsOnIteration = (varianceCoef * 3).toInt(),
                    minStepsCountInAutogeneration = (difficultyCoef * 0.5).toInt(),
                    mandatoryResultTransformations = mandatoryResultTransformations,
                    newVariablesExpressionSubstitutions = newVariablesSubstitutions,
                    nodeIdsToTransformSelector = nodeIdsToTransformSelector,
                    extendingSubstitutionsFilter = extendingSubstitutionsFilter,
                    reducingSubstitutionsFilter = reducingSubstitutionsFilter,
                    substitutionChains = substitutionChains
                )
            )
        }
    }

    private fun ExpressionNode.checkLastAppliedSubstitutionForAllChildren(expectedAppliedSubstitution: ExpressionSubstitution) {
        assertEquals(expectedAppliedSubstitution, this.lastAppliedSubstitution)
        for (child in this.children) {
            child.checkLastAppliedSubstitutionForAllChildren(expectedAppliedSubstitution)
        }
    }

    @Test
    fun checkLastAppliedSubstitutionInGeneratedTasks() {
        val substitution = ExpressionSubstitution(
            parseStringExpression("S(i, a, a, f(i))", true),
            parseStringExpression("f(a)", true)
        )
        val root = parseStringExpression("S(i, b, b, i^2)")
        val resultExpression = substitution.checkAndApply(root.children[0])
        assertNotNull(resultExpression)
        assertEquals("^(b;2)", resultExpression.toString())
        resultExpression.children[0].checkLastAppliedSubstitutionForAllChildren(substitution)
    }

    @Test
    fun checkLastAppliedSubstitutionInGeneratedTasksForNormalSubstitutions() {
        val rule = defaultRulePacksMap["ArithmeticMultiplication"]?.rules?.
            filter { it.leftStructureString == "(*(a;0))" && it.rightStructureString == "(0)" }?.get(0)
        assertNotNull(rule)
        val substitution = expressionSubstitutionFromRuleITR(rule)
        val root = parseStringExpression("x*0")
        val resultExpression = substitution.checkAndApply(root.children[0])
        assertNotNull(resultExpression)
        assertEquals("0", resultExpression.toString())
        resultExpression.checkLastAppliedSubstitutionForAllChildren(substitution)
        assertEquals("(*(a;0))", substitution.left.toString())
    }

    @Test
    fun checkLastAppliedSubstitutionInGeneratedTasksForAbnormalSubstitutions() {
        val expression = parseStringExpression("((sin(x))^2+11.11+(cos(x))^2)^2")
        val selectedNodeIds = listOf(7).toTypedArray()
        val res = findApplicableSubstitutionsInSelectedPlace(expression,selectedNodeIds,createCompiledConfigurationFromExpressionSubstitutionsAndParams(
            listOf(
                expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"),
                expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT),
                expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                expressionSubstitutionFromStructureStrings(code = "PowFactorization")
            ).toTypedArray()
        ), withReadyApplicationResult = true, withFullExpressionChangingPart = false)
        res[0].resultExpression.setLastAppliedSubstitutionForAllNodes(res[0].expressionSubstitution)
        assertEquals(res[0].expressionSubstitution, res[0].resultExpression.children[0].lastAppliedSubstitution)
    }
}