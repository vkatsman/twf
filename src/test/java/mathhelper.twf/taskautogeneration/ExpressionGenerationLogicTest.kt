package mathhelper.twf.taskautogeneration

// TODO: dkorotchenko move all tests about logic task autogeneration here. Make always runnable testset.

import mathhelper.twf.api.*
import mathhelper.twf.taskautogeneration.ExpressionGenerationKtTest.Companion.randomModifyExpression
import mathhelper.twf.defaultcontent.defaultrulepacks.logic.TaskSpecificLogicRulePacks
import mathhelper.twf.defaultcontent.defaultrulepacks.logic.NewWeightsLogicRulePack
import org.junit.Ignore
import org.junit.Test

class ExpressionGenerationLogicTest {

    @Ignore
    @Test
    fun deMorganLawInLogicCompletionsFromNotAB() {
        val tasksSimple = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).map {
            randomModifyExpression(
                stringToExpression("\\neg(a&b)"),
                getAllLogicSubstitutions(),
                it.toDouble(), 7.0)
        }.reduce { result, cur -> result + cur }
        //print(tasksSimple.size)
        tasksSimple.forEach {
            println(it.goalExpressionStructureString)
        }
        assert(tasksSimple.filter { it.goalExpressionStructureString!! == "(or(not(a);not(b)))" }.isNotEmpty())
        { "В генерации выражений для !(a&b) не найдено результата (!a)|(!b)" }
    }

    @Ignore
    @Test
    fun deMorganLawInLogicCompletionsFromAB() {
        val tasksSimple = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).map {
            randomModifyExpression(
                stringToExpression("a&b"),
                getAllLogicSubstitutions(),
                it.toDouble(), 7.0)
        }.reduce { result, cur -> result + cur }
        print(tasksSimple.size)
        tasksSimple.forEach {
            println(it.goalExpressionStructureString)
        }
        assert(tasksSimple.filter { it.goalExpressionStructureString!! == "(not(or(not(a);not(b))))" }.isNotEmpty())
        { "В генерации выражений для a&b не найдено результата !((!a)|(!b))" }
    }

    @Ignore
    @Test
    fun equalsTrueSimple() {
        val tasksSimple = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).map {
            randomModifyExpression(
                stringToExpression("(a|(\\neg(a)))"),
                getAllLogicSubstitutions(),
                it.toDouble(), 7.0)
        }.reduce { result, cur -> result + cur }
        println(tasksSimple.size)
        tasksSimple.forEach {
            println(it.goalExpressionStructureString)
        }
        assert(tasksSimple.filter { it.goalExpressionStructureString!! == "(1)" }.isNotEmpty())
        { "В генерации выражений для (a|!a) не найдено результата True" }
    }

    @Ignore
    @Test
    fun equalsTrue() {
        val tasksSimple = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).map {
            randomModifyExpression(
                stringToExpression("(a&b)|((\\neg(b))|(\\neg(a)))"),
                getAllLogicSubstitutions(),
                it.toDouble(), 7.0)
        }.reduce { result, cur -> result + cur }
        print(tasksSimple.size)
        tasksSimple.forEach {
            println(it.goalExpressionStructureString)
        }
        assert(tasksSimple.filter { it.goalExpressionStructureString!! == "(1)" }.isNotEmpty())
        { "В генерации выражений для (a&b)|(!b|!a) не найдено результата True" }
    }

    @Ignore
    @Test
    fun equalsFalseSimple() {
        val tasksSimple = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).map {
            randomModifyExpression(
                stringToExpression("(a&(\\neg(a)))"),
                getAllLogicSubstitutions(),
                it.toDouble(), 7.0)
        }.reduce { result, cur -> result + cur }
        print(tasksSimple.size)
        tasksSimple.forEach {
            println(it.goalExpressionStructureString)
        }
        assert(tasksSimple.filter { it.goalExpressionStructureString!! == "(0)" }.isNotEmpty())
        { "В генерации выражений для (a&!a) не найдено результата False" }
    }

    @Ignore
    @Test
    fun equalsFalse() {
        print(stringToExpression("(a&b)&((\\neg(a))&(\\neg(b)))"))
        val tasksSimple = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).map {
            randomModifyExpression(
                stringToExpression("(a&b)&((\\neg(a))&(\\neg(b)))"),
                getAllLogicSubstitutions(),
                it.toDouble(), 7.0)
        }.reduce { result, cur -> result + cur }
        print(tasksSimple.size)
        tasksSimple.forEach {
            println(it.goalExpressionStructureString)
        }
        assert(tasksSimple.filter { it.goalExpressionStructureString!! == "(0)" }.isNotEmpty())
        { "В генерации выражений для (a&b)&(!a&!b) не найдено результата False" }
    }

    @Ignore
    @Test
    fun logicSimpleStartGenerationFindConstants() {
        val tasks =
            randomModifyExpression(
                stringToExpression("a&b"),
                getAllLogicSubstitutions(),
                7.6, 7.0
            ) + randomModifyExpression(
                stringToExpression("a|b"),
                getAllLogicSubstitutions(),
                7.6, 7.0
            )
        tasks.forEach {
            println(it.goalExpressionStructureString)
        }
        val haveConstants = tasks.count {
            it.goalExpressionStructureString?.contains(Regex("[01]")) ?: false
        }

        println("$haveConstants выражений из ${tasks.size} содержат константы (true/false)")
    }

    @Ignore
    @Test
    fun logicSimpleStartGenerationFilterConstants() {
        print(stringToExpression("(a|b)&((\\neg(b))|(\\neg(a)))"))
        val tasks =
            randomModifyExpression(
                stringToExpression("(a|b)&((\\neg(b))|(\\neg(a)))"),
                getAllLogicSubstitutions(),
                7.6, 7.0
            )

        tasks.filter {
            true
        }.forEach {
            println("${structureStringToString(it.goalExpressionStructureString!!)}  ->  '${it.goalExpressionStructureString}'")
            println("${it.solutionPlainText}")
        }
    }


    private val startExpressionsStrings = listOf<String>(
        "a&b",
        "a|b",
        "(b\\implies a)&(\\neg (a|b))&(\\neg a|c)", // !b & !a,
        "(a&b)|(a&\\neg b)", // a
        "((a\\implies b)&(b\\implies a))&(a|b)", //a & b
        "\\neg(\\neg a&\\neg b)|(a\\implies b)&a", //a|b
        "a&b|a&\\neg b&c|\\neg b&a&\\neg c|a&\\neg c",//a
        "1",
        "a&\\neg a",
        "(a\\implies b)\\implies ((a \\implies (\\neg b))\\implies (\\neg a))" // 1
    )
    private val startExpressions = startExpressionsStrings.map { stringToExpression(it) }


    @Test
    @Ignore
    fun generateLogicCompletionsWithNewWeightsInAutoGeneration() {
        val usingExpressionsIndexes = listOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
        for (expressionIndex in usingExpressionsIndexes) {
            if (expressionIndex >= startExpressions.size || expressionIndex < 0)
                continue
            println("Start expression: ${expressionToString(startExpressions[expressionIndex])}")
            val tasks =
                randomModifyExpression(startExpressions[expressionIndex],
                    getLogicBaseOrAndNot(NewWeightsLogicRulePack.map())
                            + getLogicBaseImplicXorAlleq(NewWeightsLogicRulePack.map())
                            + getLogicBaseSubstitutions(NewWeightsLogicRulePack.map(), false)
                            + getLogicAbsorptionLawSubstitutions(NewWeightsLogicRulePack.map(), false)
                            + getLogicResolutionSubstitutions(NewWeightsLogicRulePack.map(), false),
                    4.6, 1.0,
                    newVariablesSubstitutions = getLogicNewVariablesSubstitutions(NewWeightsLogicRulePack.map(), numberOfVariables = 2),
                    nodeIdsToTransformSelector = { expressionNode -> expressionNode.selectNodeIdsToTransformByLastStepId() },
                    extendingSubstitutionsFilter = { it.isExtending },
                    reducingSubstitutionsFilter = { !it.isExtending })

            tasks.forEach {
                println("Goal expression: ${structureStringToString(it.goalExpressionStructureString!!)}")//. Path: ${it.solutionPlainText}")
            }
            println()
        }


        //val fileName = "generatedLogicCompletions.tex"
        //TexReportUtils.generateReport(fileName, tasks, ReportType.RESULT_WITH_SUBSTITUTIONS)
    }

    @Ignore
    @Test
    fun generateLogicCompletionsDeMorgan() {
        val usingExpressionsIndexes = listOf(0, 1, 3, 6, 7, 8)
        for (expressionIndex in usingExpressionsIndexes) {
            if (expressionIndex >= startExpressions.size || expressionIndex < 0)
                continue
            println("Start expression: ${expressionToString(startExpressions[expressionIndex])}")
            val tasks =
                randomModifyExpression(startExpressions[expressionIndex],
                    getLogicBaseOrAndNot(TaskSpecificLogicRulePacks.map(), false),
                    6.6, 3.0,
                    nodeIdsToTransformSelector = { expressionNode -> expressionNode.selectNodeIdsToTransformByLastStepId() },
                    extendingSubstitutionsFilter = { it.isExtending},
                    reducingSubstitutionsFilter = { !it.isExtending })

            tasks.forEach {
                println("Goal expression: ${structureStringToString(it.goalExpressionStructureString!!)}. Path: ${it.solutionPlainText}")
            }
            println()
        }
    }
}