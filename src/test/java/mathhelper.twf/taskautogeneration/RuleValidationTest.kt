package mathhelper.twf.taskautogeneration

import mathhelper.twf.api.structureStringToExpression
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.RuleITR
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.TrigonometricRulePacks
import mathhelper.twf.expressiontree.ExpressionNode
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertTrue

class RuleValidationTest {

    val compiledConfiguration = CompiledConfiguration()
    val expressionComparator = compiledConfiguration.factComparator.expressionComparator

    companion object {
        @JvmStatic
        fun rulesStream() = TrigonometricRulePacks.get()
                .flatMap { it.rules ?: emptyList() }
                .map { Arguments.of(it) }
    }

    @Disabled("too much false negative results")
    @ParameterizedTest
    @MethodSource("rulesStream")
    fun validateTrigonometricRules(rule: RuleITR) {
        val leftNode : ExpressionNode = structureStringToExpression(rule.leftStructureString ?: "")
        val rightNode : ExpressionNode = structureStringToExpression(rule.rightStructureString ?: "")
        assertTrue { expressionComparator.fastProbabilityCheckOnIncorrectTransformation(leftNode, rightNode) }
    }
}