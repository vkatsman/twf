package mathhelper.twf.taskautogeneration

import mathhelper.twf.config.RuleITR
import mathhelper.twf.defaultcontent.defaultrulepacks.StandardMathRulePacks
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.TrigonometricRulePacks
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.collections.LinkedHashSet

class RuleTagTest {

    @Test
    fun printRulesByRuleTags() {
        val standardMathRules = StandardMathRulePacks.get()
                .flatMap { it.rules ?: emptyList() }
                .filter { it.tagsForTaskGenerator.contains(RuleTag.BASIC_MATH) }
        val trigonometricRules = TrigonometricRulePacks.get()
                .flatMap { it.rules ?: emptyList() }
        val allRules = standardMathRules + trigonometricRules

        val ruleTagToRuleMap: MutableMap<RuleTag, LinkedHashSet<RuleITR>> = EnumMap(RuleTag::class.java)
        for (rule : RuleITR in allRules) {
            val ruleTags = rule.tagsForTaskGenerator
            for (tag: RuleTag in ruleTags) {
                var rulesForThisTag = ruleTagToRuleMap[tag]
                if (rulesForThisTag == null) {
                    rulesForThisTag = LinkedHashSet()
                }
                rulesForThisTag.add(rule)
                ruleTagToRuleMap[tag] = rulesForThisTag
            }
        }

        ruleTagToRuleMap.forEach { (tag, rules) ->
            run {
                println(tag.name)
                println()
                rules.forEach { println("${it.code}; weightInTaskAutoGeneration = ${it.weightInTaskAutoGeneration}; weight = ${it.difficultyInTaskAutoGeneration}") }
                println("------------------------------------------------------------------------------")
            }
        }

    }
}