package mathhelper.twf.taskautogeneration.report

import mathhelper.twf.api.getLogarithmSubstitutions
import mathhelper.twf.api.stringToExpression
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.ReportType
import mathhelper.twf.taskautogeneration.*
import mathhelper.twf.taskautogeneration.tasksets.GeneratorTests
import org.junit.Ignore
import org.junit.Test

class ReportGroupsTest: GeneratorTests() {
    @Ignore
    @Test
    fun demo() {
        val substitutions = getLogarithmSubstitutions()

        val compiledConfiguration = CompiledConfiguration(
            similarityMetric = CompoundSimilarityMetric(listOf(
                disparityMatrixMetric,
                variableOnlyInLeft,
                variableOnlyInRight
            ))
        )

        generateReport(
            fileName = "reportGroupsDemo.tex",
            reportType = ReportType.RESULT_WITH_SUBSTITUTIONS_AND_INTERMEDIATE_EXPRESSIONS,
            startExpressions = listOf("1 + log(a, b)"),
            substitutions = substitutions,
            aggregator =  {
                +byStepsNumber { it in 0..12 }
                +bySubstitution(substitutions, includeEmpty=true)
            }
        ) { startExpr, exprSubstitutions ->
            ExpressionGenerationKtTest.randomModifyExpression(
                stringToExpression(startExpr),
                exprSubstitutions,
                difficultyCoef = 7.5,
                varianceCoef = 10.0,
                compiledConfiguration = compiledConfiguration
            )
        }
    }
}
