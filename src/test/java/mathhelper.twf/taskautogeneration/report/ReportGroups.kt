package mathhelper.twf.taskautogeneration.report

import mathhelper.twf.config.SolutionsStepITR
import mathhelper.twf.config.TaskITR
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.TexReportUtils
import mathhelper.twf.expressiontree.ExpressionSubstitution

// Base class for task group
open class TaskGroup(val title: String, val tasks: List<TaskITR>)

// Helper class for grouping DSL
class GroupingContext(val tasks: List<TaskITR>, val groups: MutableList<TaskGroup>) {
    operator fun List<TaskGroup>.unaryPlus(): MutableList<TaskGroup> {
        groups.addAll(this)
        return groups
    }
}

fun List<TaskITR>.createReportGroups(aggregator: GroupingContext.() -> List<TaskGroup>): List<TaskGroup> {
    val groups = mutableListOf<TaskGroup>()
    aggregator(GroupingContext(this, groups))

    return groups
}

// Groups expressions by used substitutions
fun GroupingContext.bySubstitution(
    substitutions: List<ExpressionSubstitution>,
    includeEmpty: Boolean = false
): List<TaskGroup> {
    val uniqueSubs = substitutions.toSet()

    val groupedTasks = uniqueSubs.map { subs ->
        TaskGroup(TexReportUtils.ruleToTex(subs.code), tasks.filter { task ->
            val solutionSteps = task.solutionsStepsTree!!["data"] as List<SolutionsStepITR>
            return@filter solutionSteps.map { it.substitution }.contains(subs)
        })
    }.toMutableList()

    // Категория, куда никто не попал
    groupedTasks.add(TaskGroup("Другие", tasks.filter { task ->
        val solutionSteps = task.solutionsStepsTree!!["data"] as List<SolutionsStepITR>
        return@filter solutionSteps.toSet().intersect(uniqueSubs).isEmpty()
    }))

    return if (includeEmpty)
        groupedTasks
    else
        groupedTasks.filter { it.tasks.isNotEmpty() }
}

// Groups expressions by number of steps in auto generation
fun GroupingContext.byStepsNumber(lengthPredicate: (Int) -> Boolean): List<TaskGroup> = tasks.filter {
    lengthPredicate(it.stepsNumber ?: 0)
}.groupBy { it.stepsNumber }.map { (stepsNumber, groupedTasks) ->
    TaskGroup("Steps num: $stepsNumber", groupedTasks)
}

class TaskGroupBySubstitutions(title: String, tasks: MutableList<TaskITR>, val substitutions: Set<ExpressionSubstitution>) :
    TaskGroup(title, tasks)

// Groups expressions by specified named groups of substitution
fun GroupingContext.bySubstitutionGroups(
    substitutionGroups: Map<String, Set<ExpressionSubstitution>>,
    matchAny: Boolean = true
): List<TaskGroup> {
    val resultGroups = substitutionGroups.map {
        Pair(it, mutableListOf<TaskITR>())
    }.toList()

    tasks.forEach { task ->
        val solutionSteps = task.solutionsStepsTree!!["data"] as List<SolutionsStepITR>
        val taskSubstitutions = solutionSteps.map { it.substitution }.toSet()

        resultGroups.forEach {
            val groupSubstitutions = it.first.value
            val intersectionSize = (taskSubstitutions intersect groupSubstitutions).size

            val matches = if (matchAny) intersectionSize > 0 else intersectionSize == groupSubstitutions.size

            if (matches) {
                it.second.add(task)
            }
        }
    }

    return resultGroups.map { TaskGroupBySubstitutions(it.first.key, it.second, it.first.value) }
}