package mathhelper.twf.taskautogeneration.report

import mathhelper.twf.config.TaskITR
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.ReportType
import mathhelper.twf.defaultcontent.defaultrulepacks.autogeneration.TexReportUtils
import java.io.File

class FileUtils {

    companion object {

        fun generateReport(fileName: String, tasks: List<TaskITR>, reportType: ReportType) {
            File(fileName).appendText(TexReportUtils.renderToTex(tasks, reportType))
        }

        fun appendLegend(fileName: String) {
            val reportBuilder = StringBuilder()
            reportBuilder.append("Legend: \n\n")
            reportBuilder.append("\\textcolor{blue}{Blue} - tasks\n\n")
            reportBuilder.append("\\textcolor{red}{Red} - extending substitutions\n\n")
            reportBuilder.append("\\textcolor{orange}{Orange} - reducing substitutions\n\n")
            reportBuilder.append("\\textcolor{green}{Green} - substitutions used by postprocessor \\newline")
            File(fileName).appendText(reportBuilder.toString())
        }

        fun deleteFileIfExists(fileName: String) {
            val myFile = File(fileName)
            if (myFile.exists()) {
                myFile.delete()
            }
        }
    }

}