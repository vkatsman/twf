package mathhelper.twf.trivialoffcheck

import mathhelper.twf.api.checkChainCorrectnessInTex
import mathhelper.twf.logs.log
import kotlin.test.assertEquals
import org.junit.Test

class Equality {
    @Test
    fun logArgumentsPlaces() {
        val result = checkChainCorrectnessInTex("\\textcolor{purple}{\\log _{16}4\\ \\textcolor{red}{=}\\ 0.5}")

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\log _{16}4\\ \\textcolor{green}{=}\\ 0.5}",
                result.validatedSolution)
    }

    @Test
    fun equalitySumProdN() {
        val result = checkChainCorrectnessInTex("\\textcolor{purple}{a+5\\ +3!\\textcolor{green}{=}\\ a+5+\\sum _{i=1}^3i}")

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{a+5\\ +3!\\textcolor{green}{=}\\ a+5+\\sum _{i=1}^3i}",
                result.validatedSolution)
    }

    @Test
    fun equalitySumN6() {
        val result = checkChainCorrectnessInTex("\\textcolor{purple}{a+5\\ +6\\textcolor{green}{=}\\ a+5+\\sum _{i=1}^3i}")

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{a+5\\ +6\\textcolor{green}{=}\\ a+5+\\sum _{i=1}^3i}",
                result.validatedSolution)
    }
}