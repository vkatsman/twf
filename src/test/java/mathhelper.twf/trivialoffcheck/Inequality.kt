package mathhelper.twf.trivialoffcheck

import mathhelper.twf.api.checkChainCorrectnessInTex
import mathhelper.twf.api.checkSolutionInTex
import mathhelper.twf.logs.log
import kotlin.test.assertEquals
import org.junit.Ignore
import org.junit.Test

class Inequality {
    @Test
    fun simpleInequality() {
        val result = checkChainCorrectnessInTex(
                originalTexSolution = "\\textcolor{purple}{a\\textcolor{green}<a+1}"
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{a\\textcolor{green}{<}a+1}",
                result.validatedSolution)
    }

    @Test
    fun sameFactorialInequality() {
        val result = checkChainCorrectnessInTex("n!\\le n!+1")

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{n!\\textcolor{green}{\\le} n!+1}",
                result.validatedSolution)
    }

    @Test
    fun oneStepFactorialInequality() {
        val result = checkChainCorrectnessInTex("(n+1)!\\le n!*(n+1)+1")

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{(n+1)!\\textcolor{green}{\\le} n!*(n+1)+1}",
                result.validatedSolution)
    }

    @Test
    fun twoStepFactorialInequalityInOneStep() { //now is forbidden, TODO: automaticaly increase weight of comparison
        val result = checkChainCorrectnessInTex("(n+2)!\\le n!*(n+1)*(n+2)+1")

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation '(factorial(+(n;2)))' <= '(+(*(factorial(n);+(n;1);+(n;2));1))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{(n+2)!\\textcolor{red}{\\le} n!*(n+1)*(n+2)+1}",
                result.validatedSolution)
    }

    @Test
    fun twoStepFactorialInequalityInTwoSteps() {
        val result = checkChainCorrectnessInTex("n!*(n+1)*(n+2)+1 \\ge (n+1)!*(n+2)+1 \\ge (n+2)!")

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{n!*(n+1)*(n+2)+1 \\textcolor{green}{\\ge} (n+1)!*(n+2)+1 \\textcolor{green}{\\ge} (n+2)!}",
                result.validatedSolution)
    }

    @Test
    fun logInequality() {
        val result = checkChainCorrectnessInTex("\\log _{x^2+1} \\left(3+x+5\\right) < \\log _{x^2+1} \\left(3+x+6\\right)")

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\log _{x^2+1} \\left(3+x+5\\right) \\textcolor{green}{<} \\log _{x^2+1} \\left(3+x+6\\right)}",
                result.validatedSolution)
    }

    @Test
    fun logInequalityWrong() {
        val result = checkChainCorrectnessInTex("\\log _{x^2+1} \\left(3+x+5\\right)+0.0001 < \\log _{x^2+1} \\left(3+x+6\\right)")

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation '(+(log(+(3;x;5);+(^(x;2);1));0.0001))' < '(log(+(3;x;6);+(^(x;2);1)))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\log _{x^2+1} \\left(3+x+5\\right)+0.0001 \\textcolor{red}{<} \\log _{x^2+1} \\left(3+x+6\\right)}",
                result.validatedSolution)
    }

    @Test
    @Ignore
    fun logInequalityWrongHard() {
        val result = checkChainCorrectnessInTex("\\log _{x^2+1} \\left(3+x+5\\right)+0.00001 < \\log _{x^2+1} \\left(3+x+6\\right)")

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\log _{x^2+1} \\left(3+x+5\\right) \\textcolor{green}{<} \\log _{x^2+1} \\left(3+x+6\\right)}",
                result.validatedSolution)
    }
}