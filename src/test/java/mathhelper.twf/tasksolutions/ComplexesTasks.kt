package mathhelper.twf.tasksolutions

import mathhelper.twf.api.checkSolutionInTex
import mathhelper.twf.api.checkSolutionInTexWithCompiledConfiguration
import mathhelper.twf.api.createConfigurationFromRulePacksAndDetailSolutionCheckingParams
import mathhelper.twf.api.expressionSubstitutionFromStrings
import mathhelper.twf.config.FunctionIdentifier
import mathhelper.twf.expressiontree.ExpressionSubstitution
import mathhelper.twf.factstransformations.FactConstructorViewer
import mathhelper.twf.factstransformations.parseFromFactIdentifier
import mathhelper.twf.logs.log
import mathhelper.twf.mainpoints.checkFactsInMathML
import mathhelper.twf.mainpoints.compiledConfigurationBySettings
import mathhelper.twf.mainpoints.configSeparator
import kotlin.test.assertEquals
import org.junit.Ignore
import org.junit.Test
import kotlin.math.pow

class ComplexesTasks {
    @Test
    fun proveComplexOne() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Complexes").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "i\\cdot i+10=-1+10=9",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(+(*(sys_def_i_complex;sys_def_i_complex);10))",
                endExpressionIdentifier = "(9)"
        )

        val logRef = log.getLogInPlainText()

        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{i\\cdot i+10\\textcolor{green}{=}-1+10\\textcolor{green}{=}9}",
            result.validatedSolution)
    }

    @Test
    fun proveComplexOneIncorrect() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Complexes").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "i\\cdot i+10=-2+10=8",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(+(*(sys_def_i_complex;sys_def_i_complex);10))",
                endExpressionIdentifier = "(8)"
        )

        val logRef = log.getLogInPlainText()

        assertEquals("Error: Unclear transformation between '(+(*(sys_def_i_complex;sys_def_i_complex);10))' and '(+(-(2);10))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{i\\cdot i+10\\textcolor{red}{=}-2+10=8}",
            result.validatedSolution)
    }

    @Test
    fun proveMulWithComplexOne() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Complexes").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "10+2i\\cdot i=-2+10=8",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(+(10;*(2;sys_def_i_complex;sys_def_i_complex)))",
                endExpressionIdentifier = "(8)"
        )

        val logRef = log.getLogInPlainText()

        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{10+2i\\cdot i\\textcolor{green}{=}-2+10\\textcolor{green}{=}8}",
                result.validatedSolution)
    }

    @Test
    fun findDivRes() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Complexes").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "\\frac{1-i}{1+2i}=\\frac{\\left(1-i\\right)\\left(1-2i\\right)}{\\left(1+2i\\right)\\left(1-2i\\right)}=-\\frac{1}{5}-\\frac{3i}{5}",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(/(+(1;-(sys_def_i_complex));+(1;*(2;sys_def_i_complex))))",
                endExpressionIdentifier = "(+(-(/(1;5));-(/(*(3;sys_def_i_complex);5))))"
        )

        val logRef = log.getLogInPlainText()

        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{1-i}{1+2i}\\textcolor{green}{=}\\frac{\\left(1-i\\right)\\left(1-2i\\right)}{\\left(1+2i\\right)\\left(1-2i\\right)}\\textcolor{green}{=}-\\frac{1}{5}-\\frac{3i}{5}}",
                result.validatedSolution)
    }

    @Test
    fun findModRes1() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Complexes").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "abs\\left(3+4i\\right)=\\sqrt{3^2+4^2}=\\ 5",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(abs(+(3;*(4;sys_def_i_complex))))",
                endExpressionIdentifier = "(5)"
        )

        val logRef = log.getLogInPlainText()

        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{abs\\left(3+4i\\right)\\textcolor{green}{=}\\sqrt{3^2+4^2}\\textcolor{green}{=}\\ 5}",
                result.validatedSolution)
    }

    @Test
    fun findModRes2() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Complexes").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "abs\\left(3+i4\\right)=\\sqrt{3^2+4^2}=\\ 5",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(abs(+(3;*(sys_def_i_complex;4))))",
                endExpressionIdentifier = "(5)"
        )

        val logRef = log.getLogInPlainText()

        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{abs\\left(3+i4\\right)\\textcolor{green}{=}\\sqrt{3^2+4^2}\\textcolor{green}{=}\\ 5}",
                result.validatedSolution)
    }

    @Test
    fun findModRes3() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Complexes").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "abs\\left(4i+3\\right)=\\sqrt{3^2+4^2}=\\ 5",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(abs(+(*(4;sys_def_i_complex);3)))",
                endExpressionIdentifier = "(5)"
        )

        val logRef = log.getLogInPlainText()

        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{abs\\left(4i+3\\right)\\textcolor{green}{=}\\sqrt{3^2+4^2}\\textcolor{green}{=}\\ 5}",
                result.validatedSolution)
    }

    @Test
    fun findModRes4() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Complexes").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "abs\\left(i4+3\\right)=\\sqrt{3^2+4^2}=\\ 5",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(abs(+(*(sys_def_i_complex;4);3)))",
                endExpressionIdentifier = "(5)"
        )

        val logRef = log.getLogInPlainText()

        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{abs\\left(i4+3\\right)\\textcolor{green}{=}\\sqrt{3^2+4^2}\\textcolor{green}{=}\\ 5}",
                result.validatedSolution)
    }

    @Test
    fun findExpVal() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Complexes").toTypedArray(), maxExpressionTransformationWeight = "3.0")
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "\\exp \\left(\\ln \\left(2\\right)+\\frac{\\pi }{2}i\\right)=\\exp \\left(\\ln \\left(2\\right)\\right)\\left(\\cos \\left(\\frac{\\pi }{2}\\right)+\\sin \\left(\\frac{\\pi }{2}\\right)i\\right)=2\\cdot \\left(0+i\\right)=2i",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(exp(+(ln(2);*(/(π;2);sys_def_i_complex))))",
                endExpressionIdentifier = "(*(2;sys_def_i_complex))"
        )

        val logRef = log.getLogInPlainText()
        assertEquals("", result.errorMessage)//TODO: Error: Unclear transformation between '(*(^(2.7182818284590452353602874713526;ln(2));+(cos(/(π;2));*(sin(/(π;2));sys_def_i_complex))))' and '(*(2;+(0;sys_def_i_complex)))'
        assertEquals("\\textcolor{purple}{\\exp \\left(\\ln \\left(2\\right)+\\frac{\\pi }{2}i\\right)\\textcolor{green}{=}\\exp \\left(\\ln \\left(2\\right)\\right)\\left(\\cos \\left(\\frac{\\pi }{2}\\right)+\\sin \\left(\\frac{\\pi }{2}\\right)i\\right)\\textcolor{green}{=}2\\cdot \\left(0+i\\right)\\textcolor{green}{=}2i}",
                result.validatedSolution)
    }

    @Test
    fun findExpValWithoutNormalMultForm() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Complexes").toTypedArray(), maxExpressionTransformationWeight = "3.0")
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "\\exp \\left(\\ln \\left(2\\right)+\\frac{\\pi i}{2}\\right)=2\\left(\\cos \\left(\\frac{\\pi }{2}\\right)+\\sin \\left(\\frac{\\pi }{2}\\right)i\\right)=2\\left(0+i\\right)=2i",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(exp(+(ln(2);*(/(π;2);sys_def_i_complex))))",
                endExpressionIdentifier = "(*(2;sys_def_i_complex))"
        )

        val logRef = log.getLogInPlainText()

        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\exp \\left(\\ln \\left(2\\right)+\\frac{\\pi i}{2}\\right)\\textcolor{green}{=}2\\left(\\cos \\left(\\frac{\\pi }{2}\\right)+\\sin \\left(\\frac{\\pi }{2}\\right)i\\right)\\textcolor{green}{=}2\\left(0+i\\right)\\textcolor{green}{=}2i}",
                result.validatedSolution)
    }
}

