package mathhelper.twf.tasksolutions

import mathhelper.twf.api.*
import mathhelper.twf.assert
import mathhelper.twf.config.*
import mathhelper.twf.defaultcontent.TaskTagCode
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks
import mathhelper.twf.factstransformations.FactConstructorViewer
import mathhelper.twf.factstransformations.FactSubstitution
import mathhelper.twf.factstransformations.MainLineAndNode
import mathhelper.twf.factstransformations.parseFromFactIdentifier
import mathhelper.twf.logs.log
import mathhelper.twf.mainpoints.checkFactsInMathML
import mathhelper.twf.mainpoints.compiledConfigurationBySettings
import mathhelper.twf.mainpoints.configSeparator
import mathhelper.twf.platformdependent.JsonParser
import kotlin.test.assertEquals
import org.junit.Ignore
import org.junit.Test

class SetTheory {
    val wellKnownFunctions = listOf(
            FunctionIdentifier("", 0),
            FunctionIdentifier("", 1)
    )

    val wellKnownFunctionsString = "" +
            "${configSeparator}0"

    val expressionTransformationRules = listOf(
            expressionSubstitutionFromStructureStrings("(or(A;and(B;C)))", "(and(or(A;B);or(A;C)))"),
            expressionSubstitutionFromStructureStrings("(or(and(B;C);A))", "(and(or(B;A);or(C;A)))"),
            expressionSubstitutionFromStructureStrings("(and(A;or(B;C)))", "(or(and(A;B);and(A;C)))"),
            expressionSubstitutionFromStructureStrings("(and(or(B;C);A))", "(or(and(B;A);and(C;A)))"),

            expressionSubstitutionFromStructureStrings("(not(not(A)))", "(A)"),
            expressionSubstitutionFromStructureStrings("(or(A;A))", "(A)"),
            expressionSubstitutionFromStructureStrings("(and(A;A))", "(A)"),

            expressionSubstitutionFromStructureStrings("(not(and(A;B)))","(or(not(A);not(B)))"),
            expressionSubstitutionFromStructureStrings("(not(or(A;B)))","(and(not(A);not(B)))"),

            expressionSubstitutionFromStructureStrings("(or(not(A);B))","(implic(A;B))"),
            expressionSubstitutionFromStructureStrings("(or(B;not(A)))","(implic(A;B))"),
            expressionSubstitutionFromStructureStrings("(and(A;not(B)))","(set-(A;B))"),
            expressionSubstitutionFromStructureStrings("(and(not(B);A))","(set-(A;B))"),
            expressionSubstitutionFromStructureStrings("(set-(A;B))","(not(implic(A;B)))"),
            expressionSubstitutionFromStructureStrings("(implic(A;B))","(not(set-(A;B)))"),

            expressionSubstitutionFromStructureStrings("(or(A;not(A)))","(1)"),
            expressionSubstitutionFromStructureStrings("(or(not(A);A))","(1)"),
            expressionSubstitutionFromStructureStrings("(and(A;not(A)))","(0)"),
            expressionSubstitutionFromStructureStrings("(and(not(A);A))","(0)"),

            expressionSubstitutionFromStructureStrings("(or(A;1))","(1)"),
            expressionSubstitutionFromStructureStrings("(or(1;A))","(1)"),
            expressionSubstitutionFromStructureStrings("(and(A;1))","(A)"),
            expressionSubstitutionFromStructureStrings("(and(1;A))","(A)"),
            expressionSubstitutionFromStructureStrings("(or(A;0))","(A)"),
            expressionSubstitutionFromStructureStrings("(or(0;A))","(A)"),
            expressionSubstitutionFromStructureStrings("(and(A;0))","(0)"),
            expressionSubstitutionFromStructureStrings("(and(0;A))","(0)"),

            expressionSubstitutionFromStructureStrings("(set-(A;0))","(A)"),
            expressionSubstitutionFromStructureStrings("(set-(A;1))","(0)"),
            expressionSubstitutionFromStructureStrings("(set-(0;A))","(0)"),
            expressionSubstitutionFromStructureStrings("(set-(1;A))","(not(A))"),

            expressionSubstitutionFromStructureStrings("(implic(A;0))","(not(A))"),
            expressionSubstitutionFromStructureStrings("(implic(A;1))","(1)"),
            expressionSubstitutionFromStructureStrings("(implic(0;A))","(1)"),
            expressionSubstitutionFromStructureStrings("(implic(1;A))","(A)"),

            expressionSubstitutionFromStructureStrings("(not(0))","(1)"),
            expressionSubstitutionFromStructureStrings("(not(1))","(0)")
    )

    val expressionTransformationRulesString = "" +
            "A\\/(A/\\B)${configSeparator}A${configSeparator}" +
            "A/\\(A\\/B)${configSeparator}A${configSeparator}" +
            "A\\/A${configSeparator}A${configSeparator}" +
            "A/\\A${configSeparator}A${configSeparator}" +
            "A\\/(B\\/C)${configSeparator}A\\/B\\/C${configSeparator}" +
            "A/\\(B/\\C)${configSeparator}A/\\B/\\C${configSeparator}" +
            "A\\/(B/\\C)${configSeparator}(A\\/B)/\\(A\\/C)${configSeparator}" +
            "A/\\(B\\/C)${configSeparator}(A/\\B)\\/(A/\\C)${configSeparator}" +
            "!(A/\\B)${configSeparator}!A\\/!B${configSeparator}" +
            "!(A\\/B)${configSeparator}!A/\\!B${configSeparator}" +
            "A->B${configSeparator}!A\\/B${configSeparator}" +
            "A\\B${configSeparator}A/\\!B${configSeparator}" +

            "A\\/B${configSeparator}B\\/A${configSeparator}" +
            "A/\\B${configSeparator}B/\\A${configSeparator}" +

            "!(!(A))${configSeparator}A"


    @Test
    fun asIsWithoutSubstitutions() {
        val result = compareWithoutSubstitutions(
                "!(C/\\A)/\\(B\\/!C)",
                "!(A/\\C)/\\(B\\/!C)",
                "setTheory"
        )
        assertEquals(true, result)
    }

    @Test
    fun asIsWithoutSubstitutionsInternalBrackets() {
        val result = compareWithoutSubstitutions(
                "not(or(or(B,A),C))",
                "not(or(A,B,C))",
                "setTheory"
        )
        assertEquals(true, result)
    }

    @Test
    fun substitutionApplicationTest (){
        val places = findSubstitutionPlacesCoordinatesInExpressionJSON (
                "A|A",
                "A",
                "B",
                scope = "setTheory",
                basedOnTaskContext = true
        )
        assertEquals("{\"substitutionPlaces\":[{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"3\",\"startPosition\":\"0\",\"endPosition\":\"1\"},{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"3\",\"startPosition\":\"2\",\"endPosition\":\"3\"}]}", places)
    }

    @Test
    fun wrongWithoutSubstitutions() {
        val result = compareWithoutSubstitutions(
                "!(B/\\A)/\\(B\\/!C)",
                "!(A/\\C)/\\(B\\/!C)",
                "setTheory",
                maxExpressionBustCount = 2
        )
        assertEquals(false, result)
    }

    @Test
    fun LongConjunction() {
        val result = compareWithoutSubstitutions(
                "C/\\A/\\B/\\D",
                "C/\\A/\\(B/\\D)",
                "setTheory"
        )
        assertEquals(true, result)
    }

    @Test
    fun LongConjunctionWithPermutation() {
        val result = compareWithoutSubstitutions(
                "(C/\\B)/\\A/\\D",
                "D/\\(A/\\(B/\\C))",
                "setTheory"
        )
        assertEquals(true, result)
    }

    @Test
    @Ignore
    fun proveTaskSet1() {
        val result = checkFactsInMathML(
                brushedMathML = "(!A/\\B)\\/C=(!A\\/!C)/\\(B\\/!C)=!(C/\\A)/\\(B\\/!C)",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(or(and(not(A);B);C))}{=}{(and(not(and(C;A));or(B;not(C))))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "",
                scopeFilter = ";;;setTheory"
        )

        val logRef = log.getLogInPlainText()

        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
//        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>(</mo><mn>1</mn><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>(</mo><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>8</mn><mo>*</mo><mo>(</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mi>x</mi><mrow><mo>)</mo><msup><mo>)</mo><mn>4</mn></msup></mrow></math>", result)
    }

    @Test
    fun proveTaskSet2() {
        val result = checkFactsInMathML(
                brushedMathML = "(!A/\\B)\\/C=(!A\\/!C)/\\(B)=!(C/\\A)/\\(B\\/!C)",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(or(and(not(A);B);C))}{=}{(and(not(and(C;A));or(B;not(C))))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "",
                scopeFilter = ";;;setTheory"
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
    }

    @Test
    fun proveTaskTexSimpleWrongNotObvious() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\neg A\\wedge \\neg B\\wedge \\neg C=\\neg \\left(A\\vee B\\vee C\\right)",
                wellKnownFunctions = wellKnownFunctions,
                expressionTransformationRules = expressionTransformationRules,
                startExpressionIdentifier = "(and(not(A);not(B);not(C)))",
                endExpressionIdentifier = "(not(or(A;B;C)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(and(not(A);not(B);not(C)))' and '(not(or(A;B;C)))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{red}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSimpleFull() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\neg A\\wedge \\neg B\\wedge \\neg C=\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C=\\neg \\left(A\\vee B\\right)\\wedge \\neg C=\\neg (\\left(A\\vee B\\right)\\vee C)=\\neg \\left(A\\vee B\\vee C\\right)",
                wellKnownFunctions = wellKnownFunctions,
                expressionTransformationRules = expressionTransformationRules,
                startExpressionIdentifier = "(and(not(A);not(B);not(C)))",
                endExpressionIdentifier = "(not(or(A;B;C)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{green}{=}\\neg \\left(A\\vee B\\right)\\wedge \\neg C\\textcolor{green}{=}\\neg (\\left(A\\vee B\\right)\\vee C)\\textcolor{green}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSimple() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\neg A\\wedge \\neg B\\wedge \\neg C=\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C=\\neg \\left(A\\vee B\\right)\\wedge \\neg C=\\neg \\left(A\\vee B\\vee C\\right)",
                rulePacks = listOf("Logic").toTypedArray(),
                startExpressionIdentifier = "(and(not(A);not(B);not(C)))",
                endExpressionIdentifier = "(not(or(A;B;C)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{green}{=}\\neg \\left(A\\vee B\\right)\\wedge \\neg C\\textcolor{green}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSimpleITR() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\neg A\\wedge \\neg B\\wedge \\neg C=\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C=\\neg \\left(A\\vee B\\right)\\wedge \\neg C=\\neg \\left(A\\vee B\\vee C\\right)",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(and(not(A);not(B);not(C)))",
                        goalExpressionStructureString = "(not(or(A;B;C)))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        difficulty = 2.9
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{green}{=}\\neg \\left(A\\vee B\\right)\\wedge \\neg C\\textcolor{green}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }



    @Test
    fun proveTaskCompleteTransformationTexCorrectSolution() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\textcolor{purple}{\\left(A\\vee \\neg B\\right)\\wedge \\left(A\\vee \\neg C\\right)\\textcolor{red}{=}A\\vee \\left(\\neg B\\wedge \\neg C\\right)=A\\vee \\neg \\left(B\\vee C\\right)}",
                rulePacks = listOf("Logic").toTypedArray(),
                startExpressionIdentifier = "(and(or(A;not(B));or(A;not(C))))",
                endExpressionIdentifier = "(or(A;not(or(B;C))))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\left(A\\vee \\neg B\\right)\\wedge \\left(A\\vee \\neg C\\right)\\textcolor{green}{=} A\\vee \\left(\\neg B\\wedge \\neg C\\right)\\textcolor{green}{=}A\\vee \\neg \\left(B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskCompleteTransformationTexCorrectSolutionITR() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\left(A\\vee \\neg B\\right)\\wedge \\left(A\\vee \\neg C\\right)\\textcolor{red}{=}A\\vee \\left(\\neg B\\wedge \\neg C\\right)=A\\vee \\neg \\left(B\\vee C\\right)}",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(and(or(A;not(B));or(A;not(C))))",
                        goalExpressionStructureString = "(or(A;not(or(B;C))))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        difficulty = 2.9
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()

        assertEquals("",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\left(A\\vee \\neg B\\right)\\wedge \\left(A\\vee \\neg C\\right)\\textcolor{green}{=} A\\vee \\left(\\neg B\\wedge \\neg C\\right)\\textcolor{green}{=}A\\vee \\neg \\left(B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskCompleteTransformationTexSimpleITR() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\left(A\\vee B\\vee C\\right)\\wedge \\left(A\\Rightarrow \\left(C\\vee D\\right)\\right)\\wedge \\left(C\\Rightarrow \\left(B\\vee D\\right)\\right)\\wedge \\neg B\\wedge \\neg D\\textcolor{green}{=}\\left(0\\right)}",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(and(or(A;B;C);implic(A;or(C;D));implic(C;or(B;D));not(B);not(D)))",
                        otherGoalData = JsonParser.parseMap("{\"hiddenGoalExpressions\":[\"(0)\"]}") as Map<String, Any>,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        difficulty = 2.9
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(and(or(A;B;C);implic(A;or(C;D));implic(C;or(B;D));not(B);not(D)))' and '(0)' ",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\left(A\\vee B\\vee C\\right)\\wedge \\left(A\\Rightarrow \\left(C\\vee D\\right)\\right)\\wedge \\left(C\\Rightarrow \\left(B\\vee D\\right)\\right)\\wedge \\neg B\\wedge \\neg D\\textcolor{red}{=}\\left(0\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskCompleteTransformationNoLimitationsTexSimpleITR() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\left(A\\vee B\\vee C\\right)\\wedge \\left(A\\Rightarrow \\left(C\\vee D\\right)\\right)\\wedge \\left(C\\Rightarrow \\left(B\\vee D\\right)\\right)\\wedge \\neg B\\wedge \\neg D\\textcolor{green}{=}\\left(0\\right)}",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(and(or(A;B;C);implic(A;or(C;D));implic(C;or(B;D));not(B);not(D)))",
                        otherGoalData = JsonParser.parseMap("{\"hiddenGoalExpressions\":[\"(0)\"]}") as Map<String, Any>,
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        difficulty = 2.9,
                        otherCheckSolutionData = mapOf(
                                notChangesOnVariablesInComparisonFunctionJsonName to listOf(
                                        FunctionIdentifierITR("and", "-1"),
                                        FunctionIdentifierITR("or", "-1"),
                                        FunctionIdentifierITR("implic", "2"),
                                        FunctionIdentifierITR("not", "1")
                                )
                        )
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\left(A\\vee B\\vee C\\right)\\wedge \\left(A\\Rightarrow \\left(C\\vee D\\right)\\right)\\wedge \\left(C\\Rightarrow \\left(B\\vee D\\right)\\right)\\wedge \\neg B\\wedge \\neg D\\textcolor{green}{=}\\left(0\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTask4CompleteTransformationTexIncorrectSolutionITR() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\left(A\\Rightarrow \\neg B\\right)\\Rightarrow C\\textcolor{green}{=}!\\left(A\\Rightarrow \\neg B\\right)\\vee C\\textcolor{red}{=}!\\left(!A\\vee \\vee B\\right)\\vee C==!\\ !\\left(A\\wedge B\\right)\\vee C}",
                taskITR = TaskITR(
                        subjectType = "logic",
                        originalExpressionStructureString = "(implic(implic(A;not(B));C))",
                        goalExpressionStructureString = "(or(and(A;B);C))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        difficulty = 2.9
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()

        assertEquals("Syntax error (underlined): Unexpected binary operation '\\vee'",
                result.errorMessage)
        assertEquals("\\left(A\\Rightarrow \\neg B\\right)\\Rightarrow C=!\\left(A\\Rightarrow \\neg B\\right)\\vee C=!\\left(!A\\vee \\underline{\\vee B}\\right)\\vee C==!\\ !\\left(A\\wedge B\\right)\\vee C",
                result.validatedSolution)
    }

    @Test
    fun proveTask4CompleteTransformationTexCorrectSolutionITR() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\left(A\\Rightarrow \\neg B\\right)\\Rightarrow C\\textcolor{green}{=}!\\left(A\\Rightarrow \\neg B\\right)\\vee C\\textcolor{green}{=}!\\left(!A\\vee !B\\right)\\vee C\\textcolor{red}{==}\\left(A\\wedge B\\right)\\vee C}",
                taskITR = TaskITR(
                        subjectType = "logic",
                        originalExpressionStructureString = "(implic(implic(A;not(B));C))",
                        goalExpressionStructureString = "(or(and(A;B);C))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        difficulty = 2.9,
                        otherCheckSolutionData = mapOf(
                        notChangesOnVariablesInComparisonFunctionJsonName to listOf(
                                FunctionIdentifierITR("not", "1")
                        ))
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()

        assertEquals("",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\left(A\\Rightarrow \\neg B\\right)\\Rightarrow C\\textcolor{green}{=}!\\left(A\\Rightarrow \\neg B\\right)\\vee C\\textcolor{green}{=}!\\left(!A\\vee !B\\right)\\vee C\\textcolor{green}{==}\\left(A\\wedge B\\right)\\vee C}",
                result.validatedSolution)
    }

    @Test
    fun simplificationTaskCompleteTransformationTexWrongSolutionITRCurcleError() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\neg \\left(\\left(A\\wedge \\neg A\\right)\\vee \\left(B\\vee \\neg B\\right)\\vee \\left(A\\wedge \\neg B\\right)\\right)\\textcolor{green}{=}\\neg \\left(\\left(\\left(A\\wedge \\neg A\\right)\\vee 1\\right)\\vee \\left(A\\wedge \\neg B\\right)\\right)\\textcolor{green}{=}\\neg \\left(1\\vee \\left(A\\wedge \\neg B\\right)\\right)}",
                taskITR =
                TaskITR(originalExpressionStructureString = "(not(or(and(A;not(A));or(B;not(B));and(A;not(B)))))",
                        goalType = "simplification",
                        goalPattern = "?:0:?:?N",
                        otherGoalData = mapOf(
                                "hiddenGoalExpressions" to listOf("(0)")
                        ),
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "SetBase")),
                        subjectType = "set",
                        difficulty = 1.0,
                        tags = mutableSetOf(TaskTagCode.SET.code, TaskTagCode.TRICK.code)
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray(),
                comparisonSettings = ComparisonSettings()
        )

        val logRef = log.getLogInPlainText()

        assertEquals("Error: answer does not match given condition",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg \\left(\\left(A\\wedge \\neg A\\right)\\vee \\left(B\\vee \\neg B\\right)\\vee \\left(A\\wedge \\neg B\\right)\\right)\\textcolor{green}{=}\\neg \\left(\\left(\\left(A\\wedge \\neg A\\right)\\vee 1\\right)\\vee \\left(A\\wedge \\neg B\\right)\\right)\\textcolor{green}{=}\\neg \\left(1\\vee \\left(A\\wedge \\neg B\\right)\\right)}",
                result.validatedSolution)
    }

    @Test
    fun simplificationTaskCompleteTransformationTexCorrectSolutionITRCurcleError() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\neg \\left(\\left(A\\wedge \\neg A\\right)\\vee \\left(B\\vee \\neg B\\right)\\vee \\left(A\\wedge \\neg B\\right)\\right)\\textcolor{green}{=}\\neg \\left(\\left(\\left(A\\wedge \\neg A\\right)\\vee 1\\right)\\vee \\left(A\\wedge \\neg B\\right)\\right)\\textcolor{green}{=}\\neg \\left(1\\vee \\left(A\\wedge \\neg B\\right)\\right)\\textcolor{green}{=}\\neg \\left(1\\right)}=0",
                taskITR =
                TaskITR(originalExpressionStructureString = "(not(or(and(A;not(A));or(B;not(B));and(A;not(B)))))",
                        goalType = "simplification",
                        goalPattern = "?:0:?:?N",
                        otherGoalData = mapOf(
                                "hiddenGoalExpressions" to listOf("(0)")
                        ),
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "SetBase")),
                        subjectType = "set",
                        difficulty = 1.0,
                        tags = mutableSetOf(TaskTagCode.SET.code, TaskTagCode.TRICK.code)
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray(),
                comparisonSettings = ComparisonSettings()
        )

        val logRef = log.getLogInPlainText()

        assertEquals("",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg \\left(\\left(A\\wedge \\neg A\\right)\\vee \\left(B\\vee \\neg B\\right)\\vee \\left(A\\wedge \\neg B\\right)\\right)\\textcolor{green}{=}\\neg \\left(\\left(\\left(A\\wedge \\neg A\\right)\\vee 1\\right)\\vee \\left(A\\wedge \\neg B\\right)\\right)\\textcolor{green}{=}\\neg \\left(1\\vee \\left(A\\wedge \\neg B\\right)\\right)\\textcolor{green}{=}\\neg \\left(1\\right)\\textcolor{green}{=}0}",
                result.validatedSolution, "\n${result.validatedSolution}\n")
    }

    @Test
    fun proveTask7ShortCompleteTransformationTexWrongSolutionITR() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\left(A\\wedge B\\right)\\vee \\left(A\\wedge \\neg B\\right)\\textcolor{red}{=}\\left(A\\vee \\left(B\\wedge \\neg B\\right)\\right)=A}",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(or(and(A;B);and(A;not(B))))",
                        goalExpressionStructureString = "(A)",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "SetBase"), RulePackLinkITR(rulePackCode = "SetAbsorptionLaw")),
                        subjectType = "set",
                        difficulty = 3.0,
                        tags = mutableSetOf(TaskTagCode.SET.code, TaskTagCode.TRICK.code)
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray(),
                comparisonSettings = ComparisonSettings()
        )

        val logRef = log.getLogInPlainText()

        assertEquals("Error: Unclear transformation between '(or(and(A;B);and(A;not(B))))' and '(or(A;and(B;not(B))))' ",
                result.errorMessage)
    }

    @Test
    fun proveTask4mapTextcolorToSpaceTexWrongSolutionITR() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\left(A\\wedge B\\right)\\vee\\left(\\neg A\\wedge B\\right)\\vee\\left(A\\wedge\\neg B\\right)\\textcolor{green}{=}\\left(A\\wedge B\\right)\\vee\\left(A\\wedge\\neg B\\right)\\ \\vee\\left(\\neg A\\wedge B\\right)\\textcolor{green}{=}\\ A\\ \\wedge\\left(B\\vee\\neg B\\right)\\vee\\left(\\neg A\\wedge B\\right)\\textcolor{green}{=}\\left(A\\wedge1\\right)\\vee\\left(\\neg A\\wedge B\\right)\\textcolor{green}{=}A\\ \\vee\\left(\\neg A\\wedge B\\right)\\ \\textcolor{green}{=}\\ A\\vee\\neg\\textcolor{red}{A\\wedge A\\vee B=}A\\vee B}",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(or(and(A;B);and(not(A);B);and(A;not(B))))",
                        goalExpressionStructureString = "(or(A;B))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "SetBase"), RulePackLinkITR(rulePackCode = "SetAbsorptionLaw")),
                        subjectType = "set",
                        difficulty = 3.0,
                        tags = mutableSetOf(TaskTagCode.SET.code, TaskTagCode.TRICK.code)
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray(),
                comparisonSettings = ComparisonSettings()
        )

        val logRef = log.getLogInPlainText()

        assertEquals("Error: Unclear transformation between '(or(A;and(not(A);B)))' and '(or(A;and(not(A);A);B))' ",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\left(A\\wedge B\\right)\\vee\\left(\\neg A\\wedge B\\right)\\vee\\left(A\\wedge\\neg B\\right)\\textcolor{green}{=}\\left(A\\wedge B\\right)\\vee\\left(A\\wedge\\neg B\\right)\\ \\vee\\left(\\neg A\\wedge B\\right)\\textcolor{green}{=}\\ A\\ \\wedge\\left(B\\vee\\neg B\\right)\\vee\\left(\\neg A\\wedge B\\right)\\textcolor{green}{=}\\left(A\\wedge1\\right)\\vee\\left(\\neg A\\wedge B\\right)\\textcolor{green}{=} A\\ \\vee\\left(\\neg A\\wedge B\\right)\\ \\textcolor{red}{=}\\ A\\vee\\neg A\\wedge A\\vee B= A\\vee B}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskAllEqSolutionITR() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\left(a\\Rightarrow b\\right)\\wedge \\left(b\\Rightarrow a\\right)=\\left(!a\\vee b\\right)\\wedge \\left(!b\\vee a\\right)=¬¬(a\\ \\underline{\\equiv \\ b})",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(and(implic(a;b);implic(b;a)))",
                        goalExpressionStructureString = "(alleq(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 3.0
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray(),
                comparisonSettings = ComparisonSettings()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(and(or(not(a);b);or(not(b);a)))' and '(not(not(alleq(a;b))))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\left(a\\Rightarrow b\\right)\\wedge \\left(b\\Rightarrow a\\right)\\textcolor{green}{=}\\left(!a\\vee b\\right)\\wedge \\left(!b\\vee a\\right)\\textcolor{red}{=}¬¬(a\\ \\equiv \\ b)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskAllEqSolutionITR_enter() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\left(a\\Rightarrow b\\right)\\wedge\n \\left(b\\Rightarrow a\\right)=\\left(!a\\vee b\\right)\\wedge \\left(!b\\vee a\\right)=\n! !(a\\ \\underline{\\equiv\n \\ b})",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(and(implic(a;b);implic(b;a)))",
                        goalExpressionStructureString = "(alleq(a;b))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
                        subjectType = "logic",
                        difficulty = 3.0
                ),
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray(),
                comparisonSettings = ComparisonSettings()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(and(or(not(a);b);or(not(b);a)))' and '(not(not(alleq(a;b))))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\left(a\\Rightarrow b\\right)\\wedge\n \\left(b\\Rightarrow a\\right)\\textcolor{green}{=}\\left(!a\\vee b\\right)\\wedge \\left(!b\\vee a\\right)\\textcolor{red}{=}\n! !(a\\ \\equiv\n \\ b)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSimpleUnclearTransformations() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\neg A\\wedge \\neg B\\wedge \\neg C=\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C=\\neg \\left(A\\vee B\\vee C\\right)",
                wellKnownFunctions = wellKnownFunctions,
                rulePacks = listOf("Logic").toTypedArray(),
                startExpressionIdentifier = "(and(not(A);not(B);not(C)))",
                endExpressionIdentifier = "(not(or(A;B;C)))"
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(and(and(not(A);not(B));not(C)))' and '(not(or(A;B;C)))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{red}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSimpleUnclearTransformationsColored() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{red}{=}\\neg} \\left(A\\vee B\\vee C\\right)",
                wellKnownFunctions = wellKnownFunctions,
                rulePacks = listOf("Logic").toTypedArray(),
                startExpressionIdentifier = "(and(not(A);not(B);not(C)))",
                endExpressionIdentifier = "(not(or(A;B;C)))"
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(and(and(not(A);not(B));not(C)))' and '(not(or(A;B;C)))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{red}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSolutionWithReductionRules() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\textcolor{purple}{a\\wedge \\left(a\\vee b\\right)\\textcolor{green}{=}\\left(a\\vee 0\\right)\\wedge \\left(a\\vee b\\right)\\textcolor{red}{=}a\\vee \\left(0\\wedge b\\right)=a\\vee 0=a}",
                rulePacks = listOf("Logic").toTypedArray(),
                startExpressionIdentifier = "(and(a;or(a;b)))",
                endExpressionIdentifier = "(a)"
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{a\\wedge \\left(a\\vee b\\right)\\textcolor{green}{=}\\left(a\\vee 0\\right)\\wedge \\left(a\\vee b\\right)\\textcolor{green}{=} a\\vee \\left(0\\wedge b\\right)\\textcolor{green}{=}a\\vee 0\\textcolor{green}{=}a}",
                result.validatedSolution)
    }

    @Test
    fun correctCustomTransformations() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>&#xAC;</mo><mo>&#xAC;</mo><mi>b</mi><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">=</mo><mi>b</mi></math>",
                wellKnownFunctions = "",
                expressionTransformationRules = " ",
                targetFactIdentifier = "",
                targetVariablesNames = "",
                minNumberOfMultipliersInAnswer = "",
                additionalFactsIdentifiers = "",
                makeFirstSingleTransformationChainFactCorrectWithoutAdditionalFacts = "1"
        )

        val logRef = log.getLogInPlainText()

        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>&#xAC;</mo><mo>&#xAC;</mo><mi>b</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>b</mi></math>",
                result)
    }

    @Test
    fun wrongCustomTransformations() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>&#xAC;</mo><mo>&#xAC;</mo><mi>b</mi><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">=</mo><mi>a</mi></math>",
                wellKnownFunctions = "",
                expressionTransformationRules = " ",
                targetFactIdentifier = "",
                targetVariablesNames = "",
                minNumberOfMultipliersInAnswer = "",
                additionalFactsIdentifiers = "",
                makeFirstSingleTransformationChainFactCorrectWithoutAdditionalFacts = "1"
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>&#xAC;</mo><mo>&#xAC;</mo><mi>b</mi><mrow mathvariant=\"bold\" mathcolor=\"#FF0000\"><mo>=</mo></mrow><mi>a</mi><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation between '(not(not(b)))' and '(a)' </mtext></math>",
                result)
    }

    @Test
    @Ignore
    fun oldIdentifierToNew() {
        val functionConfiguration = FunctionConfiguration(setOf("", "setTheory"))
        val factConstructorViewer: FactConstructorViewer = FactConstructorViewer(compiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration))
        val fact = parseFromFactIdentifier("(!A/\\B)\\/C;ec;=;ec;!(C/\\A)/\\(B\\/!C)", functionConfiguration = functionConfiguration)!!
        val newIdentifier = factConstructorViewer.constructIdentifierByFact(fact)
        print(newIdentifier)
    }
}