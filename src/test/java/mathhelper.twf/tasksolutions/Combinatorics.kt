package mathhelper.twf.tasksolutions

import mathhelper.twf.api.checkSolutionInTexITR
import mathhelper.twf.api.createCompiledConfigurationFromITR
import mathhelper.twf.api.createOtherCheckSolutionData
import mathhelper.twf.config.*
import mathhelper.twf.defaultcontent.TaskTagCode
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks
import mathhelper.twf.defaultcontent.defaulttasksets.CombinatoricTaskSets
import mathhelper.twf.logs.log
import mathhelper.twf.platformdependent.JsonParser
import org.junit.Test
import kotlin.test.assertEquals

class Combinatorics {
    @Test
    fun testCompiledConfiguration() {
        val taskITR = TaskITR(
                originalExpressionStructureString = "(*(V(+(m;1);n);*(P(m);P(n))))",
                goalExpressionStructureString = "(*(P(m);A(+(m;n);n)))",
                subjectType = "combinatorics",
                difficulty = 3.0,
                otherCheckSolutionData = createOtherCheckSolutionData(" {\"otherCheckSolutionData\": {  \"nullWeightOperations\": [  {  \"name\": \"+\",  \"numberOfArguments\": \"-1\",  \"needRules\": \"false\"  },  {  \"name\": \"-\",  \"numberOfArguments\": \"-1\",  \"needRules\": \"false\"  },  {  \"name\": \"*\",  \"numberOfArguments\": \"-1\",  \"needRules\": \"false\"  },  {  \"name\": \"/\",  \"numberOfArguments\": \"-1\",  \"needRules\": \"false\"  }  ],  \"lightWeightOperations\": [  {  \"name\": \"+\",  \"numberOfArguments\": \"-1\",  \"needRules\": \"false\"  },  {  \"name\": \"-\",  \"numberOfArguments\": \"-1\",  \"needRules\": \"false\"  },  {  \"name\": \"*\",\"numberOfArguments\": \"-1\",\"needRules\": \"false\"}, { \"name\": \"/\",\"numberOfArguments\": \"-1\",\"needRules\": \"false\"}]}}"),
                tags = mutableSetOf(TaskTagCode.COMBINATORICS.code)
        )

        val compiledConfiguration = createCompiledConfigurationFromITR(taskITR, DefaultRulePacks.defaultRulePacks.toTypedArray())
        assertEquals(listOf(
                FunctionIdentifier(name="+", numberOfArguments=-1, needRules=false),
                FunctionIdentifier(name="-", numberOfArguments=-1, needRules=false),
                FunctionIdentifier(name="*", numberOfArguments=-1, needRules=false),
                FunctionIdentifier(name="/", numberOfArguments=-1, needRules=false)
        ), compiledConfiguration.functionConfiguration.notChangesOnVariablesInComparisonFunction)
        assertEquals(listOf(
                FunctionIdentifier(name="+", numberOfArguments=-1, needRules=false),
                FunctionIdentifier(name="-", numberOfArguments=-1, needRules=false),
                FunctionIdentifier(name="*", numberOfArguments=-1, needRules=false),
                FunctionIdentifier(name="/", numberOfArguments=-1, needRules=false)
        ), compiledConfiguration.functionConfiguration.notChangesOnVariablesInComparisonFunctionWithoutTransformations)
        assertEquals(mutableSetOf("+_-1", "-_-1", "*_-1", "/_-1"), compiledConfiguration.noTransformationDefinedFunctionNameNumberOfArgsSet)
    }

    @Test
    fun provePVATaskInITRTexIncorrect() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{V\\left(m+1,n\\right)\\cdot \\left(P\\left(m\\right)\\cdot P\\left(n\\right)\\right)\\textcolor{green}{=}P\\left(m+1\\right)\\cdot A\\left(m+n,n\\right)}",
                taskITR = CombinatoricTaskSets.сombinatoricsBaseTrainSetTasks[0],
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(*(V(+(m;1);n);*(P(m);P(n))))' and '(*(P(+(m;1));A(+(m;n);n)))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{V\\left(m+1,n\\right)\\cdot \\left(P\\left(m\\right)\\cdot P\\left(n\\right)\\right)\\textcolor{red}{=} P\\left(m+1\\right)\\cdot A\\left(m+n,n\\right)}",
                result.validatedSolution)
    }

    @Test
    fun provePVATaskInITRTexIncorrectFrontSample() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "V\\left(m+1,n\\right)\\cdot \\left(P\\left(m\\right)\\cdot P\\left(n\\right)\\right)=P\\left(m\\right)\\cdot A\\left(m+n,n\\right)",
                taskITR = CombinatoricTaskSets.сombinatoricsBaseTrainSetTasks[0],
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(*(V(+(m;1);n);*(P(m);P(n))))' and '(*(P(m);A(+(m;n);n)))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{V\\left(m+1,n\\right)\\cdot \\left(P\\left(m\\right)\\cdot P\\left(n\\right)\\right)\\textcolor{red}{=}P\\left(m\\right)\\cdot A\\left(m+n,n\\right)}",
                result.validatedSolution)
    }

    @Test
    fun provePVATaskInITRTexCorrect() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{V\\left(m+1,n\\right)\\cdot \\left(P\\left(m\\right)\\cdot P\\left(n\\right)\\right)\\textcolor{green}{=}V\\left(m+1,n\\right)\\cdot \\left(m!\\cdot n!\\right)\\textcolor{green}{=}C\\left(m+n,n\\right)\\cdot \\left(m!\\cdot n!\\right)\\textcolor{green}{=}\\left(\\frac{\\left(m+n\\right)!}{n!\\cdot m!}\\right)\\cdot \\left(m!\\cdot n!\\right)\\textcolor{green}{=}\\left(\\frac{\\left(m+n\\right)!}{m!}\\right)\\cdot m!\\textcolor{green}{=}\\left(\\frac{\\left(m+n\\right)!}{m!}\\right)\\cdot P\\left(m\\right)\\textcolor{green}{=}P\\left(m\\right)\\cdot A\\left(m+n,n\\right)}",
                taskITR = CombinatoricTaskSets.сombinatoricsBaseTrainSetTasks[0],
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{V\\left(m+1,n\\right)\\cdot \\left(P\\left(m\\right)\\cdot P\\left(n\\right)\\right)\\textcolor{green}{=} V\\left(m+1,n\\right)\\cdot \\left(m!\\cdot n!\\right)\\textcolor{green}{=} C\\left(m+n,n\\right)\\cdot \\left(m!\\cdot n!\\right)\\textcolor{green}{=}\\left(\\frac{\\left(m+n\\right)!}{n!\\cdot m!}\\right)\\cdot \\left(m!\\cdot n!\\right)\\textcolor{green}{=}\\left(\\frac{\\left(m+n\\right)!}{m!}\\right)\\cdot m!\\textcolor{green}{=}\\left(\\frac{\\left(m+n\\right)!}{m!}\\right)\\cdot P\\left(m\\right)\\textcolor{green}{=} P\\left(m\\right)\\cdot A\\left(m+n,n\\right)}",
                result.validatedSolution)
    }

    @Test
    fun prove5PartTaskInITRTexCorrect() {
        val taskITR = TaskITR(
                originalExpressionStructureString = "(/(*(S2(n;m);factorial(+(m;n)));*(C(+(n;m);n);factorial(m);factorial(n))))",
                goalExpressionStructureString = "(/(*(S2(n;m);factorial(+(m;n)));*(/(factorial(+(n;m));*(factorial(m);factorial(n)));factorial(m);factorial(n))))",
                rulePacks = listOf(RulePackLinkITR(rulePackCode = "BasicCombinatorics")),
                subjectType = "combinatorics",
                difficulty = 4.0,
                otherCheckSolutionData = mapOf(
                        notChangesOnVariablesInComparisonFunctionJsonName to listOf(
                                FunctionIdentifierITR("+", "-1"),
                                FunctionIdentifierITR("-", "-1"),
                                FunctionIdentifierITR("*", "-1"),
                                FunctionIdentifierITR("/", "-1")
                        ),
                        notChangesOnVariablesInComparisonFunctionWithoutTransformationsJsonName to listOf(
                                FunctionIdentifierITR("+", "-1"),
                                FunctionIdentifierITR("-", "-1"),
                                FunctionIdentifierITR("*", "-1"),
                                FunctionIdentifierITR("/", "-1")
                        )
                ),
                tags = mutableSetOf(TaskTagCode.COMBINATORICS.code)
        )
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{C\\left(m+n,n\\right)\\cdot m!\\cdot n!}\\textcolor{red}{=}\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{\\frac{\\left(m+n\\right)!}{\\left(\\left(m+n\\right)-n\\right)!\\cdot n!}\\cdot m!\\cdot n!}\\textcolor{red}{=}\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{\\frac{\\left(n+m\\right)!}{m!\\cdot n!}\\cdot m!\\cdot n!}",
                taskITR = taskITR,
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{C\\left(m+n,n\\right)\\cdot m!\\cdot n!}\\textcolor{green}{=}\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{\\frac{\\left(m+n\\right)!}{\\left(\\left(m+n\\right)-n\\right)!\\cdot n!}\\cdot m!\\cdot n!}\\textcolor{green}{=}\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{\\frac{\\left(n+m\\right)!}{m!\\cdot n!}\\cdot m!\\cdot n!}}",
                result.validatedSolution)
    }

    @Test
    fun prove3TaskInITRTexCorrect() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\frac{S1\\left(n,m\\right)\\cdot A\\left(m+n,n\\right)}{V\\left(m+1,n\\right)\\cdot m!\\cdot n!}\\textcolor{green}{=}\\frac{S1\\left(n,m\\right)\\cdot \\frac{\\left(m+n\\right)!}{m!}}{V\\left(m+1,n\\right)\\cdot m!\\cdot n!}\\textcolor{red}{=}\\frac{S1\\left(n,m\\right)\\cdot \\frac{\\left(m+n\\right)!}{m!}}{C\\left(n+m,n\\right)\\cdot m!\\cdot n!}=\\frac{S1\\left(n,m\\right)\\cdot \\left(m+n\\right)!}{C\\left(n+m,n\\right)\\cdot m!\\cdot n!\\cdot m!}=\\frac{S1\\left(n,m\\right)\\cdot \\frac{\\left(m+n\\right)!}{m!\\cdot n!}}{C\\left(n+m,n\\right)\\cdot m!}=\\frac{S2\\left(n,m\\right)\\cdot m!\\cdot \\frac{\\left(m+n\\right)!}{m!\\cdot n!}\\ }{C\\left(n+m,n\\right)\\cdot m!}=\\frac{S2\\left(n,m\\right)\\cdot \\frac{\\left(m+n\\right)!}{m!\\cdot n!}\\ }{C\\left(n+m,n\\right)}=\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{C\\left(n+m,n\\right)\\cdot m!\\cdot n!}=\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{\\frac{\\left(n+m\\right)!}{\\left(\\left(n+m\\right)-n\\right)!\\cdot n!}\\cdot m!\\cdot n!}=\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{\\frac{\\left(n+m\\right)!}{\\left(n+m-n\\right)!\\cdot n!}\\cdot m!\\cdot n!}=\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{\\frac{\\left(n+m\\right)!}{\\left(m\\right)!\\cdot n!}\\cdot m!\\cdot n!}=S2\\left(n,m\\right)}",
                taskITR = CombinatoricTaskSets.basicCombinatoricsFormulasCheckYourselfTasksOne[2],
                rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{S1\\left(n,m\\right)\\cdot A\\left(m+n,n\\right)}{V\\left(m+1,n\\right)\\cdot m!\\cdot n!}\\textcolor{green}{=}\\frac{S1\\left(n,m\\right)\\cdot \\frac{\\left(m+n\\right)!}{m!}}{V\\left(m+1,n\\right)\\cdot m!\\cdot n!}\\textcolor{green}{=}\\frac{S1\\left(n,m\\right)\\cdot \\frac{\\left(m+n\\right)!}{m!}}{C\\left(n+m,n\\right)\\cdot m!\\cdot n!}\\textcolor{green}{=}\\frac{S1\\left(n,m\\right)\\cdot \\left(m+n\\right)!}{C\\left(n+m,n\\right)\\cdot m!\\cdot n!\\cdot m!}\\textcolor{green}{=}\\frac{S1\\left(n,m\\right)\\cdot \\frac{\\left(m+n\\right)!}{m!\\cdot n!}}{C\\left(n+m,n\\right)\\cdot m!}\\textcolor{green}{=}\\frac{S2\\left(n,m\\right)\\cdot m!\\cdot \\frac{\\left(m+n\\right)!}{m!\\cdot n!}\\ }{C\\left(n+m,n\\right)\\cdot m!}\\textcolor{green}{=}\\frac{S2\\left(n,m\\right)\\cdot \\frac{\\left(m+n\\right)!}{m!\\cdot n!}\\ }{C\\left(n+m,n\\right)}\\textcolor{green}{=}\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{C\\left(n+m,n\\right)\\cdot m!\\cdot n!}\\textcolor{green}{=}\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{\\frac{\\left(n+m\\right)!}{\\left(\\left(n+m\\right)-n\\right)!\\cdot n!}\\cdot m!\\cdot n!}\\textcolor{green}{=}\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{\\frac{\\left(n+m\\right)!}{\\left(n+m-n\\right)!\\cdot n!}\\cdot m!\\cdot n!}\\textcolor{green}{=}\\frac{S2\\left(n,m\\right)\\cdot \\left(m+n\\right)!\\ }{\\frac{\\left(n+m\\right)!}{\\left(m\\right)!\\cdot n!}\\cdot m!\\cdot n!}\\textcolor{green}{=}S2\\left(n,m\\right)}",
                result.validatedSolution)
    }
}