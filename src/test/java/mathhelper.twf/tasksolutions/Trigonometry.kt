package mathhelper.twf.tasksolutions

import mathhelper.twf.api.*
import mathhelper.twf.api.checkSolutionInTexWithCompiledConfiguration
import mathhelper.twf.api.createConfigurationFromRulePacksAndDetailSolutionCheckingParams
import mathhelper.twf.api.expressionSubstitutionFromStrings
import mathhelper.twf.assert
import mathhelper.twf.config.*
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks.Companion.defaultRulePacks
import mathhelper.twf.defaultcontent.defaulttasksets.TrigonometryStepByStep
import mathhelper.twf.factstransformations.FactConstructorViewer
import mathhelper.twf.factstransformations.parseFromFactIdentifier
import mathhelper.twf.logs.log
import mathhelper.twf.mainpoints.checkFactsInMathML
import mathhelper.twf.mainpoints.compiledConfigurationBySettings
import mathhelper.twf.mainpoints.configSeparator
import kotlin.test.assertEquals
import org.junit.Ignore
import org.junit.Test

class Trigonometry {
    val wellKnownFunctions = listOf(
            FunctionIdentifier("", 0),
            FunctionIdentifier("", 1),
            FunctionIdentifier("+", -1),
            FunctionIdentifier("-", -1),
            FunctionIdentifier("*", -1),
            FunctionIdentifier("/", -1),
            FunctionIdentifier("^", -1)
    )

    val wellKnownFunctionsString = "" +
            "${configSeparator}0$configSeparator" +
            "${configSeparator}1$configSeparator" +
            "+$configSeparator-1$configSeparator" +
            "-$configSeparator-1$configSeparator" +
            "*$configSeparator-1$configSeparator" +
            "/$configSeparator-1$configSeparator" +
            "^$configSeparator-1"

    val wellKnownFunctionsWithoutDegree = listOf(
            FunctionIdentifier("", 0),
            FunctionIdentifier("", 1),
            FunctionIdentifier("+", -1),
            FunctionIdentifier("-", -1),
            FunctionIdentifier("*", -1),
            FunctionIdentifier("/", -1)
    )

    val wellKnownFunctionsWithoutDegreeString = "" +
            "${configSeparator}0$configSeparator" +
            "${configSeparator}1$configSeparator" +
            "+$configSeparator-1$configSeparator" +
            "-$configSeparator-1$configSeparator" +
            "*$configSeparator-1$configSeparator" +
            "/$configSeparator-1"

    val expressionTransformationRulesSample = listOf(
            expressionSubstitutionFromStrings("sin(x)^2", "1-cos(x)^2"),
            expressionSubstitutionFromStrings("cos(x)^2", "1-sin(x)^2"),
            expressionSubstitutionFromStrings("cos(x)^2+sin(x)^2", "1")
    )

    val expressionTransformationRulesString = "" +
            "sin(x)^2${configSeparator}1 - cos(x)^2${configSeparator}" +
            "cos(x)^2${configSeparator}1 - sin(x)^2${configSeparator}" +
            "cos(x)^2+sin(x)^2${configSeparator}1${configSeparator}" +
            "tg(x)${configSeparator}sin(x)/cos(x)${configSeparator}" +
            "ctg(x)${configSeparator}cos(x)/sin(x)${configSeparator}" +
            "ctg(x)${configSeparator}1/tg(x)${configSeparator}" +
            "ctg(x)tg(x)${configSeparator}1${configSeparator}" +
            "tg(x)^2+1${configSeparator}1/cos(x)^2${configSeparator}" +
            "ctg(x)^2+1${configSeparator}1/sin(x)^2${configSeparator}" +
            "sin(x+y)${configSeparator}sin(x)cos(y)+sin(y)cos(x)${configSeparator}" +
            "sin(x-y)${configSeparator}sin(x)cos(y)-sin(y)cos(x)${configSeparator}" +
            "cos(x+y)${configSeparator}cos(x)cos(y)-sin(x)sin(y)${configSeparator}" +
            "cos(x-y)${configSeparator}cos(x)cos(y)+sin(x)sin(y)${configSeparator}" +
            "tg(x+y)${configSeparator}(tg(x)+tg(y))/(1-tg(x)tg(y))${configSeparator}" +
            "tg(x-y)${configSeparator}(tg(x)-tg(y))/(1+tg(x)tg(y))${configSeparator}" +
            "ctg(x+y)${configSeparator}(ctg(x)ctg(y)-1)/(ctg(x)+ctg(y))${configSeparator}" +
            "ctg(x-y)${configSeparator}(ctg(x)ctg(y)+1)/(ctg(x)-ctg(y))${configSeparator}" +
            "sin(2*x)${configSeparator}2*sin(x)cos(x)${configSeparator}" +
            "sin(2*x)${configSeparator}2*tg(x)/(1+(tg(x))^2)${configSeparator}" +
            "cos(2*x)${configSeparator}cos(x)^2 - sin(x)^2${configSeparator}" +
            "cos(2*x)${configSeparator}2*cos(x)^2 - 1${configSeparator}" +
            "cos(2*x)${configSeparator}1 - 2*sin(x)^2${configSeparator}" +
            "cos(2*x)${configSeparator}(1-(tg(x))^2)/(1+(tg(x))^2)${configSeparator}" +
            "sin(x)^2${configSeparator}(1 - cos(2*x))/2${configSeparator}" +
            "cos(x)^2${configSeparator}(1 + cos(2*x))/2${configSeparator}" +
            "sin(x)+sin(y)${configSeparator}2*sin((x+y)/2)*cos((x-y)/2)${configSeparator}" +
            "sin(x)-sin(y)${configSeparator}2*sin((x-y)/2)*cos((x+y)/2)${configSeparator}" +
            "cos(x)+cos(y)${configSeparator}2*cos((x+y)/2)*cos((x-y)/2)${configSeparator}" +
            "cos(x)-cos(y)${configSeparator}2*sin((x+y)/2)*sin((x-y)/2)${configSeparator}" +
            "sin(x)sin(y)${configSeparator}(cos(x-y)-cos(x+y))/2${configSeparator}" +
            "sin(x)cos(y)${configSeparator}(sin(x-y)+sin(x+y))/2${configSeparator}" +
            "cos(x)cos(y)${configSeparator}(cos(x-y)+cos(x+y))/2${configSeparator}" +
            "sin(0)${configSeparator}0${configSeparator}" +
            "sin(pi/2)${configSeparator}1${configSeparator}" +
            "cos(0)${configSeparator}1${configSeparator}" +
            "cos(pi/2)${configSeparator}0${configSeparator}" +
            "sin(pi/6)${configSeparator}1/2${configSeparator}" +
            "sin(pi/4)${configSeparator}2^0.5/2${configSeparator}" +
            "sin(pi/3)${configSeparator}3^0.5/2${configSeparator}" +
            "cos(pi/3)${configSeparator}1/2${configSeparator}" +
            "cos(pi/4)${configSeparator}2^0.5/2${configSeparator}" +
            "cos(pi/6)${configSeparator}3^0.5/2${configSeparator}" +
            "cos(pi/6)${configSeparator}3^0.5/2${configSeparator}" +
            "cos(-x)${configSeparator}cos(x)${configSeparator}" +
            "sin(-x)${configSeparator}-sin(x)${configSeparator}" +
            "sin(pi-x)${configSeparator}sin(x)${configSeparator}" +
            "cos(pi-x)${configSeparator}-cos(x)${configSeparator}" +
            "sin(x+2*pi)${configSeparator}sin(x)${configSeparator}" +
            "cos(x+2*pi)${configSeparator}cos(x)${configSeparator}" +
            "cos(pi/2-x)${configSeparator}sin(x)${configSeparator}" +
            "sin(pi/2-x)${configSeparator}cos(x)"

    @Test
    fun proveTaskLite14() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>(</mo><mn>1</mn><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>(</mo><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">=</mo><mn>8</mn><mo>*</mo><mo>(</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mi>x</mi><mrow><mo>)</mo><msup><mo>)</mo><mn>4</mn></msup></mrow></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>(</mo><mn>1</mn><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>(</mo><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>8</mn><mo>*</mo><mo>(</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mi>x</mi><mrow><mo>)</mo><msup><mo>)</mo><mn>4</mn></msup></mrow></math>", result)
    }

    @Test
    fun proveTaskLite14Inequality() {
        val result = checkFactsInMathML(
                brushedMathML = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>&lt;</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>=</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>-</mo><mn>1</mn><mo>)</mo><mo>=</mo><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(2;*(4;cos(*(2;x)));cos(*(4;x))))}{<=}{(*(8;^(cos(x);4)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>&lt;</mo></mrow><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>-</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>", result)
        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
    }

    @Test
    fun proveTaskLite14InequalityShort() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">&lt;</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>-</mo><mn>1</mn><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">=</mo><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(2;*(4;cos(*(2;x)));cos(*(4;x))))}{<=}{(*(8;^(cos(x);4)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>&lt;</mo></mrow><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>-</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>", result)
        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
    }

    @Test
    fun proveTaskLite14InequalityTooCompleteStep() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">&lt;</mo><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(2;*(4;cos(*(2;x)));cos(*(4;x))))}{<}{(*(8;^(cos(x);4)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#FF0000\"><mo>&lt;</mo></mrow><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation '(+(2;*(4;cos(*(2;x)));cos(*(4;x))))' </mtext><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">&lt;</mo><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\"> '(*(8;^(cos(x);4)))' </mtext></math>", result)
        assert(result.contains("Error"))
        assert(result.contains("#FF"))
    }

    @Test
    fun proveTaskLite14InequalityIncorrectMatchingWithCondition() {
        val result = checkFactsInMathML(
                brushedMathML = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>&lt;</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>=</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>-</mo><mn>1</mn><mo>)</mo><mo>=</mo><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(2;*(4;cos(*(2;x)));cos(*(4;x))))}{>}{(*(8;^(cos(x);4)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>&lt;</mo></mrow><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>-</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation between 'AND_NODE(transformation chains:((+(2;*(4;cos(*(2;x)));cos(*(4;x))))</mtext><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">&le;</mo><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">(+(2;*(4;+(*(2;^(cos(x);2));-(1)));+(*(2;^(cos(*(2;x));2));-(1))))</mtext><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">&le;</mo><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">(+(3;*(4;+(*(2;^(cos(x);2));-(1)));+(*(2;^(cos(*(2;x));2));-(1))))</mtext><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">&le;</mo><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">(+(3;*(4;+(*(2;^(cos(x);2));-(1)));+(*(2;^(+(*(2;^(cos(x);2));-(1));2));-(1))))</mtext><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">&le;</mo><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">(*(8;^(cos(x);4))));out:((+(2;*(4;cos(*(2;x)));cos(*(4;x))));</mtext><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">&le;</mo><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">;(*(8;^(cos(x);4)))))' and 'AND_NODE(in:((+(2;*(4;cos(*(2;x)));cos(*(4;x))));>;(*(8;^(cos(x);4)))))' </mtext></math>", result)
    }

    @Test
    fun proveTaskLite14InequalityIncorrect() {
        val result = checkFactsInMathML(
                brushedMathML = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>&gt;</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>=</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>-</mo><mn>1</mn><mo>)</mo><mo>=</mo><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(2;*(4;cos(*(2;x)));cos(*(4;x))))}{>}{(*(8;^(cos(x);4)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#FF0000\"><mo>&gt;</mo></mrow><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>=</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>-</mo><mn>1</mn><mo>)</mo><mo>=</mo><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation '(+(2;*(4;+(*(2;^(cos(x);2));-(1)));+(*(2;^(cos(*(2;x));2));-(1))))' </mtext><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">&ge;</mo><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\"> '(+(3;*(4;+(*(2;^(cos(x);2));-(1)));+(*(2;^(cos(*(2;x));2));-(1))))' </mtext></math>", result)
    }

    @Test
    fun proveTaskPiPeriodNormalizationTexStartEnsExpression() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Trigonometry").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "\\sin \\left(x+\\pi \\cdot 9\\right)=\\sin \\left(x+\\pi +\\pi \\cdot 8\\right)=\\sin \\left(x+\\pi \\right)=\\sin \\left(-x\\right)=-\\sin \\left(x\\right)",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(sin(+(x;*(pi;9))))",
                endExpressionIdentifier = "(+(-(sin(x))))",
                comparisonSign = "="
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\sin \\left(x+\\pi \\cdot 9\\right)\\textcolor{green}{=}\\sin \\left(x+\\pi +\\pi \\cdot 8\\right)\\textcolor{green}{=}\\sin \\left(x+\\pi \\right)\\textcolor{green}{=}\\sin \\left(-x\\right)\\textcolor{green}{=}-\\sin \\left(x\\right)}",
                result.validatedSolution)

        assert(log.probabilityTestComparisonCount < 100, { "probabilityTestComparisonCount = '${log.probabilityTestComparisonCount}" })
    }

    @Test
    fun proveTaskLite14TexStartEnsExpression() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Trigonometry").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "\\textcolor{purple}{3+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{green}{=}3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(2\\cdot x\\right)-1\\right)\\textcolor{green}{=}2+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2\\textcolor{red}{=}2\\left(1+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2\\right)=2\\left(1+\\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)\\right)^2=8\\cdot \\cos \\left(x\\right)^4}",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(+(3;*(4;cos(*(2;x)));cos(*(4;x))))",
                endExpressionIdentifier = "(*(8;^(cos(x);4)))",
                comparisonSign = "="
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{3+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{green}{=} 3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(2\\cdot x\\right)-1\\right)\\textcolor{green}{=} 2+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2\\textcolor{green}{=} 2\\left(1+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2\\right)\\textcolor{green}{=}2\\left(1+\\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)\\right)^2\\textcolor{green}{=}8\\cdot \\cos \\left(x\\right)^4}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite14TexStartEnsExpressionDetailSolution() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Trigonometry").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "\\textcolor{purple}{2+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{red}{\\le }3+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)=3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(2\\cdot x\\right)-1\\right)=3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2-1=2\\left(1+2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2=2\\left(2\\cdot \\cos ^2\\left(x\\right)\\right)^2=2\\cdot 4\\cdot \\cos ^4\\left(x\\right)=8\\cdot \\cos \\left(x\\right)^4}",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(+(2;*(4;cos(*(2;x)));cos(*(4;x))))",
                endExpressionIdentifier = "(*(8;^(cos(x);4)))",
                comparisonSign = "<="
        )

        val logRef = log.getLogInPlainText()

        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{2+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{green}{\\le}  3+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{green}{=}3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(2\\cdot x\\right)-1\\right)\\textcolor{green}{=}3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2-1\\textcolor{green}{=}2\\left(1+2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2\\textcolor{green}{=}2\\left(2\\cdot \\cos ^2\\left(x\\right)\\right)^2\\textcolor{green}{=}2\\cdot 4\\cdot \\cos ^4\\left(x\\right)\\textcolor{green}{=}8\\cdot \\cos \\left(x\\right)^4}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite14TexStartEnsExpressionInequality() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Trigonometry").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "2+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\le 3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(2\\cdot x\\right)-1\\right)=3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2-1=8\\cdot \\cos \\left(x\\right)^4",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(+(2;*(4;cos(*(2;x)));cos(*(4;x))))",
                endExpressionIdentifier = "(*(8;^(cos(x);4)))",
                comparisonSign = "<="
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{2+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{green}{\\le} 3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(2\\cdot x\\right)-1\\right)\\textcolor{green}{=}3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2-1\\textcolor{green}{=}8\\cdot \\cos \\left(x\\right)^4}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite14TexStartEnsExpressionInequality_le_without_space() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Trigonometry").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "2+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\le3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(2\\cdot x\\right)-1\\right)=3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2-1=8\\cdot \\cos \\left(x\\right)^4",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(+(2;*(4;cos(*(2;x)));cos(*(4;x))))",
                endExpressionIdentifier = "(*(8;^(cos(x);4)))",
                comparisonSign = "<="
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{2+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{green}{\\le}3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(2\\cdot x\\right)-1\\right)\\textcolor{green}{=}3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2-1\\textcolor{green}{=}8\\cdot \\cos \\left(x\\right)^4}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite14TexStartEnsExpressionInequalityWrongComparisonWithStart() {
        val compiledConfiguration = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(actualRulePackCodes = listOf("Trigonometry").toTypedArray())
        val result = checkSolutionInTexWithCompiledConfiguration(
                originalTexSolution = "4+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\le 3+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\le 8\\cdot \\cos \\left(x\\right)^4",
                compiledConfiguration = compiledConfiguration,
                startExpressionIdentifier = "(+(3;*(4;cos(*(2;x)));cos(*(4;x))))",
                endExpressionIdentifier = "(*(8;^(cos(x);4)))",
                comparisonSign = "<="
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation '(+(4;*(4;cos(*(2;x)));cos(*(4;x))))' <= '(+(3;*(4;cos(*(2;x)));cos(*(4;x))))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{4+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{red}{\\le} 3+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\le 8\\cdot \\cos \\left(x\\right)^4}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite14TexStartEnsExpressionInequalityWrongColored() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\textcolor{purple}{2+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{red}{\\le }3+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\le 8\\cdot \\cos \\left(x\\right)^4}",
                startExpressionIdentifier = "(+(2;*(4;cos(*(2;x)));cos(*(4;x))))",
                wellKnownFunctions = wellKnownFunctions,
                additionalFactsIdentifiers = "",
                endExpressionIdentifier = "(*(8;^(cos(x);4)))",
                comparisonSign = "<=",
                rulePacks = listOf("Trigonometry").toTypedArray()
        );

        val logRef = log.getLogInPlainText()

        assertEquals("Error: Unclear transformation '(+(3;*(4;cos(*(2;x)));cos(*(4;x))))' <= '(*(8;^(cos(x);4)))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{2+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{green}{\\le}  3+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{red}{\\le} 8\\cdot \\cos \\left(x\\right)^4}",
                result.validatedSolution)

        assert(log.probabilityTestComparisonCount < 13000, { "probabilityTestComparisonCount = '${log.probabilityTestComparisonCount}'" })
        assert(log.expressionDoubleComputationsCount + log.expressionComplexComputationsCount < 300000,
                { "expressionDoubleComputationsCount = '${log.expressionDoubleComputationsCount}'; expressionComplexComputationsCount = '${log.expressionComplexComputationsCount}'" })
    }

    @Test
    fun proveTaskLite14WrongManualSolution() {
        val result = checkFactsInMathML(
                brushedMathML = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn><mo>)</mo><mo>=</mo><mo>&#xA0;</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mo>&#xA0;</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn><mo>)</mo><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation between 'AND_NODE(facts chains:(((+(*(2;^(cos(*(2;x));2));-(1)));=;(+(*(2;^(cos(*(2;x));2));-(1)))))out:((+(*(2;^(cos(*(2;x));2));-(1)));=;(+(*(2;^(cos(*(2;x));2));-(1)))))' and 'AND_NODE(in:((+(3;*(4;cos(*(2;x)));cos(*(4;x))));=;(*(8;^(cos(x);4)))))' </mtext></math>",
                result)
    }

    @Test
    fun proveTaskLite14ShortCorrectManualSolution() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn><mo>)</mo><mo>=</mo><mo>&#xA0;</mo><mo>(</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mo>(</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mo>&#xA0;</mo><mo>(</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mo>(</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn><mo>)</mo></math>",
                result)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSignTexStartEndExpressionsIncorrectSkipUnderline() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\textcolor{purple}{3+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{red}{=}\\underline{3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\cos \\left(4\\cdot x\\right)}=3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(2\\cdot x\\right)-1\\right)=3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2-1=8\\cdot \\cos \\left(x\\right)^4}",
                rulePacks = listOf("Trigonometry").toTypedArray(),
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}"
        )

        val logRef = log.getLogInPlainText()

        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{3+4\\cdot \\cos \\left(2\\cdot x\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{green}{=} 3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\cos \\left(4\\cdot x\\right)\\textcolor{green}{=}3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+\\left(2\\cdot \\cos ^2\\left(2\\cdot x\\right)-1\\right)\\textcolor{green}{=}3+4\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)+2\\cdot \\left(2\\cdot \\cos ^2\\left(x\\right)-1\\right)^2-1\\textcolor{green}{=}8\\cdot \\cos \\left(x\\right)^4}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite14WrongCheatingSolution() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#FF0000\"><mo>=</mo></mrow><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation between '(+(3;*(4;cos(*(2;x)));cos(*(4;x))))' and '(*(8;^(cos(x);4)))' </mtext></math>",
                result)
    }

    @Test
    fun proveTaskLite14SpaceAliases() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>3</mn><mo>&#x2000;</mo><mo>+</mo><mo>&#x2008;</mo><mo>&#x2009;</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>3</mn><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mo>&#xA0;</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#FF0000\"><mo>=</mo></mrow><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation between '(+(3;*(4;cos(*(2;x)));cos(*(4;x))))' and '(*(8;^(cos(x);4)))' </mtext></math>",
                result)
    }

    @Test
    fun proveTaskLite17WithAliases() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>6</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>7</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">=</mo><mspace linebreak=\"newline\"/><mo>(</mo><mi>cos</mi><mo>(</mo><mi mathvariant=\"bold-italic\">x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#x2009;</mo><mi>cos</mi><mo>(</mo><mn>7</mn><mo>*</mo><mi mathvariant=\"bold-italic\">x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#x2009;</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi mathvariant=\"bold-italic\">x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>6</mn><mo>*</mo><mi mathvariant=\"bold-italic\">x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mo>=</mo><mo>&#xA0;</mo><mspace linebreak=\"newline\"/><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>3</mn><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mo>&#xA0;</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>=</mo><mo>&#xA0;</mo><mspace linebreak=\"newline\"/><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#x2009;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#x2009;</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>3</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mi>cos</mi><mo>&#x2009;</mo><mo>(</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mo>=</mo><mspace linebreak=\"newline\"/><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#x2009;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#x2009;</mo><mi>cos</mi><mo>(</mo><mn>5</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#x2009;</mo><mi>x</mi><mo>&#xA0;</mo><mo>/</mo><mo>&#xA0;</mo><mn>2</mn><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#x2009;</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>&#xA0;</mo><mo>/</mo><mo>&#xA0;</mo><mn>2</mn><mo>)</mo><mo>&#xA0;</mo><mo>=</mo><mo>&#xA0;</mo><mspace linebreak=\"newline\"/><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi>x</mi><mn>2</mn></mfrac><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mrow><mn>5</mn><mo>*</mo><mi>x</mi></mrow><mn>2</mn></mfrac><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(cos(x);cos(*(2;x));cos(*(6;x));cos(*(7;x))))}{=}{(*(4;cos(/(x;2));cos(/(*(5;x);2));cos(*(4;x))))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>6</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>7</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mo>(</mo><mi>cos</mi><mo>(</mo><mi mathvariant=\"bold-italic\">x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>7</mn><mo>*</mo><mi mathvariant=\"bold-italic\">x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi mathvariant=\"bold-italic\">x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>6</mn><mo>*</mo><mi mathvariant=\"bold-italic\">x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mo>&#xA0;</mo><mspace linebreak=\"newline\"/><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>3</mn><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mo>&#xA0;</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mo>&#xA0;</mo><mspace linebreak=\"newline\"/><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>3</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mi>cos</mi><mo>&#xA0;</mo><mo>(</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>5</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>&#xA0;</mo><mo>/</mo><mo>&#xA0;</mo><mn>2</mn><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>&#xA0;</mo><mo>/</mo><mo>&#xA0;</mo><mn>2</mn><mo>)</mo><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mo>&#xA0;</mo><mspace linebreak=\"newline\"/><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi>x</mi><mn>2</mn></mfrac><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mrow><mn>5</mn><mo>*</mo><mi>x</mi></mrow><mn>2</mn></mfrac><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></math>", result)
    }

    @Test
    fun proveTaskLite17ParserProblemAvduchenko() {
        val result = checkFactsInMathML(
                brushedMathML = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>6</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>7</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>=</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><menclose mathcolor=\"#7F0000\" notation=\"bottom\"><mpadded lspace=\"+1px\"><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo></mpadded><mo>*</mo><mpadded lspace=\"-4px\"><mo>&#xA0;</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>3</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo></mpadded><mo>+</mo><mo>&#xA0;</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mpadded lspace=\"-4px\"><mo>&#xA0;</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mpadded></menclose><mo>=</mo><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi>x</mi><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mfrac><mrow><mn>5</mn><mo>*</mo><mi>x</mi></mrow><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Syntax&#xA0;error&#xA0;(underlined): Unknown function tag: 'mpadded' with '2' arguments</mtext></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(cos(x);cos(*(2;x));cos(*(6;x));cos(*(7;x))))}{=}{(*(4;cos(/(x;2));cos(/(*(5;x);2));cos(*(4;x))))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>6</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>7</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#FF0000\"><mo>=</mo></mrow><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mrow><mrow><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo></mrow><mo>*</mo><mrow><mo>&#xA0;</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>3</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo></mrow><mo>+</mo><mo>&#xA0;</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mrow><mo>&#xA0;</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mrow><mo>=</mo><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi>x</mi><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mfrac><mrow><mn>5</mn><mo>*</mo><mi>x</mi></mrow><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation between '(+(cos(x);cos(*(2;x));cos(*(6;x));cos(*(7;x))))' and '(+(*(2;cos(*(4;x));cos(*(3;x)));*(2;cos(*(4;x));cos(*(2;x)))))' </mtext></math>", result)
    }

    @Test
    fun proveTaskLite16OriginalTemplate() {
        val result = checkFactsInMathML(
                brushedMathML = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mstyle displaystyle=\"true\"><mo>(</mo></mstyle><mstyle displaystyle=\"true\"><mi>x</mi></mstyle><mstyle displaystyle=\"true\"><mo>)</mo></mstyle></mrow></mfrac><mo>&#xA0;</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo><mo>=</mo><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{=}{(/(2;sin(x)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assert(result.contains("rror"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mrow><mo>(</mo></mrow><mrow><mi>x</mi></mrow><mrow><mo>)</mo></mrow></mrow></mfrac><mo>&#xA0;</mo><mo>=</mo><menclose mathcolor=\"#7F0000\" notation=\"bottom\"><mo>.</mo><mo>.</mo><mo>.</mo></menclose><mo>=</mo><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Syntax&#xA0;error&#xA0;(underlined): Unexpected: '.'</mtext></math>",
                result)
    }

    @Test
    fun proveTaskLite16WithComment() {
        val result = checkFactsInMathML(
                brushedMathML = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>/</mo><mo>*</mo><mi>c</mi><mi>o</mi><mi>m</mi><mi>m</mi><mi>e</mi><mi>n</mi><mi>t</mi><mo>*</mo><mo>/</mo><mspace linebreak=\"newline\"/><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow><mo>^</mo><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>os</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{=}{(/(2;sin(x)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assert(!result.contains("rror"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>/</mo><mo>*</mo><mi>c</mi><mi>o</mi><mi>m</mi><mi>m</mi><mi>e</mi><mi>n</mi><mi>t</mi><mo>*</mo><mo>/</mo><mspace linebreak=\"newline\"/><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow><mo>^</mo><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>os</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                result)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSign() {
        val result = checkFactsInMathML(
                brushedMathML = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>/</mo><mo>*</mo><mi>c</mi><mi>o</mi><mi>m</mi><mo>=</mo><mi>m</mi><mi>e</mi><mi>n</mi><mi>t</mi><mo>*</mo><mo>/</mo><mspace linebreak=\"newline\"/><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow><mo>^</mo><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>os</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{=}{(/(2;sin(x)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assert(!result.contains("rror"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>/</mo><mo>*</mo><mi>c</mi><mi>o</mi><mi>m</mi><mo>=</mo><mi>m</mi><mi>e</mi><mi>n</mi><mi>t</mi><mo>*</mo><mo>/</mo><mspace linebreak=\"newline\"/><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow><mo>^</mo><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>os</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                result)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSignTex() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                wellKnownFunctionsString = wellKnownFunctionsString,
                expressionTransformationRulesString = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{=}{(/(2;sin(x)))}",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assert(result.errorMessage.isEmpty())
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16InITRTex() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                        goalExpressionStructureString = "(/(2;sin(x)))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "AdvancedTrigonometry")),
                        difficulty = 2.9
                ),
                rulePacksITR = defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assert(result.errorMessage.isEmpty())
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16InITRTexAsSimplification() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{1 * 2}{\\sin \\left(x\\right)}",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                        goalType = "simplification",
                        goalPattern = "and",
                        otherGoalData = mapOf(
                                "hiddenGoalExpressions" to listOf("(/(*(2;1);sin(x)))")
                        ),
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "AdvancedTrigonometry")),
                        difficulty = 2.9
                ),
                rulePacksITR = defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{1 * 2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    @Ignore
    fun proveTaskWithSinDivCosEqTangent() {
        val result = checkSolutionInTexITR(
            originalTexSolution = "\\textcolor{purple}{\\frac{3\\cdot \\sin \\left(x\\right)-\\cos \\left(x\\right)}{\\sin \\left(x\\right)+2\\cdot \\cos \\left(x\\right)}\\textcolor{green}{=}\\frac{\\cos \\left(x\\right)\\cdot \\left(3\\cdot \\frac{\\sin \\left(x\\right)}{\\cos \\left(x\\right)}-1\\right)}{\\cos \\left(x\\right)\\cdot \\left(\\frac{\\sin \\left(x\\right)}{\\cos \\left(x\\right)}+2\\right)}\\textcolor{green}{=}\\frac{3\\cdot \\frac{\\sin \\left(x\\right)}{\\cos \\left(x\\right)}-1}{\\frac{\\sin \\left(x\\right)}{\\cos \\left(x\\right)}+2}\\textcolor{red}{=}\\frac{3\\cdot \\text{tg}\\left(x\\right)-1}{\\frac{\\sin \\left(x\\right)}{\\cos \\left(x\\right)}+2}=\\frac{3\\cdot \\text{tg}\\left(x\\right)-1}{\\text{tg}\\left(x\\right)\\ +\\ 2}=2}",
            taskITR = TaskITR(
                originalExpressionStructureString = "(/(+(*(3;sin(x));-(cos(x)));+(sin(x);*(2;cos(x)))))",
                goalType = "computation",
                goalPattern = "+:0-1(-:1):?:?R",
                rulePacks = listOf(RulePackLinkITR(rulePackCode = "AdvancedTrigonometry")),
                difficulty = 2.6
            ),
            rulePacksITR = arrayOf(
                RulePackITR(
                    rulePacks = listOf(RulePackLinkITR(rulePackCode = "AdvancedTrigonometry"),RulePackLinkITR(rulePackCode = "ArithmeticDivision")),
                    rules = listOf(RuleITR(code = "(tg(x))__to__(5)"))
                )
            )
        )


        assertEquals(
            "",
            result.errorMessage
        )
    }

    @Test
    fun proveTaskLite16InITRTexAsSimplificationWrong() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                        goalType = "simplification",
                        otherGoalData = mapOf(
                                "hiddenGoalExpressions" to listOf("(/(*(2;2);sin(x)))", "(/(3;sin(x)))")
                        ),
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "AdvancedTrigonometry")),
                        difficulty = 2.9
                ),
                rulePacksITR = defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: answer does not match given condition",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16InITRTexAllNullWeightOperationOnlyPlus() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin \\left(x\\right)\\cdot \\sin \\left(x\\right)+1+\\cos \\left(x\\right)\\cdot \\cos \\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{red}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}}",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                        goalExpressionStructureString = "(/(2;sin(x)))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "AdvancedTrigonometry")),
                        difficulty = 2.9,
                        otherCheckSolutionData = mapOf(
                                notChangesOnVariablesInComparisonFunctionJsonName to listOf(FunctionIdentifierITR("+", "-1")),
                                notChangesOnVariablesInComparisonFunctionWithoutTransformationsJsonName to listOf(FunctionIdentifierITR("+", "-1"))
                        )
                ),
                rulePacksITR = defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))' and '(/(+(*(sin(x);sin(x));1;*(cos(x);cos(x));*(2;cos(x)));*(+(1;cos(x));sin(x))))' ",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{red}{=}\\frac{\\sin \\left(x\\right)\\cdot \\sin \\left(x\\right)+1+\\cos \\left(x\\right)\\cdot \\cos \\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16InITRTexAllNullWeightOperationPlusProduct() {
        val nullWeightOperations = listOf(
                FunctionIdentifierITR("+", "-1"),
                FunctionIdentifierITR("-", "-1"),
                FunctionIdentifierITR("*", "-1"),
                FunctionIdentifierITR("/", "-1")
        )
        val result = checkSolutionInTexITR(
                originalTexSolution = "\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{red}{=}\\frac{\\sin \\left(x\\right)\\cdot \\sin \\left(x\\right)+1+\\cos \\left(x\\right)\\cdot \\cos \\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}}",
                taskITR = TaskITR(
                        originalExpressionStructureString = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                        goalExpressionStructureString = "(/(2;sin(x)))",
                        rulePacks = listOf(RulePackLinkITR(rulePackCode = "AdvancedTrigonometry")),
                        difficulty = 2.9,
                        otherCheckSolutionData = mapOf(
                                notChangesOnVariablesInComparisonFunctionJsonName to nullWeightOperations,
                                notChangesOnVariablesInComparisonFunctionWithoutTransformationsJsonName to nullWeightOperations
                        )
                ),
                rulePacksITR = defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(/(+(*(sin(x);sin(x));1;*(cos(x);cos(x));*(2;cos(x)));*(+(1;cos(x));sin(x))))' and '(/(+(2;*(2;cos(x)));*(+(1;cos(x));sin(x))))' ",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin \\left(x\\right)\\cdot \\sin \\left(x\\right)+1+\\cos \\left(x\\right)\\cdot \\cos \\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{red}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }



    @Test
    fun proveTaskTanCotanComputation4() {
        val result = checkSolutionInTexITR(
                originalTexSolution = "tg\\left(\\frac{pi}{4}\\right)=tg\\left(\\frac{pi\\cdot 2}{4\\cdot 2+x-x}\\right)",
                taskITR = TrigonometryStepByStep.basicTrigonometryFormulaComputationTask(
                        originalExpressionStructureString = "(tg(/(pi;4)))",
                        goalPattern = "!cos,!sin,!tg,!ctg,!- : ? : ?"
                ),
                rulePacksITR = defaultRulePacks.toTypedArray()
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: answer does not match given condition",
                result.errorMessage)
        assertEquals("\\textcolor{purple}{tg\\left(\\frac{pi}{4}\\right)\\textcolor{green}{=}tg\\left(\\frac{pi\\cdot 2}{4\\cdot 2+x-x}\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSignTexStartEndExpressions() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                wellKnownFunctionsString = wellKnownFunctionsString,
                expressionTransformationRulesString = expressionTransformationRulesString,
                startExpressionIdentifier = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                endExpressionIdentifier = "(/(2;sin(x)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assert(result.errorMessage.isEmpty())
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSignTexStartEndExpressionsRulesByRulePack() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                rulePacks = listOf("Trigonometry").toTypedArray(),
                startExpressionIdentifier = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                endExpressionIdentifier = "(/(2;sin(x)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTestingTaskBugByRulePack() {
        val result = checkSolutionInTex(
                originalTexSolution = "tg\\left(x\\right)\\cdot\\operatorname{ctg}\\left(x\\right)+2...3",
                rulePacks = listOf("Trigonometry").toTypedArray(),
                startExpressionIdentifier = "(+(*(tg(x);ctg(x));2))",
                endExpressionIdentifier = "(3)",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: No transformations found", result.errorMessage)
        assertEquals("tg\\left(x\\right)\\cdot\\operatorname{ctg}\\left(x\\right)+2...3",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSignTexStartEndExpressionsNotStrings() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                wellKnownFunctions = wellKnownFunctions,
                expressionTransformationRules = expressionTransformationRulesSample,
                startExpressionIdentifier = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                endExpressionIdentifier = "(/(2;sin(x)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSignTexStartEndExpressionsNotStringsWrongComparisonSignInTaskSolution() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                wellKnownFunctions = wellKnownFunctions,
                expressionTransformationRules = expressionTransformationRulesSample,
                startExpressionIdentifier = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                endExpressionIdentifier = "(/(2;sin(x)))",
                comparisonSign = "<",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation '(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))' < '(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSignTexStartEndExpressionsNotStringsPossibleComparisonSignInTaskSolution() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                wellKnownFunctions = wellKnownFunctions,
                expressionTransformationRules = expressionTransformationRulesSample,
                startExpressionIdentifier = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                endExpressionIdentifier = "(/(2;sin(x)))",
                comparisonSign = ">=",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSignTexStartEndExpressionsNotStringsIncorrect() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                wellKnownFunctions = wellKnownFunctions,
                expressionTransformationRules = listOf(),
                startExpressionIdentifier = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                endExpressionIdentifier = "(/(2;sin(x)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(/(+(^(sin(x);2);1;^(cos(x);2);*(2;cos(x)));*(+(1;cos(x));sin(x))))' and '(/(+(2;*(2;cos(x)));*(+(1;cos(x));sin(x))))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{red}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSignTexStartEndExpressionsNotStringsIncorrectRulesByRulePack() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                rulePacks = listOf("Logarithm").toTypedArray(),
                wellKnownFunctions = wellKnownFunctions,
                startExpressionIdentifier = "(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))",
                endExpressionIdentifier = "(/(2;sin(x)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(/(+(^(sin(x);2);1;^(cos(x);2);*(2;cos(x)));*(+(1;cos(x));sin(x))))' and '(/(+(2;*(2;cos(x)));*(+(1;cos(x));sin(x))))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}\\textcolor{green}{=}\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}\\textcolor{red}{=}\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16WithCommentWithEqualSignTexStartEndExpressionsIncorrect() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}",
                wellKnownFunctionsString = wellKnownFunctionsString,
                expressionTransformationRulesString = expressionTransformationRulesString,
                startExpressionIdentifier = "(+(/(sin(x);+(2;cos(x)));/(+(1;cos(x));sin(x))))",
                endExpressionIdentifier = "(/(2;sin(x)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()

        assertEquals("Error: Unclear transformation between '(+(/(sin(x);+(2;cos(x)));/(+(1;cos(x));sin(x))))' and '(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\sin \\left(x\\right)}{1+\\cos \\left(x\\right)}+\\frac{1+\\cos \\left(x\\right)}{\\sin \\left(x\\right)}=\\frac{\\sin ^2\\left(x\\right)+1+\\cos ^2\\left(x\\right)+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2+2\\cdot \\cos \\left(x\\right)}{\\left(1+\\cos \\left(x\\right)\\right)\\sin \\left(x\\right)}=\\frac{2}{\\sin \\left(x\\right)}}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskLite16CheatingCorrectIfDegreeAllowedWithRules() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{=}{(/(2;sin(x)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assert(!result.contains("rror"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                result)
    }

    @Test
    fun proveTaskLite16CheatingUnCorrectIfDegreeNotAllowedWithRules() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{=}{(/(2;sin(x)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "",
                unlimitedWellKnownFunctions = wellKnownFunctionsWithoutDegreeString
        )

        val logRef = log.getLogInPlainText()


        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#FF0000\"><mo>=</mo></mrow><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation between '(/(+(^(sin(x);2);^(+(1;cos(x));2));*(+(1;cos(x));sin(x))))' and '(/(2;sin(x)))' </mtext></math>",
                result)
    }

    @Test
    fun proveTaskLite16TooShortSolutionIfDegreeNotAllowedWithRules() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow><mo>^</mo><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = "cos(x)^2+sin(x)^2${configSeparator}1",
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{=}{(/(2;sin(x)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "",
                unlimitedWellKnownFunctions = wellKnownFunctionsWithoutDegreeString
        )

        val logRef = log.getLogInPlainText()


        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow><mo>^</mo><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#FF0000\"><mo>=</mo></mrow><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation between '(/(+(^(sin(x);2);1;^(cos(x);2);*(2;cos(x)));*(+(1;cos(x));sin(x))))' and '(/(2;sin(x)))' </mtext></math>",
                result)
    }

    @Test
    fun proveTaskLite13NumericWithConditionChangedAdditions() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>4</mn><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mn>1</mn></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">=</mo><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mfrac><mn>1</mn><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>=</mo><mn>2</mn><mo>*</mo><mfrac><mn>1</mn><mrow><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mo>-</mo><mn>1</mn><mo>=</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mn>4</mn><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>/</mo><mn>4</mn><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>/</mo><mn>4</mn><mo>=</mo><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mn>4</mn></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><msup><mn>4</mn><mn>2</mn></msup></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>/</mo><mn>4</mn><mo>=</mo><mn>1</mn><mo>/</mo><mn>4</mn></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(sin(*(4;x));*(cos(*(4;x));ctg(*(2;x)))))}{=}{(0.25)}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "EXPRESSION_COMPARISON{(tg(*(2;x)))}{=}{(4)}",
                unlimitedWellKnownFunctions = wellKnownFunctionsWithoutDegreeString
        )

        val logRef = log.getLogInPlainText()


        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>[</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>4</mn><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mn>1</mn></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mfrac><mn>1</mn><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mfrac><mn>1</mn><mrow><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mo>-</mo><mn>1</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mn>4</mn></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><msup><mn>4</mn><mn>2</mn></msup></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>1</mn><mo>/</mo><mn>4</mn></math>",
                result)
    }

    @Test
    fun proveTaskLite13WithComment() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>4</mn><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mn>1</mn></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mfrac><mn>1</mn><mrow><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><mfrac><mn>1</mn><mrow><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mo>-</mo><mn>1</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>/</mo><mo>*</mo><mi>c</mi><mi>o</mi><mi>m</mi><mi>m</mi><mi>e</mi><mi>n</mi><mi>t</mi><mo>*</mo><mo>/</mo><mspace linebreak=\"newline\"/><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">=</mo><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mn>4</mn><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>/</mo><mn>4</mn><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>/</mo><mn>4</mn><mo>=</mo><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mn>4</mn></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><msup><mn>4</mn><mn>2</mn></msup></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>/</mo><mn>4</mn><mo>=</mo><mn>1</mn><mo>/</mo><mn>4</mn><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation or incomplete solution. Try to fix errors or to write more details.</mtext></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(sin(*(4;x));*(cos(*(4;x));ctg(*(2;x)))))}{=}{(0.25)}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "EXPRESSION_COMPARISON{(tg(*(2;x)))}{=}{(4)}",
                unlimitedWellKnownFunctions = wellKnownFunctionsWithoutDegreeString
        )

        val logRef = log.getLogInPlainText()


        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>[</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>4</mn><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mn>1</mn></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mfrac><mn>1</mn><mrow><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mfrac><mn>1</mn><mrow><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mo>-</mo><mn>1</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>/</mo><mo>*</mo><mi>c</mi><mi>o</mi><mi>m</mi><mi>m</mi><mi>e</mi><mi>n</mi><mi>t</mi><mo>*</mo><mo>/</mo><mspace linebreak=\"newline\"/><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mn>4</mn></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><msup><mn>4</mn><mn>2</mn></msup></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>1</mn><mo>/</mo><mn>4</mn></math>",
                result)
    }

    @Test
    fun proveTaskLite13NumericWithCondition() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>4</mn><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mn>1</mn></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mfrac><mn>1</mn><mrow><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>2</mn><mo>*</mo><mfrac><mn>1</mn><mrow><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mo>-</mo><mn>1</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mn>4</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>/</mo><mn>4</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>/</mo><mn>4</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mn>4</mn></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><msup><mn>4</mn><mn>2</mn></msup></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>/</mo><mn>4</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>1</mn><mo>/</mo><mn>4</mn></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(sin(*(4;x));*(cos(*(4;x));ctg(*(2;x)))))}{=}{(0.25)}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "EXPRESSION_COMPARISON{(tg(*(2;x)))}{=}{(4)}",
                unlimitedWellKnownFunctions = wellKnownFunctionsWithoutDegreeString
        )

        val logRef = log.getLogInPlainText()


        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>[</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>4</mn><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mn>1</mn></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mfrac><mn>1</mn><mrow><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mfrac><mn>1</mn><mrow><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mo>-</mo><mn>1</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mn>4</mn></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><msup><mn>4</mn><mn>2</mn></msup></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>1</mn><mo>/</mo><mn>4</mn></math>",
                result)
    }

    @Test
    fun proveTaskLite13UseReadyRules() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>4</mn><mo>]</mo><mspace linebreak=\"newline\"/><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mn>4</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>/</mo><mn>4</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>/</mo><mn>4</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mn>4</mn></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><msup><mn>4</mn><mn>2</mn></msup></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>/</mo><mn>4</mn><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mn>1</mn><mo>/</mo><mn>4</mn></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(sin(*(4;x));*(cos(*(4;x));ctg(*(2;x)))))}{=}{(0.25)}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "EXPRESSION_COMPARISON{(tg(*(2;x)))}{=}{(4)}",
                unlimitedWellKnownFunctions = wellKnownFunctionsWithoutDegreeString
        )

        val logRef = log.getLogInPlainText()


        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>[</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>4</mn><mo>]</mo><mspace linebreak=\"newline\"/><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mn>4</mn></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><msup><mn>4</mn><mn>2</mn></msup></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>/</mo><mn>4</mn><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>1</mn><mo>/</mo><mn>4</mn></math>",
                result)
    }

    @Test
    fun proveTaskLite16CheatingCorrectIfDegreeNotAllowedWithRules() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mo>&#xA0;</mo><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn><mo>+</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mo>&#xA0;</mo><mfrac><mrow><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{=}{(/(2;sin(x)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "",
                unlimitedWellKnownFunctions = wellKnownFunctionsWithoutDegreeString
        )

        val logRef = log.getLogInPlainText()


        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mo>&#xA0;</mo><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn><mo>+</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mo>&#xA0;</mo><mfrac><mrow><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
                result)
    }

    @Test
    fun proveTaskMiddle1Correct() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>t</mi><mi>g</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mfrac><mn>1</mn><mrow><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mn>2</mn><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn></mrow><mrow><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn></mrow><mrow><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn></mrow><mrow><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mspace linebreak=\"newline\"/><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo mathvariant=\"bold\" mathcolor=\"#007F00\">=</mo><mfrac><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(tg(*(4;x));-(/(1;cos(*(4;x))))))}{=}{(/(+(sin(*(2;x));-(cos(*(2;x))));+(sin(*(2;x));cos(*(2;x)))))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "",
                unlimitedWellKnownFunctions = wellKnownFunctionsWithoutDegreeString
        )

        val logRef = log.getLogInPlainText()


        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mi>t</mi><mi>g</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mfrac><mn>1</mn><mrow><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mi>sin</mi><mo>(</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mn>2</mn><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn></mrow><mrow><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn></mrow><mrow><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn></mrow><mrow><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac></math>",
                result)
    }

    @Test
    fun proveTaskComlicated1Correct() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>=</mo><mo>&gt;</mo><mo>(</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mo>)</mo><mo>-</mo><mi>z</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi><mo>=</mo><mo>&gt;</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi><mo>=</mo><mo>&gt;</mo><mfrac><mrow><mi mathvariant=\"normal\">x</mi><mo>+</mo><mi mathvariant=\"normal\">y</mi></mrow><mn>2</mn></mfrac><mo>=</mo><mfrac><mrow><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi></mrow><mn>2</mn></mfrac><mo>=</mo><mo>&gt;</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>=</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>=</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>=</mo><mo>&gt;</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>=</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>=</mo><mo>&gt;</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>=</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>]</mo><mspace linebreak=\"newline\"/><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>ctg</mi><mfenced><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac></mfenced><mo>+</mo><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>=</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>=</mo><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>=</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mo>=</mo><mspace linebreak=\"newline\"/><mo>=</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mo>=</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mo>=</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mo>=</mo><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>ctg</mi><mfenced><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac></mfenced><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(+(ctg(/(x;2));ctg(/(y;2));ctg(/(z;2))))}{=}{(*(ctg(/(x;2));ctg(/(y;2));ctg(/(z;2))))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "EXPRESSION_COMPARISON{(+(x;y;z))}{=}{(pi)}",
                unlimitedWellKnownFunctions = wellKnownFunctionsWithoutDegreeString
        )

        val logRef = log.getLogInPlainText()

        val expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>[</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi mathvariant=\"normal\">&#x3C0;</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi mathvariant=\"normal\">&#x3C0;</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mo>(</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mo>)</mo><mo>-</mo><mi>z</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mi>x</mi><mo>+</mo><mi>y</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mfrac><mrow><mi mathvariant=\"normal\">x</mi><mo>+</mo><mi mathvariant=\"normal\">y</mi></mrow><mn>2</mn></mfrac><mo>=</mo><mfrac><mrow><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi></mrow><mn>2</mn></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>=</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>=</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>=</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>]</mo><mspace linebreak=\"newline\"/><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>ctg</mi><mfenced><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac></mfenced><mo>+</mo><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mo>=</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>ctg</mi><mfenced><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac></mfenced><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></math>"
        assert(!result.contains("Error"), {
            println("Expected : $expected")
            println("Actual   : $result")
        })
        assert(!result.contains("#FF"))
        assertEquals(expected, result)
    }

    @Test
    fun proveTaskMiddle1ExpressionNotEndedError() {
        val result = checkFactsInMathML(
                brushedMathML = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>6</mn></mfrac><mo>-</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>3</mn></mfrac><mo>-</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>2</mn><mrow><mo>*</mo><mo>(</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>6</mn></mfrac><mo>)</mo><mo>)</mo><mo>=</mo><mn>2</mn><mo>*</mo><mo>(</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>0</mn><mo>.</mo><mn>5</mn><mo>)</mo></mrow><mo>=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mn>3</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Syntax&#xA0;error&#xA0;(underlined): data not ended</mtext></math>",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(*(4;cos(+(/(pi;6);-(x)));sin(+(/(pi;3);-(x)))))}{=}{(/(sin(*(3;x));sin(x)))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "",
                unlimitedWellKnownFunctions = wellKnownFunctionsWithoutDegreeString
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>6</mn></mfrac><mo>-</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>3</mn></mfrac><mo>-</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mrow><mo>*</mo><mo>(</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>6</mn></mfrac><mo>)</mo><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>*</mo><mo>(</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>0</mn><mo>.</mo><mn>5</mn><mo>)</mo></mrow><mrow mathvariant=\"bold\" mathcolor=\"#FF0000\"><mo>=</mo></mrow><mfrac><mrow><mi>sin</mi><mo>(</mo><mn>3</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation between '(*(2;+(sin(+(/($PI_STRING_USUAL;2);-(*(2;x))));0.5)))' and '(/(sin(*(3;x));sin(x)))' </mtext></math>",
                result)

        assert(log.consideredExpressionTransformationsCount < 1000, { "consideredExpressionTransformationsCount = '${log.consideredExpressionTransformationsCount}" })
        assert(log.probabilityTestComparisonCount < 1000, { "probabilityTestComparisonCount = '${log.probabilityTestComparisonCount}" })
    }

    @Test
    fun demoOneStepSolutionIsUnclear() {
        val result = checkSolutionInTex(
                originalTexSolution =  "\\frac{\\operatorname{ctg}\\left(x\\right)\\cdot\\left(\\operatorname{ctg}\\left(x\\right)-tg\\left(x+\\pi\\right)\\right)}{\\operatorname{ctg}\\left(x\\right)^2-\\left(\\frac{1}{\\sin\\left(x\\right)^2}-\\operatorname{ctg}\\left(x\\right)^2\\right)}=1",
                startExpressionIdentifier = "(/(*(ctg(x);+(ctg(x);-(tg(+(x;π)))));+(^(ctg(x);2);-(+(/(1;^(sin(x);2));-(^(ctg(x);2)))))))",
                endExpressionIdentifier = "(1)",
                comparisonSign = "=",
                wellKnownFunctionsString = ";;;0;;;;;;1;;;+;;;-1;;;-;;;-1;;;*;;;-1;;;/;;;-1;;;^;;;-1",
                unlimitedWellKnownFunctionsString = ";;;0;;;;;;1;;;+;;;-1;;;-;;;-1;;;*;;;-1;;;/;;;-1;;;^;;;-1",
                rulePacks = arrayOf("Trigonometry")
        )

        val logRef = log.getLogInPlainText()

        assertEquals("Error: Unclear transformation between '(/(*(ctg(x);+(ctg(x);-(tg(+(x;π)))));+(^(ctg(x);2);-(+(/(1;^(sin(x);2));-(^(ctg(x);2)))))))' and '(1)' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\frac{\\operatorname{ctg}\\left(x\\right)\\cdot\\left(\\operatorname{ctg}\\left(x\\right)-tg\\left(x+\\pi\\right)\\right)}{\\operatorname{ctg}\\left(x\\right)^2-\\left(\\frac{1}{\\sin\\left(x\\right)^2}-\\operatorname{ctg}\\left(x\\right)^2\\right)}\\textcolor{red}{=}1}",
                result.validatedSolution)

        assert(log.consideredExpressionTransformationsCount < 1300, { "consideredExpressionTransformationsCount = '${log.consideredExpressionTransformationsCount}'" })
        assert(log.probabilityTestComparisonCount < 22000, { "probabilityTestComparisonCount = '${log.probabilityTestComparisonCount}'" })

    }

    @Test
    @Ignore //should be no transformation
    fun incorrectApplicationOfRuleAddEqualExpressionsToBothSides(){
        val compiledConfiguration = compiledConfigurationBySettings(wellKnownFunctionsString, expressionTransformationRulesString, "", wellKnownFunctionsString)
        val factComparator = compiledConfiguration.factComparator

        val factConstructorViewer = FactConstructorViewer()
        val left = factConstructorViewer.constructFactByIdentifier("MAIN_LINE_AND_NODE{RULES{}FACT_TRANSFORMATION_CHAINS{}EXPRESSION_TRANSFORMATION_CHAINS{}IN_FACTS{{MAIN_LINE_AND_NODE{RULES{}FACT_TRANSFORMATION_CHAINS{}EXPRESSION_TRANSFORMATION_CHAINS{}IN_FACTS{{EXPRESSION_COMPARISON{*(2;^(cos(*(2;x));2))}{=}{*(2;^(+(*(2;^(cos(x);2));-(1));2))}}}OUT_FACTS{{EXPRESSION_COMPARISON{*(2;^(cos(*(2;x));2))}{=}{*(2;^(+(*(2;^(cos(x);2));-(1));2))}}}}}}OUT_FACTS{{MAIN_LINE_AND_NODE{RULES{}FACT_TRANSFORMATION_CHAINS{}EXPRESSION_TRANSFORMATION_CHAINS{}IN_FACTS{{EXPRESSION_COMPARISON{*(2;^(cos(*(2;x));2))}{=}{*(2;^(+(*(2;^(cos(x);2));-(1));2))}}}OUT_FACTS{{EXPRESSION_COMPARISON{*(2;^(cos(*(2;x));2))}{=}{*(2;^(+(*(2;^(cos(x);2));-(1));2))}}}}}}}")
        val right = factConstructorViewer.constructFactByIdentifier("MAIN_LINE_AND_NODE{RULES{}FACT_TRANSFORMATION_CHAINS{}EXPRESSION_TRANSFORMATION_CHAINS{}IN_FACTS{{MAIN_LINE_AND_NODE{RULES{}FACT_TRANSFORMATION_CHAINS{}EXPRESSION_TRANSFORMATION_CHAINS{}IN_FACTS{{EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}}}OUT_FACTS{{EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}}}}}}OUT_FACTS{{MAIN_LINE_AND_NODE{RULES{}FACT_TRANSFORMATION_CHAINS{}EXPRESSION_TRANSFORMATION_CHAINS{}IN_FACTS{{EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}}}OUT_FACTS{{EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}}}}}}}")
        val result = factComparator.compareWithTreeTransformationRules(left, right, listOf(), compiledConfiguration.compiledFactTreeTransformationRules,
                additionalFactUsed = mutableListOf())
        assert(result,
                { print(log.getLogInPlainText()) })
    }

    @Test
    @Ignore
    fun oldIdentifierToNew() {
        val factConstructorViewer: FactConstructorViewer = FactConstructorViewer()
        val fact = parseFromFactIdentifier("x+y+z;ec;=;ec;pi")!!
        val newIdentifier = factConstructorViewer.constructIdentifierByFact(fact)
        print(newIdentifier)
    }


    @Test
    @Ignore
    fun testReal() {
        val result = checkFactsInMathML(
                "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>6</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>7</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo><mo>=</mo><mo>&#xA0;</mo><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi>x</mi><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mfrac><mrow><mn>5</mn><mo>*</mo><mi>x</mi></mrow><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></math>",
                ";;;0;;;;;;1;;;+;;;-1;;;-;;;-1;;;*;;;-1;;;/;;;-1;;;^;;;-1",
                ";;;0;;;;;;1;;;+;;;-1;;;-;;;-1;;;*;;;-1;;;/;;;-1",
                "sin(x)^2;;;1 - cos(x)^2;;;cos(x)^2;;;1 - sin(x)^2;;;cos(x)^2+sin(x)^2;;;1;;;tg(x);;;sin(x)/cos(x);;;ctg(x);;;cos(x)/sin(x);;;ctg(x)tg(x);;;1;;;tg(x)^2+1;;;1/cos(x)^2;;;ctg(x)^2+1;;;1/sin(x)^2;;;sin(x+y);;;sin(x)cos(y)+sin(y)cos(x);;;sin(x-y);;;sin(x)cos(y)-sin(y)cos(x);;;cos(x+y);;;cos(x)cos(y)-sin(x)sin(y);;;cos(x-y);;;cos(x)cos(y)+sin(x)sin(y);;;tg(x+y);;;(tg(x)+tg(y))/(1-tg(x)tg(y));;;tg(x-y);;;(tg(x)-tg(y))/(1+tg(x)tg(y));;;ctg(x+y);;;(ctg(x)ctg(y)-1)/(ctg(x)+ctg(y));;;ctg(x-y);;;(ctg(x)ctg(y)+1)/(ctg(x)-ctg(y));;;sin(2*x);;;2*sin(x)cos(x);;;cos(2*x);;;cos(x)^2 - sin(x)^2;;;cos(2*x);;;2*cos(x)^2 - 1;;;cos(2*x);;;1 - 2*sin(x)^2;;;sin(x)^2;;;(1 - cos(2*x))/2;;;cos(x)^2;;;(1 + cos(2*x))/2;;;sin(x)+sin(y);;;2*sin((x+y)/2)*cos((x-y)/2);;;sin(x)-sin(y);;;2*sin((x-y)/2)*cos((x+y)/2);;;cos(x)+cos(y);;;2*cos((x+y)/2)*cos((x-y)/2);;;cos(x)-cos(y);;;2*sin((x+y)/2)*sin((x-y)/2);;;sin(x)sin(y);;;(cos(x-y)-cos(x+y))/2;;;sin(x)cos(y);;;(sin(x-y)+sin(x+y))/2;;;cos(x)cos(y);;;(cos(x-y)+cos(x+y))/2;;;sin(0);;;0;;;sin(pi/2);;;1;;;cos(0);;;1;;;cos(pi/2);;;0;;;sin(pi/6);;;1/2;;;sin(pi/4);;;2^0.5/2;;;sin(pi/3);;;3^0.5/2;;;cos(pi/3);;;1/2;;;cos(pi/4);;;2^0.5/2;;;cos(pi/6);;;3^0.5/2;;;cos(pi/6);;;3^0.5/2;;;cos(-x);;;cos(x);;;sin(-x);;;-sin(x);;;sin(pi-x);;;sin(x);;;cos(pi-x);;;-cos(x);;;sin(x+2*pi);;;sin(x);;;cos(x+2*pi);;;cos(x);;;cos(pi/2-x);;;sin(x);;;sin(pi/2-x);;;cos(x)",
                "EXPRESSION_COMPARISON{(+(cos(x);cos(*(2;x));cos(*(6;x));cos(*(7;x))))}{=}{(*(4;cos(/(x;2));cos(/(*(5;x);2));cos(*(4;x))))}",
                "",
                ""
        )

        val logRef = log.getLogInPlainText()


        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>[</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi mathvariant=\"normal\">&#x3C0;</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi mathvariant=\"normal\">&#x3C0;</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mo>(</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mo>)</mo><mo>-</mo><mi>z</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mi>x</mi><mo>+</mo><mi>y</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mfrac><mrow><mi mathvariant=\"normal\">x</mi><mo>+</mo><mi mathvariant=\"normal\">y</mi></mrow><mn>2</mn></mfrac><mo>=</mo><mfrac><mrow><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi></mrow><mn>2</mn></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>=</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>=</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo><mo>&gt;</mo></mrow><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>=</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>]</mo><mspace linebreak=\"newline\"/><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>ctg</mi><mfenced><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac></mfenced><mo>+</mo><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mo>=</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>ctg</mi><mfenced><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac></mfenced><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></math>",
                result)
    }
}