package mathhelper.twf.baseoperations

import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.substitutiontests.parseStringExpression
import mathhelper.twf.taskautogeneration.simplifyAndNormalizeExpression
import org.junit.Test
import kotlin.test.assertEquals

class BaseOperationsDefinitionsTest {
    private val allToNumbers = CompiledConfiguration()
    private val piOrE = CompiledConfiguration()
    private val piOrEAndGroup = CompiledConfiguration()

    init {
        allToNumbers.factComparator.expressionComparator.baseOperationsDefinitions.computeExpressionVariableType = ComputeExpressionVariableType.ALL_TO_NUMBERS
        piOrE.factComparator.expressionComparator.baseOperationsDefinitions.computeExpressionVariableType = ComputeExpressionVariableType.ALL_TO_NUMBERS_OR_PI_OR_E
        piOrEAndGroup.factComparator.expressionComparator.baseOperationsDefinitions.computeExpressionVariableType = ComputeExpressionVariableType.ALL_TO_NUMBERS_OR_PI_OR_E_ADDITIVELY_GROUPED
    }

    @Test
    fun testSum() {
        val expr = parseStringExpression("pi+e+e+pi").children[0]
        expr.variableReplacement(allToNumbers.variableConfiguration.variableImmediateReplacementMap)

        val resultAllToNumbers = expr.clone()
        val resultPiOrE = expr.clone()
        val resultPiOrEAndGroup = expr.clone()

        simplifyAndNormalizeExpression(resultAllToNumbers, allToNumbers)
        simplifyAndNormalizeExpression(resultPiOrE, piOrE)
        simplifyAndNormalizeExpression(resultPiOrEAndGroup, piOrEAndGroup)

        assertEquals("11.71974896", resultAllToNumbers.toString())
        assertEquals("+(e;e;π;π)", resultPiOrE.toString())
        assertEquals("+((*(2;e));(*(2;π)))", resultPiOrEAndGroup.toString())
    }

    @Test
    fun testProd() {
        val expr = parseStringExpression("pi*e*e*pi").children[0]
        expr.variableReplacement(allToNumbers.variableConfiguration.variableImmediateReplacementMap)

        val resultAllToNumbers = expr.clone()
        val resultPiOrE = expr.clone()
        val resultPiOrEAndGroup = expr.clone()

        simplifyAndNormalizeExpression(resultAllToNumbers, allToNumbers)
        simplifyAndNormalizeExpression(resultPiOrE, piOrE)
        simplifyAndNormalizeExpression(resultPiOrEAndGroup, piOrEAndGroup)

        assertEquals("72.92706050992207", resultAllToNumbers.toString())
        assertEquals("*(e;e;π;π)", resultPiOrE.toString())
        assertEquals("*((^(e;2));(^(π;2)))", resultPiOrEAndGroup.toString())
    }

    @Test
    fun testDiv() {
        val expr = parseStringExpression("pi/e").children[0]
        expr.variableReplacement(allToNumbers.variableConfiguration.variableImmediateReplacementMap)

        val resultAllToNumbers = expr.clone()
        val resultPiOrE = expr.clone()
        val resultPiOrEAndGroup = expr.clone()

        simplifyAndNormalizeExpression(resultAllToNumbers, allToNumbers, 20)
        simplifyAndNormalizeExpression(resultPiOrE, piOrE)
        simplifyAndNormalizeExpression(resultPiOrEAndGroup, piOrEAndGroup)

        assertEquals("1.1557273478151455", resultAllToNumbers.toString())
        assertEquals("/(π;e)", resultPiOrE.toString())
        assertEquals("/(π;e)", resultPiOrEAndGroup.toString())
    }

    @Test
    fun testMinus() {
        val expr = parseStringExpression("pi-e").children[0]
        expr.variableReplacement(allToNumbers.variableConfiguration.variableImmediateReplacementMap)

        val resultAllToNumbers = expr.clone()
        val resultPiOrE = expr.clone()
        val resultPiOrEAndGroup = expr.clone()

        simplifyAndNormalizeExpression(resultAllToNumbers, allToNumbers)
        simplifyAndNormalizeExpression(resultPiOrE, piOrE)
        simplifyAndNormalizeExpression(resultPiOrEAndGroup, piOrEAndGroup)

        assertEquals("0.42331082", resultAllToNumbers.toString())
        assertEquals("+(-(e);π)", resultPiOrE.toString())
        assertEquals("+(-(e);π)", resultPiOrEAndGroup.toString())
    }

}