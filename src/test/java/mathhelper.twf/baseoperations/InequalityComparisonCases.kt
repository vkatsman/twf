package mathhelper.twf.baseoperations

import mathhelper.twf.config.ComparisonType
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.optimizerutils.Case
import mathhelper.twf.optimizerutils.SpecialCaseSolver
import org.junit.Ignore
import org.junit.Test
import mathhelper.twf.platformdependent.random
import mathhelper.twf.substitutiontests.parseStringExpression
import kotlin.math.abs
import kotlin.test.assertTrue
import kotlin.test.assertEquals

class ComparisonSpecialCasesTests {
    val compiledConfiguration = CompiledConfiguration()

    @Test
    fun polynomialComparisonTest1() {
        val left = parseStringExpression("x^3")
        val right = parseStringExpression("(x+1)*(x+1)")
        val solver = SpecialCaseSolver(left, right)
        assertTrue(solver.currentCase() == Case.ONE_DIMENSIONAL_POLYNOMIAL)
        assertEquals(false, solver.solve(ComparisonType.LEFT_LESS))
        assertEquals(false, solver.solve(ComparisonType.LEFT_MORE))
        assertEquals(false, solver.solve(ComparisonType.LEFT_LESS_OR_EQUAL))
        assertEquals(false, solver.solve(ComparisonType.LEFT_MORE_OR_EQUAL))
        assertEquals(false, solver.solve(ComparisonType.EQUAL))
    }

    @Test
    fun polynomialComparisonTest2() {
        val left = parseStringExpression("x^4-2*x^2+1")
        val right = parseStringExpression("0")
        val solver = SpecialCaseSolver(left, right)
        assertTrue(solver.currentCase() == Case.ONE_DIMENSIONAL_POLYNOMIAL)
        assertEquals(false, solver.solve(ComparisonType.LEFT_LESS))
        assertEquals(false, solver.solve(ComparisonType.LEFT_MORE))
        assertEquals(false, solver.solve(ComparisonType.LEFT_LESS_OR_EQUAL))
        assertEquals(true, solver.solve(ComparisonType.LEFT_MORE_OR_EQUAL))
        assertEquals(false, solver.solve(ComparisonType.EQUAL))
    }

    @Test
    fun polynomialComparisonTest3() {
        val left = parseStringExpression("x^4-2*x^2+1")
        val right = parseStringExpression("(x^2-1)^2")
        val solver = SpecialCaseSolver(left, right)
        assertTrue(solver.currentCase() == Case.ONE_DIMENSIONAL_POLYNOMIAL)
        assertEquals(false, solver.solve(ComparisonType.LEFT_LESS))
        assertEquals(false, solver.solve(ComparisonType.LEFT_MORE))
        assertEquals(true, solver.solve(ComparisonType.LEFT_LESS_OR_EQUAL))
        assertEquals(true, solver.solve(ComparisonType.LEFT_MORE_OR_EQUAL))
        assertEquals(true, solver.solve(ComparisonType.EQUAL))
    }

    @Test
    @Ignore //todo: fix Bug
    fun polynomialComparisonTest4() {
        val left = parseStringExpression("x^100")
        val right = parseStringExpression("x^112+1")
        val solver = SpecialCaseSolver(left, right)
        assertTrue(solver.currentCase() == Case.ONE_DIMENSIONAL_POLYNOMIAL)
        assertEquals(true, solver.solve(ComparisonType.LEFT_LESS))
        assertEquals(false, solver.solve(ComparisonType.LEFT_MORE))
        assertEquals(true, solver.solve(ComparisonType.LEFT_LESS_OR_EQUAL))
        assertEquals(false, solver.solve(ComparisonType.LEFT_MORE_OR_EQUAL))
        assertEquals(false, solver.solve(ComparisonType.EQUAL))
    }

    @Test
    @Ignore //todo: fix Bug
    fun polynomialComparisonTest5() {
        val left = parseStringExpression("x^100")
        val right = parseStringExpression("x^200")
        val solver = SpecialCaseSolver(left, right)
        assertTrue(solver.currentCase() == Case.ONE_DIMENSIONAL_POLYNOMIAL)
        assertEquals(false, solver.solve(ComparisonType.LEFT_LESS, -1.0, 1.0))
        assertEquals(false, solver.solve(ComparisonType.LEFT_MORE, -1.0, 1.0))
        assertEquals(false, solver.solve(ComparisonType.LEFT_LESS_OR_EQUAL, -1.0, 1.0))
        assertEquals(true, solver.solve(ComparisonType.LEFT_MORE_OR_EQUAL, -1.0, 1.0))
        assertEquals(true, solver.solve(ComparisonType.EQUAL, 0.0, 0.0))
    }
}

class ComparisonToolsTests {
    val compiledConfiguration = CompiledConfiguration()

    val eps = 1e-7

    fun equalArrays(left: ArrayList<Double>, right: ArrayList<Double>): Boolean {
        if (left.size != right.size)
            return false
        for (i in left.indices)
            if (abs(left[i] - right[i]) > eps)
                return false
        return true
    }

    fun checkRoots(roots: ArrayList<Double>, res: ArrayList<Double>): Boolean {
        roots.sort()
        res.sort()
        return equalArrays(roots, res)
    }

    @Test
    // make function derivativeForPolynomial() public to run this test
    fun derivativeTest1() {
        val expression = arrayListOf(1.0, 2.0, 3.0, 4.0, 5.0)
        val derivative = SpecialCaseSolver(parseStringExpression(""), parseStringExpression("")).derivativeForPolynomial(expression)
        assertTrue(equalArrays(derivative, arrayListOf(2.0, 6.0, 12.0, 20.0)))
    }

    @Test
    // make function toPolynomial() public to run this test
    fun toPolynomialTest1() {
        val poly = SpecialCaseSolver(parseStringExpression(""), parseStringExpression("")).toPolynomial(parseStringExpression("(1+x^2)^3"))
        assertTrue(equalArrays(poly, arrayListOf(1.0, 0.0, 3.0, 0.0, 3.0, 0.0, 1.0)))
    }

    @Test
    // make function toPolynomial() public to run this test
    fun toPolynomialTest2() {
        val poly = SpecialCaseSolver(parseStringExpression(""), parseStringExpression("")).toPolynomial(parseStringExpression("(2+5*x^2)^0"))
        assertTrue(equalArrays(poly, arrayListOf(1.0)))
    }

    @Test
    // make function toPolynomial() public to run this test
    fun toPolynomialTest3() {
        val poly = SpecialCaseSolver(parseStringExpression(""), parseStringExpression("")).toPolynomial(parseStringExpression("(2-5*x^3)^2"))
        assertTrue(equalArrays(poly, arrayListOf(4.0, 0.0, 0.0, -20.0, 0.0, 0.0, 25.0)))
    }

    @Test
    // to run this test you should make function rootsOfCubicPolynomial() public first
    fun toPolynomialTest4() {
        for (i in 0..10) {
            val r1 = random(-100.0, 100.0)
            val r2 = random(-100.0, 100.0)
            val r3 = random(-100.0, 100.0)
            val c = random(-100.0, 100.0)
            // poly = c * (x - r1) * (x - r2) * (x - r3)
            val poly = arrayListOf(c, -c * (r1 + r2 + r3), c * (r1 * r2 + r1 * r3 + r2 * r3), -c * (r1 * r2 * r3))
            poly.reverse()
            val roots = SpecialCaseSolver(parseStringExpression(""), parseStringExpression("")).rootsOfCubicPolynomial(poly)
            assertTrue(checkRoots(arrayListOf(r1, r2, r3), roots))

        }
        for (i in 0..10) {
            val r = random(0.01, 100.0)
            val r3 = random(100.0, 100.0)
            val c = random(-100.0, 100.0)
            // poly = c * (x^2 + r^2) * (x - r3)
            val poly = arrayListOf(c, -c * r3, c * r * r, -c * r3 * r * r)
            poly.reverse()
            val roots = SpecialCaseSolver(parseStringExpression(""), parseStringExpression("")).rootsOfCubicPolynomial(poly)
            assertTrue(checkRoots(arrayListOf(r3), roots))
        }
    }
}