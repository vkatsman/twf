package mathhelper.twf.standartlibextensions

import org.junit.Test

import kotlin.test.assertEquals

internal class StringExtensionKtTest {

    @Test
    fun isNamePartFunTest() {
        assertEquals(false, '?'.isNamePart())
        assertEquals(false, ':'.isNamePart())
        assertEquals(false, '-'.isNamePart())
        assertEquals(false, '+'.isNamePart())
        assertEquals(false, '('.isNamePart())
        assertEquals(false, ')'.isNamePart())
        assertEquals(false, '*'.isNamePart())
        assertEquals(false, '/'.isNamePart())
        assertEquals(false, '^'.isNamePart())
        assertEquals(true, 'a'.isNamePart())
    }

    @Test
    fun sortStringsDigitsFirst() {
        val strings = listOf("2", "pi", "-", "~", "A", "4")
        assertEquals("-, 2, 4, A, pi, ~", strings.sorted().joinToString())
    }
}