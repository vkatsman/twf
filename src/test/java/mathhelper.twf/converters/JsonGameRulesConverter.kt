package mathhelper.twf.converters

import mathhelper.twf.api.expressionToString
import mathhelper.twf.api.stringToStructureString
import mathhelper.twf.api.structureStringToExpression
import org.junit.Ignore
import org.junit.Test

class JsonGameRulesConverter {


    @Test
    @Ignore
    fun takeConditions (){
        val originalGame = "" +
                "  \"levels\":[\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Hello_World_Level_Prove\",\n" +
                "      \"name\": \"Hello World Level Prove\",\n" +
                "      \"difficulty\": 1.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 2,\n" +
                "      \"time\": 50,\n" +
                "      \"originalExpression\": \"(A(n;n))\",\n" +
                "      \"finalExpression\": \"(factorial(n))\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"CombinatoricBase\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Level_01_Prove\",\n" +
                "      \"name\": \"Level 1 Prove\",\n" +
                "      \"difficulty\": 2.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 5,\n" +
                "      \"time\": 90,\n" +
                "      \"originalExpression\": \"(*(V(+(m;1);n);*(P(m);P(n))))\",\n" +
                "      \"finalExpression\": \"(*(P(m);A(+(m;n);n)))\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"CombinatoricBase\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Level_02_Prove\",\n" +
                "      \"name\": \"Level 2 Prove Stirling-II\",\n" +
                "      \"difficulty\": 2.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 5,\n" +
                "      \"time\": 90,\n" +
                "      \"originalExpression\": \"(S2(/(P(+(m;1));P(m));+(n;S2(m;m))))\",\n" +
                "      \"finalExpression\": \"(+(S2(m;n);*(n;S2(m;+(n;1)))))\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"CombinatoricBase\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Level_03_Prove\",\n" +
                "      \"name\": \"Level 3 Prove Catalan\",\n" +
                "      \"difficulty\": 2.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 3,\n" +
                "      \"time\": 90,\n" +
                "      \"originalExpression\": \"(*(C(n)*C(0);A(+(n;1);1)))\",\n" +
                "      \"finalExpression\": \"(C(*(2;n);n))\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"CombinatoricBase\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Level_04_Prove\",\n" +
                "      \"name\": \"Level 4 Prove\",\n" +
                "      \"difficulty\": 3.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 15,\n" +
                "      \"time\": 90,\n" +
                "      \"originalExpression\": \"(*(+(m;n;1);C(+(m;n);n)))\",\n" +
                "      \"finalExpression\": \"(*(+(n;1);C(+(m;n;1);+(n;1))))\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"CombinatoricBase\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Level_05_Prove\",\n" +
                "      \"name\": \"Level 5 Prove\",\n" +
                "      \"difficulty\": 3.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 9,\n" +
                "      \"time\": 90,\n" +
                "      \"originalExpression\": \"(/(*(S1(n;m);A(+(m;n);n));*(V(+(m;1);n);factorial(m);factorial(n))))\",\n" +
                "      \"finalExpression\": \"(S2(n;m))\",\n" +
                "      \"rules\": [\n" +
                "        {\n" +
                "          \"rulePack\": \"CombinatoricBase\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Level_06_Prove\",\n" +
                "      \"name\": \"Level 6 Prove\",\n" +
                "      \"difficulty\": 4.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 30,\n" +
                "      \"time\": 200,\n" +
                "      \"originalExpression\": \"(+(C(+(*(2;n);1);n);C(+(*(2;n);1);+(n;1))))\",\n" +
                "      \"finalExpression\": \"(C(*(2;+(n;1));+(n;1)))\",\n" +
                "      \"rules\": [\n" +
                "        {\n" +
                "          \"rulePack\": \"CombinatoricBase\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Level_07_Prove\",\n" +
                "      \"name\": \"Level 7 Prove\",\n" +
                "      \"difficulty\": 4.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 26,\n" +
                "      \"time\": 200,\n" +
                "      \"originalExpression\": \"(*(/(A(+(n;m;1);m);+(m;n;1));/(+(n;1);P(m))))\",\n" +
                "      \"finalExpression\": \"(V(+(n;1);m))\",\n" +
                "      \"rules\": [\n" +
                "        {\n" +
                "          \"rulePack\": \"CombinatoricBase\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Level_08_Prove\",\n" +
                "      \"name\": \"Level 8 Prove\",\n" +
                "      \"difficulty\": 5.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 33,\n" +
                "      \"time\": 200,\n" +
                "      \"originalExpression\": \"(*(C(+(n;k;m);+(k;m));C(+(k;m);m)))\",\n" +
                "      \"finalExpression\": \"(*(C(+(n;k;m);m);C(+(n;k);k)))\",\n" +
                "      \"rules\": [\n" +
                "        {\n" +
                "          \"rulePack\": \"CombinatoricBase\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Level_09_Prove\",\n" +
                "      \"name\": \"Level 9 Prove\",\n" +
                "      \"difficulty\": 5.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 41,\n" +
                "      \"time\": 200,\n" +
                "      \"originalExpression\": \"(/(V(+(m;1);+(n;k;1));A(+(k;m;1);+(k;1))))\",\n" +
                "      \"finalExpression\": \"(/(A(+(m;n;k;1);n);P(+(n;k;1))))\",\n" +
                "      \"rules\": [\n" +
                "        {\n" +
                "          \"rulePack\": \"CombinatoricBase\"\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"simple_combinatorics__Level_10_Prove\",\n" +
                "      \"name\": \"Level 10 Prove\",\n" +
                "      \"difficulty\": 5.0,\n" +
                "      \"type\": \"algebra\",\n" +
                "      \"stepsNum\": 23,\n" +
                "      \"time\": 200,\n" +
                "      \"originalExpression\": \"(*(/(A(+(n;1;m);m);+(m;n;1));/(C(*(2;n);n);*(C(n);P(m)))))\",\n" +
                "      \"finalExpression\": \"(V(+(m;1);n))\",\n" +
                "      \"rules\": [\n" +
                "        {\n" +
                "          \"rulePack\": \"CombinatoricBase\"\n" +
                "        }\n" +
                "      ]\n" +
                "    }"
        var resultConditions = StringBuilder()
        val tokens = originalGame.split("\"")
        var i = 0
        var levelNum = 1
        while (i < tokens.size){
            if (tokens[i] == "originalExpression"){
                i+=2
                val expr = tokens[i]
                print("$levelNum. Выражение '")
                val ss = expressionToString(structureStringToExpression(expr))
                print(ss + "' свести к ")
            }
            if (tokens[i] == "finalExpression"){
                i+=2
                val expr = tokens[i]
                if (expr.endsWith("NF")){
                    print(expr + ".\n")
                } else {
                    val ss = expressionToString(structureStringToExpression(expr))
                    print("'$ss'" + ".\n")
                }
                levelNum++
            }
            i++
        }
        print(resultConditions.toString())
    }


    @Test
    @Ignore
    fun convert (){
        val originalGame = "" +
                "{\n" +
                "  \"gamespace\": \"\",\n" +
                "  \"gameCode\": \"SetCombo\",\n" +
                "  \"name\": \"Set Combo\",\n" +
                "  \"version\": 123,\n" +
                "  \"levels\":[\n" +
                "    {\n" +
                "      \"levelCode\": \"Level_1_Prove\",\n" +
                "      \"name\": \"Level 1 Prove\",\n" +
                "      \"difficulty\": 1.1,\n" +
                "      \"type\": \"set\",\n" +
                "      \"stepsNum\": 4,\n" +
                "      \"time\": 90,\n" +
                "      \"originalExpression\": \"A&(A|B)\",\n" +
                "      \"finalExpression\": \"A\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"SetBase\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"Level_2_CNF\",\n" +
                "      \"name\": \"Level 2 CNF\",\n" +
                "      \"difficulty\": 1.2,\n" +
                "      \"type\": \"set\",\n" +
                "      \"stepsNum\": 6,\n" +
                "      \"time\": 120,\n" +
                "      \"originalExpression\": \"A\\(A\\B)\",\n" +
                "      \"finalExpression\": \"CNF\",\n" +
                "      \"finalPattern\": \"and : (or) : : : not\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"SetBase\"},\n" +
                "        {\"rulePack\": \"SetAbsorptionLawReduction\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"Level_9_Resolution\",\n" +
                "      \"name\": \"Level 9 Resolution\",\n" +
                "      \"difficulty\": 2.9,\n" +
                "      \"type\": \"set\",\n" +
                "      \"stepsNum\": 6,\n" +
                "      \"time\": 250,\n" +
                "      \"originalExpression\": \"A&B&(A->C)&!C\",\n" +
                "      \"finalExpression\": \"0\",\n" +
                "      \"finalPattern\": \"\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"SetResolution\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"levelCode\": \"Level_17_DNF\",\n" +
                "      \"name\": \"Level 17 DNF\",\n" +
                "      \"difficulty\": 3.7,\n" +
                "      \"type\": \"set\",\n" +
                "      \"stepsNum\": 12,\n" +
                "      \"time\": 200,\n" +
                "      \"originalExpression\": \"(A|B|!C)&(A|(!B)|C)&((!A)|B|C)\",\n" +
                "      \"finalExpression\": \"DNF\",\n" +
                "      \"finalPattern\": \"or : (and) : : : not\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"SetBase\"},\n" +
                "        {\"rulePack\": \"SetMultiArgumentAnd\"},\n" +
                "        {\"rulePack\": \"SetMultiArgumentOr\"}\n" +
                "      ]\n" +
                "    }\n" +
                "  ],\n" +
                "  \"rulePacks\":[\n" +
                "    {\n" +
                "      \"name\": \"SetBase\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"SetTrivialLetters\"},\n" +
                "\n" +
                "        {\"rulePack\": \"SetThreeСommutativity\"},\n" +
                "        {\"rulePack\": \"SetThreeAssociativity\"},\n" +
                "\n" +
                "        {\"rulePack\": \"SetDeMorgan\"},\n" +
                "\n" +
                "        {\"rulePack\": \"SetImplication\"},\n" +
                "        {\"rulePack\": \"SetDifference\"},\n" +
                "\n" +
                "        {\"rulePack\": \"SetEmptyAndUniverse\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetThreeСommutativity\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"A|(B|C)\",\"right\":\"A|B|C\"},\n" +
                "        {\"left\":\"A|B|C\",\"right\":\"A|(B|C)\"},\n" +
                "        {\"left\":\"A|(B|C)\",\"right\":\"(A|B)|C\"},\n" +
                "        {\"left\":\"(A|B)|C\",\"right\":\"A|(B|C)\"},\n" +
                "        {\"left\":\"(A|B)|C\",\"right\":\"A|B|C\"},\n" +
                "        {\"left\":\"A|B|C\",\"right\":\"(A|B)|C\"},\n" +
                "\n" +
                "        {\"left\":\"A&(B&C)\",\"right\":\"A&B&C\"},\n" +
                "        {\"left\":\"A&B&C\",\"right\":\"A&(B&C)\"},\n" +
                "        {\"left\":\"A&(B&C)\",\"right\":\"(A&B)&C\"},\n" +
                "        {\"left\":\"(A&B)&C\",\"right\":\"A&(B&C)\"},\n" +
                "        {\"left\":\"(A&B)&C\",\"right\":\"A&B&C\"},\n" +
                "        {\"left\":\"A&B&C\",\"right\":\"(A&B)&C\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetThreeAssociativity\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"A|(B&C)\",\"right\":\"(A|B)&(A|C)\"},\n" +
                "        {\"left\":\"(B&C)|A\",\"right\":\"(B|A)&(C|A)\"},\n" +
                "        {\"left\":\"(A|B)&(A|C)\",\"right\":\"A|(B&C)\"},\n" +
                "        {\"left\":\"(B|C)&(A|C)\",\"right\":\"(B&A)|C\"},\n" +
                "        {\"left\":\"A&(B|C)\",\"right\":\"(A&B)|(A&C)\"},\n" +
                "        {\"left\":\"(B|C)&A\",\"right\":\"(B&A)|(C&A)\"},\n" +
                "        {\"left\":\"(A&B)|(A&C)\",\"right\":\"A&(B|C)\"},\n" +
                "        {\"left\":\"(B&C)|(A&C)\",\"right\":\"(B|A)&C\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetTwoСommutativity\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"A&B\",\"right\":\"B&A\"},\n" +
                "        {\"left\":\"A|B\",\"right\":\"B|A\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetTrivialLetters\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"SetTwoСommutativity\"},\n" +
                "        {\"left\":\"!(!A)\",\"right\":\"A\"},\n" +
                "        {\"left\":\"A\",\"right\":\"!(!A)\"},\n" +
                "        {\"left\":\"A|A\",\"right\":\"A\"},\n" +
                "        {\"left\":\"A\",\"right\":\"A|A\"},\n" +
                "        {\"left\":\"A&A\",\"right\":\"A\"},\n" +
                "        {\"left\":\"A\",\"right\":\"A&A\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetDeMorgan\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"!(A&B)\",\"right\":\"!A|!B\"},\n" +
                "        {\"left\":\"!A|!B\",\"right\":\"!(A&B)\"},\n" +
                "        {\"left\":\"!(A|B)\",\"right\":\"!A&!B\"},\n" +
                "        {\"left\":\"!A&!B\",\"right\":\"!(A|B)\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetImplication\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"!A|B\",\"right\":\"A->B\"},\n" +
                "        {\"left\":\"B|!A\",\"right\":\"A->B\"},\n" +
                "        {\"left\":\"A->B\",\"right\":\"!A|B\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetDifference\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"A&!B\",\"right\":\"A\\B\"},\n" +
                "        {\"left\":\"!B&A\",\"right\":\"A\\B\"},\n" +
                "        {\"left\":\"A\\B\",\"right\":\"A&!B\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetDifferenceImplication\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"A\\B\",\"right\":\"!(A->B)\"},\n" +
                "        {\"left\":\"!(A->B)\",\"right\":\"A\\B\"},\n" +
                "        {\"left\":\"A->B\",\"right\":\"!(A\\B)\"},\n" +
                "        {\"left\":\"!(A\\B)\",\"right\":\"A->B\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetAbsorptionLawReduction\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"A&(A|B)\",\"right\":\"A\"},\n" +
                "        {\"left\":\"A|(A&B)\",\"right\":\"A\"},\n" +
                "        {\"left\":\"A&(B|A)\",\"right\":\"A\"},\n" +
                "        {\"left\":\"A|(B&A)\",\"right\":\"A\"},\n" +
                "        {\"left\":\"(A|B)&A\",\"right\":\"A\"},\n" +
                "        {\"left\":\"(A&B)|A\",\"right\":\"A\"},\n" +
                "        {\"left\":\"(B|A)&A\",\"right\":\"A\"},\n" +
                "        {\"left\":\"(B&A)|A\",\"right\":\"A\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetAbsorptionLawExtension\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"A\",\"right\":\"A&(A|B)\"},\n" +
                "        {\"left\":\"A\",\"right\":\"A|(A&B)\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetEmptyAndUniverse\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"A|!A\",\"right\":\"1\"},\n" +
                "        {\"left\":\"!A|A\",\"right\":\"1\"},\n" +
                "        {\"left\":\"A&!A\",\"right\":\"0\"},\n" +
                "        {\"left\":\"!A&A\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A|1\",\"right\":\"1\"},\n" +
                "        {\"left\":\"1|A\",\"right\":\"1\"},\n" +
                "        {\"left\":\"A&1\",\"right\":\"A\"},\n" +
                "        {\"left\":\"1&A\",\"right\":\"A\"},\n" +
                "        {\"left\":\"A|0\",\"right\":\"A\"},\n" +
                "        {\"left\":\"0|A\",\"right\":\"A\"},\n" +
                "        {\"left\":\"A&0\",\"right\":\"0\"},\n" +
                "        {\"left\":\"0&A\",\"right\":\"0\"},\n" +
                "\n" +
                "        {\"left\":\"A\\0\",\"right\":\"A\"},\n" +
                "        {\"left\":\"A\\1\",\"right\":\"0\"},\n" +
                "        {\"left\":\"0\\A\",\"right\":\"0\"},\n" +
                "        {\"left\":\"1\\A\",\"right\":\"!A\"},\n" +
                "\n" +
                "        {\"left\":\"0->A\",\"right\":\"1\"},\n" +
                "        {\"left\":\"A->1\",\"right\":\"1\"},\n" +
                "        {\"left\":\"A->0\",\"right\":\"!A\"},\n" +
                "        {\"left\":\"1->A\",\"right\":\"A\"},\n" +
                "\n" +
                "        {\"left\":\"!0\",\"right\":\"1\"},\n" +
                "        {\"left\":\"!1\",\"right\":\"0\"},\n" +
                "\n" +
                "        {\"rulePack\": \"SetEmptyAndUniverseExtension\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetEmptyAndUniverseExtension\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"A\",\"right\":\"A|0\"},\n" +
                "        {\"left\":\"A\",\"right\":\"A&1\"},\n" +
                "        {\"left\":\"A\",\"right\":\"A\\0\"},\n" +
                "        {\"left\":\"A\",\"right\":\"1->A\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetMultiArgumentAnd\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"(A&B)&(C&D)\",\"right\":\"A&B&C&D\"},\n" +
                "        {\"left\":\"A&(B&C&D)\",\"right\":\"A&B&C&D\"},\n" +
                "        {\"left\":\"(A&B&C)&D\",\"right\":\"A&B&C&D\"},\n" +
                "\n" +
                "        {\"left\":\"A&B&(C&D)\",\"right\":\"A&B&C&D\"},\n" +
                "        {\"left\":\"A&B&C&D\",\"right\":\"A&B&(C&D)\"},\n" +
                "        {\"left\":\"A&(B&C)&D\",\"right\":\"A&B&C&D\"},\n" +
                "        {\"left\":\"A&B&C&D\",\"right\":\"A&(B&C)&D\"},\n" +
                "        {\"left\":\"(A&B)&C&D\",\"right\":\"A&B&C&D\"},\n" +
                "        {\"left\":\"A&B&C&D\",\"right\":\"(A&B)&C&D\"},\n" +
                "\n" +
                "        {\"left\":\"A&B&C&(D&E)\",\"right\":\"A&B&C&D&E\"},\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"A&B&C&(D&E)\"},\n" +
                "        {\"left\":\"A&B&(C&D)&E\",\"right\":\"A&B&C&D&E\"},\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"A&B&(C&D)&E\"},\n" +
                "        {\"left\":\"A&(B&C)&D&E\",\"right\":\"A&B&C&D&E\"},\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"A&(B&C)&D&E\"},\n" +
                "        {\"left\":\"(A&B)&C&D&E\",\"right\":\"A&B&C&D&E\"},\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"(A&B)&C&D&E\"},\n" +
                "\n" +
                "        {\"left\":\"A&B&C&D&(E&F)\",\"right\":\"A&B&C&D&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"A&B&C&D&(E&F)\"},\n" +
                "        {\"left\":\"A&B&C&(D&E)&F\",\"right\":\"A&B&C&D&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"A&B&C&(D&E)&F\"},\n" +
                "        {\"left\":\"A&B&(C&D)&E&F\",\"right\":\"A&B&C&D&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"A&B&(C&D)&E&F\"},\n" +
                "        {\"left\":\"A&(B&C)&D&E&F\",\"right\":\"A&B&C&D&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"A&(B&C)&D&E&F\"},\n" +
                "        {\"left\":\"(A&B)&C&D&E&F\",\"right\":\"A&B&C&D&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"(A&B)&C&D&E&F\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetMultiArgumentOr\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"left\":\"(A|B)|(C|D)\",\"right\":\"A|B|C|D\"},\n" +
                "        {\"left\":\"A|(B|C|D)\",\"right\":\"A|B|C|D\"},\n" +
                "        {\"left\":\"(A|B|C)|D\",\"right\":\"A|B|C|D\"},\n" +
                "\n" +
                "        {\"left\":\"A|B|(C|D)\",\"right\":\"A|B|C|D\"},\n" +
                "        {\"left\":\"A|B|C|D\",\"right\":\"A|B|(C|D)\"},\n" +
                "        {\"left\":\"A|(B|C)|D\",\"right\":\"A|B|C|D\"},\n" +
                "        {\"left\":\"A|B|C|D\",\"right\":\"A|(B|C)|D\"},\n" +
                "        {\"left\":\"(A|B)|C|D\",\"right\":\"A|B|C|D\"},\n" +
                "        {\"left\":\"A|B|C|D\",\"right\":\"(A|B)|C|D\"},\n" +
                "\n" +
                "        {\"left\":\"A|B|C|(D|E)\",\"right\":\"A|B|C|D|E\"},\n" +
                "        {\"left\":\"A|B|C|D|E\",\"right\":\"A|B|C|(D|E)\"},\n" +
                "        {\"left\":\"A|B|(C|D)|E\",\"right\":\"A|B|C|D|E\"},\n" +
                "        {\"left\":\"A|B|C|D|E\",\"right\":\"A|B|(C|D)|E\"},\n" +
                "        {\"left\":\"A|(B|C)|D|E\",\"right\":\"A|B|C|D|E\"},\n" +
                "        {\"left\":\"A|B|C|D|E\",\"right\":\"A|(B|C)|D|E\"},\n" +
                "        {\"left\":\"(A|B)|C|D|E\",\"right\":\"A|B|C|D|E\"},\n" +
                "        {\"left\":\"A|B|C|D|E\",\"right\":\"(A|B)|C|D|E\"},\n" +
                "\n" +
                "        {\"left\":\"A|B|C|D|(E|F)\",\"right\":\"A|B|C|D|E|F\"},\n" +
                "        {\"left\":\"A|B|C|D|E|F\",\"right\":\"A|B|C|D|(E|F)\"},\n" +
                "        {\"left\":\"A|B|C|(D|E)|F\",\"right\":\"A|B|C|D|E|F\"},\n" +
                "        {\"left\":\"A|B|C|D|E|F\",\"right\":\"A|B|C|(D|E)|F\"},\n" +
                "        {\"left\":\"A|B|(C|D)|E|F\",\"right\":\"A|B|C|D|E|F\"},\n" +
                "        {\"left\":\"A|B|C|D|E|F\",\"right\":\"A|B|(C|D)|E|F\"},\n" +
                "        {\"left\":\"A|(B|C)|D|E|F\",\"right\":\"A|B|C|D|E|F\"},\n" +
                "        {\"left\":\"A|B|C|D|E|F\",\"right\":\"A|(B|C)|D|E|F\"},\n" +
                "        {\"left\":\"(A|B)|C|D|E|F\",\"right\":\"A|B|C|D|E|F\"},\n" +
                "        {\"left\":\"A|B|C|D|E|F\",\"right\":\"(A|B)|C|D|E|F\"}\n" +
                "      ]\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"SetResolution\",\n" +
                "      \"type\": \"set\",\n" +
                "      \"rules\":[\n" +
                "        {\"rulePack\": \"SetBase\"},\n" +
                "\n" +
                "        {\"left\":\"A&B&C\",\"right\":\"B&A&C\"},\n" +
                "        {\"left\":\"A&B&C\",\"right\":\"C&A&B\"},\n" +
                "        {\"left\":\"A&B&C\",\"right\":\"A&C&B\"},\n" +
                "        {\"left\":\"!A&A&C\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&!A&C\",\"right\":\"0\"},\n" +
                "        {\"left\":\"(A|X)&!A&C\",\"right\":\"X&(A|X)&!A&C\"},\n" +
                "        {\"left\":\"!A&(A|X)&C\",\"right\":\"X&!A&(A|X)&C\"},\n" +
                "        {\"left\":\"(!A|X)&A&C\",\"right\":\"X&(!A|X)&A&C\"},\n" +
                "        {\"left\":\"A&(!A|X)&C\",\"right\":\"X&A&(!A|X)&C\"},\n" +
                "        {\"left\":\"(A|X)&(!A|Y)&C\",\"right\":\"(X|Y)&(A|X)&(!A|Y)&C\"},\n" +
                "        {\"left\":\"(!A|X)&(A|Y)&C\",\"right\":\"(X|Y)&(!A|X)&(A|Y)&C\"},\n" +
                "        {\"left\":\"0&B&C\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&0&C\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&0\",\"right\":\"0\"},\n" +
                "\n" +
                "        {\"left\":\"A&B&C&D\",\"right\":\"B&A&C&D\"},\n" +
                "        {\"left\":\"A&B&C&D\",\"right\":\"C&A&B&D\"},\n" +
                "        {\"left\":\"A&B&C&D\",\"right\":\"D&A&B&C\"},\n" +
                "        {\"left\":\"A&B&C&D\",\"right\":\"A&C&B&D\"},\n" +
                "        {\"left\":\"A&B&C&D\",\"right\":\"A&D&B&C\"},\n" +
                "        {\"left\":\"!A&A&C&D\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&!A&C&D\",\"right\":\"0\"},\n" +
                "        {\"left\":\"(A|X)&!A&C&D\",\"right\":\"X&(A|X)&!A&C&D\"},\n" +
                "        {\"left\":\"!A&(A|X)&C&D\",\"right\":\"X&!A&(A|X)&C&D\"},\n" +
                "        {\"left\":\"(!A|X)&A&C&D\",\"right\":\"X&(!A|X)&A&C&D\"},\n" +
                "        {\"left\":\"A&(!A|X)&C&D\",\"right\":\"X&A&(!A|X)&C&D\"},\n" +
                "        {\"left\":\"(A|X)&(!A|Y)&C&D\",\"right\":\"(X|Y)&(A|X)&(!A|Y)&C&D\"},\n" +
                "        {\"left\":\"(!A|X)&(A|Y)&C&D\",\"right\":\"(X|Y)&(!A|X)&(A|Y)&C&D\"},\n" +
                "        {\"left\":\"0&B&C&D\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&0&C&D\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&0&D\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&0\",\"right\":\"0\"},\n" +
                "\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"B&A&C&D&E\"},\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"C&A&B&D&E\"},\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"D&A&B&C&E\"},\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"E&A&B&C&D\"},\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"A&C&B&D&E\"},\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"A&D&B&C&E\"},\n" +
                "        {\"left\":\"A&B&C&D&E\",\"right\":\"A&E&B&C&D\"},\n" +
                "        {\"left\":\"!A&A&C&D&E\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&!A&C&D&E\",\"right\":\"0\"},\n" +
                "        {\"left\":\"(A|X)&!A&C&D&E\",\"right\":\"X&(A|X)&!A&C&D&E\"},\n" +
                "        {\"left\":\"!A&(A|X)&C&D&E\",\"right\":\"X&!A&(A|X)&C&D&E\"},\n" +
                "        {\"left\":\"(!A|X)&A&C&D&E\",\"right\":\"X&(!A|X)&A&C&D&E\"},\n" +
                "        {\"left\":\"A&(!A|X)&C&D&E\",\"right\":\"X&A&(!A|X)&C&D&E\"},\n" +
                "        {\"left\":\"(A|X)&(!A|Y)&C&D&E\",\"right\":\"(X|Y)&(A|X)&(!A|Y)&C&D&E\"},\n" +
                "        {\"left\":\"(!A|X)&(A|Y)&C&D&E\",\"right\":\"(X|Y)&(!A|X)&(A|Y)&C&D&E\"},\n" +
                "        {\"left\":\"0&B&C&D&E\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&0&C&D&E\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&0&D&E\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&0&E\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&0\",\"right\":\"0\"},\n" +
                "\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"B&A&C&D&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"C&A&B&D&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"D&A&B&C&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"E&A&B&C&D&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"F&A&B&C&D&E\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"A&C&B&D&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"A&D&B&C&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"A&E&B&C&D&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F\",\"right\":\"A&F&B&C&D&E\"},\n" +
                "        {\"left\":\"!A&A&C&D&E&F\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&!A&C&D&E&F\",\"right\":\"0\"},\n" +
                "        {\"left\":\"(A|X)&!A&C&D&E&F\",\"right\":\"X&(A|X)&!A&C&D&E&F\"},\n" +
                "        {\"left\":\"!A&(A|X)&C&D&E&F\",\"right\":\"X&!A&(A|X)&C&D&E&F\"},\n" +
                "        {\"left\":\"(!A|X)&A&C&D&E&F\",\"right\":\"X&(!A|X)&A&C&D&E&F\"},\n" +
                "        {\"left\":\"A&(!A|X)&C&D&E&F\",\"right\":\"X&A&(!A|X)&C&D&E&F\"},\n" +
                "        {\"left\":\"(A|X)&(!A|Y)&C&D&E&F\",\"right\":\"(X|Y)&(A|X)&(!A|Y)&C&D&E&F\"},\n" +
                "        {\"left\":\"(!A|X)&(A|Y)&C&D&E&F\",\"right\":\"(X|Y)&(!A|X)&(A|Y)&C&D&E&F\"},\n" +
                "        {\"left\":\"0&B&C&D&E&F\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&0&C&D&E&F\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&0&D&E&F\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&0&E&F\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&0&F\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&E&0\",\"right\":\"0\"},\n" +
                "\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"B&A&C&D&E&F&G\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"C&A&B&D&E&F&G\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"D&A&B&C&E&F&G\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"E&A&B&C&D&F&G\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"F&A&B&C&D&E&G\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"G&A&B&C&D&E&F\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"A&C&B&D&E&F&G\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"A&D&B&C&E&F&G\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"A&E&B&C&D&F&G\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"A&F&B&C&D&E&G\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G\",\"right\":\"A&G&B&C&D&E&F\"},\n" +
                "        {\"left\":\"!A&A&C&D&E&F&G\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&!A&C&D&E&F&G\",\"right\":\"0\"},\n" +
                "        {\"left\":\"(A|X)&!A&C&D&E&F&G\",\"right\":\"X&(A|X)&!A&C&D&E&F&G\"},\n" +
                "        {\"left\":\"!A&(A|X)&C&D&E&F&G\",\"right\":\"X&!A&(A|X)&C&D&E&F&G\"},\n" +
                "        {\"left\":\"(!A|X)&A&C&D&E&F&G\",\"right\":\"X&(!A|X)&A&C&D&E&F&G\"},\n" +
                "        {\"left\":\"A&(!A|X)&C&D&E&F&G\",\"right\":\"X&A&(!A|X)&C&D&E&F&G\"},\n" +
                "        {\"left\":\"(A|X)&(!A|Y)&C&D&E&F&G\",\"right\":\"(X|Y)&(A|X)&(!A|Y)&C&D&E&F&G\"},\n" +
                "        {\"left\":\"(!A|X)&(A|Y)&C&D&E&F&G\",\"right\":\"(X|Y)&(!A|X)&(A|Y)&C&D&E&F&G\"},\n" +
                "        {\"left\":\"0&B&C&D&E&F&G\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&0&C&D&E&F&G\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&0&D&E&F&G\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&0&E&F&G\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&0&F&G\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&E&0&G\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&0\",\"right\":\"0\"},\n" +
                "\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"B&A&C&D&E&F&G&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"C&A&B&D&E&F&G&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"D&A&B&C&E&F&G&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"E&A&B&C&D&F&G&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"F&A&B&C&D&E&G&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"G&A&B&C&D&E&F&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"H&A&B&C&D&E&F&G\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"A&C&B&D&E&F&G&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"A&D&B&C&E&F&G&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"A&E&B&C&D&F&G&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"A&F&B&C&D&E&G&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"A&G&B&C&D&E&F&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H\",\"right\":\"A&H&B&C&D&E&F&G\"},\n" +
                "        {\"left\":\"!A&A&C&D&E&F&G&H\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&!A&C&D&E&F&G&H\",\"right\":\"0\"},\n" +
                "        {\"left\":\"(A|X)&!A&C&D&E&F&G&H\",\"right\":\"X&(A|X)&!A&C&D&E&F&G&H\"},\n" +
                "        {\"left\":\"!A&(A|X)&C&D&E&F&G&H\",\"right\":\"X&!A&(A|X)&C&D&E&F&G&H\"},\n" +
                "        {\"left\":\"(!A|X)&A&C&D&E&F&G&H\",\"right\":\"X&(!A|X)&A&C&D&E&F&G&H\"},\n" +
                "        {\"left\":\"A&(!A|X)&C&D&E&F&G&H\",\"right\":\"X&A&(!A|X)&C&D&E&F&G&H\"},\n" +
                "        {\"left\":\"(A|X)&(!A|Y)&C&D&E&F&G&H\",\"right\":\"(X|Y)&(A|X)&(!A|Y)&C&D&E&F&G&H\"},\n" +
                "        {\"left\":\"(!A|X)&(A|Y)&C&D&E&F&G&H\",\"right\":\"(X|Y)&(!A|X)&(A|Y)&C&D&E&F&G&H\"},\n" +
                "        {\"left\":\"0&B&C&D&E&F&G&H\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&0&C&D&E&F&G&H\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&0&D&E&F&G&H\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&0&E&F&G&H\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&0&F&G&H\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&E&0&G&H\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&0&H\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&0\",\"right\":\"0\"},\n" +
                "\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"B&A&C&D&E&F&G&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"C&A&B&D&E&F&G&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"D&A&B&C&E&F&G&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"E&A&B&C&D&F&G&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"F&A&B&C&D&E&G&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"G&A&B&C&D&E&F&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"H&A&B&C&D&E&F&G&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"I&A&B&C&D&E&F&G&H\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"A&C&B&D&E&F&G&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"A&D&B&C&E&F&G&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"A&E&B&C&D&F&G&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"A&F&B&C&D&E&G&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"A&G&B&C&D&E&F&H&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"A&H&B&C&D&E&F&G&I\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&I\",\"right\":\"A&I&B&C&D&E&F&G&H\"},\n" +
                "        {\"left\":\"!A&A&C&D&E&F&G&H&I\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&!A&C&D&E&F&G&H&I\",\"right\":\"0\"},\n" +
                "        {\"left\":\"(A|X)&!A&C&D&E&F&G&H&I\",\"right\":\"X&(A|X)&!A&C&D&E&F&G&H&I\"},\n" +
                "        {\"left\":\"!A&(A|X)&C&D&E&F&G&H&I\",\"right\":\"X&!A&(A|X)&C&D&E&F&G&H&I\"},\n" +
                "        {\"left\":\"(!A|X)&A&C&D&E&F&G&H&I\",\"right\":\"X&(!A|X)&A&C&D&E&F&G&H&I\"},\n" +
                "        {\"left\":\"A&(!A|X)&C&D&E&F&G&H&I\",\"right\":\"X&A&(!A|X)&C&D&E&F&G&H&I\"},\n" +
                "        {\"left\":\"(A|X)&(!A|Y)&C&D&E&F&G&H&I\",\"right\":\"(X|Y)&(A|X)&(!A|Y)&C&D&E&F&G&H&I\"},\n" +
                "        {\"left\":\"(!A|X)&(A|Y)&C&D&E&F&G&H&I\",\"right\":\"(X|Y)&(!A|X)&(A|Y)&C&D&E&F&G&H&I\"},\n" +
                "        {\"left\":\"0&B&C&D&E&F&G&H&I\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&0&C&D&E&F&G&H&I\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&0&D&E&F&G&H&I\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&0&E&F&G&H&I\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&0&F&G&H&I\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&E&0&G&H&I\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&0&H&I\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&0&I\",\"right\":\"0\"},\n" +
                "        {\"left\":\"A&B&C&D&E&F&G&H&0\",\"right\":\"0\"}\n" +
                "      ]\n" +
                "    }\n" +
                "  ]\n" +
                "}"

        var resultGame = StringBuilder()
        val tokens = originalGame.split("\"")
        var i = 0
        while (i < tokens.size){
            if (tokens[i] == "left" || tokens[i] == "right" || tokens[i] == "originalExpression"){
                resultGame.append(tokens[i] + "\": \"")
                i+=2
                val expr = tokens[i]
                val ss = stringToStructureString(expr, scope = "setTheory")
                resultGame.append(ss + "\"")
                i++
            } else {
                resultGame.append(tokens[i] + "\"")
                i++
            }
        }
        print(resultGame.toString())
    }
}