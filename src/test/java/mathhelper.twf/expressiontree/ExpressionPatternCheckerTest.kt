package mathhelper.twf.expressiontree

import mathhelper.twf.api.structureStringToExpression
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.assertFalse

class ExpressionPatternCheckerTest {
    @Test
    fun expressionContainsDoubleMinusTest() {
        val expression1 = structureStringToExpression("(+(-(+(-(x)))))")
        assertTrue(expression1.patternDoubleMinus())

        val expression2 = structureStringToExpression("+(+(-(+(-(1;^(cos(x);2)))));^(cos(x);2))") //(1--cos(x)^2)+cos(x)^2
        assertTrue(expression2.patternDoubleMinus())
    }

    @Test
    fun expressionNotContainsDoubleMinusTest() {
        val expression1 = structureStringToExpression("(+(+(-(x))))")
        assertFalse(expression1.patternDoubleMinus())

        val expression2 = structureStringToExpression("(+(-(+(1;-(x);0))))")
        assertFalse(expression2.patternDoubleMinus())
    }

    @Test
    fun expressionStartsWithUnaryMinusTest() {
        val expression1 = structureStringToExpression("(+(-(A)))")
        assertTrue(expression1.patternStartWithUnaryMinus())

        val expression2 = structureStringToExpression("(+(-(+(-(x)))))")
        assertTrue(expression2.patternStartWithUnaryMinus())

        val expression3 = structureStringToExpression("(+(+(-(x))))")
        assertTrue(expression3.patternStartWithUnaryMinus())

        val expression4 = structureStringToExpression("(+(-(+(-(x;y)))))")
        assertTrue(expression4.patternStartWithUnaryMinus())
    }

    @Test
    fun expressionNotStartsWithUnaryMinusTest() {
        val expression1 = structureStringToExpression("(+(-(x;y)))")
        assertFalse(expression1.patternDoubleMinus())

        val expression2 = structureStringToExpression("(+(^(sin(+(-(pi;x)));2);^(sin(/(-(pi);-(2)));2)))")//sin(π-x)^2+sin(-π/-2-x)^2
        assertFalse(expression2.patternStartWithUnaryMinus())

        val expression3 = structureStringToExpression("+(+(-(+(-(1;^(cos(x);2)))));^(cos(x);2))") //(1--cos(x)^2)+cos(x)^2
        assertFalse(expression3.patternStartWithUnaryMinus())
    }

    @Test
    fun expressionContainsUnaryMinusTest() {
        val expression1 = structureStringToExpression("(+(-(A)))")
        assertTrue(expression1.patternUnaryMinus())

        val expression2 = structureStringToExpression("(+(-(+(-(x)))))")
        assertTrue(expression2.patternUnaryMinus())

        val expression3 = structureStringToExpression("(+(+(-(x))))")
        assertTrue(expression3.patternUnaryMinus())

        val expression4 = structureStringToExpression("(+(-(+(-(x;y)))))")
        assertTrue(expression4.patternUnaryMinus())

        val expression5 = structureStringToExpression("(+(-(A);B))")
        assertTrue(expression5.patternUnaryMinus())

        val expression6 = structureStringToExpression("(+(^(sin(+(-(pi;x)));2);^(sin(/(-(pi);-(2)));2)))")//sin(π-x)^2+sin(-π/-2-x)^2
        assertTrue(expression6.patternUnaryMinus())

        val expression7 = structureStringToExpression("+(+(-(+(-(1;^(cos(x);2)))));^(cos(x);2))") //(1--cos(x)^2)+cos(x)^2
        assertTrue(expression7.patternUnaryMinus())

        val expression8 = structureStringToExpression("+(1;^(+(-(tg(x)));2))") //(1+(-tg(x))^2)
        assertTrue(expression8.patternUnaryMinus())

        //val expression6 = structureStringToExpression("+(a; -x)");
        //assertTrue(expression6.patternUnaryMinus())

    }

    @Test
    fun expressionNotContainsUnaryMinusTest() {
        val expression1 = structureStringToExpression("/(a;b)")
        assertFalse(expression1.patternDoubleMinusInFraction())

        val expression2 = structureStringToExpression("+(-(x;y))")
        assertFalse(expression2.patternDoubleMinusInFraction())

    }


    @Test
    fun expressionContainsDoubleMinusInFraction() {
        val expression1 = structureStringToExpression("(/(-(A);-(B)))")
        assertTrue(expression1.patternDoubleMinusInFraction())

        val expression2 = structureStringToExpression("(/(-(pi);-(2)))")
        assertTrue(expression2.patternDoubleMinusInFraction())

        val expression3 = structureStringToExpression("(+(^(sin(+(-(pi;x)));2);^(sin(/(-(pi);-(2)));2)))")//sin(π-x)^2+sin(-π/-2-x)^2
        assertTrue(expression3.patternDoubleMinusInFraction())
    }

    @Test
    fun expressionNotContainsDoubleMinusInFraction() {
        val expression1 = structureStringToExpression("+(1;^(+(-(tg(x)));2))") //(1+(-tg(x))^2)
        assertFalse(expression1.patternDoubleMinusInFraction())

        val expression2 = structureStringToExpression("/(a;b)")
        assertFalse(expression2.patternDoubleMinusInFraction())
    }

    @Test
    fun expressionContainsThreeLevelsInFraction() {
        val expression1 = structureStringToExpression("(+(/(1;/(1;/(1;cos(x))));^(cos(x);2)))")
        assertTrue(expression1.patternThreeLevelsInFraction())
    }

    @Test
    fun expressionNotContainsThreeLevelsInFraction() {
        val expression1 = structureStringToExpression("(/(-(A);-(B)))")
        assertFalse(expression1.patternThreeLevelsInFraction())

        val expression2 = structureStringToExpression("(/(-(pi);-(2)))")
        assertFalse(expression2.patternThreeLevelsInFraction())

        val expression3 = structureStringToExpression("(+(^(sin(+(-(pi;x)));2);^(sin(/(-(pi);-(2)));2)))")//sin(π-x)^2+sin(-π/-2-x)^2
        assertFalse(expression3.patternThreeLevelsInFraction())

        val expression4 = structureStringToExpression("/(a;b)")
        assertFalse(expression4.patternThreeLevelsInFraction())
    }

    @Test
    fun expressionContainsTooManyLevelsInFraction() {
        val expression1 = structureStringToExpression("(+(/(1;/(1;/(1;cos(x))));^(cos(x);2)))")
        assertTrue(expression1.patternTooManyLevelsInFraction())
    }

    @Test
    fun expressionNotContainsTooManyLevelsInFraction() {
        val expression1 = structureStringToExpression("(/(-(A);-(B)))")
        assertFalse(expression1.patternTooManyLevelsInFraction())

        val expression2 = structureStringToExpression("(/(-(pi);-(2)))")
        assertFalse(expression2.patternTooManyLevelsInFraction())

        val expression3 = structureStringToExpression("(+(^(sin(+(-(pi;x)));2);^(sin(/(-(pi);-(2)));2)))")//sin(π-x)^2+sin(-π/-2-x)^2
        assertFalse(expression3.patternTooManyLevelsInFraction())

        val expression4 = structureStringToExpression("/(a;b)")
        assertFalse(expression4.patternTooManyLevelsInFraction())
    }

    @Test
    fun expressionContainsConstMulConst() {
        val expression1 = structureStringToExpression("(+(sin(x);*(1;A)))")
        assertTrue(expression1.patternConstMulConst())

        val expression2 = structureStringToExpression("*(1;3)")
        assertTrue(expression2.patternConstMulConst())
    }

    @Test
    fun expressionNotContainsConstMulConst() {
        val expression1 = structureStringToExpression("/(a;b)")
        assertFalse(expression1.patternConstMulConst())
    }

    @Test
    fun expressionContainsFraction() {
        val expression1 = structureStringToExpression("/(a;b)")
        assertTrue(expression1.patternFractionExist())

        val expression2 = structureStringToExpression("(+(/(1;/(1;/(1;cos(x))));^(cos(x);2)))")
        assertTrue(expression2.patternFractionExist())
    }

    @Test
    fun expressionNotContainsFraction() {
        val expression1 = structureStringToExpression("(+(sin(x);*(1;A)))")
        assertFalse(expression1.patternFractionExist())

        val expression2 = structureStringToExpression("+(+(-(+(-(1;^(cos(x);2)))));^(cos(x);2))")
        assertFalse(expression2.patternFractionExist())
    }

    @Test
    fun expressionContainsThreeLevels() { //have to add more
        val expression1 = structureStringToExpression("(+(/(1;/(1;/(1;cos(x))));^(cos(x);2)))")
        assertTrue(expression1.patternThreeLevelsExist())
    }

    @Test
    fun expressionNotContainsThreeLevels() { //have to add more
        val expression1 = structureStringToExpression("/(a;b)")
        assertFalse(expression1.patternThreeLevelsExist())
    }

    @Test
    fun expressionContainsTooManyLevels() {
        val expression1 = structureStringToExpression("(+(/(1;/(1;/(1;cos(x))));^(cos(x);2)))")
        assertTrue(expression1.patternTooManyLevelsExist())
    }

    @Test
    fun expressionNotContainsTooManyLevels() {
        val expression1 = structureStringToExpression("(/(-(A);-(B)))")
        assertFalse(expression1.patternTooManyLevelsExist())

        val expression2 = structureStringToExpression("(/(-(pi);-(2)))")
        assertFalse(expression2.patternTooManyLevelsExist())

        val expression3 = structureStringToExpression("(+(^(sin(+(-(pi;x)));2);^(sin(/(-(pi);-(2)));2)))")//sin(π-x)^2+sin(-π/-2-x)^2
        assertFalse(expression3.patternTooManyLevelsExist())

        val expression4 = structureStringToExpression("/(a;b)")
        assertFalse(expression4.patternTooManyLevelsExist())
    }
}
