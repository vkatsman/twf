package mathhelper.twf.expressiontree

import mathhelper.twf.api.stringToExpression
import mathhelper.twf.api.structureStringToExpression
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.FunctionConfiguration
import org.junit.Ignore
import org.junit.Test

import kotlin.test.assertEquals

internal class ExpressionStructureCheckerTest {

    val expressionStructureConditionConstructor = ExpressionStructureConditionConstructor(CompiledConfiguration(functionConfiguration = FunctionConfiguration()))

    @Test
    fun parse3KNF() {
        val node = expressionStructureConditionConstructor.parse("and : (or : 3)")
//        val gson = GsonBuilder().serializeSpecialFloatingPointValues().create()
//        val nodeJson = gson.toJson(node)
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[,childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,,children=[[count=[[[3.0;3.0]:N]],]] }]]] }", node.toString())
    }

    @Test
    fun check3KNF() {
        val node = expressionStructureConditionConstructor.parse("and : (or : 3)")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[,childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,,children=[[count=[[[3.0;3.0]:N]],]] }]]] }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("(a|b|x1)&(x2|c|x1)&(x2|yx5|x1)", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("(a|b|x1)&(x2|c|x1)&(x2|yx5|x1|x2)", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("(a|x1)&(x2|c|x1)&(x2|yx5|x1)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a|b|x1)&(x2|c|x1)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a|b|x1)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("a|b|x1", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a|b", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a|b|b|a", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a&b", scope = "setTheory"), node))
    }

    @Test
    fun check1_3KNF() {
        val node = expressionStructureConditionConstructor.parse("and : (or : 1-3)")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[,childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,,children=[[count=[[[1.0;3.0]:N]],]] }]]] }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("(a|b|x1)&(x2|c|x1)&(x2|yx5|x1)", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("(a|b|x1)&(x2|c|x1)&(x2|yx5|x1|x2)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a|x1)&(x2|c|x1)&(x2|yx5|x1)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a|b|x1)&(x2|c|x1)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a|b|x1)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("a|b|x1", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("a|b", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a|b|b|a", scope = "setTheory"), node))
//        assertEquals(true, checkExpressionStructure(stringToExpression("a&b", scope = "setTheory"), node))  //TODO: fix
    }

    @Test
    fun checkKNF() {
        val node = expressionStructureConditionConstructor.parse("and : (or : ?)")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[,childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,, }]]] }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("a&b", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a->b", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a&(a->b)", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a&(c|(a->b))", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a|b|x1)&(x2|c|x1)&(x2|yx5|x1)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a|b|x1)&(x2|c|x1)&(x2|yx5|x1|x2)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a|x1)&(x2|c|x1)&(x2|yx5|x1)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a|b|x1)&(x2|c|x1)", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("(a|b|x1)&(x2|c&y|x1)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a|b|x1)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("a|b|x1", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("a|b", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("a|b|b|a", scope = "setTheory"), node))
    }

    @Test
    fun parseKNF_short() {
        val node = expressionStructureConditionConstructor.parse("and : (or:)")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[,childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,, }]]] }", node.toString())
    }

    @Test
    fun parseKNF_shortest() {
        val node = expressionStructureConditionConstructor.parse("and:(or)")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[,childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,, }]]] }", node.toString())
    }

    @Test
    fun check04DNF_shortest() {
        val node = expressionStructureConditionConstructor.parse("or:?-4(and):::not")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,,children=[[count=[[[-Inf;4.0]:N]],childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,,functionsJoinedWithVariables=[not] }]]],functionsJoinedWithVariables=[not] }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("!a&b&!c", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("!(a&b&c)", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("!(a|b|c)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("!a", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("a&b&c", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("a|!b|!b|a", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a|b|!b|a|c", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("a|b", scope = "setTheory"), node))
    }

    @Test
    fun check04_23DNF_shortest() {
        val node = expressionStructureConditionConstructor.parse("or:?-4(and:2-3)")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,,children=[[count=[[[-Inf;4.0]:N]],childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[count=[[[2.0;3.0]:N]],]] }]]] }", node.toString())

        assertEquals(false, checkExpressionStructure(stringToExpression("a|b|b|a", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a&c)|(a&c)|(c&c&c)|(a&c)", scope = "setTheory"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("(a&c)|(a&c)|(c&c&c)", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a|b|b|a|c", scope = "setTheory"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a+c", scope = "setTheory"), node))
    }

    @Test
    fun parse04_2_2_3_2DNF() {
        val node = expressionStructureConditionConstructor.parse("or:?-4(and:2-3);1(and:4-?)")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,,children=[[count=[[[-Inf;4.0]:N]],childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[count=[[[2.0;3.0]:N]],]] }]], [count=[[[1.0;1.0]:N]],childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[count=[[[4.0;Inf]:N]],]] }]]] }", node.toString())
    }

    @Test

    fun parse04_2_1_DNF() {
        val node = expressionStructureConditionConstructor.parse("or:?-4(and:2-3)(xor:3-?);1(and:4-?)")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,,children=[[count=[[[-Inf;4.0]:N]],childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[count=[[[2.0;3.0]:N]],]] }, { nodeFunctions=[treePermittedFunctions=[[name=xor,,,]],],,,children=[[count=[[[3.0;Inf]:N]],]] }]], [count=[[[1.0;1.0]:N]],childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,,children=[[count=[[[4.0;Inf]:N]],]] }]]] }", node.toString())
    }

    @Test
    fun parse_no_A() {
        val node = expressionStructureConditionConstructor.parse("?:?:?:!A")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[,,,]],],treeFunctions=[treePermittedFunctions=[[,,,]],],treeVariables=[,treeForbiddenVariables=[[,variableName=A]]], }", node.toString())
    }

    @Test
    fun parse_summ_of_two() {
        val node = expressionStructureConditionConstructor.parse("+ : 2")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=+,,,]],],,,children=[[count=[[[2.0;2.0]:N]],]] }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("a+b"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a*b"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("f(1,2)"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("7.7+a"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("7.7-a"), node))
    }

    @Test
    fun parse_sin_of_x() {
        val node = expressionStructureConditionConstructor.parse("sin : 1")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=sin,,,]],],,,children=[[count=[[[1.0;1.0]:N]],]] }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("sin(b)"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("sin(a*b)"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("b"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("sin(4)"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("7.7-a"), node))
    }

    @Test
    fun parse_no_cos_of_two() {
        val node = expressionStructureConditionConstructor.parse("!cos,!sin,!tg,!ctg,!- : ? : ?")
        assertEquals("{ nodeFunctions=[,treeForbiddenFunctions=[[name=cos,,,], [name=sin,,,], [name=tg,,,], [name=ctg,,,], [name=-,,,]]],treeFunctions=[treePermittedFunctions=[[,,,]],],, }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("a/b"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("sqrt(3)/2"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("3^0.5/2"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("cos(a/b)"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("cos(0)"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("tg(a/b)"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("sin(0)"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("-sin(0)"), node))
    }

    @Test
    fun parse_tg_of_multiplication_of_two() {
        val node = expressionStructureConditionConstructor.parse("tg,* : (* : 2)")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=tg,,,], [name=*,,,]],],,,children=[[,childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=*,,,]],],,,children=[[count=[[[2.0;2.0]:N]],]] }]]] }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("tg(a*b)"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("tg(2*b)"), node))
        assertEquals(false, checkExpressionStructure(structureStringToExpression("(*(*(a;b)))"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("tg(2*(b+7))"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("tg(2+b)"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("tg(b)"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a+b"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a*b"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("tg(2*b*f)"), node))
    }

    @Test
    fun parse_one_variable() {
        val node = expressionStructureConditionConstructor.parse("?:0:?:?")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[,,,]],],treeFunctions=[treePermittedFunctions=[[,,,]],],,children=[[count=[[[0.0;0.0]:N]],]] }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("a"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("7*8"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("f(1,2)"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("7"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("7.7"), node))
    }

    @Test
    fun parse_natural_number() {
        val node = expressionStructureConditionConstructor.parse("?:0:?:?N")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[,,,]],],treeFunctions=[treePermittedFunctions=[[,,,]],],treeVariables=[treePermittedVariables=[[intervals=[[-Inf;Inf]:N],]],],children=[[count=[[[0.0;0.0]:N]],]] }", node.toString())

        assertEquals(false, checkExpressionStructure(stringToExpression("a"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("7*8"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("7"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("0"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("+7"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("7.7"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("-7"), node))
    }

    @Test
    fun parse_Z_number() {
        val node = expressionStructureConditionConstructor.parse("+:0-1(-:1):?:?Z")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=+,,,]],],treeFunctions=[treePermittedFunctions=[[,,,]],],treeVariables=[treePermittedVariables=[[intervals=[[-Inf;Inf]:Z],]],],children=[[count=[[[0.0;1.0]:N]],childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=-,,,]],],,treeVariables=[treePermittedVariables=[[intervals=[[-Inf;Inf]:Z],]],],children=[[count=[[[1.0;1.0]:N]],]] }]]] }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("-7"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("a"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("7*8"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("7"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("7.7"), node))
    }

    @Test
    fun parse_real_number() {
        val node = expressionStructureConditionConstructor.parse("+:0-1(-:1):?:?R")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=+,,,]],],treeFunctions=[treePermittedFunctions=[[,,,]],],treeVariables=[treePermittedVariables=[[intervals=[[-Inf;Inf]:R],]],],children=[[count=[[[0.0;1.0]:N]],childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=-,,,]],],,treeVariables=[treePermittedVariables=[[intervals=[[-Inf;Inf]:R],]],],children=[[count=[[[1.0;1.0]:N]],]] }]]] }", node.toString())

        assertEquals(false, checkExpressionStructure(stringToExpression("a"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("7*8"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("7"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("1"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("7.7"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("-7"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("-7.7"), node))

        // TODO: vlad_u -> enrich patterns to check 1/4 (not only 0.25)
        //
    }

    @Test
    @Ignore
    fun parse_real_number_problem_todo() {
        val node = expressionStructureConditionConstructor.parse("?:0:?:?R")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[,,,]],],treeFunctions=[treePermittedFunctions=[[,,,]],],treeVariables=[treePermittedVariables=[[intervals=[[-Inf;Inf]:R],]],],children=[[count=[[[0.0;0.0]:N]],]] }", node.toString())

        assertEquals(true, checkExpressionStructure(stringToExpression("-7"), node))  //fails because of complete tree structure: (+(-(7))) instead of (-7) - TODO: invent something
    }

    @Test
    fun parse_no_d_only_A_and_F() {
        val node = expressionStructureConditionConstructor.parse("?:?:?:A,F")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[,,,]],],treeFunctions=[treePermittedFunctions=[[,,,]],],treeVariables=[treePermittedVariables=[[,variableName=A], [,variableName=F]],], }", node.toString())

        assertEquals(false, checkExpressionStructure(stringToExpression("a*d"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("U/E"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("A/F"), node))
    }

    @Test
    fun parse_no_d_only_e_p_q_Fa() {
        val node = expressionStructureConditionConstructor.parse("?:?:?:e,p,q,ν,Fₐ")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[,,,]],],treeFunctions=[treePermittedFunctions=[[,,,]],],treeVariables=[treePermittedVariables=[[,variableName=e], [,variableName=p], [,variableName=q], [,variableName=ν], [,variableName=Fₐ]],], }", node.toString())


        assertEquals(false, checkExpressionStructure(structureStringToExpression("(/(^(υ;2);R))"), node))
        assertEquals(true, checkExpressionStructure(structureStringToExpression("(/(^(ν;2);p))"), node))
        assertEquals(false, checkExpressionStructure(structureStringToExpression("(/(^(ν;2);R))"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("1-r"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("1-q+Fₐ"), node))
        assertEquals(true, checkExpressionStructure(stringToExpression("p/q"), node))
        assertEquals(true, checkExpressionStructure(structureStringToExpression("(mod(^(e;-(1));*(+(p;-(1));+(q;-(1)))))"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("U/E"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("q/E"), node))
        assertEquals(false, checkExpressionStructure(stringToExpression("q*p/(E*2)-e"), node))
    }

    @Test
    fun parse04_23DNF_no_A() {
        val node = expressionStructureConditionConstructor.parse("or:?-4(and:2-3):?:!A")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],treeFunctions=[treePermittedFunctions=[[,,,]],],treeVariables=[,treeForbiddenVariables=[[,variableName=A]]],children=[[count=[[[-Inf;4.0]:N]],childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,treeVariables=[,treeForbiddenVariables=[[,variableName=A]]],children=[[count=[[[2.0;3.0]:N]],]] }]]] }", node.toString())
    }

    @Test
    fun parse04_23DNF_no_f_no_A() {
        val node = expressionStructureConditionConstructor.parse("or:?-4(and:2-3)::!A")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=or,,,]],],,treeVariables=[,treeForbiddenVariables=[[,variableName=A]]],children=[[count=[[[-Inf;4.0]:N]],childNodes=[{ nodeFunctions=[treePermittedFunctions=[[name=and,,,]],],,treeVariables=[,treeForbiddenVariables=[[,variableName=A]]],children=[[count=[[[2.0;3.0]:N]],]] }]]] }", node.toString())
    }

    @Test
    @Ignore
    fun parse_factorization() {
        val node = expressionStructureConditionConstructor.parse("*:2-?(!*:?:?):+,-,*,^(:1-?)")
        assertEquals("{ nodeFunctions=[treePermittedFunctions=[[name=*,,,]],],treeFunctions=[treePermittedFunctions=[[name=+,,,], [name=-,,,], [name=*,,,], [name=^,,,internalVariableCondition=[treePermittedVariables=[[intervals=[[1.0;Inf]:N],]],]]],],,children=[[count=[[[2.0;Inf]:N]],childNodes=[{ nodeFunctions=[,treeForbiddenFunctions=[[name=*,,,]]],treeFunctions=[treePermittedFunctions=[[,,,], [name=+,,,], [name=-,,,], [name=*,,,], [name=^,,,internalVariableCondition=[treePermittedVariables=[[intervals=[[1.0;Inf]:N],]],]]],],, }]]] }", node.toString())
    }

    @Test
    fun parse_reduceFraction() {
        val node = expressionStructureConditionConstructor.parse("!/:?:+,-,*,^(:1-?)")
        assertEquals("{ nodeFunctions=[,treeForbiddenFunctions=[[name=/,,,]]],treeFunctions=[treePermittedFunctions=[[name=+,,,], [name=-,,,], [name=*,,,], [name=^,,,internalVariableCondition=[treePermittedVariables=[[intervals=[[1.0;Inf]:N],]],]]],],, }", node.toString())
    }
}