package mathhelper.twf.expressiontree

import mathhelper.twf.api.*
import org.junit.Ignore
import kotlin.test.assertEquals
import org.junit.Test

import kotlin.test.assertEquals
import mathhelper.twf.substitutiontests.parseStringExpression

class ExpressionRuleGenerationBySelectedPointsTest {
    val generalCompiledConfiguration = createCompiledConfigurationFromExpressionSubstitutionsAndParams(
            listOf(
                    expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                    expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                    expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                    expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                    expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                    expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                    expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic", priority = 5),
                    expressionSubstitutionFromStructureStrings(code = "TwoSidesArithmeticReduce", priority = 5),
                    expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                    expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                    expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                    expressionSubstitutionFromStructureStrings(code = "MinusInOutBrackets", priority = 61),
                    expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
    )


    val setCompiledConfiguration = createCompiledConfigurationFromExpressionSubstitutionsAndParams(
            listOf(
                    expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                    expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                    expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                    expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                    expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                    expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                    expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                    expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic", priority = 5),
                    expressionSubstitutionFromStructureStrings(code = "TwoSidesArithmeticReduce", priority = 5),
                    expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                    expressionSubstitutionFromStructureStrings(code = "SetComplicatingExtension", priority = 60),
                    expressionSubstitutionFromStructureStrings(code = "AbsorptionComplicatingExtension", priority = 60),
                    expressionSubstitutionFromStructureStrings(code = "MinusInOutBrackets", priority = 61),
                    expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT),
                    expressionSubstitutionFromStructureStrings(code = "ZeroComputation")).toTypedArray()
    )

    @Test
    fun findLowestSubtreeOfNodesEmptySelection() {
        val expression = parseStringExpression("a+b+c+d+e")
        assertEquals("(+(a;b;c;d;e))", expression.toString())
        val selectedPlaces = listOf<ExpressionNode>()
        val subExpression = expression.findLowestSubtreeWithNodes(selectedPlaces, false)
        assertEquals(null, subExpression)
    }

    @Test
    fun findLowestSubtreeOfNodesSimple() {
        val expression = parseStringExpression("a+b+c+d+e")
        assertEquals("(+(a;b;c;d;e))", expression.toString())
        val selectedPlaces = listOf(
                expression.children[0].children[1],
                expression.children[0].children[2],
                expression.children[0].children[4]
        )
        val subExpression = expression.findLowestSubtreeWithNodes(selectedPlaces, false)
        assertEquals("+(b;c;e)", subExpression.toString())
    }

    @Test
    fun findLowestSubtreeOfNodesSimpleSum() {
        val expression = parseStringExpression("(a+b)+3+(sin(c*d))+e")
        assertEquals("(+(+(a;b);3;sin(*(c;d));e))", expression.toString())
        val selectedPlaces = listOf(
                expression.children[0].children[0].children[1],
                expression.children[0].children[2].children[0].children[0]
        )
        val subExpression = expression.findLowestSubtreeWithNodes(selectedPlaces, false)
        assertEquals("+(+(b);sin(*(c)))", subExpression.toString())
    }

    @Test
    fun findLowestSubtreeTopOfNodesSimpleSum() {
        val expression = parseStringExpression("x*((a+b)+3+(sin(c*d))+e)")
        assertEquals("(*(x;+(+(a;b);3;sin(*(c;d));e)))", expression.toString())
        val selectedPlaces = listOf(
                expression.children[0].children[1].children[0].children[1],
                expression.children[0].children[1].children[2].children[0].children[0]
        )
        val subExpression = expression.findLowestSubtreeTopOfNodes(selectedPlaces)
        assertEquals("+(+(a;b);3;sin(*(c;d));e)", subExpression.toString())
    }

    @Test
    fun findLowestSubtreeByNodeIds() {
        val expression = parseStringExpression("a+b+c+d+e")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    a  :  [2]\n" +
                "    b  :  [3]\n" +
                "    c  :  [4]\n" +
                "    d  :  [5]\n" +
                "    e  :  [6]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3, 4, 6).toTypedArray()
        val selectedPlaces = expression.nodeIdsToNodeLinksInSameOrder(selectedNodeIds)
        val subExpression = expression.findLowestSubtreeWithNodes(selectedPlaces, false)
        assertEquals("+(b;c;e)", subExpression.toString())
    }

    @Test
    fun findLowestSubtreeOfNodesInOriginalOrder() {
        val expression = parseStringExpression("a+b+c+d+e")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    a  :  [2]\n" +
                "    b  :  [3]\n" +
                "    c  :  [4]\n" +
                "    d  :  [5]\n" +
                "    e  :  [6]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(4, 6, 3).toTypedArray()
        val selectedPlaces = expression.nodeIdsToNodeLinksInSameOrder(selectedNodeIds)
        val nodeIdsPositionsMap = nodeIdsPositionsMap(selectedNodeIds)
        val subExpression = expression.findLowestSubtreeWithNodes(selectedPlaces, false)
        assertEquals("+(b;c;e)", subExpression.toString())
        subExpression!!.sortChildrenByNodeIdsOrder(nodeIdsPositionsMap)
        assertEquals("+(c;e;b)", subExpression.toString())
    }

    @Test
    fun findLowestSubtreeOfNodesInOriginalOrderBigTree() {
        val expression = parseStringExpression("(a+6-r)+b+(c*6*log(x))+d+cos(e)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    +  :  [2]\n" +
                "      a  :  [3]\n" +
                "      6  :  [4]\n" +
                "      -  :  [5]\n" +
                "        r  :  [6]\n" +
                "    b  :  [7]\n" +
                "    *  :  [8]\n" +
                "      c  :  [9]\n" +
                "      6  :  [10]\n" +
                "      ln  :  [11]\n" +
                "        x  :  [12]\n" +
                "    d  :  [13]\n" +
                "    cos  :  [14]\n" +
                "      e  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(11, 9).toTypedArray()
        val selectedPlaces = expression.nodeIdsToNodeLinksInSameOrder(selectedNodeIds)
        val nodeIdsPositionsMap = nodeIdsPositionsMap(selectedNodeIds)
        val subExpression = expression.findLowestSubtreeWithNodes(selectedPlaces, false)
        assertEquals("*(c;ln(x))", subExpression.toString())
        subExpression!!.sortChildrenByNodeIdsOrder(nodeIdsPositionsMap)
        assertEquals("*(ln(x);c)", subExpression.toString())
    }

    @Test
    fun findLowestSubtreeOfNodesInOriginalOrderBigAdditionTreeMultiLevelSelection() {
        val expression = parseStringExpression("a+b+(s+6+8)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    a  :  [2]\n" +
                "    b  :  [3]\n" +
                "    +  :  [4]\n" +
                "      s  :  [5]\n" +
                "      6  :  [6]\n" +
                "      8  :  [7]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(7, 5, 3).toTypedArray()
        val selectedPlaces = expression.nodeIdsToNodeLinksInSameOrder(selectedNodeIds)
        val nodeIdsPositionsMap = nodeIdsPositionsMap(selectedNodeIds)
        val subExpression = expression.findLowestSubtreeWithNodes(selectedPlaces, false)
        assertEquals("+(b;+(s;8))", subExpression.toString())
        subExpression!!.sortChildrenByNodeIdsOrder(nodeIdsPositionsMap)
        assertEquals("+(b;+(8;s))", subExpression.toString()) //to make order inn which it was specified also parent ids must be specified in order list
    }

    @Test
    fun findLowestSubtreeOfNodesInOriginalOrderBigTreeMultiLevelSelection() {
        val expression = parseStringExpression("(a+6-r)+b+(c*6*log(x))+d+cos(e)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    +  :  [2]\n" +
                "      a  :  [3]\n" +
                "      6  :  [4]\n" +
                "      -  :  [5]\n" +
                "        r  :  [6]\n" +
                "    b  :  [7]\n" +
                "    *  :  [8]\n" +
                "      c  :  [9]\n" +
                "      6  :  [10]\n" +
                "      ln  :  [11]\n" +
                "        x  :  [12]\n" +
                "    d  :  [13]\n" +
                "    cos  :  [14]\n" +
                "      e  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2, 14, 7, 11, 9).toTypedArray()
        val selectedPlaces = expression.nodeIdsToNodeLinksInSameOrder(selectedNodeIds)
        val nodeIdsPositionsMap = nodeIdsPositionsMap(selectedNodeIds)
        val subExpression = expression.findLowestSubtreeWithNodes(selectedPlaces, false)
        assertEquals("+(+(a;6;-(r));b;*(c;ln(x));cos(e))", subExpression.toString())
        subExpression!!.sortChildrenByNodeIdsOrder(nodeIdsPositionsMap)
        assertEquals("+(+(a;6;-(r));b;*(ln(x);c);cos(e))", subExpression.toString()) //to make order inn which it was specified also parent ids must be specified in order list
        assertEquals(1, subExpression.linkOnOriginalTreeNode!!.nodeId)
    }

    @Test
    fun findLowestSubtreeOfNodesInOriginalOrderBigTreeMultiLevelMultiNodeSelection() {
        val expression = parseStringExpression("(a+6-r)+b+(c*6*log(x))+d+cos(e)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    +  :  [2]\n" +
                "      a  :  [3]\n" +
                "      6  :  [4]\n" +
                "      -  :  [5]\n" +
                "        r  :  [6]\n" +
                "    b  :  [7]\n" +
                "    *  :  [8]\n" +
                "      c  :  [9]\n" +
                "      6  :  [10]\n" +
                "      ln  :  [11]\n" +
                "        x  :  [12]\n" +
                "    d  :  [13]\n" +
                "    cos  :  [14]\n" +
                "      e  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2, 14, 7, 11, 9, 8).toTypedArray()
        val selectedPlaces = expression.nodeIdsToNodeLinksInSameOrder(selectedNodeIds)
        val nodeIdsPositionsMap = nodeIdsPositionsMap(selectedNodeIds)
        val subExpression = expression.findLowestSubtreeWithNodes(selectedPlaces, false)
        assertEquals("+(+(a;6;-(r));b;*(c;6;ln(x));cos(e))", subExpression.toString())
        subExpression!!.sortChildrenByNodeIdsOrder(nodeIdsPositionsMap)
        assertEquals("+(+(a;6;-(r));cos(e);b;*(c;6;ln(x)))", subExpression.toString())
    }

    @Test
    fun findLowestSubtreeOfNodesInOriginalOrderBigTreeMultiLevelMultiNodeSelectionOnlyHigh() {
        val expression = parseStringExpression("(a+6-r)+b+(c*6*log(x))+d+cos(e)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    +  :  [2]\n" +
                "      a  :  [3]\n" +
                "      6  :  [4]\n" +
                "      -  :  [5]\n" +
                "        r  :  [6]\n" +
                "    b  :  [7]\n" +
                "    *  :  [8]\n" +
                "      c  :  [9]\n" +
                "      6  :  [10]\n" +
                "      ln  :  [11]\n" +
                "        x  :  [12]\n" +
                "    d  :  [13]\n" +
                "    cos  :  [14]\n" +
                "      e  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2, 14, 7, 11, 9, 8).toTypedArray()
        val selectedPlaces = expression.nodeIdsToNodeLinksInSameOrder(selectedNodeIds)
        val nodeIdsPositionsMap = nodeIdsPositionsMap(selectedNodeIds)
        val subExpression = expression.findLowestSubtreeWithNodes(selectedPlaces, true)
        assertEquals("+(+();b;*(c;ln());cos())", subExpression.toString())
        subExpression!!.sortChildrenByNodeIdsOrder(nodeIdsPositionsMap)
        assertEquals("+(+();cos();b;*(ln();c))", subExpression.toString())
    }

    @Test
    fun fillSubstitutionSelectionData() {
        val expression = parseStringExpression("(a+6-r)+b+(c*6*log(x))+d+cos(e)+(g-f+sin(t))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    +  :  [2]\n" +
                "      a  :  [3]\n" +
                "      6  :  [4]\n" +
                "      -  :  [5]\n" +
                "        r  :  [6]\n" +
                "    b  :  [7]\n" +
                "    *  :  [8]\n" +
                "      c  :  [9]\n" +
                "      6  :  [10]\n" +
                "      ln  :  [11]\n" +
                "        x  :  [12]\n" +
                "    d  :  [13]\n" +
                "    cos  :  [14]\n" +
                "      e  :  [15]\n" +
                "    +  :  [16]\n" +
                "      g  :  [17]\n" +
                "      -  :  [18]\n" +
                "        f  :  [19]\n" +
                "      sin  :  [20]\n" +
                "        t  :  [21]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2, 14, 7, 11, 9, 8).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("+(+(a;6;-(r));b;*(c;6;ln(x));cos(e))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("+(+();b;*(c;ln());cos())", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("+(place_for_result();d;+(g;-(f);sin(t)))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("+(+(a;6;-(r));b;*(c;6;ln(x));cos(e))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("+(+(a;6;-(r));cos(e);b;*(c;6;ln(x)))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals("+(d;+(g;-(f);sin(t)))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())
    }

    @Test
    fun fillSubstitutionSelectionDataInternalSelections() {
        val expression = parseStringExpression("(a+6-r)+b+(c*6*log(x))+d+cos(e)+(g-f+sin(t))")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    +  :  [2]\n" +
                "      a  :  [3]\n" +
                "      6  :  [4]\n" +
                "      -  :  [5]\n" +
                "        r  :  [6]\n" +
                "    b  :  [7]\n" +
                "    *  :  [8]\n" +
                "      c  :  [9]\n" +
                "      6  :  [10]\n" +
                "      ln  :  [11]\n" +
                "        x  :  [12]\n" +
                "    d  :  [13]\n" +
                "    cos  :  [14]\n" +
                "      e  :  [15]\n" +
                "    +  :  [16]\n" +
                "      g  :  [17]\n" +
                "      -  :  [18]\n" +
                "        f  :  [19]\n" +
                "      sin  :  [20]\n" +
                "        t  :  [21]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3, 6, 7, 11, 9, 18).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("+(+(a;-(r));b;*(c;ln(x));+(-(f)))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("+(+(a;-(r));b;*(c;ln());+(-()))", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("+(place_for_result();6;d;cos(e);g;sin(t))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString(), substitutionSelectionData.toString())
        assertEquals("+(a;-(r);b;*(c;6;ln(x));-(f))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("+(a;-(r);b;*(c;6;ln(x));-(f))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals("+(+(6);d;cos(e);+(g;sin(t)))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString()) //TODO: add function to normalize such commutative functions '+(6)' '*(6)'

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: 'a+6-r+b+c*6*ln(x)+d+cos(e)+g-f+sin(t)'\n" +
                "expressionSubstitution.left: '(+(+(a;6;-(r));b;*(c;6;ln(x));d;cos(e);+(g;-(f);sin(t))))'\n" +
                "expressionSubstitution.right: '(+(a;6;-(r);b;*(c;6;ln(x));d;cos(e);g;-(f);sin(t)))'\n" +
                "originalExpression: '(+(+(a;6;-(r));b;*(c;6;ln(x));d;cos(e);+(g;-(f);sin(t))))'\n" +
                "originalExpressionChangingPart: '+(+(a;6;-(r));b;*(c;6;ln(x));d;cos(e);+(g;-(f);sin(t)))'\n" +
                "resultExpression: '(+(a;6;-(r);b;*(c;6;ln(x));d;cos(e);g;-(f);sin(t)))'\n" +
                "resultExpressionChangingPart: '+(a;6;-(r);b;*(c;6;ln(x));d;cos(e);g;-(f);sin(t))'\n" +
                "substitutionType: 'OpeningBrackets'\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '(a-r+b+c*6*ln(x)-f)+6+d+cos(e)+g+sin(t)'\n" +
                "expressionSubstitution.left: '(+(a;-(r);b;*(c;6;ln(x));-(f)))'\n" +
                "expressionSubstitution.right: '(+(+(a;-(r);b;*(c;6;ln(x));-(f))))'\n" +
                "originalExpression: '(+(+(a;6;-(r));b;*(c;6;ln(x));d;cos(e);+(g;-(f);sin(t))))'\n" +
                "originalExpressionChangingPart: '+(a;-(r);b;*(c;6;ln(x));-(f))'\n" +
                "resultExpression: '(+(+(a;-(r);b;*(c;6;ln(x));-(f));6;d;cos(e);g;sin(t)))'\n" +
                "resultExpressionChangingPart: '+(+(a;-(r);b;*(c;6;ln(x));-(f)))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '90'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun fillSubstitutionAndRuleTransformationsSelectionDataInternalDeepMinusSelections() {
        val expression = parseStringExpression("t-(r-(6*y-r+8)-(-g+5-(v-3*w)))-(4*y+5-(y+6-(2*y-(4-(t-b-8*y))))-u)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    t  :  [2]\n" +
                "    -  :  [3]\n" +
                "      +  :  [4]\n" +
                "        r  :  [5]\n" +
                "        -  :  [6]\n" +
                "          +  :  [7]\n" +
                "            *  :  [8]\n" +
                "              6  :  [9]\n" +
                "              y  :  [10]\n" +
                "            -  :  [11]\n" +
                "              r  :  [12]\n" +
                "            8  :  [13]\n" +
                "        -  :  [14]\n" +
                "          +  :  [15]\n" +
                "            -  :  [16]\n" +
                "              g  :  [17]\n" +
                "            5  :  [18]\n" +
                "            -  :  [19]\n" +
                "              +  :  [20]\n" +
                "                v  :  [21]\n" +
                "                -  :  [22]\n" +
                "                  *  :  [23]\n" +
                "                    3  :  [24]\n" +
                "                    w  :  [25]\n" +
                "    -  :  [26]\n" +
                "      +  :  [27]\n" +
                "        *  :  [28]\n" +
                "          4  :  [29]\n" +
                "          y  :  [30]\n" +
                "        5  :  [31]\n" +
                "        -  :  [32]\n" +
                "          +  :  [33]\n" +
                "            y  :  [34]\n" +
                "            6  :  [35]\n" +
                "            -  :  [36]\n" +
                "              +  :  [37]\n" +
                "                *  :  [38]\n" +
                "                  2  :  [39]\n" +
                "                  y  :  [40]\n" +
                "                -  :  [41]\n" +
                "                  +  :  [42]\n" +
                "                    4  :  [43]\n" +
                "                    -  :  [44]\n" +
                "                      +  :  [45]\n" +
                "                        t  :  [46]\n" +
                "                        -  :  [47]\n" +
                "                          b  :  [48]\n" +
                "                        -  :  [49]\n" +
                "                          *  :  [50]\n" +
                "                            8  :  [51]\n" +
                "                            y  :  [52]\n" +
                "        -  :  [53]\n" +
                "          u  :  [54]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(50, 40, 34, 8, 28).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "TwoSidesArithmeticReduce", priority = 5),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(^(sin(x);2))", "(^(sin(x);2))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));y;-(*(2;a));*(8;a)))", "(+(1;-(^(cos(x);2))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", "(*(9;a))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(a;-(b);y;d;h))", "(+(h;a;-(b);y;d))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("+(-(+(-(+(*(6;y)))));-(+(*(4;y);-(+(y;-(+(*(y);-(+(-(+(-(*(8;y)))))))))))))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("+(-(+(-(+(*()))));-(+(*();-(+(y;-(+(*(y);-(+(-(+(-(*()))))))))))))", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("+(place_for_result();t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u)", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y)))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals("+(t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u))))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = findConfiguredSubstitutionsApplications(substitutionSelectionData, true, true)
        assertEquals(3, result.size)
        assertEquals("(+(*(6;a);-(*(4;a));y;-(*(2;a));*(8;a)))", result[0].expressionSubstitution.left.toString())
        assertEquals("(+(1;-(^(cos(x);2))))", result[0].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", result[0].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", result[0].originalExpressionChangingPart.toString())
        assertEquals("(+(+(1;-(^(cos(x);2)));t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u))", result[0].resultExpression.toString())
        assertEquals("+(1;-(^(cos(x);2)))", result[0].resultExpressionChangingPart.toString())

        assertEquals("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", result[1].expressionSubstitution.left.toString())
        assertEquals("(*(9;a))", result[1].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", result[1].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", result[1].originalExpressionChangingPart.toString())
        assertEquals("(+(*(9;y);t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u))", result[1].resultExpression.toString())
        assertEquals("*(9;y)", result[1].resultExpressionChangingPart.toString())

        assertEquals("(+(a;-(b);y;d;h))", result[2].expressionSubstitution.left.toString())
        assertEquals("(+(h;a;-(b);y;d))", result[2].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", result[2].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", result[2].originalExpressionChangingPart.toString())
        assertEquals("(+(+(*(8;y);*(6;y);-(*(4;y));y;-(*(2;y)));t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u))", result[2].resultExpression.toString())
        assertEquals("+(*(8;y);*(6;y);-(*(4;y));y;-(*(2;y)))", result[2].resultExpressionChangingPart.toString())


        val resultNotSimplified = findConfiguredSubstitutionsApplications(substitutionSelectionData, false, true)
        assertEquals(3, resultNotSimplified.size)
        assertEquals("(+(*(6;a);-(*(4;a));y;-(*(2;a));*(8;a)))", resultNotSimplified[0].expressionSubstitution.left.toString())
        assertEquals("(+(1;-(^(cos(x);2))))", resultNotSimplified[0].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", resultNotSimplified[0].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", resultNotSimplified[0].originalExpressionChangingPart.toString())
        assertEquals("(+(+(1;-(^(cos(x);2)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", resultNotSimplified[0].resultExpression.toString())
        assertEquals("+(1;-(^(cos(x);2)))", resultNotSimplified[0].resultExpressionChangingPart.toString())

        assertEquals("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", resultNotSimplified[1].expressionSubstitution.left.toString())
        assertEquals("(*(9;a))", resultNotSimplified[1].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", resultNotSimplified[1].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", resultNotSimplified[1].originalExpressionChangingPart.toString())
        assertEquals("(+(*(9;y);t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", resultNotSimplified[1].resultExpression.toString())
        assertEquals("*(9;y)", resultNotSimplified[1].resultExpressionChangingPart.toString())

        assertEquals("(+(a;-(b);y;d;h))", resultNotSimplified[2].expressionSubstitution.left.toString())
        assertEquals("(+(h;a;-(b);y;d))", resultNotSimplified[2].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", resultNotSimplified[2].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", resultNotSimplified[2].originalExpressionChangingPart.toString())
        assertEquals("(+(+(*(8;y);*(6;y);-(*(4;y));y;-(*(2;y)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", resultNotSimplified[2].resultExpression.toString())
        assertEquals("+(*(8;y);*(6;y);-(*(4;y));y;-(*(2;y)))", resultNotSimplified[2].resultExpressionChangingPart.toString())


        val permutationsResult = generatePermutationSubstitutions(substitutionSelectionData, true, true)
        assertEquals(2, permutationsResult.size)

        val permutationsResultNotSimplified = generatePermutationSubstitutions(substitutionSelectionData, false, true)
        assertEquals(2, permutationsResultNotSimplified.size)

        val reducedResult = generateReduceArithmeticSubstitutions(substitutionSelectionData, false, true)
        assertEquals(1, reducedResult.size)
        assertEquals("(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y)))", reducedResult[0].expressionSubstitution.left.toString())
        assertEquals("(*(y;+(8;-(2);1;6;-(4))))", reducedResult[0].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", reducedResult[0].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", reducedResult[0].originalExpressionChangingPart.toString())
        assertEquals("(+(*(y;+(8;-(2);1;6;-(4)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", reducedResult[0].resultExpression.toString())
        assertEquals("*(y;+(8;-(2);1;6;-(4)))", reducedResult[0].resultExpressionChangingPart.toString())


        val substitutionSelectionData2 = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "TwoSidesArithmeticReduce"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(^(sin(x);2))", "(^(sin(x);2))", code = "ConfiguredSubstitution1"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));y;-(*(2;a));*(8;a)))", "(+(1;-(^(cos(x);2))))", code = "ConfiguredSubstitution2"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", "(*(9;a))", code = "ConfiguredSubstitution3"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(a;-(b);y;d;h))", "(+(h;a;-(b);y;d))", code = "ConfiguredSubstitution4"))
        })
        val fullResult = generateSubstitutionsBySelectedNodes(substitutionSelectionData2, false, true)
        assertEquals(6, fullResult.size)
        assertEquals("(+(*(y;+(8;-(2);1;6;-(4)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", fullResult[0].resultExpression.toString())
        assertEquals("(+(+(1;-(^(cos(x);2)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", fullResult[1].resultExpression.toString())
        assertEquals("(+(*(9;y);t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", fullResult[2].resultExpression.toString())
        assertEquals("(+(+(*(8;y);*(6;y);-(*(4;y));y;-(*(2;y)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", fullResult[3].resultExpression.toString())
        assertEquals("(+(+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", fullResult[4].resultExpression.toString())
        assertEquals("(+(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", fullResult[5].resultExpression.toString())

        assertEquals("TwoSidesArithmeticReduce, ConfiguredSubstitution2, ConfiguredSubstitution3, ConfiguredSubstitution4, SelectedOrderExtraction, OriginalOrderExtraction", fullResult.map { it.substitutionType }.joinToString())
    }

    @Test
    fun permutationsTest() {
        val expression = parseStringExpression("t-(r-(6*y-r+8)-(-g+5-(v-3*w)))-(4*y+5-(y+6-(2*y-(4-(t-b-8*y))))-u)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    t  :  [2]\n" +
                "    -  :  [3]\n" +
                "      +  :  [4]\n" +
                "        r  :  [5]\n" +
                "        -  :  [6]\n" +
                "          +  :  [7]\n" +
                "            *  :  [8]\n" +
                "              6  :  [9]\n" +
                "              y  :  [10]\n" +
                "            -  :  [11]\n" +
                "              r  :  [12]\n" +
                "            8  :  [13]\n" +
                "        -  :  [14]\n" +
                "          +  :  [15]\n" +
                "            -  :  [16]\n" +
                "              g  :  [17]\n" +
                "            5  :  [18]\n" +
                "            -  :  [19]\n" +
                "              +  :  [20]\n" +
                "                v  :  [21]\n" +
                "                -  :  [22]\n" +
                "                  *  :  [23]\n" +
                "                    3  :  [24]\n" +
                "                    w  :  [25]\n" +
                "    -  :  [26]\n" +
                "      +  :  [27]\n" +
                "        *  :  [28]\n" +
                "          4  :  [29]\n" +
                "          y  :  [30]\n" +
                "        5  :  [31]\n" +
                "        -  :  [32]\n" +
                "          +  :  [33]\n" +
                "            y  :  [34]\n" +
                "            6  :  [35]\n" +
                "            -  :  [36]\n" +
                "              +  :  [37]\n" +
                "                *  :  [38]\n" +
                "                  2  :  [39]\n" +
                "                  y  :  [40]\n" +
                "                -  :  [41]\n" +
                "                  +  :  [42]\n" +
                "                    4  :  [43]\n" +
                "                    -  :  [44]\n" +
                "                      +  :  [45]\n" +
                "                        t  :  [46]\n" +
                "                        -  :  [47]\n" +
                "                          b  :  [48]\n" +
                "                        -  :  [49]\n" +
                "                          *  :  [50]\n" +
                "                            8  :  [51]\n" +
                "                            y  :  [52]\n" +
                "        -  :  [53]\n" +
                "          u  :  [54]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(50, 38, 34, 8, 28).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(^(sin(x);2))", "(^(sin(x);2))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));y;-(*(2;a));*(8;a)))", "(+(1;-(^(cos(x);2))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", "(*(9;a))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(a;-(b);y;d;e))", "(+(e;a;-(b);y;d))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("+(-(+(-(+(*(6;y)))));-(+(*(4;y);-(+(y;-(+(*(2;y);-(+(-(+(-(*(8;y)))))))))))))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("+(-(+(-(+(*()))));-(+(*();-(+(y;-(+(*();-(+(-(+(-(*()))))))))))))", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("+(place_for_result();t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u)", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y)))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals("+(t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u))))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())


        val permutationsResult = generatePermutationSubstitutions(substitutionSelectionData, true, true)
        assertEquals(2, permutationsResult.size)

        assertEquals("(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y)))", permutationsResult[0].expressionSubstitution.left.toString())
        assertEquals("(+(+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y)))))", permutationsResult[0].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResult[0].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", permutationsResult[0].originalExpressionChangingPart.toString())
        assertEquals("(+(+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y)));t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u))", permutationsResult[0].resultExpression.toString())
        assertEquals("+(+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y))))", permutationsResult[0].resultExpressionChangingPart.toString())

        assertEquals("(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y)))", permutationsResult[1].expressionSubstitution.left.toString())
        assertEquals("(+(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))))", permutationsResult[1].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResult[1].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", permutationsResult[1].originalExpressionChangingPart.toString())
        assertEquals("(+(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y));t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u))", permutationsResult[1].resultExpression.toString())
        assertEquals("+(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y)))", permutationsResult[1].resultExpressionChangingPart.toString())


        val permutationsResultNotSimplified = generatePermutationSubstitutions(substitutionSelectionData, false, true)
        assertEquals(2, permutationsResultNotSimplified.size)

        assertEquals("(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y)))", permutationsResultNotSimplified[0].expressionSubstitution.left.toString())
        assertEquals("(+(+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y)))))", permutationsResultNotSimplified[0].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResultNotSimplified[0].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", permutationsResultNotSimplified[0].originalExpressionChangingPart.toString())
        assertEquals("(+(+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", permutationsResultNotSimplified[0].resultExpression.toString())
        assertEquals("+(+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y))))", permutationsResultNotSimplified[0].resultExpressionChangingPart.toString())

        assertEquals("(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y)))", permutationsResultNotSimplified[1].expressionSubstitution.left.toString())
        assertEquals("(+(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))))", permutationsResultNotSimplified[1].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResultNotSimplified[1].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", permutationsResultNotSimplified[1].originalExpressionChangingPart.toString())
        assertEquals("(+(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))))", permutationsResultNotSimplified[1].resultExpression.toString())
        assertEquals("+(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y)))", permutationsResultNotSimplified[1].resultExpressionChangingPart.toString())


        val selectedNodeIdsNoPermutations = listOf(7, 2).toTypedArray()
        val substitutionSelectionNoPermutations = SubstitutionSelectionData(expression, selectedNodeIdsNoPermutations, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(^(sin(x);2))", "(^(sin(x);2))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));y;-(*(2;a));*(8;a)))", "(+(1;-(^(cos(x);2))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", "(*(9;a))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(a;-(b);y;d;e))", "(+(e;a;-(b);y;d))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionNoPermutations)
        assertEquals("+(t;-(+(-(+(*(6;y);-(r);8)))))", substitutionSelectionNoPermutations.lowestSubtree.toString())
        assertEquals("+(t;-(+(-(+()))))", substitutionSelectionNoPermutations.lowestSubtreeHigh.toString())
        assertEquals("+(place_for_result();-(r);+(-(g);5;-(+(v;-(*(3;w)))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u))))", substitutionSelectionNoPermutations.notSelectedSubtreeTopArguments.toString())
        assertEquals("+(t;+(*(6;y);-(r);8))", substitutionSelectionNoPermutations.selectedSubtreeTopArguments.toString())
        assertEquals("+(+(*(6;y);-(r);8);t)", substitutionSelectionNoPermutations.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals("+(-(+(r;-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u))))", substitutionSelectionNoPermutations.notSelectedSubtreeTopOriginalTree.toString())
        assertEquals(0, generatePermutationSubstitutions(substitutionSelectionNoPermutations, true, true).filter { it.substitutionType == "Swap" }.size)


        val selectedNodeIdsOnTopLevel = listOf(6, 14).toTypedArray()
        val substitutionSelectionDataOnTopLevel = SubstitutionSelectionData(expression, selectedNodeIdsOnTopLevel, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(^(sin(x);2))", "(^(sin(x);2))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));y;-(*(2;a));*(8;a)))", "(+(1;-(^(cos(x);2))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", "(*(9;a))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(a;-(b);y;d;e))", "(+(e;a;-(b);y;d))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionDataOnTopLevel)
        assertEquals("+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))))", substitutionSelectionDataOnTopLevel.lowestSubtree.toString())
        assertEquals("+(-();-())", substitutionSelectionDataOnTopLevel.lowestSubtreeHigh.toString())
        assertEquals("+(place_for_result();r)", substitutionSelectionDataOnTopLevel.notSelectedSubtreeTopArguments.toString())
        assertEquals("+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))))", substitutionSelectionDataOnTopLevel.selectedSubtreeTopArguments.toString())
        assertEquals("+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))))", substitutionSelectionDataOnTopLevel.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals("+(r)", substitutionSelectionDataOnTopLevel.notSelectedSubtreeTopOriginalTree.toString())


        val permutationsResultOnTopLevel = generatePermutationSubstitutions(substitutionSelectionDataOnTopLevel, true, true)
        assertEquals(2, permutationsResultOnTopLevel.size)
        assertEquals("(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))))", permutationsResultOnTopLevel[0].expressionSubstitution.left.toString())
        assertEquals("(+(r;-(+(-(g);5;-(+(v;-(*(3;w))))));-(+(*(6;y);-(r);8))))", permutationsResultOnTopLevel[0].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResultOnTopLevel[0].originalExpression.toString())
        assertEquals("+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))))", permutationsResultOnTopLevel[0].originalExpressionChangingPart.toString())
        assertEquals("(+(t;-(+(r;-(+(-(g);5;-(+(v;-(*(3;w))))));-(+(*(6;y);-(r);8))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResultOnTopLevel[0].resultExpression.toString())
        assertEquals("+(r;-(+(-(g);5;-(+(v;-(*(3;w))))));-(+(*(6;y);-(r);8)))", permutationsResultOnTopLevel[0].resultExpressionChangingPart.toString())

        assertEquals("(+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))))", permutationsResultOnTopLevel[1].expressionSubstitution.left.toString())
        assertEquals("(+(+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))))))", permutationsResultOnTopLevel[1].expressionSubstitution.right.toString()) //extracting to begin transformation
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResultOnTopLevel[1].originalExpression.toString())
        assertEquals("+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))))", permutationsResultOnTopLevel[1].originalExpressionChangingPart.toString())
        assertEquals("(+(t;-(+(+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))));r));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResultOnTopLevel[1].resultExpression.toString())
        assertEquals("+(+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))))", permutationsResultOnTopLevel[1].resultExpressionChangingPart.toString())


        val permutationsResultNotSimplifiedOnTopLevel = generatePermutationSubstitutions(substitutionSelectionDataOnTopLevel, false, true)
        assertEquals(2, permutationsResultNotSimplifiedOnTopLevel.size)
        assertEquals("(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))))", permutationsResultNotSimplifiedOnTopLevel[0].expressionSubstitution.left.toString())
        assertEquals("(+(r;-(+(-(g);5;-(+(v;-(*(3;w))))));-(+(*(6;y);-(r);8))))", permutationsResultNotSimplifiedOnTopLevel[0].expressionSubstitution.right.toString())
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResultNotSimplifiedOnTopLevel[0].originalExpression.toString())
        assertEquals("+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))))", permutationsResultNotSimplifiedOnTopLevel[0].originalExpressionChangingPart.toString())
        assertEquals("(+(t;-(+(r;-(+(-(g);5;-(+(v;-(*(3;w))))));-(+(*(6;y);-(r);8))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResultNotSimplifiedOnTopLevel[0].resultExpression.toString())
        assertEquals("+(r;-(+(-(g);5;-(+(v;-(*(3;w))))));-(+(*(6;y);-(r);8)))", permutationsResultNotSimplifiedOnTopLevel[0].resultExpressionChangingPart.toString())

        assertEquals("(+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))))", permutationsResultNotSimplifiedOnTopLevel[1].expressionSubstitution.left.toString())
        assertEquals("(+(+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))))))", permutationsResultNotSimplifiedOnTopLevel[1].expressionSubstitution.right.toString()) //extracting to begin transformation
        assertEquals("(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResultNotSimplifiedOnTopLevel[1].originalExpression.toString())
        assertEquals("+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))))", permutationsResultNotSimplifiedOnTopLevel[1].originalExpressionChangingPart.toString())
        assertEquals("(+(t;-(+(+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w)))))));r));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))))", permutationsResultNotSimplifiedOnTopLevel[1].resultExpression.toString())
        assertEquals("+(+(-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))))", permutationsResultNotSimplifiedOnTopLevel[1].resultExpressionChangingPart.toString())
    }

    @Test
    fun fillSubstitutionAndRuleTransformationsSelectionDataInternalDeepMinusSelectionsInOtherFunction() {
        val expression = parseStringExpression("sin(t-(r-(6*y-r+8)-(-g+5-(v-3*w)))-(4*y+5-(y+6-(2*y-(4-(t-b-8*y))))-u))^2-cos(x)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    ^  :  [2]\n" +
                "      sin  :  [3]\n" +
                "        +  :  [4]\n" +
                "          t  :  [5]\n" +
                "          -  :  [6]\n" +
                "            +  :  [7]\n" +
                "              r  :  [8]\n" +
                "              -  :  [9]\n" +
                "                +  :  [10]\n" +
                "                  *  :  [11]\n" +
                "                    6  :  [12]\n" +
                "                    y  :  [13]\n" +
                "                  -  :  [14]\n" +
                "                    r  :  [15]\n" +
                "                  8  :  [16]\n" +
                "              -  :  [17]\n" +
                "                +  :  [18]\n" +
                "                  -  :  [19]\n" +
                "                    g  :  [20]\n" +
                "                  5  :  [21]\n" +
                "                  -  :  [22]\n" +
                "                    +  :  [23]\n" +
                "                      v  :  [24]\n" +
                "                      -  :  [25]\n" +
                "                        *  :  [26]\n" +
                "                          3  :  [27]\n" +
                "                          w  :  [28]\n" +
                "          -  :  [29]\n" +
                "            +  :  [30]\n" +
                "              *  :  [31]\n" +
                "                4  :  [32]\n" +
                "                y  :  [33]\n" +
                "              5  :  [34]\n" +
                "              -  :  [35]\n" +
                "                +  :  [36]\n" +
                "                  y  :  [37]\n" +
                "                  6  :  [38]\n" +
                "                  -  :  [39]\n" +
                "                    +  :  [40]\n" +
                "                      *  :  [41]\n" +
                "                        2  :  [42]\n" +
                "                        y  :  [43]\n" +
                "                      -  :  [44]\n" +
                "                        +  :  [45]\n" +
                "                          4  :  [46]\n" +
                "                          -  :  [47]\n" +
                "                            +  :  [48]\n" +
                "                              t  :  [49]\n" +
                "                              -  :  [50]\n" +
                "                                b  :  [51]\n" +
                "                              -  :  [52]\n" +
                "                                *  :  [53]\n" +
                "                                  8  :  [54]\n" +
                "                                  y  :  [55]\n" +
                "              -  :  [56]\n" +
                "                u  :  [57]\n" +
                "      2  :  [58]\n" +
                "    -  :  [59]\n" +
                "      cos  :  [60]\n" +
                "        x  :  [61]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(53, 43, 37, 11, 31).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(^(sin(x);2))", "(^(sin(x);2))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));y;-(*(2;a));*(8;a)))", "(+(1;-(^(cos(x);2))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", "(*(9;a))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(a;-(b);y;d;h))", "(+(h;a;-(b);y;d))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("+(-(+(-(+(*(6;y)))));-(+(*(4;y);-(+(y;-(+(*(y);-(+(-(+(-(*(8;y)))))))))))))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("+(-(+(-(+(*()))));-(+(*();-(+(y;-(+(*(y);-(+(-(+(-(*()))))))))))))", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("+(place_for_result();t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u)", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y)))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals("+(t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u))))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = findConfiguredSubstitutionsApplications(substitutionSelectionData, true, true)
        assertEquals(3, result.size)
        assertEquals("(+(*(6;a);-(*(4;a));y;-(*(2;a));*(8;a)))", result[0].expressionSubstitution.left.toString())
        assertEquals("(+(1;-(^(cos(x);2))))", result[0].expressionSubstitution.right.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", result[0].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", result[0].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(+(1;-(^(cos(x);2)));t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u));2);-(cos(x))))", result[0].resultExpression.toString())
        assertEquals("+(1;-(^(cos(x);2)))", result[0].resultExpressionChangingPart.toString())

        assertEquals("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", result[1].expressionSubstitution.left.toString())
        assertEquals("(*(9;a))", result[1].expressionSubstitution.right.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", result[1].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", result[1].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(*(9;y);t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u));2);-(cos(x))))", result[1].resultExpression.toString())
        assertEquals("*(9;y)", result[1].resultExpressionChangingPart.toString())

        assertEquals("(+(a;-(b);y;d;h))", result[2].expressionSubstitution.left.toString())
        assertEquals("(+(h;a;-(b);y;d))", result[2].expressionSubstitution.right.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", result[2].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", result[2].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(+(*(8;y);*(6;y);-(*(4;y));y;-(*(2;y)));t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u));2);-(cos(x))))", result[2].resultExpression.toString())
        assertEquals("+(*(8;y);*(6;y);-(*(4;y));y;-(*(2;y)))", result[2].resultExpressionChangingPart.toString())


        val resultNotSimplified = findConfiguredSubstitutionsApplications(substitutionSelectionData, false, true)
        assertEquals(3, resultNotSimplified.size)
        assertEquals("(+(*(6;a);-(*(4;a));y;-(*(2;a));*(8;a)))", resultNotSimplified[0].expressionSubstitution.left.toString())
        assertEquals("(+(1;-(^(cos(x);2))))", resultNotSimplified[0].expressionSubstitution.right.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", resultNotSimplified[0].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", resultNotSimplified[0].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(+(1;-(^(cos(x);2)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))));2);-(cos(x))))", resultNotSimplified[0].resultExpression.toString())
        assertEquals("+(1;-(^(cos(x);2)))", resultNotSimplified[0].resultExpressionChangingPart.toString())

        assertEquals("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", resultNotSimplified[1].expressionSubstitution.left.toString())
        assertEquals("(*(9;a))", resultNotSimplified[1].expressionSubstitution.right.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", resultNotSimplified[1].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", resultNotSimplified[1].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(*(9;y);t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))));2);-(cos(x))))", resultNotSimplified[1].resultExpression.toString())
        assertEquals("*(9;y)", resultNotSimplified[1].resultExpressionChangingPart.toString())

        assertEquals("(+(a;-(b);y;d;h))", resultNotSimplified[2].expressionSubstitution.left.toString())
        assertEquals("(+(h;a;-(b);y;d))", resultNotSimplified[2].expressionSubstitution.right.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", resultNotSimplified[2].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", resultNotSimplified[2].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(+(*(8;y);*(6;y);-(*(4;y));y;-(*(2;y)));t;-(+(r;-(+(-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(5;-(+(6;-(+(-(+(4;-(+(t;-(b)))))))));-(u)))));2);-(cos(x))))", resultNotSimplified[2].resultExpression.toString())
        assertEquals("+(*(8;y);*(6;y);-(*(4;y));y;-(*(2;y)))", resultNotSimplified[2].resultExpressionChangingPart.toString())


        val permutationsResult = generatePermutationSubstitutions(substitutionSelectionData, true, true)
        assertEquals(2, permutationsResult.size)
        assertEquals("(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y)))", permutationsResult[0].expressionSubstitution.left.toString())
        assertEquals("(+(+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y)))))", permutationsResult[0].expressionSubstitution.right.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", permutationsResult[0].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", permutationsResult[0].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y)));t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u));2);-(cos(x))))", permutationsResult[0].resultExpression.toString())
        assertEquals("+(+(*(8;y);-(*(2;y));y;*(6;y);-(*(4;y))))", permutationsResult[0].resultExpressionChangingPart.toString())

        assertEquals("(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y)))", permutationsResult[1].expressionSubstitution.left.toString())
        assertEquals("(+(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))))", permutationsResult[1].expressionSubstitution.right.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", permutationsResult[1].originalExpression.toString())
        assertEquals("+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y))", permutationsResult[1].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y));t;-(r);-(r);8;+(-(g);5;-(+(v;-(*(3;w)))));-(5);6;4;-(t);b;u));2);-(cos(x))))", permutationsResult[1].resultExpression.toString())
        assertEquals("+(+(*(6;y);-(*(4;y));y;-(*(2;y));*(8;y)))", permutationsResult[1].resultExpressionChangingPart.toString())


        val permutationsResultNotSimplified = generatePermutationSubstitutions(substitutionSelectionData, false, true)
        assertEquals(2, permutationsResultNotSimplified.size)


        val selectedNodeIdsSinSqr = listOf(2).toTypedArray()
        val substitutionSelectionDataSinSqr = SubstitutionSelectionData(expression, selectedNodeIdsSinSqr, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionDataSinSqr)
        assertEquals("^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2)", substitutionSelectionDataSinSqr.lowestSubtree.toString())
        assertEquals("^()", substitutionSelectionDataSinSqr.lowestSubtreeHigh.toString())
        assertEquals(null, substitutionSelectionDataSinSqr.notSelectedSubtreeTopArguments)
        assertEquals("^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2)", substitutionSelectionDataSinSqr.selectedSubtreeTopArguments.toString())
        assertEquals("^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2)", substitutionSelectionDataSinSqr.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals(null, substitutionSelectionDataSinSqr.notSelectedSubtreeTopOriginalTree)
        assertEquals(0, generatePermutationSubstitutions(substitutionSelectionDataSinSqr, true, true).size)
        assertEquals(0, generatePermutationSubstitutions(substitutionSelectionDataSinSqr, false, true).size)
        assertEquals(0, generateSimpleComputationSubstitutions(substitutionSelectionDataSinSqr, true, true).size)
        assertEquals(0, generateSimpleComputationSubstitutions(substitutionSelectionDataSinSqr, false, true).size)
        assertEquals(0, generateNumberTransformationSubstitutions(substitutionSelectionDataSinSqr, true, true).size)
        assertEquals(0, generateNumberTransformationSubstitutions(substitutionSelectionDataSinSqr, false, true).size)

        val selectedNodeIdsPlusTop = listOf(4).toTypedArray()
        val substitutionSelectionDataPlusTop = SubstitutionSelectionData(expression, selectedNodeIdsPlusTop, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionDataPlusTop)
        assertEquals("+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u))))", substitutionSelectionDataPlusTop.lowestSubtree.toString())
        assertEquals("+()", substitutionSelectionDataPlusTop.lowestSubtreeHigh.toString())
        assertEquals(null, substitutionSelectionDataPlusTop.notSelectedSubtreeTopArguments)
        assertEquals("+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u))))", substitutionSelectionDataPlusTop.selectedSubtreeTopArguments.toString())
        assertEquals("+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u))))", substitutionSelectionDataPlusTop.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals(null, substitutionSelectionDataPlusTop.notSelectedSubtreeTopOriginalTree)
        assertEquals(0, generatePermutationSubstitutions(substitutionSelectionDataPlusTop, true, true).size)
        assertEquals(0, generatePermutationSubstitutions(substitutionSelectionDataPlusTop, false, true).size)
        assertEquals(0, generateSimpleComputationSubstitutions(substitutionSelectionDataPlusTop, true, true).size)
        assertEquals(0, generateSimpleComputationSubstitutions(substitutionSelectionDataPlusTop, false, true).size)
        assertEquals(0, generateNumberTransformationSubstitutions(substitutionSelectionDataPlusTop, true, true).size)
        assertEquals(0, generateNumberTransformationSubstitutions(substitutionSelectionDataPlusTop, false, true).size)


        val selectedNodeIdsNumbers = listOf(16, 46, 34, 38).toTypedArray()
        val substitutionSelectionDataNumbers = SubstitutionSelectionData(expression, selectedNodeIdsNumbers, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionDataNumbers)
        assertEquals("+(-(+(-(+(8))));-(+(5;-(+(6;-(+(-(+(4)))))))))", substitutionSelectionDataNumbers.lowestSubtree.toString())
        assertEquals("+(-(+(-(+(8))));-(+(5;-(+(6;-(+(-(+(4)))))))))", substitutionSelectionDataNumbers.lowestSubtreeHigh.toString())
        assertEquals("+(place_for_result();t;-(r);*(6;y);-(r);+(-(g);5;-(+(v;-(*(3;w)))));-(*(4;y));y;-(*(2;y));-(+(t;-(b);-(*(8;y))));u)", substitutionSelectionDataNumbers.notSelectedSubtreeTopArguments.toString())
        assertEquals("+(8;-(5);6;4)", substitutionSelectionDataNumbers.selectedSubtreeTopArguments.toString())
        assertEquals("+(8;4;-(5);6)", substitutionSelectionDataNumbers.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals("+(t;-(+(r;-(+(*(6;y);-(r)));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);-(+(y;-(+(*(2;y);-(+(-(+(t;-(b);-(*(8;y))))))))));-(u))))", substitutionSelectionDataNumbers.notSelectedSubtreeTopOriginalTree.toString())
        val computationResult = generateSimpleComputationSubstitutions(substitutionSelectionDataNumbers, false, true)
        assertEquals(1, computationResult.size)
        assertEquals("(+(8;-(5);6;4))", computationResult[0].expressionSubstitution.left.toString())
        assertEquals("(13)", computationResult[0].expressionSubstitution.right.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);8));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", computationResult[0].originalExpression.toString())
        assertEquals("+(8;-(5);6;4)", computationResult[0].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(13;t;-(+(r;-(+(*(6;y);-(r)));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);-(+(y;-(+(*(2;y);-(+(-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", computationResult[0].resultExpression.toString())
        assertEquals("13", computationResult[0].resultExpressionChangingPart.toString())


        val selectedNodeIdsNumberTransformation = listOf(16).toTypedArray()
        val substitutionSelectionDataNumberTransformation = SubstitutionSelectionData(expression, selectedNodeIdsNumberTransformation, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionDataNumberTransformation)
        assertEquals("8", substitutionSelectionDataNumberTransformation.lowestSubtree.toString())
        assertEquals("8", substitutionSelectionDataNumberTransformation.lowestSubtreeHigh.toString())
        assertEquals(null, substitutionSelectionDataNumberTransformation.notSelectedSubtreeTopArguments)
        assertEquals("8", substitutionSelectionDataNumberTransformation.selectedSubtreeTopArguments.toString())
        assertEquals("8", substitutionSelectionDataNumberTransformation.selectedSubtreeTopArgumentsInSelectionOrder.toString())
        assertEquals(null, substitutionSelectionDataNumberTransformation.notSelectedSubtreeTopOriginalTree)
        assertEquals(0, generatePermutationSubstitutions(substitutionSelectionDataNumberTransformation, true, true).size)
        assertEquals(0, generatePermutationSubstitutions(substitutionSelectionDataNumberTransformation, false, true).size)
        assertEquals(0, generateSimpleComputationSubstitutions(substitutionSelectionDataNumberTransformation, true, true).size)
        assertEquals(0, generateSimpleComputationSubstitutions(substitutionSelectionDataNumberTransformation, false, true).size)
        val numberTransformationResult = generateNumberTransformationSubstitutions(substitutionSelectionDataNumberTransformation, true, true)
        assertEquals(4, numberTransformationResult.size)
        assertEquals("(8)", numberTransformationResult[0].expressionSubstitution.left.toString())
        assertEquals("(*(2;4))", numberTransformationResult[0].expressionSubstitution.right.toString())
        assertEquals("(^(2;3))", numberTransformationResult[1].expressionSubstitution.right.toString())
        assertEquals("(+(7;1))", numberTransformationResult[2].expressionSubstitution.right.toString())
        assertEquals("(+(9;-(1)))", numberTransformationResult[3].expressionSubstitution.right.toString())

        assertEquals("8", numberTransformationResult[0].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);*(2;4)));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", numberTransformationResult[0].resultExpression.toString())
        assertEquals("*(2;4)", numberTransformationResult[0].resultExpressionChangingPart.toString())

        assertEquals("8", numberTransformationResult[1].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);^(2;3)));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", numberTransformationResult[1].resultExpression.toString())
        assertEquals("^(2;3)", numberTransformationResult[1].resultExpressionChangingPart.toString())

        assertEquals("8", numberTransformationResult[2].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);+(7;1)));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", numberTransformationResult[2].resultExpression.toString())
        assertEquals("+(7;1)", numberTransformationResult[2].resultExpressionChangingPart.toString())

        assertEquals("8", numberTransformationResult[3].originalExpressionChangingPart.toString())
        assertEquals("(+(^(sin(+(t;-(+(r;-(+(*(6;y);-(r);+(9;-(1))));-(+(-(g);5;-(+(v;-(*(3;w))))))));-(+(*(4;y);5;-(+(y;6;-(+(*(2;y);-(+(4;-(+(t;-(b);-(*(8;y))))))))));-(u)))));2);-(cos(x))))", numberTransformationResult[3].resultExpression.toString())
        assertEquals("+(9;-(1))", numberTransformationResult[3].resultExpressionChangingPart.toString())

    }

    @Test
    fun MulAddResuctionsWithSortingTest() {
        val expression = parseStringExpression("sin(6*x^4*y^3*z*y^2*y^2 + x^4*y^2*y*z*z*z^7 + z*6*y^2)")
        assertEquals("  :  [0]\n" +
                "  sin  :  [1]\n" +
                "    +  :  [2]\n" +
                "      *  :  [3]\n" +
                "        6  :  [4]\n" +
                "        ^  :  [5]\n" +
                "          x  :  [6]\n" +
                "          4  :  [7]\n" +
                "        ^  :  [8]\n" +
                "          y  :  [9]\n" +
                "          3  :  [10]\n" +
                "        z  :  [11]\n" +
                "        ^  :  [12]\n" +
                "          y  :  [13]\n" +
                "          2  :  [14]\n" +
                "        ^  :  [15]\n" +
                "          y  :  [16]\n" +
                "          2  :  [17]\n" +
                "      *  :  [18]\n" +
                "        ^  :  [19]\n" +
                "          x  :  [20]\n" +
                "          4  :  [21]\n" +
                "        ^  :  [22]\n" +
                "          y  :  [23]\n" +
                "          2  :  [24]\n" +
                "        y  :  [25]\n" +
                "        z  :  [26]\n" +
                "        z  :  [27]\n" +
                "        ^  :  [28]\n" +
                "          z  :  [29]\n" +
                "          7  :  [30]\n" +
                "      *  :  [31]\n" +
                "        z  :  [32]\n" +
                "        6  :  [33]\n" +
                "        ^  :  [34]\n" +
                "          y  :  [35]\n" +
                "          2  :  [36]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(3, 18, 31).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateReduceArithmeticSubstitutions(substitutionSelectionData, true, true)
            assertEquals("" +
                    "result: 'sin(z*y^2*(6*x^4*y^3*y^2+x^4*y*z*z^7+6))'\n" +
                    "expressionSubstitution.left: '(+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2))))'\n" +
                    "expressionSubstitution.right: '(*(z;^(y;2);+(*(6;^(x;4);^(y;3);^(y;2));*(^(x;4);y;z;^(z;7));6)))'\n" +
                    "originalExpression: '(sin(+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2)))))'\n" +
                    "originalExpressionChangingPart: '+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2)))'\n" +
                    "resultExpression: '(sin(*(z;^(y;2);+(*(6;^(x;4);^(y;3);^(y;2));*(^(x;4);y;z;^(z;7));6))))'\n" +
                    "resultExpressionChangingPart: '*(z;^(y;2);+(*(6;^(x;4);^(y;3);^(y;2));*(^(x;4);y;z;^(z;7));6))'\n" +
                    "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                    "priority: '5'\n" +
                    "\n" +
                    "result: 'sin(y^2*z*(6*x^4*y*y^2*y^2+x^4*y*z*z^7+6))'\n" +
                    "expressionSubstitution.left: '(+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2))))'\n" +
                    "expressionSubstitution.right: '(*(^(y;2);z;+(*(6;^(x;4);y;^(y;2);^(y;2));*(^(x;4);y;z;^(z;7));6)))'\n" +
                    "originalExpression: '(sin(+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2)))))'\n" +
                    "originalExpressionChangingPart: '+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2)))'\n" +
                    "resultExpression: '(sin(*(^(y;2);z;+(*(6;^(x;4);y;^(y;2);^(y;2));*(^(x;4);y;z;^(z;7));6))))'\n" +
                    "resultExpressionChangingPart: '*(^(y;2);z;+(*(6;^(x;4);y;^(y;2);^(y;2));*(^(x;4);y;z;^(z;7));6))'\n" +
                    "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                    "priority: '5'", result.joinToString(separator = "\n\n"))
        }
        if (true) {
            val selectedNodeIds = listOf(2).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateReduceArithmeticSubstitutions(substitutionSelectionData, true, true)
            assertEquals("" +
                    "result: 'sin(z*y^2*(6*x^4*y^3*y^2+x^4*y*z*z^7+6))'\n" +
                    "expressionSubstitution.left: '(+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2))))'\n" +
                    "expressionSubstitution.right: '(*(z;^(y;2);+(*(6;^(x;4);^(y;3);^(y;2));*(^(x;4);y;z;^(z;7));6)))'\n" +
                    "originalExpression: '(sin(+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2)))))'\n" +
                    "originalExpressionChangingPart: '+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2)))'\n" +
                    "resultExpression: '(sin(*(z;^(y;2);+(*(6;^(x;4);^(y;3);^(y;2));*(^(x;4);y;z;^(z;7));6))))'\n" +
                    "resultExpressionChangingPart: '*(z;^(y;2);+(*(6;^(x;4);^(y;3);^(y;2));*(^(x;4);y;z;^(z;7));6))'\n" +
                    "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                    "priority: '5'\n" +
                    "\n" +
                    "result: 'sin(y^2*z*(6*x^4*y*y^2*y^2+x^4*y*z*z^7+6))'\n" +
                    "expressionSubstitution.left: '(+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2))))'\n" +
                    "expressionSubstitution.right: '(*(^(y;2);z;+(*(6;^(x;4);y;^(y;2);^(y;2));*(^(x;4);y;z;^(z;7));6)))'\n" +
                    "originalExpression: '(sin(+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2)))))'\n" +
                    "originalExpressionChangingPart: '+(*(6;^(x;4);^(y;3);z;^(y;2);^(y;2));*(^(x;4);^(y;2);y;z;z;^(z;7));*(z;6;^(y;2)))'\n" +
                    "resultExpression: '(sin(*(^(y;2);z;+(*(6;^(x;4);y;^(y;2);^(y;2));*(^(x;4);y;z;^(z;7));6))))'\n" +
                    "resultExpressionChangingPart: '*(^(y;2);z;+(*(6;^(x;4);y;^(y;2);^(y;2));*(^(x;4);y;z;^(z;7));6))'\n" +
                    "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                    "priority: '5'", result.joinToString(separator = "\n\n"))
        }
    }

    @Test
    fun DivAddResuctionsWithMinusTest() {
        val expression = parseStringExpression("sin((6*x^4)/(y^3*z*y^2) - x/(y^3*z*y^2) - x^3/(y^3*z*y^2))")
        assertEquals("  :  [0]\n" +
                "  sin  :  [1]\n" +
                "    +  :  [2]\n" +
                "      /  :  [3]\n" +
                "        *  :  [4]\n" +
                "          6  :  [5]\n" +
                "          ^  :  [6]\n" +
                "            x  :  [7]\n" +
                "            4  :  [8]\n" +
                "        *  :  [9]\n" +
                "          ^  :  [10]\n" +
                "            y  :  [11]\n" +
                "            3  :  [12]\n" +
                "          z  :  [13]\n" +
                "          ^  :  [14]\n" +
                "            y  :  [15]\n" +
                "            2  :  [16]\n" +
                "      -  :  [17]\n" +
                "        /  :  [18]\n" +
                "          x  :  [19]\n" +
                "          *  :  [20]\n" +
                "            ^  :  [21]\n" +
                "              y  :  [22]\n" +
                "              3  :  [23]\n" +
                "            z  :  [24]\n" +
                "            ^  :  [25]\n" +
                "              y  :  [26]\n" +
                "              2  :  [27]\n" +
                "      -  :  [28]\n" +
                "        /  :  [29]\n" +
                "          ^  :  [30]\n" +
                "            x  :  [31]\n" +
                "            3  :  [32]\n" +
                "          *  :  [33]\n" +
                "            ^  :  [34]\n" +
                "              y  :  [35]\n" +
                "              3  :  [36]\n" +
                "            z  :  [37]\n" +
                "            ^  :  [38]\n" +
                "              y  :  [39]\n" +
                "              2  :  [40]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(2).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateReduceArithmeticSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(+(/(*(6;^(x;4));*(^(y;3);z;^(y;2)));-(/(x;*(^(y;3);z;^(y;2))));-(/(^(x;3);*(^(y;3);z;^(y;2))))))", result[0].expressionSubstitution.left.toString())
            assertEquals("(/(+(*(6;^(x;4));-(x);-(^(x;3)));*(^(y;3);z;^(y;2))))", result[0].expressionSubstitution.right.toString())
            assertEquals("(sin(+(/(*(6;^(x;4));*(^(y;3);z;^(y;2)));-(/(x;*(^(y;3);z;^(y;2))));-(/(^(x;3);*(^(y;3);z;^(y;2)))))))", result[0].originalExpression.toString())
            assertEquals("+(/(*(6;^(x;4));*(^(y;3);z;^(y;2)));-(/(x;*(^(y;3);z;^(y;2))));-(/(^(x;3);*(^(y;3);z;^(y;2)))))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(sin(/(+(*(6;^(x;4));-(x);-(^(x;3)));*(^(y;3);z;^(y;2)))))", result[0].resultExpression.toString())
            assertEquals("/(+(*(6;^(x;4));-(x);-(^(x;3)));*(^(y;3);z;^(y;2)))", result[0].resultExpressionChangingPart.toString())
        }
    }

    @Test
    fun DegMulResuctionsWithSortingTest() {
        val expression = parseStringExpression("sin((y^3+z+y^2) * (z+y^2+y^3) * (y^3+z+y^2)^(x^3))")
        assertEquals("  :  [0]\n" +
                "  sin  :  [1]\n" +
                "    *  :  [2]\n" +
                "      +  :  [3]\n" +
                "        ^  :  [4]\n" +
                "          y  :  [5]\n" +
                "          3  :  [6]\n" +
                "        z  :  [7]\n" +
                "        ^  :  [8]\n" +
                "          y  :  [9]\n" +
                "          2  :  [10]\n" +
                "      +  :  [11]\n" +
                "        z  :  [12]\n" +
                "        ^  :  [13]\n" +
                "          y  :  [14]\n" +
                "          2  :  [15]\n" +
                "        ^  :  [16]\n" +
                "          y  :  [17]\n" +
                "          3  :  [18]\n" +
                "      ^  :  [19]\n" +
                "        +  :  [20]\n" +
                "          ^  :  [21]\n" +
                "            y  :  [22]\n" +
                "            3  :  [23]\n" +
                "          z  :  [24]\n" +
                "          ^  :  [25]\n" +
                "            y  :  [26]\n" +
                "            2  :  [27]\n" +
                "        ^  :  [28]\n" +
                "          x  :  [29]\n" +
                "          3  :  [30]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(2).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateReduceArithmeticSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(*(+(^(y;3);z;^(y;2));+(z;^(y;2);^(y;3));^(+(^(y;3);z;^(y;2));^(x;3))))", result[0].expressionSubstitution.left.toString())
            assertEquals("(^(+(^(y;3);z;^(y;2));+(1;1;^(x;3))))", result[0].expressionSubstitution.right.toString())
            assertEquals("(sin(*(+(^(y;3);z;^(y;2));+(z;^(y;2);^(y;3));^(+(^(y;3);z;^(y;2));^(x;3)))))", result[0].originalExpression.toString())
            assertEquals("*(+(^(y;3);z;^(y;2));+(z;^(y;2);^(y;3));^(+(^(y;3);z;^(y;2));^(x;3)))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(sin(^(+(^(y;3);z;^(y;2));+(1;1;^(x;3)))))", result[0].resultExpression.toString())
            assertEquals("^(+(^(y;3);z;^(y;2));+(1;1;^(x;3)))", result[0].resultExpressionChangingPart.toString())
        }
    }

    @Test
    fun MulDegResuctionsWithSortingTest() {
        val expression = parseStringExpression("sin((6*x^4)^(y^3*z*y^2) * x^(z*y^2*y^3) * (x^3)^(y^3*z*y^2))")
        assertEquals("  :  [0]\n" +
                "  sin  :  [1]\n" +
                "    *  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        *  :  [4]\n" +
                "          6  :  [5]\n" +
                "          ^  :  [6]\n" +
                "            x  :  [7]\n" +
                "            4  :  [8]\n" +
                "        *  :  [9]\n" +
                "          ^  :  [10]\n" +
                "            y  :  [11]\n" +
                "            3  :  [12]\n" +
                "          z  :  [13]\n" +
                "          ^  :  [14]\n" +
                "            y  :  [15]\n" +
                "            2  :  [16]\n" +
                "      ^  :  [17]\n" +
                "        x  :  [18]\n" +
                "        *  :  [19]\n" +
                "          z  :  [20]\n" +
                "          ^  :  [21]\n" +
                "            y  :  [22]\n" +
                "            2  :  [23]\n" +
                "          ^  :  [24]\n" +
                "            y  :  [25]\n" +
                "            3  :  [26]\n" +
                "      ^  :  [27]\n" +
                "        ^  :  [28]\n" +
                "          x  :  [29]\n" +
                "          3  :  [30]\n" +
                "        *  :  [31]\n" +
                "          ^  :  [32]\n" +
                "            y  :  [33]\n" +
                "            3  :  [34]\n" +
                "          z  :  [35]\n" +
                "          ^  :  [36]\n" +
                "            y  :  [37]\n" +
                "            2  :  [38]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(2).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateReduceArithmeticSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(*(^(*(6;^(x;4));*(^(y;3);z;^(y;2)));^(x;*(z;^(y;2);^(y;3)));^(^(x;3);*(^(y;3);z;^(y;2)))))", result[0].expressionSubstitution.left.toString())
            assertEquals("(^(*(*(6;^(x;4));x;^(x;3));*(^(y;3);z;^(y;2))))", result[0].expressionSubstitution.right.toString())
            assertEquals("(sin(*(^(*(6;^(x;4));*(^(y;3);z;^(y;2)));^(x;*(z;^(y;2);^(y;3)));^(^(x;3);*(^(y;3);z;^(y;2))))))", result[0].originalExpression.toString())
            assertEquals("*(^(*(6;^(x;4));*(^(y;3);z;^(y;2)));^(x;*(z;^(y;2);^(y;3)));^(^(x;3);*(^(y;3);z;^(y;2))))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(sin(^(*(*(6;^(x;4));x;^(x;3));*(^(y;3);z;^(y;2)))))", result[0].resultExpression.toString())
            assertEquals("^(*(*(6;^(x;4));x;^(x;3));*(^(y;3);z;^(y;2)))", result[0].resultExpressionChangingPart.toString())
        }
    }

    @Test
    fun MulSsimpleAddOpeningBracketsTest() {
        val expression = parseStringExpression("-x*(x+5)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    -  :  [2]\n" +
                "      *  :  [3]\n" +
                "        x  :  [4]\n" +
                "        +  :  [5]\n" +
                "          x  :  [6]\n" +
                "          5  :  [7]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(4, 5).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateOpeningBracketsSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(*(x;+(x;5)))", result[0].expressionSubstitution.left.toString())
            assertEquals("(+(*(x;x);*(x;5)))", result[0].expressionSubstitution.right.toString())
            assertEquals("(+(-(*(x;+(x;5)))))", result[0].originalExpression.toString())
            assertEquals("*(x;+(x;5))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(+(-(+(*(x;x);*(x;5)))))", result[0].resultExpression.toString())
            assertEquals("+(*(x;x);*(x;5))", result[0].resultExpressionChangingPart.toString())
        }
    }


    @Test
    fun DivSsimpleAddOpeningBracketsTest() {
        val expression = parseStringExpression("(x-5)/(x+5)")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    +  :  [2]\n" +
                "      x  :  [3]\n" +
                "      -  :  [4]\n" +
                "        5  :  [5]\n" +
                "    +  :  [6]\n" +
                "      x  :  [7]\n" +
                "      5  :  [8]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(1).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateOpeningBracketsSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(/(+(x;-(5));+(x;5)))", result[0].expressionSubstitution.left.toString())
            assertEquals("(+(/(x;+(x;5));-(/(5;+(x;5)))))", result[0].expressionSubstitution.right.toString())
            assertEquals("(/(+(x;-(5));+(x;5)))", result[0].originalExpression.toString())
            assertEquals("/(+(x;-(5));+(x;5))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(+(/(x;+(x;5));-(/(5;+(x;5)))))", result[0].resultExpression.toString())
            assertEquals("+(/(x;+(x;5));-(/(5;+(x;5))))", result[0].resultExpressionChangingPart.toString())
        }
    }


    @Test
    fun DivRuleTest() {
        val expression = parseStringExpression("sin(x-5)/cos(x-5)")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    sin  :  [2]\n" +
                "      +  :  [3]\n" +
                "        x  :  [4]\n" +
                "        -  :  [5]\n" +
                "          5  :  [6]\n" +
                "    cos  :  [7]\n" +
                "      +  :  [8]\n" +
                "        x  :  [9]\n" +
                "        -  :  [10]\n" +
                "          5  :  [11]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(1).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                    listOf(
                            expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                            expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                            expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                            expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                            expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                            expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                            expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                            expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                            expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                            expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                            expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
            ).apply {
                compiledExpressionTreeTransformationRules.clear()
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(^(sin(x);2))", "(^(sin(x);2))"))
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(/(sin(x);cos(x)))", "(tg(x))"))
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", "(*(9;a))"))
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(a;-(b);y;d;e))", "(+(e;a;-(b);y;d))"))
            })
            val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(/(sin(x);cos(x)))", result[0].expressionSubstitution.left.toString())
            assertEquals("(tg(x))", result[0].expressionSubstitution.right.toString())
            assertEquals("(/(sin(+(x;-(5)));cos(+(x;-(5)))))", result[0].originalExpression.toString())
            assertEquals("/(sin(+(x;-(5)));cos(+(x;-(5))))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(tg(+(x;-(5))))", result[0].resultExpression.toString())
            assertEquals("tg(+(x;-(5)))", result[0].resultExpressionChangingPart.toString())
        }
        if (true) {
            val selectedNodeIds = listOf(4, 5, 7).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                    listOf(expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"), expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension")).toTypedArray()
            ).apply {
                compiledExpressionTreeTransformationRules.clear()
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(^(sin(x);2))", "(^(sin(x);2))"))
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(/(sin(x);cos(x)))", "(tg(x))"))
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", "(*(9;a))"))
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(a;-(b);y;d;e))", "(+(e;a;-(b);y;d))"))
            })
            val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(/(sin(x);cos(x)))", result[0].expressionSubstitution.left.toString())
            assertEquals("(tg(x))", result[0].expressionSubstitution.right.toString())
            assertEquals("(/(sin(+(x;-(5)));cos(+(x;-(5)))))", result[0].originalExpression.toString())
            assertEquals("/(sin(+(x;-(5)));cos(+(x;-(5))))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(tg(+(x;-(5))))", result[0].resultExpression.toString())
            assertEquals("tg(+(x;-(5)))", result[0].resultExpressionChangingPart.toString())
        }
        if (true) {
            val selectedNodeIds = listOf(2, 7).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                    listOf(expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"), expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension")).toTypedArray()
            ).apply {
                compiledExpressionTreeTransformationRules.clear()
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(^(sin(x);2))", "(^(sin(x);2))"))
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(/(sin(x);cos(x)))", "(tg(x))", priority = 5))
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(*(6;a);-(*(4;a));a;-(*(2;a));*(8;a)))", "(*(9;a))"))
                compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(a;-(b);y;d;e))", "(+(e;a;-(b);y;d))"))
            })
            val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
            assertEquals(3, result.size)
            assertEquals("(/(sin(x);cos(x)))", result[0].expressionSubstitution.left.toString())
            assertEquals("(tg(x))", result[0].expressionSubstitution.right.toString())
            assertEquals("(/(sin(+(x;-(5)));cos(+(x;-(5)))))", result[0].originalExpression.toString())
            assertEquals("/(sin(+(x;-(5)));cos(+(x;-(5))))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(tg(+(x;-(5))))", result[0].resultExpression.toString())
            assertEquals("tg(+(x;-(5)))", result[0].resultExpressionChangingPart.toString())
        }
    }


    @Test
    fun DegSsimpleAddOpeningBracketsTest() {
        val expression = parseStringExpression("(x*sin(y))^(x+5-a)")
        assertEquals("  :  [0]\n" +
                "  ^  :  [1]\n" +
                "    *  :  [2]\n" +
                "      x  :  [3]\n" +
                "      sin  :  [4]\n" +
                "        y  :  [5]\n" +
                "    +  :  [6]\n" +
                "      x  :  [7]\n" +
                "      5  :  [8]\n" +
                "      -  :  [9]\n" +
                "        a  :  [10]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(1).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateOpeningBracketsSubstitutions(substitutionSelectionData, true, true)
            assertEquals(2, result.size)
            assertEquals("(^(*(x;sin(y));+(x;5;-(a))))", result[0].expressionSubstitution.left.toString())
            assertEquals("(*(^(x;+(x;5;-(a)));^(sin(y);+(x;5;-(a)))))", result[0].expressionSubstitution.right.toString())
            assertEquals("(^(*(x;sin(y));+(x;5;-(a))))", result[0].originalExpression.toString())
            assertEquals("^(*(x;sin(y));+(x;5;-(a)))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(*(^(x;+(x;5;-(a)));^(sin(y);+(x;5;-(a)))))", result[0].resultExpression.toString())
            assertEquals("*(^(x;+(x;5;-(a)));^(sin(y);+(x;5;-(a))))", result[0].resultExpressionChangingPart.toString())

            assertEquals("(^(*(x;sin(y));+(x;5;-(a))))", result[1].expressionSubstitution.left.toString())
            assertEquals("(*(^(*(x;sin(y));x);^(*(x;sin(y));5);^(*(x;sin(y));+(-(a)))))", result[1].expressionSubstitution.right.toString())
            assertEquals("(^(*(x;sin(y));+(x;5;-(a))))", result[1].originalExpression.toString())
            assertEquals("^(*(x;sin(y));+(x;5;-(a)))", result[1].originalExpressionChangingPart.toString())
            assertEquals("(*(^(*(x;sin(y));x);^(*(x;sin(y));5);^(*(x;sin(y));+(-(a)))))", result[1].resultExpression.toString())
            assertEquals("*(^(*(x;sin(y));x);^(*(x;sin(y));5);^(*(x;sin(y));+(-(a))))", result[1].resultExpressionChangingPart.toString())
        }
    }

    @Test
    fun MulAddOpeningBracketsTest() {
        val expression = parseStringExpression("2*x+((2*x-sin(y))-(cos(z))^2)(2*cos(x)+sin(y))(3*x+5*y-7^z)/x")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    *  :  [2]\n" +
                "      2  :  [3]\n" +
                "      x  :  [4]\n" +
                "    /  :  [5]\n" +
                "      *  :  [6]\n" +
                "        +  :  [7]\n" +
                "          +  :  [8]\n" +
                "            *  :  [9]\n" +
                "              2  :  [10]\n" +
                "              x  :  [11]\n" +
                "            -  :  [12]\n" +
                "              sin  :  [13]\n" +
                "                y  :  [14]\n" +
                "          -  :  [15]\n" +
                "            ^  :  [16]\n" +
                "              cos  :  [17]\n" +
                "                z  :  [18]\n" +
                "              2  :  [19]\n" +
                "        +  :  [20]\n" +
                "          *  :  [21]\n" +
                "            2  :  [22]\n" +
                "            cos  :  [23]\n" +
                "              x  :  [24]\n" +
                "          sin  :  [25]\n" +
                "            y  :  [26]\n" +
                "        +  :  [27]\n" +
                "          *  :  [28]\n" +
                "            3  :  [29]\n" +
                "            x  :  [30]\n" +
                "          *  :  [31]\n" +
                "            5  :  [32]\n" +
                "            y  :  [33]\n" +
                "          -  :  [34]\n" +
                "            ^  :  [35]\n" +
                "              7  :  [36]\n" +
                "              z  :  [37]\n" +
                "      x  :  [38]\n", expression.toStringsWithNodeIds())
        assertEquals("(+(*(2;x);/(*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(2;cos(x));sin(y));+(*(3;x);*(5;y);-(^(7;z))));x)))", expression.toString())
        if (true) {
            val selectedNodeIds = listOf(27, 7).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(3;x);*(5;y);-(^(7;z))))", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("*(+();+())", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(3;x);*(5;y);-(^(7;z))))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("*(+(*(3;x);*(5;y);-(^(7;z)));+(+(*(2;x);-(sin(y)));-(^(cos(z);2))))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
            assertEquals("*(place_for_result();+(*(2;cos(x));sin(y)))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
            assertEquals("*(+(*(2;cos(x));sin(y)))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())


            val result = generateOpeningBracketsSubstitutions(substitutionSelectionData, true, true)
            assertEquals(2, result.size)
            assertEquals("(*(+(*(3;x);*(5;y);-(^(7;z)));+(+(*(2;x);-(sin(y)));-(^(cos(z);2)))))", result[0].expressionSubstitution.left.toString())
            assertEquals("(3*x)*(2*x-sin(y))-(3*x)*cos(z)^2+(5*y)*(2*x-sin(y))-(5*y)*cos(z)^2-7^z*(2*x-sin(y))+7^z*cos(z)^2", result[0].expressionSubstitution.right.toPlainTextView())
            assertEquals("(+(*(*(3;x);+(*(2;x);-(sin(y))));-(*(*(3;x);^(cos(z);2)));*(*(5;y);+(*(2;x);-(sin(y))));-(*(*(5;y);^(cos(z);2)));-(*(^(7;z);+(*(2;x);-(sin(y)))));*(^(7;z);^(cos(z);2))))", result[0].expressionSubstitution.right.toString())
            assertEquals("(+(*(2;x);/(*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(2;cos(x));sin(y));+(*(3;x);*(5;y);-(^(7;z))));x)))", result[0].originalExpression.toString())
            assertEquals("*(+(*(3;x);*(5;y);-(^(7;z)));+(+(*(2;x);-(sin(y)));-(^(cos(z);2))))", result[0].originalExpressionChangingPart.toString())
            assertEquals("2*x+(((3*x)*(2*x-sin(y))-(3*x)*cos(z)^2+(5*y)*(2*x-sin(y))-(5*y)*cos(z)^2-7^z*(2*x-sin(y))+7^z*cos(z)^2)*(2*cos(x)+sin(y)))/x", result[0].resultExpression.toPlainTextView())
            assertEquals("(+(*(2;x);/(*(+(*(*(3;x);+(*(2;x);-(sin(y))));-(*(*(3;x);^(cos(z);2)));*(*(5;y);+(*(2;x);-(sin(y))));-(*(*(5;y);^(cos(z);2)));-(*(^(7;z);+(*(2;x);-(sin(y)))));*(^(7;z);^(cos(z);2)));+(*(2;cos(x));sin(y)));x)))", result[0].resultExpression.toString())
            assertEquals("+(*(*(3;x);+(*(2;x);-(sin(y))));-(*(*(3;x);^(cos(z);2)));*(*(5;y);+(*(2;x);-(sin(y))));-(*(*(5;y);^(cos(z);2)));-(*(^(7;z);+(*(2;x);-(sin(y)))));*(^(7;z);^(cos(z);2)))", result[0].resultExpressionChangingPart.toString())

            assertEquals("(*(+(*(3;x);*(5;y);-(^(7;z)));+(+(*(2;x);-(sin(y)));-(^(cos(z);2)))))", result[1].expressionSubstitution.left.toString())
            assertEquals("(3*x)*((2*x-sin(y))-cos(z)^2)+(5*y)*((2*x-sin(y))-cos(z)^2)-7^z*((2*x-sin(y))-cos(z)^2)", result[1].expressionSubstitution.right.toPlainTextView())
            assertEquals("(+(*(*(3;x);+(+(*(2;x);-(sin(y)));-(^(cos(z);2))));*(*(5;y);+(+(*(2;x);-(sin(y)));-(^(cos(z);2))));-(*(^(7;z);+(+(*(2;x);-(sin(y)));-(^(cos(z);2)))))))", result[1].expressionSubstitution.right.toString())
            assertEquals("(+(*(2;x);/(*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(2;cos(x));sin(y));+(*(3;x);*(5;y);-(^(7;z))));x)))", result[1].originalExpression.toString())
            assertEquals("*(+(*(3;x);*(5;y);-(^(7;z)));+(+(*(2;x);-(sin(y)));-(^(cos(z);2))))", result[1].originalExpressionChangingPart.toString())
            assertEquals("2*x+(((3*x)*((2*x-sin(y))-cos(z)^2)+(5*y)*((2*x-sin(y))-cos(z)^2)-7^z*((2*x-sin(y))-cos(z)^2))*(2*cos(x)+sin(y)))/x", result[1].resultExpression.toPlainTextView())
            assertEquals("(+(*(2;x);/(*(+(*(*(3;x);+(+(*(2;x);-(sin(y)));-(^(cos(z);2))));*(*(5;y);+(+(*(2;x);-(sin(y)));-(^(cos(z);2))));-(*(^(7;z);+(+(*(2;x);-(sin(y)));-(^(cos(z);2))))));+(*(2;cos(x));sin(y)));x)))", result[1].resultExpression.toString())
            assertEquals("+(*(*(3;x);+(+(*(2;x);-(sin(y)));-(^(cos(z);2))));*(*(5;y);+(+(*(2;x);-(sin(y)));-(^(cos(z);2))));-(*(^(7;z);+(+(*(2;x);-(sin(y)));-(^(cos(z);2))))))", result[1].resultExpressionChangingPart.toString())
        }
        if (true) {
            val selectedNodeIds = listOf(6).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(2;cos(x));sin(y));+(*(3;x);*(5;y);-(^(7;z))))", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("*()", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(2;cos(x));sin(y));+(*(3;x);*(5;y);-(^(7;z))))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(2;cos(x));sin(y));+(*(3;x);*(5;y);-(^(7;z))))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
            assertEquals(null, substitutionSelectionData.notSelectedSubtreeTopArguments)
            assertEquals(null, substitutionSelectionData.notSelectedSubtreeTopOriginalTree)


            val result = generateOpeningBracketsSubstitutions(substitutionSelectionData, true, true)
            assertEquals(0, result.size)
        }
        if (true) {
            val selectedNodeIds = listOf(7, 27, 20).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(2;cos(x));sin(y));+(*(3;x);*(5;y);-(^(7;z))))", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("*(+();+();+())", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(2;cos(x));sin(y));+(*(3;x);*(5;y);-(^(7;z))))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(3;x);*(5;y);-(^(7;z)));+(*(2;cos(x));sin(y)))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
            assertEquals("*(place_for_result())", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
            assertEquals("()", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())


            val result = generateOpeningBracketsSubstitutions(substitutionSelectionData, true, true)
            assertEquals(0, result.size)
        }
        if (true) { //div expansion transformation
            val selectedNodeIds = listOf(20, 38).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("/(*(+(*(2;cos(x));sin(y)));x)", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("/(*(+());x)", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("/(*(+(*(2;cos(x));sin(y)));*(x))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
            assertEquals("/(*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(3;x);*(5;y);-(^(7;z))));*())", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
            assertEquals("/(*(+(+(*(2;x);-(sin(y)));-(^(cos(z);2)));+(*(3;x);*(5;y);-(^(7;z))));1)", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

            val result = generateOpeningBracketsSubstitutions(substitutionSelectionData, true, true)
            assertEquals(0, result.size)
        }
    }

    @Test
    fun minusNodePlusBeforeMinusTest() {
        val expression = parseStringExpression("-36*a*((a*b)/(c*d))*(-1/4)*1")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    -  :  [2]\n" +
                "      *  :  [3]\n" +
                "        36  :  [4]\n" +
                "        a  :  [5]\n" +
                "        /  :  [6]\n" +
                "          *  :  [7]\n" +
                "            a  :  [8]\n" +
                "            b  :  [9]\n" +
                "          *  :  [10]\n" +
                "            c  :  [11]\n" +
                "            d  :  [12]\n" +
                "        +  :  [13]\n" +
                "          -  :  [14]\n" +
                "            /  :  [15]\n" +
                "              1  :  [16]\n" +
                "              4  :  [17]\n" +
                "        1  :  [18]\n", expression.toStringsWithNodeIds())
        assertEquals("(+(-(*(36;a;/(*(a;b);*(c;d));+(-(/(1;4)));1))))", expression.toString())
        val selectedNodeIds = listOf(15, 18).toTypedArray()
        val genRes = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, generalCompiledConfiguration, withReadyApplicationResult = false).filter { it.substitutionType == TREE_COMPUTATION_DEFAULT }
        assertEquals(1, genRes.size)
        assertEquals("(*(+(-(/(1;4)));1))", genRes[0].expressionSubstitution.left.toString())
        assertEquals("(+(-(0.25)))", genRes[0].expressionSubstitution.right.toString())
        assertEquals("(*(+(-(/(1;4)));1))", genRes[0].originalExpressionChangingPart.toString())
        assertEquals("(+(-(*(+(-(0.25));36;a;/(*(a;b);*(c;d))))))", genRes[0].resultExpression.toString())
        assertEquals("(+(-(0.25)))", genRes[0].resultExpressionChangingPart.toString())
        val res = applySubstitutionInSelectedPlace(expression, selectedNodeIds, expressionSubstitutionFromStructureStrings(genRes[0].expressionSubstitution.left.toString(), genRes[0].expressionSubstitution.right.toString()),
                generalCompiledConfiguration)
        assertEquals(genRes[0].resultExpression.toString(), res.toString())
    }

    @Test
    fun DivSimpleReductionTest() {
        val expression = parseStringExpression("((x+5)(x^2-5)(x-y)) / ((x^2-5)*x) ")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    *  :  [2]\n" +
                "      +  :  [3]\n" +
                "        x  :  [4]\n" +
                "        5  :  [5]\n" +
                "      +  :  [6]\n" +
                "        ^  :  [7]\n" +
                "          x  :  [8]\n" +
                "          2  :  [9]\n" +
                "        -  :  [10]\n" +
                "          5  :  [11]\n" +
                "      +  :  [12]\n" +
                "        x  :  [13]\n" +
                "        -  :  [14]\n" +
                "          y  :  [15]\n" +
                "    *  :  [16]\n" +
                "      +  :  [17]\n" +
                "        ^  :  [18]\n" +
                "          x  :  [19]\n" +
                "          2  :  [20]\n" +
                "        -  :  [21]\n" +
                "          5  :  [22]\n" +
                "      x  :  [23]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(6, 17).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(/(+(^(x;2);-(5));+(^(x;2);-(5))))", result[0].expressionSubstitution.left.toString())
            assertEquals("(1)", result[0].expressionSubstitution.right.toString())
            assertEquals("(/(*(+(x;5);+(^(x;2);-(5));+(x;-(y)));*(+(^(x;2);-(5));x)))", result[0].originalExpression.toString())
            assertEquals("/(*(+(x;5);+(^(x;2);-(5));+(x;-(y)));*(+(^(x;2);-(5));x))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(/(*(+(x;5);+(x;-(y)));x))", result[0].resultExpression.toString())
            assertEquals("/(*(+(x;5);+(x;-(y)));x)", result[0].resultExpressionChangingPart.toString())
        }
    }

    @Test
    fun DivSimpleReductionDenominatorTest() {
        val expression = parseStringExpression("((x+5)(x^2-5)(x-y)) / (x^2-5) ")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    *  :  [2]\n" +
                "      +  :  [3]\n" +
                "        x  :  [4]\n" +
                "        5  :  [5]\n" +
                "      +  :  [6]\n" +
                "        ^  :  [7]\n" +
                "          x  :  [8]\n" +
                "          2  :  [9]\n" +
                "        -  :  [10]\n" +
                "          5  :  [11]\n" +
                "      +  :  [12]\n" +
                "        x  :  [13]\n" +
                "        -  :  [14]\n" +
                "          y  :  [15]\n" +
                "    +  :  [16]\n" +
                "      ^  :  [17]\n" +
                "        x  :  [18]\n" +
                "        2  :  [19]\n" +
                "      -  :  [20]\n" +
                "        5  :  [21]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(6, 17).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(/(+(^(x;2);-(5));+(^(x;2);-(5))))", result[0].expressionSubstitution.left.toString())
            assertEquals("(1)", result[0].expressionSubstitution.right.toString())
            assertEquals("(/(*(+(x;5);+(^(x;2);-(5));+(x;-(y)));+(^(x;2);-(5))))", result[0].originalExpression.toString())
            assertEquals("/(*(+(x;5);+(^(x;2);-(5));+(x;-(y)));+(^(x;2);-(5)))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(*(+(x;5);+(x;-(y))))", result[0].resultExpression.toString())
            assertEquals("*(+(x;5);+(x;-(y)))", result[0].resultExpressionChangingPart.toString())
        }
    }

    @Test
    fun DivSimpleReductionNumeratorTest() {
        val expression = parseStringExpression("(x^2-5)/((x+5)(x^2-5)(x-y))")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    +  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        x  :  [4]\n" +
                "        2  :  [5]\n" +
                "      -  :  [6]\n" +
                "        5  :  [7]\n" +
                "    *  :  [8]\n" +
                "      +  :  [9]\n" +
                "        x  :  [10]\n" +
                "        5  :  [11]\n" +
                "      +  :  [12]\n" +
                "        ^  :  [13]\n" +
                "          x  :  [14]\n" +
                "          2  :  [15]\n" +
                "        -  :  [16]\n" +
                "          5  :  [17]\n" +
                "      +  :  [18]\n" +
                "        x  :  [19]\n" +
                "        -  :  [20]\n" +
                "          y  :  [21]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(6, 17).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("/(+(-(5));*(+(-(5))))", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("/(+(-());*(+(-(5))))", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("/(+(^(x;2);-(5));*(+(x;5);+(^(x;2);-(5));+(x;-(y))))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("/(+(^(x;2);-(5));*(+(x;5);+(^(x;2);-(5));+(x;-(y))))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
            assertEquals(null, substitutionSelectionData.notSelectedSubtreeTopArguments)
            assertEquals(null, substitutionSelectionData.notSelectedSubtreeTopOriginalTree)
            val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
            assertEquals(0, result.size)
        }
        if (true) {
            val selectedNodeIds = listOf(2, 12).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(/(+(^(x;2);-(5));+(^(x;2);-(5))))", result[0].expressionSubstitution.left.toString())
            assertEquals("(1)", result[0].expressionSubstitution.right.toString())
            assertEquals("(/(+(^(x;2);-(5));*(+(x;5);+(^(x;2);-(5));+(x;-(y)))))", result[0].originalExpression.toString())
            assertEquals("/(+(^(x;2);-(5));*(+(x;5);+(^(x;2);-(5));+(x;-(y))))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(/(1;*(+(x;5);+(x;-(y)))))", result[0].resultExpression.toString())
            assertEquals("/(1;*(+(x;5);+(x;-(y))))", result[0].resultExpressionChangingPart.toString())
        }
    }

    @Test
    fun divNumericReductionTest() {
        val expression = parseStringExpression("1 / ( (x*(16*3)) / (1024*y*(y^2*(x^2*7))) )")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    1  :  [2]\n" +
                "    /  :  [3]\n" +
                "      *  :  [4]\n" +
                "        x  :  [5]\n" +
                "        *  :  [6]\n" +
                "          16  :  [7]\n" +
                "          3  :  [8]\n" +
                "      *  :  [9]\n" +
                "        1024  :  [10]\n" +
                "        y  :  [11]\n" +
                "        *  :  [12]\n" +
                "          ^  :  [13]\n" +
                "            y  :  [14]\n" +
                "            2  :  [15]\n" +
                "          *  :  [16]\n" +
                "            ^  :  [17]\n" +
                "              x  :  [18]\n" +
                "              2  :  [19]\n" +
                "            7  :  [20]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(7, 10).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("/(*(*(16));*(1024))", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("/(*(*(16));*(1024))", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("/(*(16);*(1024))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
            assertEquals("/(*(x;3);*(y;*(^(y;2);*(^(x;2);7))))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
            assertEquals("/(*(x;*(3));*(y;*(^(y;2);*(^(x;2);7))))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

            val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(/(16;1024))", result[0].expressionSubstitution.left.toString())
            assertEquals("(/(1;64))", result[0].expressionSubstitution.right.toString())
            assertEquals("(/(1;/(*(x;*(16;3));*(1024;y;*(^(y;2);*(^(x;2);7))))))", result[0].originalExpression.toString())
            assertEquals("/(*(x;*(16;3));*(1024;y;*(^(y;2);*(^(x;2);7))))", result[0].originalExpressionChangingPart.toString())
            assertEquals("(/(1;/(*(x;3);*(64;y;*(^(y;2);*(^(x;2);7))))))", result[0].resultExpression.toString())
            assertEquals("/(*(x;3);*(64;y;*(^(y;2);*(^(x;2);7))))", result[0].resultExpressionChangingPart.toString())
        }
    }

    @Test
    fun divNumericReductionExtractDenominatorTest() {
        val expression = parseStringExpression("1 / ( (x*(33*3)) / (16.5) )")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    1  :  [2]\n" +
                "    /  :  [3]\n" +
                "      *  :  [4]\n" +
                "        x  :  [5]\n" +
                "        *  :  [6]\n" +
                "          33  :  [7]\n" +
                "          3  :  [8]\n" +
                "      16.5  :  [9]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(7, 9).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("/(*(*(33));16.5)", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("/(*(*(33));16.5)", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("/(*(33);*(16.5))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
            assertEquals("/(*(x;3);*())", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
            assertEquals("/(*(x;*(3));1)", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

            val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(/(33;16.5))", result[0].expressionSubstitution.left.toString())
            assertEquals("(2)", result[0].expressionSubstitution.right.toString())
            assertEquals("(/(1;/(*(x;*(33;3));16.5)))", result[0].originalExpression.toString())
            assertEquals("/(*(x;*(33;3));16.5)", result[0].originalExpressionChangingPart.toString())
            assertEquals("(/(1;*(2;x;3)))", result[0].resultExpression.toString())
            assertEquals("*(2;x;3)", result[0].resultExpressionChangingPart.toString())
        }
        if (true) {
            val selectedNodeIds = listOf(8, 9).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("/(*(*(3));16.5)", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("/(*(*(3));16.5)", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("/(*(3);*(16.5))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
            assertEquals("/(*(x;33);*())", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
            assertEquals("/(*(x;*(33));1)", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

            val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
            assertEquals(0, result.size)
        }
    }

    @Test
    fun divNumericReductionSaveDenominatorTest() {
        val expression = parseStringExpression("1 / ( (x*(16.5*3)) / (33) )")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    1  :  [2]\n" +
                "    /  :  [3]\n" +
                "      *  :  [4]\n" +
                "        x  :  [5]\n" +
                "        *  :  [6]\n" +
                "          16.5  :  [7]\n" +
                "          3  :  [8]\n" +
                "      33  :  [9]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(8, 9).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("/(*(*(3));33)", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("/(*(*(3));33)", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("/(*(3);*(33))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
            assertEquals("/(*(x;16.5);*())", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
            assertEquals("/(*(x;*(16.5));1)", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

            val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
            assertEquals(1, result.size)
            assertEquals("(/(3;33))", result[0].expressionSubstitution.left.toString())
            assertEquals("(/(1;11))", result[0].expressionSubstitution.right.toString())
            assertEquals("(/(1;/(*(x;*(16.5;3));33)))", result[0].originalExpression.toString())
            assertEquals("/(*(x;*(16.5;3));33)", result[0].originalExpressionChangingPart.toString())
            assertEquals("(/(1;/(*(x;16.5);11)))", result[0].resultExpression.toString())
            assertEquals("/(*(x;16.5);11)", result[0].resultExpressionChangingPart.toString())
        }
    }

    @Test
    fun divisionTransformationTest() {
        val expression = parseStringExpression("-36*a*((a^3*b)/(c*d*a^2))*(-1/4)*1")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    -  :  [2]\n" +
                "      *  :  [3]\n" +
                "        36  :  [4]\n" +
                "        a  :  [5]\n" +
                "        /  :  [6]\n" +
                "          *  :  [7]\n" +
                "            ^  :  [8]\n" +
                "              a  :  [9]\n" +
                "              3  :  [10]\n" +
                "            b  :  [11]\n" +
                "          *  :  [12]\n" +
                "            c  :  [13]\n" +
                "            d  :  [14]\n" +
                "            ^  :  [15]\n" +
                "              a  :  [16]\n" +
                "              2  :  [17]\n" +
                "        +  :  [18]\n" +
                "          -  :  [19]\n" +
                "            /  :  [20]\n" +
                "              1  :  [21]\n" +
                "              4  :  [22]\n" +
                "        1  :  [23]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(8, 15).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("/(*(^(a;3));*(^(a;2)))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("/(*(^());*(^()))", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("/(*(^(a;3));*(^(a;2)))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("/(*(b);*(c;d))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("/(*(b);*(c;d))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
        assertEquals(1, result.size)
        assertEquals("(/(^(a;3);^(a;2)))", result[0].expressionSubstitution.left.toString())
        assertEquals("(a)", result[0].expressionSubstitution.right.toString())
        assertEquals("(+(-(*(36;a;/(*(^(a;3);b);*(c;d;^(a;2)));+(-(/(1;4)));1))))", result[0].originalExpression.toString())
        assertEquals("/(*(^(a;3);b);*(c;d;^(a;2)))", result[0].originalExpressionChangingPart.toString())
        assertEquals("(+(-(*(36;a;/(*(a;b);*(c;d));+(-(/(1;4)));1))))", result[0].resultExpression.toString())
        assertEquals("/(*(a;b);*(c;d))", result[0].resultExpressionChangingPart.toString())
    }

    @Test
    fun nestedDivisionTransformationTest() {
        val expression = parseStringExpression("((a^3*4)/g*(b-5))/(c*d/a^2)")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    /  :  [2]\n" +
                "      *  :  [3]\n" +
                "        ^  :  [4]\n" +
                "          a  :  [5]\n" +
                "          3  :  [6]\n" +
                "        4  :  [7]\n" +
                "      *  :  [8]\n" +
                "        g  :  [9]\n" +
                "        +  :  [10]\n" +
                "          b  :  [11]\n" +
                "          -  :  [12]\n" +
                "            5  :  [13]\n" +
                "    /  :  [14]\n" +
                "      *  :  [15]\n" +
                "        c  :  [16]\n" +
                "        d  :  [17]\n" +
                "      ^  :  [18]\n" +
                "        a  :  [19]\n" +
                "        2  :  [20]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(8, 15).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("/(/(*(g;+(b;-(5))));/(*(c;d)))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("/(/(*());/(*()))", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("/(*();*(*(g;+(b;-(5)));*(c;d)))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("/(*(*(^(a;3);4);^(a;2));*())", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("/(/(*(^(a;3);4);1);/(1;^(a;2)))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
        assertEquals(0, result.size)
    }

    @Test
    fun nestedMultipleDivisionTransformationTest() {
        val expression = parseStringExpression("(((((5*a^3)*r)*4)/g)*(b-5))/(c*d/a^2)")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    *  :  [2]\n" +
                "      /  :  [3]\n" +
                "        *  :  [4]\n" +
                "          *  :  [5]\n" +
                "            *  :  [6]\n" +
                "              5  :  [7]\n" +
                "              ^  :  [8]\n" +
                "                a  :  [9]\n" +
                "                3  :  [10]\n" +
                "            r  :  [11]\n" +
                "          4  :  [12]\n" +
                "        g  :  [13]\n" +
                "      +  :  [14]\n" +
                "        b  :  [15]\n" +
                "        -  :  [16]\n" +
                "          5  :  [17]\n" +
                "    /  :  [18]\n" +
                "      *  :  [19]\n" +
                "        c  :  [20]\n" +
                "        d  :  [21]\n" +
                "      ^  :  [22]\n" +
                "        a  :  [23]\n" +
                "        2  :  [24]\n", expression.toStringsWithNodeIds())
        if (true) {
            val selectedNodeIds = listOf(8, 22).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("/(*(/(*(*(*(^(a;3))))));/(^(a;2)))", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("/(*(/(*(*(*(^())))));/(^()))", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("/(*(^(a;3);^(a;2));*())", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
            assertEquals("/(*(5;r;4;+(b;-(5)));*(g;*(c;d)))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
            assertEquals("/(*(/(*(*(*(5);r);4);g);+(b;-(5)));/(*(c;d);1))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

            val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
            assertEquals(0, result.size)
        }
        if (true) {
            val selectedNodeIds = listOf(8, 15).toTypedArray()
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
            fillSubstitutionSelectionData(substitutionSelectionData)
            assertEquals("*(/(*(*(*(^(a;3)))));+(b))", substitutionSelectionData.lowestSubtree.toString())
            assertEquals("*(/(*(*(*(^()))));+(b))", substitutionSelectionData.lowestSubtreeHigh.toString())
            assertEquals("*(^(a;3);+(b;-(5)))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
            assertEquals("*(^(a;3);+(b;-(5)))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString())
            assertEquals("*(place_for_result();5;r;4;/(1;g))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
            assertEquals("*(/(*(*(*(5);r);4);g))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

            val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
            assertEquals(0, result.size)
        }
    }

    @Test
    fun nestedDivisionCompletePowTransformationTest() {
        val expression = parseStringExpression("((1/a^5*4)/g*(b-5))/(c*d/a^2)")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    /  :  [2]\n" +
                "      /  :  [3]\n" +
                "        1  :  [4]\n" +
                "        *  :  [5]\n" +
                "          ^  :  [6]\n" +
                "            a  :  [7]\n" +
                "            5  :  [8]\n" +
                "          4  :  [9]\n" +
                "      *  :  [10]\n" +
                "        g  :  [11]\n" +
                "        +  :  [12]\n" +
                "          b  :  [13]\n" +
                "          -  :  [14]\n" +
                "            5  :  [15]\n" +
                "    /  :  [16]\n" +
                "      *  :  [17]\n" +
                "        c  :  [18]\n" +
                "        d  :  [19]\n" +
                "      ^  :  [20]\n" +
                "        a  :  [21]\n" +
                "        2  :  [22]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(20, 6).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("/(/(/(*(^(a;5))));/(^(a;2)))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("/(/(/(*(^())));/(^()))", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("/(*(^(a;2));*(^(a;5)))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("/(*(1);*(4;*(g;+(b;-(5)));*(c;d)))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("/(/(/(1;*(4));*(g;+(b;-(5))));/(*(c;d);1))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
        assertEquals(1, result.size)
        assertEquals("(/(^(a;2);^(a;5)))", result[0].expressionSubstitution.left.toString())
        assertEquals("(/(1;^(a;3)))", result[0].expressionSubstitution.right.toString())
        assertEquals("(/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);^(a;2))))", result[0].originalExpression.toString())
        assertEquals("/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);^(a;2)))", result[0].originalExpressionChangingPart.toString())
        assertEquals("(/(1;*(^(a;3);4;*(g;+(b;-(5)));*(c;d))))", result[0].resultExpression.toString())
        assertEquals("/(1;*(^(a;3);4;*(g;+(b;-(5)));*(c;d)))", result[0].resultExpressionChangingPart.toString())
    }

    @Test
    fun nestedDivisionCompleteNotNumberPowTransformationTest() {
        val expression = parseStringExpression("((1/a^5*4)/g*(b-5))/(c*d/a^b)")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    /  :  [2]\n" +
                "      /  :  [3]\n" +
                "        1  :  [4]\n" +
                "        *  :  [5]\n" +
                "          ^  :  [6]\n" +
                "            a  :  [7]\n" +
                "            5  :  [8]\n" +
                "          4  :  [9]\n" +
                "      *  :  [10]\n" +
                "        g  :  [11]\n" +
                "        +  :  [12]\n" +
                "          b  :  [13]\n" +
                "          -  :  [14]\n" +
                "            5  :  [15]\n" +
                "    /  :  [16]\n" +
                "      *  :  [17]\n" +
                "        c  :  [18]\n" +
                "        d  :  [19]\n" +
                "      ^  :  [20]\n" +
                "        a  :  [21]\n" +
                "        b  :  [22]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(20, 6).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("/(/(/(*(^(a;5))));/(^(a;b)))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("/(/(/(*(^())));/(^()))", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("/(*(^(a;b));*(^(a;5)))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("/(*(1);*(4;*(g;+(b;-(5)));*(c;d)))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("/(/(/(1;*(4));*(g;+(b;-(5))));/(*(c;d);1))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = generateReduceFractionSubstitutions(substitutionSelectionData, true, true)
        assertEquals(2, result.size)
        assertEquals("(/(^(a;b);^(a;5)))", result[0].expressionSubstitution.left.toString())
        assertEquals("(^(a;+(b;-(5))))", result[0].expressionSubstitution.right.toString())
        assertEquals("(/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);^(a;b))))", result[0].originalExpression.toString())
        assertEquals("/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);^(a;b)))", result[0].originalExpressionChangingPart.toString())
        assertEquals("(/(*(1;^(a;+(b;-(5))));*(4;*(g;+(b;-(5)));*(c;d))))", result[0].resultExpression.toString())
        assertEquals("/(*(1;^(a;+(b;-(5))));*(4;*(g;+(b;-(5)));*(c;d)))", result[0].resultExpressionChangingPart.toString())

        assertEquals("(/(^(a;b);^(a;5)))", result[1].expressionSubstitution.left.toString())
        assertEquals("(/(1;^(a;+(5;-(b)))))", result[1].expressionSubstitution.right.toString())
        assertEquals("(/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);^(a;b))))", result[1].originalExpression.toString())
        assertEquals("/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);^(a;b)))", result[1].originalExpressionChangingPart.toString())
        assertEquals("(/(1;*(4;*(g;+(b;-(5)));*(c;d);^(a;+(5;-(b))))))", result[1].resultExpression.toString())
        assertEquals("/(1;*(4;*(g;+(b;-(5)));*(c;d);^(a;+(5;-(b)))))", result[1].resultExpressionChangingPart.toString())
    }


    @Test
    fun nestedDivisionCompleteNotSpecifiedPowTransformationTest() {
        val expression = parseStringExpression("((1/a^5*4)/g*(b-5))/(c*d/a)")
        assertEquals("  :  [0]\n" +
                "  /  :  [1]\n" +
                "    /  :  [2]\n" +
                "      /  :  [3]\n" +
                "        1  :  [4]\n" +
                "        *  :  [5]\n" +
                "          ^  :  [6]\n" +
                "            a  :  [7]\n" +
                "            5  :  [8]\n" +
                "          4  :  [9]\n" +
                "      *  :  [10]\n" +
                "        g  :  [11]\n" +
                "        +  :  [12]\n" +
                "          b  :  [13]\n" +
                "          -  :  [14]\n" +
                "            5  :  [15]\n" +
                "    /  :  [16]\n" +
                "      *  :  [17]\n" +
                "        c  :  [18]\n" +
                "        d  :  [19]\n" +
                "      a  :  [20]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(20, 6).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionSimpleAdditionalTreeTransformationRules.clear()
            compiledExpressionSimpleAdditionalTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(x)", "(^(x;1))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("/(/(/(*(^(a;5))));/(a))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("/(/(/(*(^())));/(a))", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("/(*(a);*(^(a;5)))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("/()", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct
        assertEquals("/(*(1);*(4;*(g;+(b;-(5)));*(c;d)))", substitutionSelectionData.notSelectedSubtreeTopArguments.toString())
        assertEquals("/(/(/(1;*(4));*(g;+(b;-(5))));/(*(c;d);1))", substitutionSelectionData.notSelectedSubtreeTopOriginalTree.toString())

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals(3, result.size)
        assertEquals("(/(^(a;1);^(a;5)))", result[2].expressionSubstitution.left.toString()) // it's correct that in case when additional transformation (like x -> x^1) is needed rule is placed after rules without additional transformations
        assertEquals("(/(1;^(a;4)))", result[2].expressionSubstitution.right.toString())
        assertEquals("(/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);^(a;1))))", result[2].originalExpression.toString())
        assertEquals("/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);^(a;1)))", result[2].originalExpressionChangingPart.toString())
        assertEquals("(/(1;*(^(a;4);4;*(g;+(b;-(5)));*(c;d))))", result[2].resultExpression.toString())
        assertEquals("/(1;*(^(a;4);4;*(g;+(b;-(5)));*(c;d)))", result[2].resultExpressionChangingPart.toString())

        assertEquals("(a)", result[0].expressionSubstitution.left.toString())
        assertEquals("(+(a;^(a;5);-(^(a;5))))", result[0].expressionSubstitution.right.toString())
        assertEquals("(/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);a)))", result[0].originalExpression.toString())
        assertEquals("a", result[0].originalExpressionChangingPart.toString())
        assertEquals("(/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);+(a;^(a;5);-(^(a;5))))))", result[0].resultExpression.toString())
        assertEquals("+(a;^(a;5);-(^(a;5)))", result[0].resultExpressionChangingPart.toString())

        assertEquals("(a)", result[1].expressionSubstitution.left.toString())
        assertEquals("(/(*(a;^(a;5));^(a;5)))", result[1].expressionSubstitution.right.toString())
        assertEquals("(/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);a)))", result[1].originalExpression.toString())
        assertEquals("a", result[1].originalExpressionChangingPart.toString())
        assertEquals("(/(/(/(1;*(^(a;5);4));*(g;+(b;-(5))));/(*(c;d);/(*(a;^(a;5));^(a;5)))))", result[1].resultExpression.toString())
        assertEquals("/(*(a;^(a;5));^(a;5))", result[1].resultExpressionChangingPart.toString())
    }


    @Test
    fun unaryMinusTransformationTest() {
        val expression = parseStringExpression("1-cos(x)-sin(x)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    1  :  [2]\n" +
                "    -  :  [3]\n" +
                "      cos  :  [4]\n" +
                "        x  :  [5]\n" +
                "    -  :  [6]\n" +
                "      sin  :  [7]\n" +
                "        x  :  [8]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(7).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(sin(x))))", "(sin(+(-(x))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(sin(x))", "(+(-(sin(+(-(x))))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(cos(x))))", "(cos(+(π;-(x))))"))

            compiledExpressionSimpleAdditionalTreeTransformationRules.clear()
            compiledExpressionSimpleAdditionalTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(x)", "(^(x;1))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("sin(x)", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("sin()", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("sin(x)", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("sin(x)", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals(2, result.size)
        assertEquals("(sin(x))", result[0].expressionSubstitution.left.toString())
        assertEquals("(+(-(sin(+(-(x))))))", result[0].expressionSubstitution.right.toString())
        assertEquals("(+(1;-(cos(x));-(sin(x))))", result[0].originalExpression.toString())
        assertEquals("sin(x)", result[0].originalExpressionChangingPart.toString())
        assertEquals("(+(1;-(cos(x));-(+(-(sin(+(-(x))))))))", result[0].resultExpression.toString())
        assertEquals("+(-(sin(+(-(x)))))", result[0].resultExpressionChangingPart.toString())

        assertEquals("(+(-(sin(x))))", result[1].expressionSubstitution.left.toString())
        assertEquals("(sin(+(-(x))))", result[1].expressionSubstitution.right.toString())
        assertEquals("(+(1;-(cos(x));+(-(sin(x)))))", result[1].originalExpression.toString())
        assertEquals("+(-(sin(x)))", result[1].originalExpressionChangingPart.toString())
        assertEquals("(+(1;-(cos(x));sin(+(-(x)))))", result[1].resultExpression.toString())
        assertEquals("sin(+(-(x)))", result[1].resultExpressionChangingPart.toString())
    }


    @Test
    fun extraBracketsExpansionTest() {
        val expression = parseStringExpression("(1+cos(x)-7)-y+sin(x)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    +  :  [2]\n" +
                "      1  :  [3]\n" +
                "      cos  :  [4]\n" +
                "        x  :  [5]\n" +
                "      -  :  [6]\n" +
                "        7  :  [7]\n" +
                "    -  :  [8]\n" +
                "      y  :  [9]\n" +
                "    sin  :  [10]\n" +
                "      x  :  [11]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(sin(x))))", "(sin(+(-(x))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(sin(x))", "(+(-(sin(+(-(x))))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(cos(x))))", "(cos(+(π;-(x))))"))

            compiledExpressionSimpleAdditionalTreeTransformationRules.clear()
            compiledExpressionSimpleAdditionalTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(x)", "(^(x;1))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionData)
        assertEquals("+(1;cos(x);-(7))", substitutionSelectionData.lowestSubtree.toString())
        assertEquals("+()", substitutionSelectionData.lowestSubtreeHigh.toString())
        assertEquals("+(1;cos(x);-(7))", substitutionSelectionData.selectedSubtreeTopArguments.toString())
        assertEquals("+(1;cos(x);-(7))", substitutionSelectionData.selectedSubtreeTopArgumentsInSelectionOrder.toString()) // selectedSubtreeTopArgumentsInSelectionOrder are not used if division, it's correct

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("ParentBracketsExpansion", result.map { it.substitutionType }.joinToString())
        assertEquals(1, result.size)
        assertEquals("(+(+(1;cos(x);-(7));-(y);sin(x)))", result[0].expressionSubstitution.left.toString())
        assertEquals("(+(1;cos(x);-(7);-(y);sin(x)))", result[0].expressionSubstitution.right.toString())
        assertEquals("(+(+(1;cos(x);-(7));-(y);sin(x)))", result[0].originalExpression.toString())
        assertEquals("+(+(1;cos(x);-(7));-(y);sin(x))", result[0].originalExpressionChangingPart.toString())
        assertEquals("(+(1;cos(x);-(7);-(y);sin(x)))", result[0].resultExpression.toString())
        assertEquals("+(1;cos(x);-(7);-(y);sin(x))", result[0].resultExpressionChangingPart.toString())
    }


    @Test
    fun extraBracketsExpansionTestInSin() {
        val expression = parseStringExpression("sin(-y+(1+cos(x)-7)-y+sin(x))")
        assertEquals("  :  [0]\n" +
                "  sin  :  [1]\n" +
                "    +  :  [2]\n" +
                "      -  :  [3]\n" +
                "        y  :  [4]\n" +
                "      +  :  [5]\n" +
                "        1  :  [6]\n" +
                "        cos  :  [7]\n" +
                "          x  :  [8]\n" +
                "        -  :  [9]\n" +
                "          7  :  [10]\n" +
                "      -  :  [11]\n" +
                "        y  :  [12]\n" +
                "      sin  :  [13]\n" +
                "        x  :  [14]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(5).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(sin(x))))", "(sin(+(-(x))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(sin(x))", "(+(-(sin(+(-(x))))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(cos(x))))", "(cos(+(π;-(x))))"))

            compiledExpressionSimpleAdditionalTreeTransformationRules.clear()
            compiledExpressionSimpleAdditionalTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(x)", "(^(x;1))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("ParentBracketsExpansion", result.map { it.substitutionType }.joinToString())
        assertEquals(1, result.size)
        assertEquals("(+(-(y);+(1;cos(x);-(7));-(y);sin(x)))", result[0].expressionSubstitution.left.toString())
        assertEquals("(+(-(y);1;cos(x);-(7);-(y);sin(x)))", result[0].expressionSubstitution.right.toString())
        assertEquals("(sin(+(-(y);+(1;cos(x);-(7));-(y);sin(x))))", result[0].originalExpression.toString())
        assertEquals("+(-(y);+(1;cos(x);-(7));-(y);sin(x))", result[0].originalExpressionChangingPart.toString())
        assertEquals("(sin(+(-(y);1;cos(x);-(7);-(y);sin(x))))", result[0].resultExpression.toString())
        assertEquals("+(-(y);1;cos(x);-(7);-(y);sin(x))", result[0].resultExpressionChangingPart.toString())
    }


    @Test
    fun permutationProblemSubstitutions() {
        val expression = parseStringExpression("sin(a^2+b^2+b*2*a)")
        assertEquals("  :  [0]\n" +
                "  sin  :  [1]\n" +
                "    +  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        a  :  [4]\n" +
                "        2  :  [5]\n" +
                "      ^  :  [6]\n" +
                "        b  :  [7]\n" +
                "        2  :  [8]\n" +
                "      *  :  [9]\n" +
                "        b  :  [10]\n" +
                "        2  :  [11]\n" +
                "        a  :  [12]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(^(a;2);*(2;a;b);^(b;2)))", "(^(+(a;b);2))", matchJumbledAndNested = true))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(sin(x))", "(+(-(sin(+(-(x))))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(cos(x))))", "(cos(+(π;-(x))))"))

            compiledExpressionSimpleAdditionalTreeTransformationRules.clear()
            compiledExpressionSimpleAdditionalTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(x)", "(^(x;1))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals(1, result.size)
        assertEquals("(+(^(a;2);*(2;a;b);^(b;2)))", result[0].expressionSubstitution.left.toString())
        assertEquals("(^(+(a;b);2))", result[0].expressionSubstitution.right.toString())
        assertEquals("(sin(+(^(a;2);^(b;2);*(b;2;a))))", result[0].originalExpression.toString())
        assertEquals("+(^(a;2);^(b;2);*(b;2;a))", result[0].originalExpressionChangingPart.toString())
        assertEquals("(sin(^(+(a;b);2)))", result[0].resultExpression.toString())
        assertEquals("^(+(a;b);2)", result[0].resultExpressionChangingPart.toString())
    }


    @Test
    fun permutationProblemSubstitutionsNestedBrackets() {
        val expression = parseStringExpression("sin(a^2+b^2+2*(b*a))")
        assertEquals("  :  [0]\n" +
                "  sin  :  [1]\n" +
                "    +  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        a  :  [4]\n" +
                "        2  :  [5]\n" +
                "      ^  :  [6]\n" +
                "        b  :  [7]\n" +
                "        2  :  [8]\n" +
                "      *  :  [9]\n" +
                "        2  :  [10]\n" +
                "        *  :  [11]\n" +
                "          b  :  [12]\n" +
                "          a  :  [13]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(^(a;2);*(2;a;b);^(b;2)))", "(^(+(a;b);2))", matchJumbledAndNested = true))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(sin(x))", "(+(-(sin(+(-(x))))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(cos(x))))", "(cos(+(π;-(x))))"))

            compiledExpressionSimpleAdditionalTreeTransformationRules.clear()
            compiledExpressionSimpleAdditionalTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(x)", "(^(x;1))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals(1, result.size)
        assertEquals("(+(^(a;2);*(2;a;b);^(b;2)))", result[0].expressionSubstitution.left.toString())
        assertEquals("(^(+(a;b);2))", result[0].expressionSubstitution.right.toString())
        assertEquals("(sin(+(^(a;2);^(b;2);*(2;*(b;a)))))", result[0].originalExpression.toString())
        assertEquals("+(^(a;2);^(b;2);*(2;*(b;a)))", result[0].originalExpressionChangingPart.toString())
        assertEquals("(sin(^(+(a;b);2)))", result[0].resultExpression.toString())
        assertEquals("^(+(a;b);2)", result[0].resultExpressionChangingPart.toString())
    }


    @Test
    fun permutationProblemSubstitutionsNestedBracketsCommutativeZerosMatchingUse() {
        val expression = parseStringExpression("sin(a^2+1+2*a)")
        assertEquals("  :  [0]\n" +
                "  sin  :  [1]\n" +
                "    +  :  [2]\n" +
                "      ^  :  [3]\n" +
                "        a  :  [4]\n" +
                "        2  :  [5]\n" +
                "      1  :  [6]\n" +
                "      *  :  [7]\n" +
                "        2  :  [8]\n" +
                "        a  :  [9]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3, 6, 7).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(^(a;2);*(2;a;b);^(b;2)))", "(^(+(a;b);2))", matchJumbledAndNested = true))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(sin(x))", "(+(-(sin(+(-(x))))))"))
            compiledExpressionTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(+(-(cos(x))))", "(cos(+(π;-(x))))"))

            compiledExpressionSimpleAdditionalTreeTransformationRules.clear()
            compiledExpressionSimpleAdditionalTreeTransformationRules.add(expressionSubstitutionFromStructureStrings("(1)", "(^(1;2))"))
        })
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals(1, result.size)
        assertEquals("(+(^(a;2);*(2;a;b);^(b;2)))", result[0].expressionSubstitution.left.toString())
        assertEquals("(^(+(a;b);2))", result[0].expressionSubstitution.right.toString())
        assertEquals("(sin(+(^(a;2);^(1;2);*(2;a))))", result[0].originalExpression.toString())
        assertEquals("+(^(a;2);^(1;2);*(2;a))", result[0].originalExpressionChangingPart.toString())
        assertEquals("(sin(^(+(a;1);2)))", result[0].resultExpression.toString())
        assertEquals("^(+(a;1);2)", result[0].resultExpressionChangingPart.toString())
    }


    @Test
    fun setSubstitutionsTest() {
        val expression = parseStringExpression("A | B | C | D")
        assertEquals("  :  [0]\n" +
                "  or  :  [1]\n" +
                "    A  :  [2]\n" +
                "    B  :  [3]\n" +
                "    C  :  [4]\n" +
                "    D  :  [5]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(1, 3).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "SetComplicatingExtension"),
                        expressionSubstitutionFromStructureStrings(code = "AbsorptionComplicatingExtension")
                ).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
        })
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals(2, result.size)
        assertEquals("(and(or(A;B;C;D);or(or(A;B;C;D);B)))", result[0].resultExpression.toString())
        assertEquals("(or(or(A;B;C;D);and(or(A;B;C;D);B)))", result[1].resultExpression.toString())
    }


    @Test
    fun simpleNestedSubstitutionsTest() {
        val expression = parseStringExpression("180+160+140+120")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    180  :  [2]\n" +
                "    160  :  [3]\n" +
                "    140  :  [4]\n" +
                "    120  :  [5]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(1, 3).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic"),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ).apply {
            compiledExpressionTreeTransformationRules.clear()
        })
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals(3, result.size)
        assertEquals("(+(+(180;160;140;120);160;-(160)))", result[1].resultExpression.toString())
        assertEquals("(/(*(+(180;160;140;120);160);160))", result[2].resultExpression.toString())
    }


    @Test
    fun additionReducingTest() {
        val expression = parseStringExpression("2*x*y^2 - 12*x^3*y + 18*x^5")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    *  :  [2]\n" +
                "      2  :  [3]\n" +
                "      x  :  [4]\n" +
                "      ^  :  [5]\n" +
                "        y  :  [6]\n" +
                "        2  :  [7]\n" +
                "    -  :  [8]\n" +
                "      *  :  [9]\n" +
                "        12  :  [10]\n" +
                "        ^  :  [11]\n" +
                "          x  :  [12]\n" +
                "          3  :  [13]\n" +
                "        y  :  [14]\n" +
                "    *  :  [15]\n" +
                "      18  :  [16]\n" +
                "      ^  :  [17]\n" +
                "        x  :  [18]\n" +
                "        5  :  [19]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(11, 17).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(
                        expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                        expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                        expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                        expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                        expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                        expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic", priority = 5),
                        expressionSubstitutionFromStructureStrings(code = "TwoSidesArithmeticReduce", priority = 5),
                        expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                        expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                        expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
        ))
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: 'x^3*(-12*y+18*x^2)+2*x*y^2'\n" +
                "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "expressionSubstitution.right: '(*(^(x;3);+(-(*(12;y));*(18;^(x;2)))))'\n" +
                "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5)))'\n" +
                "resultExpression: '(+(*(^(x;3);+(-(*(12;y));*(18;^(x;2))));*(2;x;^(y;2))))'\n" +
                "resultExpressionChangingPart: '*(^(x;3);+(-(*(12;y));*(18;^(x;2))))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: '2*x*y^2-12*(x^3+x^5-x^5)*y+18*x^5'\n" +
                "expressionSubstitution.left: '(^(x;3))'\n" +
                "expressionSubstitution.right: '(+(^(x;3);^(x;5);-(^(x;5))))'\n" +
                "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "originalExpressionChangingPart: '^(x;3)'\n" +
                "resultExpression: '(+(*(2;x;^(y;2));-(*(12;+(^(x;3);^(x;5);-(^(x;5)));y));*(18;^(x;5))))'\n" +
                "resultExpressionChangingPart: '+(^(x;3);^(x;5);-(^(x;5)))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'\n" +
                "\n" +
                "result: '2*x*y^2-12*((x^3*x^5)/x^5)*y+18*x^5'\n" +
                "expressionSubstitution.left: '(^(x;3))'\n" +
                "expressionSubstitution.right: '(/(*(^(x;3);^(x;5));^(x;5)))'\n" +
                "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "originalExpressionChangingPart: '^(x;3)'\n" +
                "resultExpression: '(+(*(2;x;^(y;2));-(*(12;/(*(^(x;3);^(x;5));^(x;5));y));*(18;^(x;5))))'\n" +
                "resultExpressionChangingPart: '/(*(^(x;3);^(x;5));^(x;5))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'\n" +
                "\n" +
                "result: '(-12*x^3*y+18*x^5)+2*x*y^2'\n" +
                "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "expressionSubstitution.right: '(+(+(-(*(12;^(x;3);y));*(18;^(x;5)))))'\n" +
                "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5)))'\n" +
                "resultExpression: '(+(+(-(*(12;^(x;3);y));*(18;^(x;5)));*(2;x;^(y;2))))'\n" +
                "resultExpressionChangingPart: '+(+(-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '90'", result.joinToString(separator = "\n\n"))
    }


    @Test
    fun addition2variablesReducingTest() {
        val expression = parseStringExpression("2*x*y^2 - 12*x^3*y + 18*x^5*y")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    *  :  [2]\n" +
                "      2  :  [3]\n" +
                "      x  :  [4]\n" +
                "      ^  :  [5]\n" +
                "        y  :  [6]\n" +
                "        2  :  [7]\n" +
                "    -  :  [8]\n" +
                "      *  :  [9]\n" +
                "        12  :  [10]\n" +
                "        ^  :  [11]\n" +
                "          x  :  [12]\n" +
                "          3  :  [13]\n" +
                "        y  :  [14]\n" +
                "    *  :  [15]\n" +
                "      18  :  [16]\n" +
                "      ^  :  [17]\n" +
                "        x  :  [18]\n" +
                "        5  :  [19]\n" +
                "      y  :  [20]\n", expression.toStringsWithNodeIds())

        if (true) {
            val selectedNodeIds = listOf(11, 20).toTypedArray()  // x^3 and y selected
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                    listOf(
                            expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                            expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                            expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                            expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                            expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                            expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                            expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic", priority = 5),
                            expressionSubstitutionFromStructureStrings(code = "TwoSidesArithmeticReduce", priority = 5),
                            expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                            expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                            expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                            expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
            ))
            fillSubstitutionSelectionData(substitutionSelectionData)

            val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
            assertEquals("" +
                    "result: 'y*(-12*x^3+18*x^5)+2*x*y^2'\n" +
                    "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "expressionSubstitution.right: '(*(y;+(-(*(12;^(x;3)));*(18;^(x;5)))))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5);y))'\n" +
                    "resultExpression: '(+(*(y;+(-(*(12;^(x;3)));*(18;^(x;5))));*(2;x;^(y;2))))'\n" +
                    "resultExpressionChangingPart: '*(y;+(-(*(12;^(x;3)));*(18;^(x;5))))'\n" +
                    "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                    "priority: '5'\n" +
                    "\n" +
                    "result: 'x^3*y*(-12+18*x^2)+2*x*y^2'\n" +
                    "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "expressionSubstitution.right: '(*(^(x;3);y;+(-(12);*(18;^(x;2)))))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5);y))'\n" +
                    "resultExpression: '(+(*(^(x;3);y;+(-(12);*(18;^(x;2))));*(2;x;^(y;2))))'\n" +
                    "resultExpressionChangingPart: '*(^(x;3);y;+(-(12);*(18;^(x;2))))'\n" +
                    "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                    "priority: '5'\n" +
                    "\n" +
                    "result: '2*x*y^2-12*(x^3+y-y)*y+18*x^5*y'\n" +
                    "expressionSubstitution.left: '(^(x;3))'\n" +
                    "expressionSubstitution.right: '(+(^(x;3);y;-(y)))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '^(x;3)'\n" +
                    "resultExpression: '(+(*(2;x;^(y;2));-(*(12;+(^(x;3);y;-(y));y));*(18;^(x;5);y)))'\n" +
                    "resultExpressionChangingPart: '+(^(x;3);y;-(y))'\n" +
                    "substitutionType: 'ComplicatingExtension'\n" +
                    "priority: '60'\n" +
                    "\n" +
                    "result: '2*x*y^2-12*((x^3*y)/y)*y+18*x^5*y'\n" +
                    "expressionSubstitution.left: '(^(x;3))'\n" +
                    "expressionSubstitution.right: '(/(*(^(x;3);y);y))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '^(x;3)'\n" +
                    "resultExpression: '(+(*(2;x;^(y;2));-(*(12;/(*(^(x;3);y);y);y));*(18;^(x;5);y)))'\n" +
                    "resultExpressionChangingPart: '/(*(^(x;3);y);y)'\n" +
                    "substitutionType: 'ComplicatingExtension'\n" +
                    "priority: '60'\n" +
                    "\n" +
                    "result: '(-12*x^3*y+18*x^5*y)+2*x*y^2'\n" +
                    "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "expressionSubstitution.right: '(+(+(-(*(12;^(x;3);y));*(18;^(x;5);y))))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5);y))'\n" +
                    "resultExpression: '(+(+(-(*(12;^(x;3);y));*(18;^(x;5);y));*(2;x;^(y;2))))'\n" +
                    "resultExpressionChangingPart: '+(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "substitutionType: 'SelectedOrderExtraction'\n" +
                    "priority: '90'", result.joinToString(separator = "\n\n"))
        }

        if (true) {
            val selectedNodeIds = listOf(11, 17).toTypedArray() // x^3 and x^5 selected
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                    listOf(
                            expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                            expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                            expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                            expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                            expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                            expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                            expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic", priority = 5),
                            expressionSubstitutionFromStructureStrings(code = "TwoSidesArithmeticReduce", priority = 5),
                            expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                            expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                            expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                            expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
            ))
            fillSubstitutionSelectionData(substitutionSelectionData)

            val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
            assertEquals("" +
                    "result: 'y*(-12*x^3+18*x^5)+2*x*y^2'\n" +
                    "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "expressionSubstitution.right: '(*(y;+(-(*(12;^(x;3)));*(18;^(x;5)))))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5);y))'\n" +
                    "resultExpression: '(+(*(y;+(-(*(12;^(x;3)));*(18;^(x;5))));*(2;x;^(y;2))))'\n" +
                    "resultExpressionChangingPart: '*(y;+(-(*(12;^(x;3)));*(18;^(x;5))))'\n" +
                    "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                    "priority: '5'\n" +
                    "\n" +
                    "result: 'x^3*(-12*y+18*x^2*y)+2*x*y^2'\n" +
                    "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "expressionSubstitution.right: '(*(^(x;3);+(-(*(12;y));*(18;^(x;2);y))))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5);y))'\n" +
                    "resultExpression: '(+(*(^(x;3);+(-(*(12;y));*(18;^(x;2);y)));*(2;x;^(y;2))))'\n" +
                    "resultExpressionChangingPart: '*(^(x;3);+(-(*(12;y));*(18;^(x;2);y)))'\n" +
                    "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                    "priority: '5'\n" +
                    "\n" +
                    "result: '2*x*y^2-12*(x^3+x^5-x^5)*y+18*x^5*y'\n" +
                    "expressionSubstitution.left: '(^(x;3))'\n" +
                    "expressionSubstitution.right: '(+(^(x;3);^(x;5);-(^(x;5))))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '^(x;3)'\n" +
                    "resultExpression: '(+(*(2;x;^(y;2));-(*(12;+(^(x;3);^(x;5);-(^(x;5)));y));*(18;^(x;5);y)))'\n" +
                    "resultExpressionChangingPart: '+(^(x;3);^(x;5);-(^(x;5)))'\n" +
                    "substitutionType: 'ComplicatingExtension'\n" +
                    "priority: '60'\n" +
                    "\n" +
                    "result: '2*x*y^2-12*((x^3*x^5)/x^5)*y+18*x^5*y'\n" +
                    "expressionSubstitution.left: '(^(x;3))'\n" +
                    "expressionSubstitution.right: '(/(*(^(x;3);^(x;5));^(x;5)))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '^(x;3)'\n" +
                    "resultExpression: '(+(*(2;x;^(y;2));-(*(12;/(*(^(x;3);^(x;5));^(x;5));y));*(18;^(x;5);y)))'\n" +
                    "resultExpressionChangingPart: '/(*(^(x;3);^(x;5));^(x;5))'\n" +
                    "substitutionType: 'ComplicatingExtension'\n" +
                    "priority: '60'\n" +
                    "\n" +
                    "result: '(-12*x^3*y+18*x^5*y)+2*x*y^2'\n" +
                    "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "expressionSubstitution.right: '(+(+(-(*(12;^(x;3);y));*(18;^(x;5);y))))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5);y))'\n" +
                    "resultExpression: '(+(+(-(*(12;^(x;3);y));*(18;^(x;5);y));*(2;x;^(y;2))))'\n" +
                    "resultExpressionChangingPart: '+(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "substitutionType: 'SelectedOrderExtraction'\n" +
                    "priority: '90'", result.joinToString(separator = "\n\n"))
        }

        if (true) {
            val selectedNodeIds = listOf(17, 14).toTypedArray()  // x^5 and y selected
            val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                    listOf(
                            expressionSubstitutionFromStructureStrings(code = "NumberPlusMinus1"),
                            expressionSubstitutionFromStructureStrings(code = "DecimalToFraction"),
                            expressionSubstitutionFromStructureStrings(code = "PowFactorization"),
                            expressionSubstitutionFromStructureStrings(code = "MultiplicationFactorization"),
                            expressionSubstitutionFromStructureStrings(code = "OpeningBrackets"),
                            expressionSubstitutionFromStructureStrings(code = "ParentBracketsExpansion"),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsSwap"),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutation", priority = 90),
                            expressionSubstitutionFromStructureStrings(code = "ArgumentsPermutationInOriginalOrder", priority = 90),
                            expressionSubstitutionFromStructureStrings(code = "ReduceArithmetic", priority = 5),
                            expressionSubstitutionFromStructureStrings(code = "TwoSidesArithmeticReduce", priority = 5),
                            expressionSubstitutionFromStructureStrings(code = "ReduceFraction"),
                            expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension", priority = 60),
                            expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension", priority = 60),
                            expressionSubstitutionFromStructureStrings(code = TREE_COMPUTATION_DEFAULT)).toTypedArray()
            ))
            fillSubstitutionSelectionData(substitutionSelectionData)

            val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
            assertEquals("" +
                    "result: 'y*(18*x^5-12*x^3)+2*x*y^2'\n" +
                    "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "expressionSubstitution.right: '(*(y;+(*(18;^(x;5));-(*(12;^(x;3))))))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5);y))'\n" +
                    "resultExpression: '(+(*(y;+(*(18;^(x;5));-(*(12;^(x;3)))));*(2;x;^(y;2))))'\n" +
                    "resultExpressionChangingPart: '*(y;+(*(18;^(x;5));-(*(12;^(x;3)))))'\n" +
                    "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                    "priority: '5'\n" +
                    "\n" +
                    "result: '2*x*y^2-12*x^3*y+18*(x^5+y-y)*y'\n" +
                    "expressionSubstitution.left: '(^(x;5))'\n" +
                    "expressionSubstitution.right: '(+(^(x;5);y;-(y)))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '^(x;5)'\n" +
                    "resultExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;+(^(x;5);y;-(y));y)))'\n" +
                    "resultExpressionChangingPart: '+(^(x;5);y;-(y))'\n" +
                    "substitutionType: 'ComplicatingExtension'\n" +
                    "priority: '60'\n" +
                    "\n" +
                    "result: '2*x*y^2-12*x^3*y+18*((x^5*y)/y)*y'\n" +
                    "expressionSubstitution.left: '(^(x;5))'\n" +
                    "expressionSubstitution.right: '(/(*(^(x;5);y);y))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '^(x;5)'\n" +
                    "resultExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;/(*(^(x;5);y);y);y)))'\n" +
                    "resultExpressionChangingPart: '/(*(^(x;5);y);y)'\n" +
                    "substitutionType: 'ComplicatingExtension'\n" +
                    "priority: '60'\n" +
                    "\n" +
                    "result: '(18*x^5*y-12*x^3*y)+2*x*y^2'\n" +
                    "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "expressionSubstitution.right: '(+(+(*(18;^(x;5);y);-(*(12;^(x;3);y)))))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5);y))'\n" +
                    "resultExpression: '(+(+(*(18;^(x;5);y);-(*(12;^(x;3);y)));*(2;x;^(y;2))))'\n" +
                    "resultExpressionChangingPart: '+(+(*(18;^(x;5);y);-(*(12;^(x;3);y))))'\n" +
                    "substitutionType: 'SelectedOrderExtraction'\n" +
                    "priority: '90'\n" +
                    "\n" +
                    "result: '(-12*x^3*y+18*x^5*y)+2*x*y^2'\n" +
                    "expressionSubstitution.left: '(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "expressionSubstitution.right: '(+(+(-(*(12;^(x;3);y));*(18;^(x;5);y))))'\n" +
                    "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "originalExpressionChangingPart: '+(-(*(12;^(x;3);y));*(18;^(x;5);y))'\n" +
                    "resultExpression: '(+(+(-(*(12;^(x;3);y));*(18;^(x;5);y));*(2;x;^(y;2))))'\n" +
                    "resultExpressionChangingPart: '+(+(-(*(12;^(x;3);y));*(18;^(x;5);y)))'\n" +
                    "substitutionType: 'OriginalOrderExtraction'\n" +
                    "priority: '90'", result.joinToString(separator = "\n\n"))
        }
    }


    @Test
    fun extensionFailingTest() {
        val expression1 = parseStringExpression("180+160+140+120")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    180  :  [2]\n" +
                "    160  :  [3]\n" +
                "    140  :  [4]\n" +
                "    120  :  [5]\n", expression1.toStringsWithNodeIds())
        val selectedNodeIds1 = listOf(4, 1).toTypedArray()
        val substitutionSelectionData1 = SubstitutionSelectionData(expression1, selectedNodeIds1, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"), expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension")).toTypedArray()
        ))
        fillSubstitutionSelectionData(substitutionSelectionData1)

        val result1 = generateSubstitutionsBySelectedNodes(substitutionSelectionData1, true, true)
        assertEquals("" +
                "result: '140+180+160+120'\n" +
                "expressionSubstitution.left: '(+(140))'\n" +
                "expressionSubstitution.right: '(140)'\n" +
                "originalExpression: '(+(180;160;140;120))'\n" +
                "originalExpressionChangingPart: '+(140)'\n" +
                "resultExpression: '(+(140;180;160;120))'\n" +
                "resultExpressionChangingPart: '140'\n" +
                "substitutionType: 'SimpleComputation'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: '180+160+(140+(180+160+140+120)-(180+160+140+120))+120'\n" +
                "expressionSubstitution.left: '(140)'\n" +
                "expressionSubstitution.right: '(+(140;+(180;160;140;120);-(+(180;160;140;120))))'\n" +
                "originalExpression: '(+(180;160;140;120))'\n" +
                "originalExpressionChangingPart: '140'\n" +
                "resultExpression: '(+(180;160;+(140;+(180;160;140;120);-(+(180;160;140;120)));120))'\n" +
                "resultExpressionChangingPart: '+(140;+(180;160;140;120);-(+(180;160;140;120)))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '180+160+(140*(180+160+140+120))/(180+160+140+120)+120'\n" +
                "expressionSubstitution.left: '(140)'\n" +
                "expressionSubstitution.right: '(/(*(140;+(180;160;140;120));+(180;160;140;120)))'\n" +
                "originalExpression: '(+(180;160;140;120))'\n" +
                "originalExpressionChangingPart: '140'\n" +
                "resultExpression: '(+(180;160;/(*(140;+(180;160;140;120));+(180;160;140;120));120))'\n" +
                "resultExpressionChangingPart: '/(*(140;+(180;160;140;120));+(180;160;140;120))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '50'", result1.joinToString(separator = "\n\n"))


        val expression = result1[2].resultExpression
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    180  :  [2]\n" +
                "    160  :  [3]\n" +
                "    /  :  [4]\n" +
                "      *  :  [5]\n" +
                "        140  :  [6]\n" +
                "        +  :  [7]\n" +
                "          180  :  [8]\n" +
                "          160  :  [9]\n" +
                "          140  :  [10]\n" +
                "          120  :  [11]\n" +
                "      +  :  [12]\n" +
                "        180  :  [13]\n" +
                "        160  :  [14]\n" +
                "        140  :  [15]\n" +
                "        120  :  [16]\n" +
                "    120  :  [17]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(7, 5).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(expressionSubstitutionFromStructureStrings(code = "AdditiveComplicatingExtension"), expressionSubstitutionFromStructureStrings(code = "MultiplicativeComplicatingExtension")).toTypedArray()
        ))
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals(3, result.size)
        assertEquals("(+(180;160;/(*(140;+(+(180;160;140;120);*(140;+(180;160;140;120));-(*(140;+(180;160;140;120)))));+(180;160;140;120));120))", result[1].resultExpression.toString())
        assertEquals("(+(180;160;/(*(140;/(*(+(180;160;140;120);*(140;+(180;160;140;120)));*(140;+(180;160;140;120))));+(180;160;140;120));120))", result[2].resultExpression.toString())
    }

    @Test
    fun minusOutTest() {
        val expression = parseStringExpression("2*x*y^2 - 12*x^3*y + 18*x^5")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    *  :  [2]\n" +
                "      2  :  [3]\n" +
                "      x  :  [4]\n" +
                "      ^  :  [5]\n" +
                "        y  :  [6]\n" +
                "        2  :  [7]\n" +
                "    -  :  [8]\n" +
                "      *  :  [9]\n" +
                "        12  :  [10]\n" +
                "        ^  :  [11]\n" +
                "          x  :  [12]\n" +
                "          3  :  [13]\n" +
                "        y  :  [14]\n" +
                "    *  :  [15]\n" +
                "      18  :  [16]\n" +
                "      ^  :  [17]\n" +
                "        x  :  [18]\n" +
                "        5  :  [19]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(1).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: 'x*(2*y^2-12*x^2*y+18*x^4)'\n" +
                "expressionSubstitution.left: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "expressionSubstitution.right: '(*(x;+(*(2;^(y;2));-(*(12;^(x;2);y));*(18;^(x;4)))))'\n" +
                "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "originalExpressionChangingPart: '+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5)))'\n" +
                "resultExpression: '(*(x;+(*(2;^(y;2));-(*(12;^(x;2);y));*(18;^(x;4)))))'\n" +
                "resultExpressionChangingPart: '*(x;+(*(2;^(y;2));-(*(12;^(x;2);y));*(18;^(x;4))))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: '-(-2*x*y^2+12*x^3*y-18*x^5)'\n" +
                "expressionSubstitution.left: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "expressionSubstitution.right: '(+(-(+(-(*(2;x;^(y;2)));*(12;^(x;3);y);-(*(18;^(x;5)))))))'\n" +
                "originalExpression: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "originalExpressionChangingPart: '+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5)))'\n" +
                "resultExpression: '(+(-(+(-(*(2;x;^(y;2)));*(12;^(x;3);y);-(*(18;^(x;5)))))))'\n" +
                "resultExpressionChangingPart: '+(-(+(-(*(2;x;^(y;2)));*(12;^(x;3);y);-(*(18;^(x;5))))))'\n" +
                "substitutionType: 'MinusFromBrackets'\n" +
                "priority: '61'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun minusInTest() {
        val expression = parseStringExpression("-(2*x*y^2 - 12*x^3*y + 18*x^5)")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    -  :  [2]\n" +
                "      +  :  [3]\n" +
                "        *  :  [4]\n" +
                "          2  :  [5]\n" +
                "          x  :  [6]\n" +
                "          ^  :  [7]\n" +
                "            y  :  [8]\n" +
                "            2  :  [9]\n" +
                "        -  :  [10]\n" +
                "          *  :  [11]\n" +
                "            12  :  [12]\n" +
                "            ^  :  [13]\n" +
                "              x  :  [14]\n" +
                "              3  :  [15]\n" +
                "            y  :  [16]\n" +
                "        *  :  [17]\n" +
                "          18  :  [18]\n" +
                "          ^  :  [19]\n" +
                "            x  :  [20]\n" +
                "            5  :  [21]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(3).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: '-x*(2*y^2-12*x^2*y+18*x^4)'\n" +
                "expressionSubstitution.left: '(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))'\n" +
                "expressionSubstitution.right: '(*(x;+(*(2;^(y;2));-(*(12;^(x;2);y));*(18;^(x;4)))))'\n" +
                "originalExpression: '(+(-(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))))'\n" +
                "originalExpressionChangingPart: '+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5)))'\n" +
                "resultExpression: '(+(-(*(x;+(*(2;^(y;2));-(*(12;^(x;2);y));*(18;^(x;4)))))))'\n" +
                "resultExpressionChangingPart: '*(x;+(*(2;^(y;2));-(*(12;^(x;2);y));*(18;^(x;4))))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: '-2*x*y^2+12*x^3*y-18*x^5'\n" +
                "expressionSubstitution.left: '(+(-(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))))'\n" +
                "expressionSubstitution.right: '(+(-(*(2;x;^(y;2)));*(12;^(x;3);y);-(*(18;^(x;5)))))'\n" +
                "originalExpression: '(+(-(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5))))))'\n" +
                "originalExpressionChangingPart: '+(-(+(*(2;x;^(y;2));-(*(12;^(x;3);y));*(18;^(x;5)))))'\n" +
                "resultExpression: '(+(-(*(2;x;^(y;2)));*(12;^(x;3);y);-(*(18;^(x;5)))))'\n" +
                "resultExpressionChangingPart: '+(-(*(2;x;^(y;2)));*(12;^(x;3);y);-(*(18;^(x;5))))'\n" +
                "substitutionType: 'MinusInBrackets'\n" +
                "priority: '61'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun combinedMonomReductionsTest() {
        val expression = parseStringExpression("2*x+3*x*2+2*x*y+2*x")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    *  :  [2]\n" +
                "      2  :  [3]\n" +
                "      x  :  [4]\n" +
                "    *  :  [5]\n" +
                "      3  :  [6]\n" +
                "      x  :  [7]\n" +
                "      2  :  [8]\n" +
                "    *  :  [9]\n" +
                "      2  :  [10]\n" +
                "      x  :  [11]\n" +
                "      y  :  [12]\n" +
                "    *  :  [13]\n" +
                "      2  :  [14]\n" +
                "      x  :  [15]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2,5,9,13).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: '2*x*(1+3+y+1)'\n" +
                "expressionSubstitution.left: '(+(*(2;x);*(3;x;2);*(2;x;y);*(2;x)))'\n" +
                "expressionSubstitution.right: '(*(2;x;+(1;3;y;1)))'\n" +
                "originalExpression: '(+(*(2;x);*(3;x;2);*(2;x;y);*(2;x)))'\n" +
                "originalExpressionChangingPart: '+(*(2;x);*(3;x;2);*(2;x;y);*(2;x))'\n" +
                "resultExpression: '(*(2;x;+(1;3;y;1)))'\n" +
                "resultExpressionChangingPart: '*(2;x;+(1;3;y;1))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun combinedMonomReductionsWithMinusTest() {
        val expression = parseStringExpression("3*x^2*2+2*x^2*y-2*x^2")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    *  :  [2]\n" +
                "      3  :  [3]\n" +
                "      ^  :  [4]\n" +
                "        x  :  [5]\n" +
                "        2  :  [6]\n" +
                "      2  :  [7]\n" +
                "    *  :  [8]\n" +
                "      2  :  [9]\n" +
                "      ^  :  [10]\n" +
                "        x  :  [11]\n" +
                "        2  :  [12]\n" +
                "      y  :  [13]\n" +
                "    -  :  [14]\n" +
                "      *  :  [15]\n" +
                "        2  :  [16]\n" +
                "        ^  :  [17]\n" +
                "          x  :  [18]\n" +
                "          2  :  [19]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2,8,14).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: 'x^2*2*(3+y-1)'\n" +
                "expressionSubstitution.left: '(+(*(3;^(x;2);2);*(2;^(x;2);y);-(*(2;^(x;2)))))'\n" +
                "expressionSubstitution.right: '(*(^(x;2);2;+(3;y;-(1))))'\n" +
                "originalExpression: '(+(*(3;^(x;2);2);*(2;^(x;2);y);-(*(2;^(x;2)))))'\n" +
                "originalExpressionChangingPart: '+(*(3;^(x;2);2);*(2;^(x;2);y);-(*(2;^(x;2))))'\n" +
                "resultExpression: '(*(^(x;2);2;+(3;y;-(1))))'\n" +
                "resultExpressionChangingPart: '*(^(x;2);2;+(3;y;-(1)))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun combinedMonomReductionsWithBracketsAndMinusTest() {
        val expression = parseStringExpression("3*x^2*2+2*(x^2*y)-2*x^2")
        assertEquals("  :  [0]\n" +
                "  +  :  [1]\n" +
                "    *  :  [2]\n" +
                "      3  :  [3]\n" +
                "      ^  :  [4]\n" +
                "        x  :  [5]\n" +
                "        2  :  [6]\n" +
                "      2  :  [7]\n" +
                "    *  :  [8]\n" +
                "      2  :  [9]\n" +
                "      *  :  [10]\n" +
                "        ^  :  [11]\n" +
                "          x  :  [12]\n" +
                "          2  :  [13]\n" +
                "        y  :  [14]\n" +
                "    -  :  [15]\n" +
                "      *  :  [16]\n" +
                "        2  :  [17]\n" +
                "        ^  :  [18]\n" +
                "          x  :  [19]\n" +
                "          2  :  [20]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2,8,15).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, generalCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: '2*(3*x^2+x^2*y-x^2)'\n" +
                "expressionSubstitution.left: '(+(*(3;^(x;2);2);*(2;*(^(x;2);y));-(*(2;^(x;2)))))'\n" +
                "expressionSubstitution.right: '(*(2;+(*(3;^(x;2));*(^(x;2);y);-(^(x;2)))))'\n" +
                "originalExpression: '(+(*(3;^(x;2);2);*(2;*(^(x;2);y));-(*(2;^(x;2)))))'\n" +
                "originalExpressionChangingPart: '+(*(3;^(x;2);2);*(2;*(^(x;2);y));-(*(2;^(x;2))))'\n" +
                "resultExpression: '(*(2;+(*(3;^(x;2));*(^(x;2);y);-(^(x;2)))))'\n" +
                "resultExpressionChangingPart: '*(2;+(*(3;^(x;2));*(^(x;2);y);-(^(x;2))))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: 'x^2*2*(3+y-1)'\n" +
                "expressionSubstitution.left: '(+(*(3;^(x;2);2);*(2;^(x;2);y);-(*(2;^(x;2)))))'\n" +
                "expressionSubstitution.right: '(*(^(x;2);2;+(3;y;-(1))))'\n" +
                "originalExpression: '(+(*(3;^(x;2);2);*(2;^(x;2);y);-(*(2;^(x;2)))))'\n" +
                "originalExpressionChangingPart: '+(*(3;^(x;2);2);*(2;^(x;2);y);-(*(2;^(x;2))))'\n" +
                "resultExpression: '(*(^(x;2);2;+(3;y;-(1))))'\n" +
                "resultExpressionChangingPart: '*(^(x;2);2;+(3;y;-(1)))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun setOpeningBracketsOrAndTest() {
        val expression = parseStringExpression("(a & b) | (c & d) | (e & f & g)")
        assertEquals("  :  [0]\n" +
                "  or  :  [1]\n" +
                "    and  :  [2]\n" +
                "      a  :  [3]\n" +
                "      b  :  [4]\n" +
                "    and  :  [5]\n" +
                "      c  :  [6]\n" +
                "      d  :  [7]\n" +
                "    and  :  [8]\n" +
                "      e  :  [9]\n" +
                "      f  :  [10]\n" +
                "      g  :  [11]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(5,8).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, setCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: '((c|e)&(c|f)&(c|g)&(d|e)&(d|f)&(d|g))|(a&b)'\n" +
                "expressionSubstitution.left: '(or(and(c;d);and(e;f;g)))'\n" +
                "expressionSubstitution.right: '(and(or(c;e);or(c;f);or(c;g);or(d;e);or(d;f);or(d;g)))'\n" +
                "originalExpression: '(or(and(a;b);and(c;d);and(e;f;g)))'\n" +
                "originalExpressionChangingPart: 'or(and(c;d);and(e;f;g))'\n" +
                "resultExpression: '(or(and(or(c;e);or(c;f);or(c;g);or(d;e);or(d;f);or(d;g));and(a;b)))'\n" +
                "resultExpressionChangingPart: 'and(or(c;e);or(c;f);or(c;g);or(d;e);or(d;f);or(d;g))'\n" +
                "substitutionType: 'OpeningBrackets'\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '((c|(e&f&g))&(d|(e&f&g)))|(a&b)'\n" +
                "expressionSubstitution.left: '(or(and(c;d);and(e;f;g)))'\n" +
                "expressionSubstitution.right: '(and(or(c;and(e;f;g));or(d;and(e;f;g))))'\n" +
                "originalExpression: '(or(and(a;b);and(c;d);and(e;f;g)))'\n" +
                "originalExpressionChangingPart: 'or(and(c;d);and(e;f;g))'\n" +
                "resultExpression: '(or(and(or(c;and(e;f;g));or(d;and(e;f;g)));and(a;b)))'\n" +
                "resultExpressionChangingPart: 'and(or(c;and(e;f;g));or(d;and(e;f;g)))'\n" +
                "substitutionType: 'OpeningBrackets'\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '(a&b)|(e&f&g)|(c&d)'\n" +
                "expressionSubstitution.left: '(or(and(a;b);and(c;d);and(e;f;g)))'\n" +
                "expressionSubstitution.right: '(or(and(a;b);and(e;f;g);and(c;d)))'\n" +
                "originalExpression: '(or(and(a;b);and(c;d);and(e;f;g)))'\n" +
                "originalExpressionChangingPart: 'or(and(a;b);and(c;d);and(e;f;g))'\n" +
                "resultExpression: '(or(and(a;b);and(e;f;g);and(c;d)))'\n" +
                "resultExpressionChangingPart: 'or(and(a;b);and(e;f;g);and(c;d))'\n" +
                "substitutionType: 'Swap'\n" +
                "priority: '50'\n" +
                "\n" +
                "result: '(a&b)|((c&d)&((c&d)|(e&f&g)))|(e&f&g)'\n" +
                "expressionSubstitution.left: '(and(c;d))'\n" +
                "expressionSubstitution.right: '(and(and(c;d);or(and(c;d);and(e;f;g))))'\n" +
                "originalExpression: '(or(and(a;b);and(c;d);and(e;f;g)))'\n" +
                "originalExpressionChangingPart: 'and(c;d)'\n" +
                "resultExpression: '(or(and(a;b);and(and(c;d);or(and(c;d);and(e;f;g)));and(e;f;g)))'\n" +
                "resultExpressionChangingPart: 'and(and(c;d);or(and(c;d);and(e;f;g)))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'\n" +
                "\n" +
                "result: '(a&b)|((c&d)|((c&d)&(e&f&g)))|(e&f&g)'\n" +
                "expressionSubstitution.left: '(and(c;d))'\n" +
                "expressionSubstitution.right: '(or(and(c;d);and(and(c;d);and(e;f;g))))'\n" +
                "originalExpression: '(or(and(a;b);and(c;d);and(e;f;g)))'\n" +
                "originalExpressionChangingPart: 'and(c;d)'\n" +
                "resultExpression: '(or(and(a;b);or(and(c;d);and(and(c;d);and(e;f;g)));and(e;f;g)))'\n" +
                "resultExpressionChangingPart: 'or(and(c;d);and(and(c;d);and(e;f;g)))'\n" +
                "substitutionType: 'ComplicatingExtension'\n" +
                "priority: '60'\n" +
                "\n" +
                "result: '((c&d)|(e&f&g))|(a&b)'\n" +
                "expressionSubstitution.left: '(or(and(c;d);and(e;f;g)))'\n" +
                "expressionSubstitution.right: '(or(or(and(c;d);and(e;f;g))))'\n" +
                "originalExpression: '(or(and(a;b);and(c;d);and(e;f;g)))'\n" +
                "originalExpressionChangingPart: 'or(and(c;d);and(e;f;g))'\n" +
                "resultExpression: '(or(or(and(c;d);and(e;f;g));and(a;b)))'\n" +
                "resultExpressionChangingPart: 'or(or(and(c;d);and(e;f;g)))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '90'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun setOpeningBracketsAndOrTest() {
        val expression = stringToExpression("!(a & (b | f | g))", scope = "setTheory")
        assertEquals("  :  [0]\n" +
                "  not  :  [1]\n" +
                "    and  :  [2]\n" +
                "      a  :  [3]\n" +
                "      or  :  [4]\n" +
                "        b  :  [5]\n" +
                "        f  :  [6]\n" +
                "        g  :  [7]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, setCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: '!((a&b)|(a&f)|(a&g))'\n" +
                "expressionSubstitution.left: '(and(a;or(b;f;g)))'\n" +
                "expressionSubstitution.right: '(or(and(a;b);and(a;f);and(a;g)))'\n" +
                "originalExpression: '(not(and(a;or(b;f;g))))'\n" +
                "originalExpressionChangingPart: 'and(a;or(b;f;g))'\n" +
                "resultExpression: '(not(or(and(a;b);and(a;f);and(a;g))))'\n" +
                "resultExpressionChangingPart: 'or(and(a;b);and(a;f);and(a;g))'\n" +
                "substitutionType: 'OpeningBrackets'\n" +
                "priority: '50'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun setReductionTest() {
        val expression = stringToExpression("!(a & (a | b))", scope = "setTheory")
        assertEquals("  :  [0]\n" +
                "  not  :  [1]\n" +
                "    and  :  [2]\n" +
                "      a  :  [3]\n" +
                "      or  :  [4]\n" +
                "        a  :  [5]\n" +
                "        b  :  [6]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, setCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: '!(a|(0&b))'\n" +
                "expressionSubstitution.left: '(and(a;or(a;b)))'\n" +
                "expressionSubstitution.right: '(or(a;and(0;b)))'\n" +
                "originalExpression: '(not(and(a;or(a;b))))'\n" +
                "originalExpressionChangingPart: 'and(a;or(a;b))'\n" +
                "resultExpression: '(not(or(a;and(0;b))))'\n" +
                "resultExpressionChangingPart: 'or(a;and(0;b))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: '!((a&a)|(a&b))'\n" +
                "expressionSubstitution.left: '(and(a;or(a;b)))'\n" +
                "expressionSubstitution.right: '(or(and(a;a);and(a;b)))'\n" +
                "originalExpression: '(not(and(a;or(a;b))))'\n" +
                "originalExpressionChangingPart: 'and(a;or(a;b))'\n" +
                "resultExpression: '(not(or(and(a;a);and(a;b))))'\n" +
                "resultExpressionChangingPart: 'or(and(a;a);and(a;b))'\n" +
                "substitutionType: 'OpeningBrackets'\n" +
                "priority: '50'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun setReductionTest2() {
        val expression = stringToExpression("!a & (d^c) | (!a & b & (d^c)) | (a & b & c & (d^c)) | (!a & c & (d^c))", scope = "setTheory")
        assertEquals("  :  [0]\n" +
                "  or  :  [1]\n" +
                "    and  :  [2]\n" +
                "      not  :  [3]\n" +
                "        a  :  [4]\n" +
                "      xor  :  [5]\n" +
                "        d  :  [6]\n" +
                "        c  :  [7]\n" +
                "    and  :  [8]\n" +
                "      not  :  [9]\n" +
                "        a  :  [10]\n" +
                "      b  :  [11]\n" +
                "      xor  :  [12]\n" +
                "        d  :  [13]\n" +
                "        c  :  [14]\n" +
                "    and  :  [15]\n" +
                "      a  :  [16]\n" +
                "      b  :  [17]\n" +
                "      c  :  [18]\n" +
                "      xor  :  [19]\n" +
                "        d  :  [20]\n" +
                "        c  :  [21]\n" +
                "    and  :  [22]\n" +
                "      not  :  [23]\n" +
                "        a  :  [24]\n" +
                "      c  :  [25]\n" +
                "      xor  :  [26]\n" +
                "        d  :  [27]\n" +
                "        c  :  [28]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(2,8,22).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, setCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: '(!a&(d^c)&(1|b|c))|(a&b&c&(d^c))'\n" +
                "expressionSubstitution.left: '(or(and(not(a);xor(d;c));and(not(a);b;xor(d;c));and(not(a);c;xor(d;c))))'\n" +
                "expressionSubstitution.right: '(and(not(a);xor(d;c);or(1;b;c)))'\n" +
                "originalExpression: '(or(and(not(a);xor(d;c));and(not(a);b;xor(d;c));and(a;b;c;xor(d;c));and(not(a);c;xor(d;c))))'\n" +
                "originalExpressionChangingPart: 'or(and(not(a);xor(d;c));and(not(a);b;xor(d;c));and(not(a);c;xor(d;c)))'\n" +
                "resultExpression: '(or(and(not(a);xor(d;c);or(1;b;c));and(a;b;c;xor(d;c))))'\n" +
                "resultExpressionChangingPart: 'and(not(a);xor(d;c);or(1;b;c))'\n" +
                "substitutionType: 'TwoSidesArithmeticReduce'\n" +
                "priority: '5'\n" +
                "\n" +
                "result: '((!a&(d^c))|(!a&b&(d^c))|(!a&c&(d^c)))|(a&b&c&(d^c))'\n" +
                "expressionSubstitution.left: '(or(and(not(a);xor(d;c));and(not(a);b;xor(d;c));and(not(a);c;xor(d;c))))'\n" +
                "expressionSubstitution.right: '(or(or(and(not(a);xor(d;c));and(not(a);b;xor(d;c));and(not(a);c;xor(d;c)))))'\n" +
                "originalExpression: '(or(and(not(a);xor(d;c));and(not(a);b;xor(d;c));and(a;b;c;xor(d;c));and(not(a);c;xor(d;c))))'\n" +
                "originalExpressionChangingPart: 'or(and(not(a);xor(d;c));and(not(a);b;xor(d;c));and(not(a);c;xor(d;c)))'\n" +
                "resultExpression: '(or(or(and(not(a);xor(d;c));and(not(a);b;xor(d;c));and(not(a);c;xor(d;c)));and(a;b;c;xor(d;c))))'\n" +
                "resultExpressionChangingPart: 'or(or(and(not(a);xor(d;c));and(not(a);b;xor(d;c));and(not(a);c;xor(d;c))))'\n" +
                "substitutionType: 'SelectedOrderExtraction'\n" +
                "priority: '90'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun zeroSetComputation() {
        val expression = stringToExpression("!(a & (a | b | 1))", scope = "setTheory")
        assertEquals("  :  [0]\n" +
                "  not  :  [1]\n" +
                "    and  :  [2]\n" +
                "      a  :  [3]\n" +
                "      or  :  [4]\n" +
                "        a  :  [5]\n" +
                "        b  :  [6]\n" +
                "        1  :  [7]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(4).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, setCompiledConfiguration)
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: '!(a&1)'\n" +
                "expressionSubstitution.left: '(or(a;b;1))'\n" +
                "expressionSubstitution.right: '(1)'\n" +
                "originalExpression: '(not(and(a;or(a;b;1))))'\n" +
                "originalExpressionChangingPart: 'or(a;b;1)'\n" +
                "resultExpression: '(not(and(a;1)))'\n" +
                "resultExpressionChangingPart: '1'\n" +
                "substitutionType: 'ZeroComputation'\n" +
                "priority: '50'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun setZeroTransformations() {
        val expression = parseStringExpression("0")
        assertEquals("  :  [0]\n" +
                "  0  :  [1]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(1).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createConfigurationFromRulePacksAndParams(listOf("Logic").toTypedArray()))
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: '!(!0)'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(not(not(a)))'\n" +
                "originalExpression: '(0)'\n" +
                "originalExpressionChangingPart: '0'\n" +
                "resultExpression: '(not(not(0)))'\n" +
                "resultExpressionChangingPart: 'not(not(0))'\n" +
                "substitutionType: '(a)__to__(not(not(a)))'\n" +
                "priority: '89'\n" +
                "\n" +
                "result: '0|0'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(or(a;a))'\n" +
                "originalExpression: '(0)'\n" +
                "originalExpressionChangingPart: '0'\n" +
                "resultExpression: '(or(0;0))'\n" +
                "resultExpressionChangingPart: 'or(0;0)'\n" +
                "substitutionType: '(a)__to__(or(a;a))'\n" +
                "priority: '90'\n" +
                "\n" +
                "result: '0&0'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(and(a;a))'\n" +
                "originalExpression: '(0)'\n" +
                "originalExpressionChangingPart: '0'\n" +
                "resultExpression: '(and(0;0))'\n" +
                "resultExpressionChangingPart: 'and(0;0)'\n" +
                "substitutionType: '(a)__to__(and(a;a))'\n" +
                "priority: '90'\n" +
                "\n" +
                "result: '0&1'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(and(a;1))'\n" +
                "originalExpression: '(0)'\n" +
                "originalExpressionChangingPart: '0'\n" +
                "resultExpression: '(and(0;1))'\n" +
                "resultExpressionChangingPart: 'and(0;1)'\n" +
                "substitutionType: '(a)__to__(and(a;1))'\n" +
                "priority: '90'\n" +
                "\n" +
                "result: '0\\0'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(set-(a;0))'\n" +
                "originalExpression: '(0)'\n" +
                "originalExpressionChangingPart: '0'\n" +
                "resultExpression: '(set-(0;0))'\n" +
                "resultExpressionChangingPart: 'set-(0;0)'\n" +
                "substitutionType: '(a)__to__(set-(a;0))'\n" +
                "priority: '91'\n" +
                "\n" +
                "result: '1->0'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(implic(1;a))'\n" +
                "originalExpression: '(0)'\n" +
                "originalExpressionChangingPart: '0'\n" +
                "resultExpression: '(implic(1;0))'\n" +
                "resultExpressionChangingPart: 'implic(1;0)'\n" +
                "substitutionType: '(a)__to__(implic(1;a))'\n" +
                "priority: '91'\n" +
                "\n" +
                "result: '0^0'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(xor(a;0))'\n" +
                "originalExpression: '(0)'\n" +
                "originalExpressionChangingPart: '0'\n" +
                "resultExpression: '(xor(0;0))'\n" +
                "resultExpressionChangingPart: 'xor(0;0)'\n" +
                "substitutionType: '(a)__to__(xor(a;0))'\n" +
                "priority: '91'\n" +
                "\n" +
                "result: '1==0'\n" +
                "expressionSubstitution.left: '(a)'\n" +
                "expressionSubstitution.right: '(alleq(1;a))'\n" +
                "originalExpression: '(0)'\n" +
                "originalExpressionChangingPart: '0'\n" +
                "resultExpression: '(alleq(1;0))'\n" +
                "resultExpressionChangingPart: 'alleq(1;0)'\n" +
                "substitutionType: '(a)__to__(alleq(1;a))'\n" +
                "priority: '91'", result.joinToString(separator = "\n\n"))
    }

    @Test
    fun resolutionMixTest() {
        val expression = structureStringToExpression("(and(A;B;or(C;not(A));not(C)))")
        assertEquals("  :  [0]\n" +
                "  and  :  [1]\n" +
                "    A  :  [2]\n" +
                "    B  :  [3]\n" +
                "    or  :  [4]\n" +
                "      C  :  [5]\n" +
                "      not  :  [6]\n" +
                "        A  :  [7]\n" +
                "    not  :  [8]\n" +
                "      C  :  [9]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(4,8).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(expressionSubstitutionFromStructureStrings("(and(or(A;X);not(A)))", "(and(X;or(A;X);not(A)))", matchJumbledAndNested = true)).toTypedArray()
        ))
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
        assertEquals("" +
                "result: '(!A&(C|!A)&!C)&A&B'\n" +
                "expressionSubstitution.left: '(and(or(A;X);not(A)))'\n" +
                "expressionSubstitution.right: '(and(X;or(A;X);not(A)))'\n" +
                "originalExpression: '(and(A;B;or(C;not(A));not(C)))'\n" +
                "originalExpressionChangingPart: 'and(or(C;not(A));not(C))'\n" +
                "resultExpression: '(and(and(not(A);or(C;not(A));not(C));A;B))'\n" +
                "resultExpressionChangingPart: 'and(not(A);or(C;not(A));not(C))'\n" +
                "substitutionType: ''(and(or(A;X);not(A)))'->'(and(X;or(A;X);not(A)))''\n" +
                "priority: '50'", result.joinToString(separator = "\n\n"))
    }

    @Test
    @Ignore
    fun resolutionMixTest1() { //TODO: change matchJumbledAndNested to work with such cases, like permutation iin internal arguments
        val expression = structureStringToExpression("(and(A;B;or(not(A);C);not(C)))")
        assertEquals("  :  [0]\n" +
                "  and  :  [1]\n" +
                "    A  :  [2]\n" +
                "    B  :  [3]\n" +
                "    or  :  [4]\n" +
                "      not  :  [5]\n" +
                "        A  :  [6]\n" +
                "      C  :  [7]\n" +
                "    not  :  [8]\n" +
                "      C  :  [9]\n", expression.toStringsWithNodeIds())
        val selectedNodeIds = listOf(4,8).toTypedArray()
        val substitutionSelectionData = SubstitutionSelectionData(expression, selectedNodeIds, createCompiledConfigurationFromExpressionSubstitutionsAndParams(
                listOf(expressionSubstitutionFromStructureStrings("(and(or(A;X);not(A)))", "(and(X;or(A;X);not(A)))", matchJumbledAndNested = true)).toTypedArray()
        ))
        fillSubstitutionSelectionData(substitutionSelectionData)

        val result = generateSubstitutionsBySelectedNodes(substitutionSelectionData, true, true)
    }
}