package mathhelper.twf.visualization

import kotlin.test.assertEquals
import org.junit.Test

import kotlin.test.assertEquals

internal class TexColoringKtTest {

    @Test
    fun dropPerformedTexBrushingInternalLess() {
        val  res1 = dropPerformedTexBrushingInternal("\\textcolor{purple}{a\\textcolor{green}<a+1}")
        assertEquals("a\\textcolor{green}<a+1", res1)

        val  res2 = dropPerformedTexBrushingInternal(res1)
        assertEquals("a<a+1", res2)
    }

    @Test
    fun dropPerformedTexBrushingInternalLessInBrackets() {
        val  res1 = dropPerformedTexBrushingInternal("\\textcolor{purple}{a\\textcolor{green}{<}a+1}")
        assertEquals("a\\textcolor{green}{<}a+1", res1)

        val  res2 = dropPerformedTexBrushingInternal(res1)
        assertEquals("a< a+1", res2)
    }
}