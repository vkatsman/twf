package mathhelper.twf.morokei

import mathhelper.twf.expressiontree.ExpressionNode
import mathhelper.twf.expressiontree.NodeType
import mathhelper.twf.platformdependent.random
import mathhelper.twf.substitutiontests.parseStringExpression

class TemplateExpressionGenerator (
        private val template: String, // something like "sqrt(.-x) + sqrt(x-.)"
        restrictions: ArrayList<String> = ArrayList(), // something like {"1<2<3", "4>5"}
        private val leftBorder: Double = 0.0,
        private val rightBorder: Double = 1.0
) {

    var correctOrder = mutableListOf<Pair<Int, Int>>()
    var constantCount = 0

    init {
        for (c in template) {
            if (c == '.')
                constantCount++
        }
        for (restriction in restrictions){
            var increasingOrder = false
            if (restriction.contains('>'))
                increasingOrder = true
            val parts = restriction.split('<', '>')
            for (i in 0 until parts.size) {
                for (j in 0 until i) {
                    if (increasingOrder)
                        correctOrder.add(Pair(parts[i].toInt() - 1, parts[j].toInt() - 1))
                    else
                        correctOrder.add(Pair(parts[j].toInt() - 1, parts[i].toInt() - 1))
                }
            }
        }
    }

    fun generateExpression() : String {
        var values = mutableListOf<Double>()
        for (i in 0 until constantCount) {
            values.add(random.nextDouble(leftBorder, rightBorder))
        }
        var ok = false
        while (!ok){
            ok = true
            for ((i, j) in correctOrder){
                if (values[i] > values[j]) {
                    ok = false;
                    values[i] = values[j].also { values[j] = values[i] }
                }
            }
        }
        var expression = ""
        var currentIndex = 0
        for (c in template) {
            if (c == '.')
                expression += values[currentIndex++].toString()
            else
                expression += c.toString()
        }
        return expression
    }
}

class TransformationExpressionGenerator(
        private var baseExpression: String = "",
        private val transformationOperation: ArrayList<String> = ArrayList(),
        private val leftBorder: Double = 0.0,
        private val rightBorder: Double = 1.0
){
    var arctiveNodeCount = 0
    lateinit var root: ExpressionNode

    init {
        cntNodes(parseStringExpression(baseExpression))
        if (arctiveNodeCount == 0){
            if (baseExpression.isNotEmpty())
                baseExpression += "+x"
            baseExpression = "x"
        }
    }

    fun generateExpression(iterCount: Int = 1, useVariables: Boolean = true): String {
        root = parseStringExpression(baseExpression)
        cntNodes(root)
        for (i in 0 until iterCount){
            var vertexToApplyOperation = random.nextInt(arctiveNodeCount)
            if (transformationOperation.isEmpty())
                vertexToApplyOperation = arctiveNodeCount
            val (newRoot, _) = applyTransform(root, vertexToApplyOperation, useVariables)
            root = newRoot
        }
        return root.toPlainTextView()
    }

    fun applyTransform(node: ExpressionNode, vertexId: Int, useVariables: Boolean): Pair<ExpressionNode, Int> {
        if (!node.containsVariables())
            return Pair(node, 0)
        if (vertexId == 0) {
            val operation = transformationOperation[random.nextInt(transformationOperation.size)]
            val newNode = ExpressionNode(NodeType.FUNCTION, operation)
            newNode.addChild(node.clone())
            if (operation == "+" || operation == "*" || operation == "-"){
                var newVar = ""
                if (useVariables) {
                    newVar = (random.nextInt(0, 26) + 'a'.toInt()).toChar().toString()
                    arctiveNodeCount
                } else
                    newVar = random.nextDouble(leftBorder, rightBorder).toString()
                newNode.addChild(ExpressionNode(NodeType.VARIABLE, newVar))
                arctiveNodeCount++
            }
            arctiveNodeCount++
            return Pair(newNode, 1)
        }
        var verticesUsed = 1
        val newNode = ExpressionNode(node.nodeType, node.value)
        for (child in node.children){
            val(newSon, verticesVisited) = applyTransform(child, vertexId - verticesUsed, useVariables)
            newNode.addChild(newSon)
            verticesUsed += verticesVisited
        }
        return Pair(newNode, verticesUsed)
    }

    private fun cntNodes(node: ExpressionNode) {
        if (node.containsVariables())
            arctiveNodeCount++
        for (son in node.children)
            cntNodes(son)
    }
}