package mathhelper.twf.morokei

import mathhelper.twf.assert
import mathhelper.twf.optimizerutils.DomainPointGenerator
import mathhelper.twf.platformdependent.random
import mathhelper.twf.substitutiontests.parseStringExpression
import kotlin.math.abs
import kotlin.math.max

class DomainBreaker (
        template: String = "",
        restrictions: ArrayList<String> = ArrayList(),
        private val functions: ArrayList<String> = ArrayList(),
        private val leftBorder: Double = 0.0,
        private val rightBorder: Double = 1.0
) {
    private val templateGenerator: TemplateExpressionGenerator

    init {
        templateGenerator = TemplateExpressionGenerator(template, restrictions, leftBorder, rightBorder)
    }

    fun breakDomainGenerator(
            templateGenerationIterationCount: Int = 10,
            transformationGenerationIterationCount: Int = 10,
            transformationDeep: Int = 3
    ) : String {
        for (i in 0 until templateGenerationIterationCount){
            val template = templateGenerator.generateExpression()
            for (j in 0 until transformationGenerationIterationCount) {
                val generator = TransformationExpressionGenerator(template, functions, leftBorder, rightBorder);
                val expression = generator.generateExpression(transformationDeep)
                val node = parseStringExpression(expression)
                val domain = DomainPointGenerator(arrayListOf(node))
                if (!domain.hasPointsInDomain())
                    return expression
            }
        }
        return ""
    }
}

class AllowedRatioGetter1( // generates sum(or prod)_1^n sqrt(a + d - x_i) op sqrt(x_i - a)
        val variableCount: Int,
        val func: String = "sqrt",
        val innerOperation: Char = '+',
        val outerOperation: Char = '+',
        val annealingRuns: Int = 1,
        val gradientDescentRuns: Int = 1
){

    init {
        assert(variableCount > 0 && variableCount <= 26)
    }

    fun getAllowedRatio(
            iterCount: Int = 10,
            randomIterCount: Int = 0,
            border: Double = 1.0
    ) : Double {
        val ratios = arrayListOf<Double>()
        for (iter in 1..iterCount) {
            val a: Double = border / iterCount * iter
            val d = runBinarySearch(a)
            println("$d, $a, ${d/a}")
            ratios.add(d / a)
        }
        for (iter in 1..randomIterCount) {
            val a: Double = random.nextDouble(border)
            val d = runBinarySearch(a)
            println("$d, $a, ${d/a}")
            ratios.add(d / a)
        }
        return aggregateRatios(ratios)
    }

    fun buildExpression(a: Double, d: Double) : String {
        var expression = ""
        for (i in 0 until variableCount) {
            expression += "${func}(${(i + 'a'.toInt()).toChar()}-$a)${innerOperation}${func}(${a+d}-${(i + 'a'.toInt()).toChar()})"
            if (i + 1 < variableCount)
                expression += outerOperation
        }
        return expression
    }

    private fun check(a: Double, d: Double) : Boolean {
        val root = parseStringExpression(buildExpression(a, d))
        val domain = DomainPointGenerator(arrayListOf(root), annealingRuns = annealingRuns, gradientDescentRuns = gradientDescentRuns)
        return domain.hasPointsInDomain()
    }

    private fun runBinarySearch(a: Double) : Double {
        var leftBorder = 0.0
        var rightBorder = 10 * a
        for (iter in 0..30){
            val med = (leftBorder + rightBorder) / 2
            if (check(a, med))
                rightBorder = med
            else
                leftBorder = med
        }
        return rightBorder
    }

    private fun aggregateRatios(ratios: ArrayList<Double>) : Double {
        ratios.sort()
        var sum = 0.0
        for (i in 1 until ratios.size - 1)
            sum += ratios[i]
        return sum / (ratios.size - 2)
    }
}

class AllowedRatioGetter2(
        val segmentsCount: Int = 5,
        val func: String = "sqrt",
        val annealingRuns: Int = 1,
        val gradientDescentRuns: Int = 1
){
    fun getAllowedRatio(border: Double, iterCount: Int, step: Double = 2.0) : Double {
        val ratios = arrayListOf<Double>()
        var curBorder = border
        for (i in 1..iterCount) {
            val a = ArrayList<Double>()
            for (j in 0 until segmentsCount)
                a.add(random.nextDouble(-curBorder, curBorder))
            val d = runBinarySearch(a)
            println("$d, ${curBorder}, ${d / curBorder}")
            ratios.add(d / curBorder)
            curBorder *= step
        }
        return aggregateRatios(ratios)
    }

    fun buildExpression(a: ArrayList<Double>, d: Double) : String {
        var prev = a[0]
        var result = "${func}(-"
        for (i in 0 until segmentsCount) {
            if (i > 0)
                result += "*"
            val m1 = if (max(a[i], prev) < 0) "+" else "-"
            val m2 = if (a[i] + d < 0) "+" else "-"
            result += "(x${m1}${abs(max(prev, a[i]))})*(x${m2}${abs(a[i]+d)})"
            prev = max(prev, a[i] + d)
        }
        return result + ")"
    }

    private fun check(a: ArrayList<Double>, d: Double) : Boolean {
        val root = parseStringExpression(buildExpression(a, d))
        val domain = DomainPointGenerator(arrayListOf(root), annealingRuns = annealingRuns, gradientDescentRuns = gradientDescentRuns)
        return domain.hasPointsInDomain()
    }

    private fun runBinarySearch(a: ArrayList<Double>) : Double {
        a.sort()
        var leftBorder = 0.0
        var rightBorder = 0.0
        for (a_i in a)
            rightBorder = max(rightBorder, abs(a_i))
        rightBorder *= 10
        for (iter in 0..30){
            val med = (leftBorder + rightBorder) / 2
            if (check(a, med))
                rightBorder = med
            else
                leftBorder = med
        }
        return rightBorder
    }

    private fun aggregateRatios(ratios: ArrayList<Double>) : Double {
        ratios.sort()
        var sum = 0.0
        for (i in 1 until ratios.size - 1)
            sum += ratios[i]
        return sum / (ratios.size - 2)
    }
}

class AllowedParamsGetter3( //func(a-sin(x)) func is sqrt or ln
        private val func: String = "sqrt",
        private val annealingRuns: Int = 1,
        private val gradientDescentRuns: Int = 1
) {
    private fun buildExpression(a: Double) : String {
        val m = if (a < 0) "+" else "-"
        return "${func}(sin(x)${m}${abs(a)})"
    }

    private fun check(a: Double) : Boolean {
        val root = parseStringExpression(buildExpression(a))
        val domain = DomainPointGenerator(arrayListOf(root), annealingRuns = annealingRuns, gradientDescentRuns = gradientDescentRuns)
        return domain.hasPointsInDomain()
    }

    fun getParam(iterCount: Int = 20) : Double {
        var l = -1.0
        var r = 1.0
        for (iter in 0 until iterCount) {
            val m = (l + r) / 2
            if (check(m))
                l = m
            else
                r = m
        }
        return l
    }
}

class AllowedParamsGetter4( //acos(cos(x)+h*s-a) + func(1-(cos(x) + h*s))
        private val func: String = "sqrt",
        private val annealingRuns: Int = 1,
        private val gradientDescentRuns: Int = 1
) {
    private fun buildExpression(a: Double) : String {
        val m = if (a < 0) "+" else "-"
        return "acos(cos(x)+h*s${m}${a})+${func}(1-(cos(x)-h/s))"
    }

    private fun check(a: Double) : Boolean {
        val root = parseStringExpression(buildExpression(a))
        val domain = DomainPointGenerator(arrayListOf(root), annealingRuns = annealingRuns, gradientDescentRuns = gradientDescentRuns)
        return domain.hasPointsInDomain()
    }

    fun getParam(iterCount: Int = 20) : Double {
        var l = -1.0
        var r = 1.0
        for (iter in 0 until iterCount) {
            val m = (l + r) / 2
            if (check(m))
                l = m
            else
                r = m
        }
        return l
    }
}

class AllowedParamsGetter5( //acos(cos(x)+h*s-a) + func(1-(cos(x) + h*s))
        private val func: String = "sqrt",
        private val annealingRuns: Int = 1,
        private val gradientDescentRuns: Int = 1
) {
    private fun buildExpression(a: Double) : String {
        return "sqrt(x-10)+sqrt(y-100)+sqrt(x-y)+sqrt(y-z)+sqrt(x+${a})"
    }

    private fun check(a: Double) : Boolean {
        val root = parseStringExpression(buildExpression(a))
        val domain = DomainPointGenerator(arrayListOf(root), annealingRuns = annealingRuns, gradientDescentRuns = gradientDescentRuns)
        return domain.hasPointsInDomain()
    }

    fun getParam(iterCount: Int = 20) : Double {
        var l = 0.0
        var r = 10.0
        for (iter in 0 until iterCount) {
            val m = (l + r) / 2
            if (check(m))
                r = m
            else
                l = m
        }
        return l
    }
}

