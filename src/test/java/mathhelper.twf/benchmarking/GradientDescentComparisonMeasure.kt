package mathhelper.twf.benchmarking

import mathhelper.twf.baseoperations.BaseOperationsDefinitions
import mathhelper.twf.config.ComparisonType
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.expressiontree.ExpressionNodeConstructor
import mathhelper.twf.optimizerutils.OptimizerUtils
import org.junit.Ignore
import org.junit.Test
import mathhelper.twf.platformdependent.random
import mathhelper.twf.substitutiontests.parseStringExpression
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.test.assertEquals

class GradientDescentComparisonMeasure {
    val compiledConfiguration = CompiledConfiguration()

    val left = "(sin(a) + cos(b) + exp(c + sin(d + e) + cos(f * g - a)))^8*100"
    val right = "-1"
    val expression = "($left)-($right)"

    fun simpleCompare (){
        val minizer = OptimizerUtils(parseStringExpression(expression), compiledConfiguration = compiledConfiguration)
        if (minizer.canStart())
            minizer.run(4)
    }


    @Test
    @Ignore
    fun stringToExpressionMeasureTest() {
        makeMeasures(listOf(
                Pair("simple", { simpleCompare() })
        ), 100)
    }
}