package mathhelper.twf.benchmarking

import mathhelper.twf.api.compareWithoutSubstitutions
import mathhelper.twf.api.stringToExpression
import mathhelper.twf.baseoperations.BaseOperationsDefinitions
import mathhelper.twf.config.ComparisonType
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.expressiontree.ExpressionNodeConstructor
import org.junit.Ignore
import org.junit.Test
import mathhelper.twf.substitutiontests.parseStringExpression
import kotlin.test.assertEquals

fun stringToExpresionShortest (){
val a = "hjgh"+"hjgk"
    val b = a + a
//    stringToExpression("A", scope = "setTheory")
}

fun stringToExpresionShort (){
    stringToExpression("(!!C&!B)|A", scope = "setTheory")
}

fun stringToExpresionMiddle (){
    stringToExpression("(!!C&!B)|A->(!!C&!B)|A", scope = "setTheory")
}



class ExpressionParserMeasure {
    @Test
    @Ignore
    fun stringToExpressionMeasureTest() {
        makeMeasures(listOf(
                Pair("shortest", { stringToExpresionShortest() }),
                Pair("short", { stringToExpresionShort() }),
                Pair("middle", { stringToExpresionMiddle() })
        ), 10)
    }
}
