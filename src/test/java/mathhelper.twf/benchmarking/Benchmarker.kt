package mathhelper.twf.benchmarking

fun makeMeasures (
        functions: List<Pair<String, () -> Any>>,
        testsCount: Int = 100,
        iterationsInTestCount: Int = 100
) {
    val results = mutableListOf<MutableList<Double>>()
    for (function in functions){
        results.add(mutableListOf())
    }
    val mutableFunctions = functions.toMutableList()

    println("$PRINT_REFIX -> go\t\t${functions.map{it.first}.joinToString(separator = "\t\t")}")

    for (i in 1..testsCount){
        mutableFunctions.shuffle()
        val oneRunResults = mutableMapOf<String, Double>()
        for (function in mutableFunctions){
            val startTime = System.currentTimeMillis()

            for (j in 1..iterationsInTestCount){
                function.second.invoke()
            }

            val time = (System.currentTimeMillis() - startTime) / 1.0 / iterationsInTestCount
            oneRunResults.put(function.first, time)
        }

        var iterationResults = "test $i\t"
        for (j in 0..functions.lastIndex){
            val time = oneRunResults[functions[j].first]!!
            iterationResults += "\t\t${time}ms"
            results[j].add(time)
        }
        println(PRINT_REFIX + iterationResults)
    }

    results.forEach { it.sort() }

    val average = results.map { it.average() }
    val median = results.map { it[it.size / 2] }

    println(PRINT_REFIX + "average\t\t${average.joinToString(separator = "\t\t")}")
    println(PRINT_REFIX + "median\t\t${median.joinToString(separator = "\t\t")}")
}


/**
 * Used to filter console messages easily
 */
private val PRINT_REFIX = "[TimeTest] "