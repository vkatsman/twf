package mathhelper.twf.benchmarking

import mathhelper.twf.api.checkSolutionInTexITR
import mathhelper.twf.api.createCompiledConfigurationFromITR
import mathhelper.twf.config.*
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks
import org.junit.Ignore
import org.junit.Test

class SolutionVerificationMeasure {
    @Test
    @Ignore
    fun logicTask4SolutionMeasure() {
        val originalTexSolution = "\\textcolor{purple}{\\left(A\\Rightarrow \\neg B\\right)\\Rightarrow C\\textcolor{green}{=}!\\left(A\\Rightarrow \\neg B\\right)\\vee C\\textcolor{green}{=}!\\left(!A\\vee !B\\right)\\vee C\\textcolor{red}{==}\\left(A\\wedge B\\right)\\vee C}"
        val taskITR = TaskITR(
                subjectType = "logic",
        originalExpressionStructureString = "(implic(implic(A;not(B));C))",
        goalExpressionStructureString = "(or(and(A;B);C))",
        rulePacks = listOf(RulePackLinkITR(rulePackCode = "LogicBase"), RulePackLinkITR(rulePackCode = "LogicAbsorptionLaw")),
        difficulty = 2.9,
        otherCheckSolutionData = mapOf(
                notChangesOnVariablesInComparisonFunctionJsonName to listOf(
                        FunctionIdentifier("not", 1)
                ))
        )
        val rulePacksITR = DefaultRulePacks.defaultRulePacks.toTypedArray()

        val compiledConfiguration = createCompiledConfigurationFromITR(taskITR, rulePacksITR, comparisonSettings = ComparisonSettings(useTransformationsSortingInExpressionComparison = true))

        val usualCheckWithCompiledConf = { checkSolutionInTexITR(originalTexSolution, taskITR, rulePacksITR, comparisonSettings = ComparisonSettings(useTransformationsSortingInExpressionComparison = false), inputCompiledConfiguration = compiledConfiguration) }
        val usualCheck = { checkSolutionInTexITR(originalTexSolution, taskITR, rulePacksITR, comparisonSettings = ComparisonSettings(useTransformationsSortingInExpressionComparison = false)) }
        val usualCheckOptimized = { checkSolutionInTexITR(originalTexSolution, taskITR, rulePacksITR, comparisonSettings = ComparisonSettings(useTransformationsSortingInExpressionComparison = true)) }

        val compiledConfigurationFromITR = { createCompiledConfigurationFromITR(taskITR, rulePacksITR, comparisonSettings = ComparisonSettings(useTransformationsSortingInExpressionComparison = true)) }

        makeMeasures(listOf(
                Pair("compiledConfiguration", { compiledConfigurationFromITR() }),
                Pair("withCompiledConf", { usualCheckWithCompiledConf() }),
                Pair("simple", { usualCheck() }),
                Pair("clever", { usualCheckOptimized() })
        ), 100)
    }
}