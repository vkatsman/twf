package mathhelper.twf.benchmarking

import mathhelper.twf.baseoperations.BaseOperationsDefinitions
import mathhelper.twf.config.ComparisonType
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.expressiontree.ExpressionNodeConstructor
import org.junit.Ignore
import org.junit.Test
import mathhelper.twf.platformdependent.random
import mathhelper.twf.substitutiontests.parseStringExpression
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.test.assertEquals

val compiledConfiguration = CompiledConfiguration()
val expressionNodeConstructor = ExpressionNodeConstructor()
val baseOperationsDefinitions = BaseOperationsDefinitions()
val expressionComparator = compiledConfiguration.factComparator.expressionComparator

//val left = parseStringExpression("(-((-asin(tg(x)-0.56))+((0.15)^0.5)))")
//val right = parseStringExpression("(acos(tg(x)))")
val left = parseStringExpression("sqrt(2*(sqrt(1000001 - x) + sqrt(x - 1000000))+sqrt(-1))")
val right = parseStringExpression("sqrt(sqrt(1000001 - x)*2 + sqrt(x - 1000000)*2+sqrt(-1))")

fun simpleCompare (){
    val result = expressionComparator.probabilityTestComparison(left, right, comparisonType = ComparisonType.LEFT_LESS)
    //assertEquals(false, result)
}


fun cleverCompare (){
    val result = expressionComparator.probabilityTestComparison(left, right, comparisonType = ComparisonType.LEFT_LESS)
//    assertEquals(true, result)
}

fun cleverSqrt(iterCount: Int){
    var summa = 0.0
    for (i in 0 until iterCount){
        summa += sqrt(random(1.0, 100.0))
    }
}

fun stupidSqrt(iterCount: Int){
    var summa = 0.0
    for (i in 0 until iterCount)
        summa += random(1.0, 100.0).pow(0.5)
}


class ExpressionComparator {
    @Test
    @Ignore
    fun stringToExpressionMeasureTest() {
        makeMeasures(listOf(
                Pair("simple", { simpleCompare() }),
                Pair("clever", { cleverCompare() })
        ), 100)
    }

    @Test
    @Ignore //clever_average: 122.82 median_average: 768.32
    fun powVsSqrt() {
        val iterCount = 1E7.toInt();
        makeMeasures(listOf(
                Pair("clever", { cleverSqrt(iterCount) }),
                Pair("stupid", { stupidSqrt(iterCount) })
        ), iterationsInTestCount = 1)
    }
}