package mathhelper.twf.platformdependent

import org.junit.Test

import kotlin.test.assertTrue
import kotlin.test.assertEquals



internal class UtilsKtTest {

    @Test
    fun randomIntTestExcludedRightOneNumber() {
        val randomsSet = mutableSetOf<Int>()
        for (i in 1..10) {
            randomsSet.add(randomInt(2, 3))
        }
        assertTrue(randomsSet.contains(2))
        assertTrue(!randomsSet.contains(3))
    }

    @Test
    fun randomIntTestExcludedRightTwumbers() {
        val randomsSet = mutableSetOf<Int>()
        for (i in 1..10) {
            randomsSet.add(randomInt(2, 4))
        }
        assertTrue(randomsSet.contains(2))
        assertTrue(randomsSet.contains(3))
    }

    @Test
    fun testDoubleToShortString() {
        assertEquals("1", (1.0).toShortString())
        assertEquals("1.2", (1.200).toShortString())
    }

    @Test
    fun jsonParsingKlaxon() {
        val goalData = JsonParser.parseMap("{\"hiddenGoalExpressions\":[\"(0)\"]}") as Map<String, Any>
        assertEquals("{hiddenGoalExpressions=[(0)]}", goalData.toString())


        val goalDataFront = JsonParser.parseMap("{\"hiddenGoalExpressions\":[\"(0.15)\",\"(/(3;20))\"]}") as Map<String, Any>
        assertEquals("{hiddenGoalExpressions=[(0.15), (/(3;20))]}", goalDataFront.toString())
    }
}