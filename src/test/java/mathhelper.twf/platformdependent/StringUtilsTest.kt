package mathhelper.twf.platformdependent

import org.junit.Test
import kotlin.test.assertTrue

class StringUtilsTest {
    @Test
    fun testStringsComparison (){
        assertTrue("^(x;3)" in "(^(x;3))")
        assertTrue("^(x;3)" in "^(x;3)")
        assertTrue("(^(x;3))".contains("^(x;3)"))
        assertTrue("^(x;3)".contains("^(x;3)"))
    }

}