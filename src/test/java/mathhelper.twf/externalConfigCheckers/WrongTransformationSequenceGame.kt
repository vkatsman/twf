package mathhelper.twf.externalConfigCheckers

import mathhelper.twf.api.compareWithoutSubstitutions
import mathhelper.twf.api.stringToExpression
import mathhelper.twf.config.ComparisonSettings
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.config.DebugOutputMessages
import mathhelper.twf.config.FunctionConfiguration
import mathhelper.twf.platformdependent.JsonParser
import org.junit.Ignore
import org.junit.Test
import java.io.BufferedReader
import java.io.File
import kotlin.math.max
import kotlin.math.min
import kotlin.test.assertEquals

class WrongTransformationSequenceGame {
    val compiledConfiguration = CompiledConfiguration(
            comparisonSettings = ComparisonSettings(highPowCorrectionInfluence = 1.0)
//            debugOutputMessages = DebugOutputMessages(expressionProbabilityComparisonFalseDetailsPrintln=true)
    )

    @Test
    @Ignore
    fun testTimGameConfigLogicExpression() {
        val path = "/Users/vkatsman/IdeaProjects/TWF-scaled-game/resources/trigonometry/lvl3/seq1.json" //path to your file with game json config
        val scope = "" //"setTheory"
        val allowOnlyOneSymbolVariables = false //true // if true 'ab' = 'a*b', else 'ab' = 'ab'

        val bufferedReader: BufferedReader = File(path).bufferedReader()
        val jsonConfig = bufferedReader.use { it.readText() }
        val parsedConfig = JsonParser.parseMap(jsonConfig)

        val sequence = parsedConfig["sequence"] as List<Map<String, Any>>
        var prevString = sequence[0]["unicode"] as String
        var prevExpr = stringToExpression(prevString, scope, functionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet(),
                allowOnlyOneSymbolVariables = allowOnlyOneSymbolVariables
        ))
        val checkedSequence = StringBuilder(sequence[0]["unicode"] as String)
        var maxScore = 0
        var minScore = 0
        var correctTransformations = 0
        var incorrectTransformations = 0
        for (i in 1..sequence.lastIndex) {
            val currentString = sequence[i]["unicode"] as String
            val currentExpr = stringToExpression(currentString, scope, functionConfiguration = FunctionConfiguration(
                    scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet(),
                    allowOnlyOneSymbolVariables = allowOnlyOneSymbolVariables
            ))
            val scoreForHit = sequence[i]["scoreForHit"] as Int
            val scoreForSkip = sequence[i]["scoreForSkip"] as Int

            val isEqual = compiledConfiguration.factComparator.expressionComparator.compareWithoutSubstitutions(currentExpr, prevExpr)
            if (isEqual && scoreForHit >= 0) {
                println("Error: correct step from '$prevString' to '$currentString' with non-negative scoreForHit ${scoreForHit}; parsed '$prevExpr' to '$currentExpr'")
            } else if (!isEqual && scoreForSkip >= 0) {
                println("Error: wrong step from '$prevString' to '$currentString' with non-negative scoreForSkip ${scoreForSkip}; parsed '$prevExpr' to '$currentExpr'")
            }
            if (isEqual)
                correctTransformations++
            else
                incorrectTransformations++

            prevString = currentString
            prevExpr = currentExpr
            checkedSequence.append(" ${if (isEqual) "=" else "!="} ${sequence[i]["unicode"] as String}")
            maxScore += max(scoreForHit,scoreForSkip)
            minScore += min(scoreForHit, scoreForSkip)
        }
        println(checkedSequence)
        print("max score: ")
        println(maxScore)
        print("min score: ")
        println(minScore)
        print("correct transformations: ")
        println(correctTransformations)
        print("incorrect transformations: ")
        println(incorrectTransformations)
    }

    @Test
    fun checkWrongSubstitutionWithImplication() {
        val firstExpr = stringToExpression("a→(b→c)", "setTheory")
        val secondExpr = stringToExpression("a→b→c", "setTheory")
        val isEqual = compiledConfiguration.factComparator.expressionComparator.compareWithoutSubstitutions(firstExpr, secondExpr)
        assertEquals(false, isEqual)
    }

    @Test
    fun checkCorrectSubstitutionWith3argImplication() {
        val firstExpr = stringToExpression("(a→b)→c", "setTheory")
        val secondExpr = stringToExpression("a→b→c", "setTheory")
        val isEqual = compiledConfiguration.factComparator.expressionComparator.compareWithoutSubstitutions(firstExpr, secondExpr)
        assertEquals(true, isEqual)
    }

    @Test
    fun checkWrongSubstitutionWith3argImplicationPermuted() {
        val firstExpr = stringToExpression("(a→b)→c", "setTheory")
        val secondExpr = stringToExpression("b→a→c", "setTheory")
        val isEqual = compiledConfiguration.factComparator.expressionComparator.compareWithoutSubstitutions(firstExpr, secondExpr)
        assertEquals(false, isEqual)
    }

    @Test
    fun checkCorrectSubstitutionWith3argAlleq() {
        val firstExpr = stringToExpression("a≡b≡c", "setTheory")
        val secondExpr = stringToExpression("(a≡b)≡c", "setTheory")
        val isEqual = compiledConfiguration.factComparator.expressionComparator.compareWithoutSubstitutions(firstExpr, secondExpr)
        assertEquals(false, isEqual)
    }

    @Test
    fun checkCorrectSubstitutionWith3argNand() {
        val firstExpr = stringToExpression("a↑b↑c", "setTheory")
        val secondExpr = stringToExpression("(a↑b)↑c", "setTheory")
        val isEqual = compiledConfiguration.factComparator.expressionComparator.compareWithoutSubstitutions(firstExpr, secondExpr)
        assertEquals(false, isEqual)
    }

    @Test
    fun checkCorrectSubstitutionWith3argNor() {
        val firstExpr = stringToExpression("a↑b↑c", "setTheory")
        val secondExpr = stringToExpression("(a↓b)↓c", "setTheory")
        val isEqual = compiledConfiguration.factComparator.expressionComparator.compareWithoutSubstitutions(firstExpr, secondExpr)
        assertEquals(false, isEqual)
    }

    @Test
    fun checkCorrectSubstitutionWith3argAnd() {
        val firstExpr = stringToExpression("a&b&c", "setTheory")
        val secondExpr = stringToExpression("(a&b)&c", "setTheory")
        val isEqual = compiledConfiguration.factComparator.expressionComparator.compareWithoutSubstitutions(firstExpr, secondExpr)
        assertEquals(true, isEqual)
    }
}