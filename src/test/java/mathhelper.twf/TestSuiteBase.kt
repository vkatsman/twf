package mathhelper.twf

import mathhelper.twf.platformdependent.random
import org.junit.jupiter.api.BeforeEach
import kotlin.random.Random

/**
 * Base class for all test classes
 * Used for global configuration
 */
open class TestSuiteBase {
    @BeforeEach
    fun seedRandom() {
        //random = Random(RANDOM_SEED)
        //println("Test random value: ${random.nextInt()}")
    }
}