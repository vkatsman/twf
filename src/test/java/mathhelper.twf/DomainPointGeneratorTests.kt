import mathhelper.twf.assert
import mathhelper.twf.optimizerutils.DomainPointGenerator
import mathhelper.twf.config.ComparisonType
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.morokei.*
import org.junit.Ignore
import org.junit.Test
import mathhelper.twf.substitutiontests.parseStringExpression
import kotlin.test.assertEquals

class DomainPointGeneratorTests {
    val compiledConfiguration = CompiledConfiguration()

    @Test
    fun domainPointGeneratorTest1() {
        val expression = parseStringExpression("sqrt((x - 5)*(7 - x))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun domainPointGeneratorTest2() {
        val expression = parseStringExpression("sqrt(x * y * z)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun domainPointGeneratorTest3() {
        val expression = parseStringExpression("ln(x * y * z) + sqrt(-(x + y + z))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun domainPointGeneratorTest4() {
        val expression = parseStringExpression("sqrt(101 - x) + sqrt(x - 100)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun domainPointGeneratorTest5() {
        val expression = parseStringExpression("sqrt(10001 - x) + sqrt(y - 10000)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun domainPointGeneratorTest6() {
        val expression = parseStringExpression("sqrt(10001 - x) + sqrt(x - 10000)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    @Ignore //failed 3 times TODO: fix - vlad
    fun domainPointGeneratorTest8() {
        val expression = parseStringExpression("sqrt(1000001 - x) + sqrt(x - 1000000)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun domainPointGeneratorTest11() {
        val expression = parseStringExpression("sqrt(-5)")
        assertEquals(false, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    @Ignore
    fun domainPointGeneratorTest13() {
        val expression = parseStringExpression("ln(x - 1000000) + ln(1000001 - x)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun domainPointGeneratorTest15() {
        val expression = parseStringExpression("(cos(x)+h)*s")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun domainPointGeneratorTest16() {
        val expression = parseStringExpression("(sqrt(sin(x-512)-0.99))*sqrt(s-10)*sqrt(11-s)")
        val domain = DomainPointGenerator(arrayListOf(expression))
        assertEquals(true, domain.hasPointsInDomain())
    }

    @Test
    fun domainPointGeneratorTest17() {
        val expression = parseStringExpression("sqrt(a-10.0)*sqrt(11.0-a)+sqrt(b-10.0)*sqrt(11.0-b)+sqrt(c-10.0)*sqrt(11.0-c)+sqrt(d-10.0)*sqrt(11.0-d)+sqrt(e-10.0)*sqrt(11.0-e)")
        val domain = DomainPointGenerator(arrayListOf(expression))
        println(domain.getResult())
        assertEquals(true, domain.hasPointsInDomain())
    }
}

class DomainPointGeneratorTestsFromDemidovich {
    val compiledConfiguration = CompiledConfiguration()

    @Test
    fun demidovich1688() {
        val expression = parseStringExpression("1/sqrt(x*(1-x))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich1694() {
        val a = 1000
        val expression = parseStringExpression("1/(x*ln(x-${a})*ln(ln(10009.5-x)))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich152() {
        val expression = parseStringExpression("sqrt(3*x-x^3)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich153() {
        val expression = parseStringExpression("(x - 2)sqrt((1 + x)/(1 - x))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich154a() {
        val expression = parseStringExpression("ln(x^2-4)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich154b() {
        val expression = parseStringExpression("ln(x-2) + ln(x + 2)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich155() {
        val expression = parseStringExpression("sqrt(sin(sqrt(x)))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich156() {
        val expression = parseStringExpression("sqrt(cos(x^2))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich157() {
        val expression = parseStringExpression("ln(sin(3.14/x))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich158() {
        val expression = parseStringExpression("sqrt(x)/(sin(3.14x))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich159() {
        val expression = parseStringExpression("asin(2x/(1 + x))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich160() {
        val expression = parseStringExpression("acos(2sin(x))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich161() {
        val expression = parseStringExpression("ln(cos(ln(x)))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich162() {
        val expression = parseStringExpression("sqrt(x - abs(x)*sqrt(-sin(3.14*x)^3))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich166() {
        val expression = parseStringExpression("sqrt(2 + x - x^2)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich167() {
        val expression = parseStringExpression("ln(1-2*cos(x))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun demidovich168() {
        val expression = parseStringExpression("acos(2*x/(1 + x^2))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }
}

class ExpressionGeneratorTests {
    val compiledConfiguration = CompiledConfiguration()

    @Test
    fun expressionGeneratorTest1() {
        val generator = TemplateExpressionGenerator("sqrt(.-x) + sqrt(x-.)", arrayListOf("1>2"), 0.0, 10.0)
        val generatedExpression = generator.generateExpression()
        val expression = parseStringExpression(generatedExpression)
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun expressionGeneratorTest2() {
        val generator = TemplateExpressionGenerator(".+.+.+.+.+.", arrayListOf("1>2>3", "4<5<6"), 0.0, 10.0)
        val generatedExpression = generator.generateExpression()
        val expression = parseStringExpression(generatedExpression)
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun expressionGeneratorTest3() {
        val generator = TransformationExpressionGenerator("x+y", arrayListOf("sin", "cos", "sqrt", "ln"))
        val generatedExpression = generator.generateExpression(3)
        val expression = parseStringExpression(generatedExpression)
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun expressionGeneratorTest4() {
        val generator = TransformationExpressionGenerator("x+y+z", arrayListOf("+", "-", "*"))
        val expression = parseStringExpression(generator.generateExpression())
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun expressionGeneratorTest5() {
        val expression = parseStringExpression("x+y+z*r")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    @Ignore //TODO: fix "sys_def_i_complex cannot be converted to complex"
    fun expressionGeneratorTest5_with_complex() {
        val expression = parseStringExpression("x+y+z*sys_def_i_complex")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    fun expressionGeneratorTest6() {
        val generator = TransformationExpressionGenerator("sqrt(x-512)^2", arrayListOf("+", "*", "ln", "cos", "sin"))
        val expression = parseStringExpression(generator.generateExpression())
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }
}

class BreakingDomainTests {
    val compiledConfiguration = CompiledConfiguration()

    @Test
    @Ignore
    fun breakingDomainTest1() {
        val breaker = DomainBreaker("sqrt(x-a)+sqrt((a+d)-x)", arrayListOf("1<2"), arrayListOf("ln", "sqrt", "+"), 0.0, 100.0)
        println(breaker.breakDomainGenerator())
    }
}

class AllowedParams1 {
    val compiledConfiguration = CompiledConfiguration()

    @Test
    @Ignore
    fun allowedParamsTest1() {
        val annealingRatioGetter = AllowedRatioGetter1(1, annealingRuns = 1, gradientDescentRuns = 1)
        println(annealingRatioGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest2() {
        val annealingRatioGetter = AllowedRatioGetter1(1, innerOperation = '*', annealingRuns = 1, gradientDescentRuns = 1)
        println(annealingRatioGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest3() {
        val rationGetter = AllowedRatioGetter1(5, gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest4() {
        val rationGetter = AllowedRatioGetter1(5, innerOperation = '*', annealingRuns = 1, gradientDescentRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest5() {
        val rationGetter = AllowedRatioGetter1(1, func = "ln", annealingRuns = 1, gradientDescentRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest6() {
        val rationGetter = AllowedRatioGetter1(1, func = "ln", innerOperation = '*', annealingRuns = 1, gradientDescentRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest7() {
        val rationGetter = AllowedRatioGetter1(5, func = "ln", gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest8() {
        val ratioGetter = AllowedRatioGetter1(5, func = "ln", innerOperation = '*', annealingRuns = 1, gradientDescentRuns = 1)
        println(ratioGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest9() {
        val rationGetter = AllowedRatioGetter1(5, outerOperation = '*', gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest10() {
        val rationGetter = AllowedRatioGetter1(5, outerOperation = '*', innerOperation = '*', annealingRuns = 1, gradientDescentRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest11() {
        val rationGetter = AllowedRatioGetter1(5, func = "ln", outerOperation = '*', gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest12() {
        val rationGetter = AllowedRatioGetter1(5, func = "ln", outerOperation = '*', innerOperation = '*', annealingRuns = 1, gradientDescentRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest13() {
        val rationGetter = AllowedRatioGetter1(10, gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest14() {
        val rationGetter = AllowedRatioGetter1(15, gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest15() {
        val rationGetter = AllowedRatioGetter1(20, gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest16() {
        val rationGetter = AllowedRatioGetter1(26, gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10, 10, 100000.0))
    }
}


class AllowedParams2 {
    val compiledConfiguration = CompiledConfiguration()

    @Test
    @Ignore
    fun allowedParamsTest1() {
        val rationGetter = AllowedRatioGetter2(5, gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(1.0, 15, 1.3))
    }

    @Test
    @Ignore
    fun allowedParamsTest2() {
        val rationGetter = AllowedRatioGetter2(3, gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10.0, 15, 1.4))
    }


    @Test
    @Ignore
    fun allowedParamsTest3() {
        val rationGetter = AllowedRatioGetter2(15, gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10.0, 15, 2.0))
    }


    @Test
    @Ignore
    fun allowedParamsTest4() {
        val rationGetter = AllowedRatioGetter2(20, gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10.0, 15, 2.0))
    }

    @Test
    @Ignore
    fun allowedParamsTest5() {
        val rationGetter = AllowedRatioGetter2(26, gradientDescentRuns = 1, annealingRuns = 1)
        println(rationGetter.getAllowedRatio(10.0, 15, 2.0))
    }

}

class AllowedParams3 {
    val compiledConfiguration = CompiledConfiguration()

    @Test
    @Ignore
    fun coolTest1() {
        val ratioGetter = AllowedParamsGetter3()
        println(ratioGetter.getParam())
    }

    @Test
    @Ignore
    fun coolTest2() {
        val ratioGetter = AllowedParamsGetter4()
        println(ratioGetter.getParam())
    }

    @Test
    @Ignore
    fun coolTest3() {
        val ratioGetter = AllowedParamsGetter5()
        println(ratioGetter.getParam())
    }
}

class DomainPointGeneratorInequalityTests {
    val compiledConfiguration = CompiledConfiguration()
    val expressionComparator = compiledConfiguration.factComparator.expressionComparator

    @Test
    @Ignore
    fun inequalityTest1() {
        val left = parseStringExpression("x")
        val right = parseStringExpression("100")
        val result1 = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_LESS)
        val result2 = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_MORE)
        assertEquals(false, result1.or(result2))  //TODO: split on test
    }

    @Test
    @Ignore
    fun inequalityTest2() {
        val left = parseStringExpression("x")
        val right = parseStringExpression("10^5")
        val result1 = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_LESS)
        val result2 = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_MORE)
        assertEquals(false, result1.or(result2))
    }

    @Test
    fun inequalityTest3() {
        val left = parseStringExpression("x")
        val right = parseStringExpression("10^5+y+z+w+t")
        val result1 = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_LESS)
        val result2 = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_MORE)
        assertEquals(false, result1.or(result2))
    }

    @Test
    fun inequalityTest4() {
        val left = parseStringExpression("sin(x)*sqrt(x)")
        val right = parseStringExpression("x*sqrt(x)")
        val result = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_LESS)
        assertEquals(false, result)
    }

    @Test
    fun inequalityTest5() {
        val left = parseStringExpression("sin(x^2)*sqrt(x^2)")
        val right = parseStringExpression("x^2*sqrt(x^2)")
        val result = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_LESS_OR_EQUAL)
        assertEquals(true, result)
    }

    @Test
    @Ignore
    fun inequalityTest6() {
        val t = 1000
        val left = parseStringExpression("sin(x-$t})*sqrt(x-$t)")
        val right = parseStringExpression("(x-$t)*sqrt(x-$t)")
        val result = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_LESS)
        assertEquals(true, result)
    }

    @Test
    @Ignore
    fun inequalityTest7() {
        val t = 10000
        val left = parseStringExpression("sin(x-$t})*sqrt(x-$t)")
        val right = parseStringExpression("(x-$t)*sqrt(x-$t)")
        val result = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_LESS)
        assertEquals(true, result)
    }

    @Test
    fun inequalityTest8() {
        val left = parseStringExpression("x+5")
        val right = parseStringExpression("x+5")
        val result1 = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_LESS_OR_EQUAL)
        val result2 = expressionComparator.probabilityTestComparison(left, right, ComparisonType.LEFT_MORE_OR_EQUAL)
        assertEquals(true, result1.and(result2))
    }
}

class DomainPointGeneratorInequalityIgnoreTests {
    val compiledConfiguration = CompiledConfiguration()


    @Test
    @Ignore // there is no function ctg
    fun demidovich163() {
        val expression = parseStringExpression("sqrt(ln(ctg(3.14*x) + acos(2^x)))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    @Ignore
    fun demidovich164() {
        val expression = parseStringExpression("asin(1000 - x) + ln(ln(x))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    @Ignore
    fun domainPointGeneratorTest7() {
        val expression = parseStringExpression("sqrt(100001 - x) + sqrt(x - 100000)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    @Ignore
    fun domainPointGeneratorTest9() {
        val expression = parseStringExpression("sqrt(1000001 - x) + sqrt(x - 1000000)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    @Ignore
    fun domainPointGeneratorTest10() {
        val expression = parseStringExpression("n!")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    @Ignore
    fun domainPointGeneratorTest12() {
        val expression = parseStringExpression("ln(x - 100000) / ln(100000.6 - x) - ln(x - 100000.5) * ln(100001 - x)")
        val domain = DomainPointGenerator(arrayListOf(expression))
        assertEquals(true, domain.hasPointsInDomain())
        assert(domain.generateNewPointInDomain()["x"]!!.toDouble() < 100000.6 && domain.generateNewPointInDomain()["x"]!!.toDouble() > 100000.5)
    }

    @Test
    @Ignore
    fun domainPointGeneratorTest19() {
        val expression = parseStringExpression("sqrt(a-10000.0)*sqrt(11000.0-a)")
        val domain = DomainPointGenerator(arrayListOf(expression), annealingRuns = 0, gradientDescentRuns = 1)
        println(domain.getResult())
        assertEquals(true, domain.hasPointsInDomain())
    }

    @Test
    @Ignore
    fun domainPointGeneratorTest20() {
        val expression = parseStringExpression("sqrt(a-60000.0)+sqrt(460000.0-a)+sqrt(b-60000.0)+sqrt(460000.0-b)+sqrt(c-60000.0)+sqrt(460000.0-c)+sqrt(d-60000.0)+sqrt(460000.0-d)+sqrt(e-60000.0)+sqrt(460000.0-e)")
        val domain = DomainPointGenerator(arrayListOf(expression), annealingRuns = 0, gradientDescentRuns = 1)
        println(domain.getResult())
        assertEquals(true, domain.hasPointsInDomain())
    }

    @Test
    @Ignore
    fun domainPointGeneratorTest21() {
        val expression = parseStringExpression("sqrt(a-10000.0)+sqrt(10500.0-a)+sqrt(b-10000.0)+sqrt(10500.0-b)+sqrt(c-10000.0)+sqrt(10500.0-c)+sqrt(d-10000.0)+sqrt(10500.0-d)+sqrt(e-10000.0)+sqrt(10500.0-e)")
        val domain = DomainPointGenerator(arrayListOf(expression), annealingRuns = 0, gradientDescentRuns = 1)
        println(domain.getResult())
        assertEquals(true, domain.hasPointsInDomain())
    }

    @Test
    @Ignore
    fun domainPointGeneratorTest14() {
        val expression = parseStringExpression("sqrt(100000001 - x) + sqrt(x - 100000000)")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }

    @Test
    @Ignore
    fun domainPointGeneratorTest18() {
        val expression = parseStringExpression("sqrt(a-10000.0)+sqrt(11000.0-a)+sqrt(b-10000.0)+sqrt(11000.0-b)+sqrt(c-10000.0)+sqrt(11000.0-c)+sqrt(d-10000.0)+sqrt(11000.0-d)+sqrt(e-10000.0)+sqrt(11000.0-e)")
        val domain = DomainPointGenerator(arrayListOf(expression), annealingRuns = 0, gradientDescentRuns = 1)
        assertEquals(true, domain.hasPointsInDomain())
    }

    @Test
    @Ignore
    fun demidovich1701() {
        val expression = parseStringExpression("1/(sin(x)^2*(tg(x)^3 - 100000)^(1/4))")
        assertEquals(true, DomainPointGenerator(arrayListOf(expression)).hasPointsInDomain())
    }
}