package mathhelper.twf.expressionstests

import mathhelper.twf.api.compareWithoutSubstitutions
import mathhelper.twf.config.ComparisonType
import mathhelper.twf.config.CompiledConfiguration
import org.junit.Ignore
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class ExpressionInequalityComparisonTest {
    @Test
    fun testSimpleLessThenTest() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_LESS
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val res = compareWithoutSubstitutions(
                "x",
                "x + 1",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, res);

    }

    @Test
    fun testLessSumm3Vars() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_LESS
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val res = compareWithoutSubstitutions(
                "x + y + z",
                "3.14",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(false, res);
    }

}