package mathhelper.twf.expressionstests

import mathhelper.twf.api.compareWithoutSubstitutions
import mathhelper.twf.api.compareWithoutSubstitutionsStructureStrings
import mathhelper.twf.config.ComparisonType
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.optimizerutils.OptimizerUtils
import org.junit.Ignore
import org.junit.Test
import mathhelper.twf.substitutiontests.parseStringExpression
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ExpressionEqualityComparisonTest {
    @Test
    fun testEqualTrigonometricExpressionsCorrect() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        val result = compareWithoutSubstitutions(
                "2 * sin(x) * cos(x)",
                "sin(2x)",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)
    }


    @Test
    fun testEqualTrigonometricExpressionsWrong() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        val result = compareWithoutSubstitutions(
                "2 * sin(x) * cos(x)",
                "cos(2x)",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(false, result)
    }

    @Test
    fun testFunctionWithSmallDomainOfDefinition() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val result = compareWithoutSubstitutions(
                "ln(1 - sqrt(x)) + ln(y)",
                "ln(y * (1 - sqrt(x)))",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)
    }

    @Test
    fun testSimpleAndComplexEqual() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val result = compareWithoutSubstitutions(
                "x + y",
                "(x + y) * ((sin(x))^2 + (cos(x))^2) * sqrt(-x^2) / sqrt(-x^2)",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)
    }

    @Test
    fun testTgEquality() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        val result = compareWithoutSubstitutionsStructureStrings(
                "(*(+(tg(x);ctg(x));+(/(4;^(sin(*(2;x));2));-(3))))",
                "(+(^(tg(x);3);^(ctg(x);3)))",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)
    }

    @Test
    @Ignore
    fun testPowEquality() { //TODO: think about domain limited transformations
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        val result = compareWithoutSubstitutionsStructureStrings(
                "(cos(a))",
                "(^(+(1;-(^(sin(a);2)));0.5))",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)
    }

    @Test
    fun testPrimitiveEquality() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        var result = compareWithoutSubstitutions(
                "x + y",
                "-(-x - y)",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)

        result = compareWithoutSubstitutions(
                "x * (1 / y)",
                "x / y",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)
    }

    @Test
    fun testFastTestOnEquality() {
        val left = parseStringExpression("sqrt(x * y)")
        val right = parseStringExpression("sqrt(x) * sqrt(y)")
        val compiledConfiguration = CompiledConfiguration()

        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false

        assertTrue(compiledConfiguration.factComparator
                .expressionComparator.probabilityTestComparison(left, right))
    }

    // Approximately 11 out of 24 tests pass
    @Test
    fun testSumOfThreeLogsWithSmallDomain() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val result = compareWithoutSubstitutions(
                "ln(1 - sqrt(x)) + ln(y) + ln(z)",
                "ln(z * y * (1 - sqrt(x)))",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)
    }

    // Approximately 12 out of 14 tests pass
    @Test
    fun testSumOfThreeLogsWithBigDomain() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val result = compareWithoutSubstitutions(
                "ln(x) + ln(y) + ln(z)",
                "ln(z * y * x)",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)
    }
}


class ExpressionEqualityComparisonUseGradientDescentTest {
    @Test
    fun test0() {
        val expression = parseStringExpression("x")
        val expressionMinizer = OptimizerUtils(expression)
        assertEquals(true, expressionMinizer.canStart() && expressionMinizer.run())
    }

    @Test
    fun test1() {
        val expression = parseStringExpression("x*100+x^3+100")
        val expressionMinizer = OptimizerUtils(expression)
        assertEquals(true, expressionMinizer.canStart() && expressionMinizer.run())
    }

}