package mathhelper.twf.expressionstests

import mathhelper.twf.assert
import mathhelper.twf.baseoperations.BaseOperationsDefinitions
import mathhelper.twf.config.ComparisonType
import mathhelper.twf.config.CompiledConfiguration
import mathhelper.twf.expressiontree.ExpressionNodeConstructor
import mathhelper.twf.substitutiontests.parseStringExpression
import org.junit.Ignore
import org.junit.Test

val compiledConfiguration = CompiledConfiguration()
val expressionNodeConstructor = ExpressionNodeConstructor()
val baseOperationsDefinitions = BaseOperationsDefinitions()
val expressionComparator = compiledConfiguration.factComparator.expressionComparator

fun calculateProbabilityOfSuccess(
        leftt: String,
        rightt: String,
        compiledConfiguration: CompiledConfiguration,
        comparisonIsCorrect: Boolean = false,
        iters: Int = 1000
) : Double {
    var cnt = 0
    val left = parseStringExpression(leftt)
    val right = parseStringExpression(rightt)
    for (iter in 0 until iters) {
        val res = expressionComparator.probabilityTestComparison(
                        left,
                        right,
                        comparisonType = compiledConfiguration.comparisonSettings.defaultComparisonType,
                        useGradientDescentComparison = true
                )
        if (res == comparisonIsCorrect)
            cnt++
    }
    return cnt.toDouble() / iters
}

class LightInequalityTests {
    @Test
    fun test1() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_LESS
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "sin(x)*sqrt(x)+0.1"
        val right = "x*sqrt(x)"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 90.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test2() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "exp(x)"
        val right = "1+x+0.1"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 90.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }


    @Test
    fun test4() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(x+1/x)*sqrt(x)"
        val right = "2*sqrt(x)+0.1"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 99.9
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun testGeometricMeanAndMean() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_LESS
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "2*a*b+0.03"
        val right = "a^2+b^2"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 90.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test5() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(a^2+b^2)*(z^2+y^2)"
        val right = "(a*z+b*y)^2 + 1"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 90.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test8() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "a^2+b^2"
        val right = "2.1 * a * b"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 90.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})

    }

    @Test
    fun test10() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "ln(a-777) + 1/ln(a-777)"
        val right = "3"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 90.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})

    }

    @Test
    fun test10a() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "ln(a-777) + 1/ln(a-777)"
        val right = "1"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 90.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test10b() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_LESS
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "ln(a-777) + 1/ln(a-777)"
        val right = "-10"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 90.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test10c() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "ln(a-777) + 1/ln(a-777)"
        val right = "-100"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 80.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }


    @Test
    fun test11() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "ln(a-17) + 1/ln(a-17)"
        val right = "3"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false,
            iters = 10
        ) * 100
        val goodEnough = 9.0 // 9/10
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})

    }
}


@Ignore
class HeavyInequalityTests {
    @Test
    fun test1a() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_LESS
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "sin(x)*sqrt(x)-0.1"
        val right = "x*sqrt(x)"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            true
        ) * 100
        val goodEnough = 99.9
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test3() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_LESS
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "ln(1+x)*sqrt(x)+0.1"
        val right = "x*sqrt(x)"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 99.9
        assert(prob >= goodEnough, { "$prob >= $goodEnough" })
    }

    @Test
    fun test3a() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_LESS
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "ln(1+x)*sqrt(x)-0.1"
        val right = "x*sqrt(x)"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            true
        ) * 100
        val goodEnough = 99.9
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test6() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(a^2+b^2+c^2)*(z^2+y^2+x^2)"
        val right = "(a*z+b*y+c*z)^2 + 1"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 99.9
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test7() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(a^2+b^2+c^2)*(z^2+y^2+x^2)"
        val right = "(a*z+b*y+c*z)^2 + 1"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 99.9
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test9() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(sqrt(a)-7)^2+(ln(b)-17)^2"
        val right = "3 * (sqrt(a)-7) * (ln(b)-17)"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 98.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test12() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "exp(exp(x))"
        val right = "exp(x^10)"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 99.5
        print(prob)
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test13() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(a + b + c + d + e + f + g)^2*100"
        val right = "x + y + z + t + w + v + h + 10"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 99.9
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test14() { // TODO: impove
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(x - 100000)^2"
        val right = "x - 100000"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 96.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test14a() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(x - 10)^2+1"
        val right = "x - 10"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            true
        ) * 100
        val goodEnough = 99.9
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test15() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(1+x)^n*sqrt(x+1)*sqrt(n)"
        val right = "(2+n*x)*sqrt(x+1)*sqrt(n)"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 98.7
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }


    @Test
    fun test16() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(a-7+b+7+c-17+d-13)/4"
        val right = "((a-7)*(b+7)*(c-17)*(d-13))^(1/4)"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 99.9
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test17() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "(a-7+b+7+c-17+d-13)/4"
        val right = "5/(1/(a-7)+1/(b+7)+1/(c-17)+1/(d-13))"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 98.7
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test18() { // TODO: improve
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "sqrt(((a-3)^2+(b+88)^2+(c-17)^2+d^4)/4)"
        val right = "(a-3+b+88+c-17+d^2)/4+sin(d)"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 88.0
        assert(prob >= goodEnough, {"$prob >= $goodEnough"})
    }

    @Test
    fun test19() { // TODO: improve
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_MORE
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val left = "3*((a-77)^2+(b+99)^2+(c-128)^2)"
        val right = "(a-77+b+99+c-128+(1))^2"

        val prob = calculateProbabilityOfSuccess(
            left,
            right,
            compiledConfiguration,
            false
        ) * 100
        val goodEnough = 42.1
        assert(prob >= goodEnough, { "$prob >= $goodEnough" })
    }
}
