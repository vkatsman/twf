package mathhelper.twf.expressionstests

import mathhelper.twf.expressiontree.ExpressionTreeParser
import org.junit.Ignore
import org.junit.Test
import kotlin.test.assertEquals

class ParserComplexVarTests {
    @Test
    fun testComplex() {
        val expressionTreeParser = ExpressionTreeParser("i")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(sys_def_i_complex)", root.toString())
    }

    @Test
    fun testComplexConjugateParse() {
        val expressionTreeParser = ExpressionTreeParser("compconj\\left(x+yi\\right)")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(compconj(+(x;*(y;sys_def_i_complex))))", root.toString())
    }

    @Test
    fun testTreeWithIMultNormAndWithoutFactors() {
        val expressionTreeParser = ExpressionTreeParser("\\exp \\left(\\ln \\left(2\\right)+\\frac{i}{2}\\right)")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(exp(+(ln(2);/(sys_def_i_complex;2))))", root.toString())
        val rootWithNorm = root.cloneWithIMultipleNorm()
        assertEquals("(exp(+(ln(2);*(/(1;2);sys_def_i_complex))))", rootWithNorm.toString())
    }

    @Test
    fun testTreeWithIMultNormAndOneFactors() {
        val expressionTreeParser = ExpressionTreeParser("\\exp \\left(\\ln \\left(2\\right)+\\frac{\\pi i}{2}\\right)")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(exp(+(ln(2);/(*(π;sys_def_i_complex);2))))", root.toString())
        val rootWithNorm = root.cloneWithIMultipleNorm()
        assertEquals("(exp(+(ln(2);*(/(*(π);2);sys_def_i_complex))))", rootWithNorm.toString())
    }

    @Test
    fun testTreeWithIMultNormAndManyFactors() {
        val expressionTreeParser = ExpressionTreeParser("\\exp \\left(\\ln \\left(2\\right)+\\frac{12\\cdot ab\\cdot c\\cdot \\pi i}{2}\\right)")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(exp(+(ln(2);/(*(12;ab;c;π;sys_def_i_complex);2))))", root.toString())
        val rootWithNorm = root.cloneWithIMultipleNorm()
        assertEquals("(exp(+(ln(2);*(/(*(12;ab;c;π);2);sys_def_i_complex))))", rootWithNorm.toString())
        print(rootWithNorm.toTexView())
    }

    @Test
    fun testSortOperands() {
        val expressionTreeParser = ExpressionTreeParser("cos(x+iy)+3+i+4+yi+sin(x+y)+x+cos((a+bi)*(ic+d))+10+a*i*b*i*c+t")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(+(10;3;4;sin(+(x;y));*(y;sys_def_i_complex);*(a;b;c;sys_def_i_complex;sys_def_i_complex);cos(+(*(y;sys_def_i_complex);x));cos(*(+(*(b;sys_def_i_complex);a);+(*(c;sys_def_i_complex);d)));x;t;sys_def_i_complex))",
                root.sortChildrenForExpressionSubstitutionComparison().toString())
    }

    @Test
    fun testPi() {
        val expressionTreeParser = ExpressionTreeParser("pi")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(pi)", root.toString())
    }

    @Test
    fun testPII() {
        val expressionTreeParser = ExpressionTreeParser("pii")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(*(pi;sys_def_i_complex))", root.toString())
    }

    @Test
    fun testIPI() {
        val expressionTreeParser = ExpressionTreeParser("ipi")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(*(sys_def_i_complex;pi))", root.toString())
    }

    @Test
    fun testSumVarsComplex() {
        val expressionTreeParser = ExpressionTreeParser("ai + ib + iai + iaiai + 5pi + i + c + d - i + 78i")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(+(*(a;sys_def_i_complex);*(sys_def_i_complex;b);*(ia;sys_def_i_complex);*(iaia;sys_def_i_complex);*(5;pi);sys_def_i_complex;c;d;-(sys_def_i_complex);*(78;sys_def_i_complex)))", root.toString())
    }

    @Test
    fun testSumOfMultipleComplex() {
        val expressionTreeParser = ExpressionTreeParser("a*i + ib + ai + i*b")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(+(*(a;sys_def_i_complex);*(sys_def_i_complex;b);*(a;sys_def_i_complex);*(sys_def_i_complex;b)))", root.toString())
    }

    @Test
    fun testInternalPWithExternalComplexVarSimple() {
        val expressionTreeParser = ExpressionTreeParser("S(j, 1, n, (P(i, 1, n, f(i)) + i))")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(S(j;1;n;+(P(i;1;n;*(f;i));sys_def_i_complex)))", root.toString())
    }

    @Test
    fun testInternalPWithExternalComplexVarIPI() {
        val expressionTreeParser = ExpressionTreeParser("S(j, 1, n, (P(i, 1, n, f(i)) + ipi))")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(S(j;1;n;+(P(i;1;n;*(f;i));*(sys_def_i_complex;pi))))", root.toString())
    }

    @Test
    fun testInternalPWithExternalComplexVarIAI() {
        val expressionTreeParser = ExpressionTreeParser("S(j, 1, n, (P(i, 1, n, f(i)) + iai))")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(S(j;1;n;+(P(i;1;n;*(f;i));*(ia;sys_def_i_complex))))", root.toString())
    }

    @Test
    fun testInternalSWithExternalICounterVarSimple() {
        val expressionTreeParser = ExpressionTreeParser("P(i, 1, n, (S(j, 1, n, (f(j) + i))))")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(P(i;1;n;S(j;1;n;+(*(f;j);i))))", root.toString())
    }

    @Test
    fun testInternalSWithExternalICounterVarIAI() {
        val expressionTreeParser = ExpressionTreeParser("P(iai, 1, n, (S(j, 1, n, (f(j) + iai))))")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(P(iai;1;n;S(j;1;n;+(*(f;j);iai))))", root.toString())
    }

    @Test
    fun testInternalSWithExternalICounterVarIPI() {
        val expressionTreeParser = ExpressionTreeParser("P(ipi, 1, n, (S(j, 1, n, (f(j) + ipi))))")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(P(ipi;1;n;S(j;1;n;+(*(f;j);ipi))))", root.toString())
    }

    @Test
    fun testSumWithFirstIInCounterName() {
        val expressionTreeParser = ExpressionTreeParser("S(ia, 1, n, ia+1)")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(S(ia;1;n;+(ia;1)))", root.toString())
    }

    @Test
    fun testPowMulBracketsWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msup><mi>a</mi><mrow><mn>2</mn><mo>+</mo><mi>i</mi></mrow></msup><mo>(</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>)</mo></math>")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(*(^(a;+(2;sys_def_i_complex));+(x;y)))", root.toString())
    }

    @Test
    fun testTexProductMultifunctionUnderProductionWithComplex() { //TODO: think about correct behaviour in such case
        val expressionTreeParser = ExpressionTreeParser("\\prod_{i=a}^bf(i)", true)
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(*(P(i;a;b;f);sys_def_i_complex))", root.toString())
    }

    @Test
    fun testTexCDOTWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("i\\cdot\ti\\cdot i")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(*(sys_def_i_complex;sys_def_i_complex;sys_def_i_complex))", root.toString())
    }

    @Test
    fun testMinusWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("-i")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(+(-(sys_def_i_complex)))", root.toString())
    }

    @Test
    fun testComplicatedMinusWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("-i-9+8-g")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(+(-(sys_def_i_complex);-(9);8;-(g)))", root.toString())
    }

    @Test
    fun testPSumWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("P(f(i) + g(i))", true)
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(P(+(f(sys_def_i_complex);g(sys_def_i_complex))))", root.toString())
    }

    @Test
    fun testUPWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("U(m,P(i))")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(U(m;P(sys_def_i_complex)))", root.toString())
    }

    @Test
    fun testSumnInSumnWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("<munderover><mrow><mo>&#x2211;</mo><mi>j</mi><mo>!</mo><mo>-</mo><mi>i</mi><mo>!</mo><mo>+</mo><mn>67</mn><mo>-</mo><munderover><mo>&#x2211;</mo><mrow><mi>i</mi><mo>=</mo><mn>1</mn></mrow><mi>k</mi></munderover><mi>k</mi></mrow><mrow><mi>j</mi><mo>=</mo><mn>1</mn></mrow><mi>n</mi></munderover>")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(S(j;1;n;+(factorial(j);-(factorial(sys_def_i_complex));67;-(S(i;1;k;k)))))", root.toString())
    }

    @Test
    fun testTexShortFractionWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("\\frac{a}{b}f\\left(i\\right)", true)
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(*(/(a;b);f(sys_def_i_complex)))", root.toString())
    }

    @Test
    fun testPowMulAddWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msup><mi>a</mi><mrow><mn>2</mn><mo>+</mo><mi>i</mi></mrow></msup><msup><mi>x</mi><mrow><mi>t</mi><mo>*</mo><mi>j</mi></mrow></msup></math>")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(*(^(a;+(2;sys_def_i_complex));^(x;*(t;j))))", root.toString())
    }

    @Test
    fun testMsubLogWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>x</mi><msub><mi>log</mi><mi>u</mi></msub><mi>i</mi></math>")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(*(x;log(sys_def_i_complex;u)))", root.toString())
    }

    @Test
    fun testFunctionWithWrongNuberOfArgumetsAsVariableWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>S</mi><mn>1</mn><mo>(</mo><mi>i</mi><mo>)</mo></math>")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(*(S1;sys_def_i_complex))", root.toString())
    }

    @Test
    fun testTexNegWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("\\neg i")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(not(sys_def_i_complex))", root.toString())
    }

    @Test
    fun testSumnInSumnWithMstyleWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("<munderover><mrow><mo>&#x2211;</mo><mi>j</mi><mo>!</mo><mo>-</mo><mi>i</mi><mo>!</mo><mo>+</mo><mn>67</mn><mo>-</mo><mstyle displaystyle=\"false\"><munderover><mo>&#x2211;</mo><mrow><mi>i</mi><mo>=</mo><mn>1</mn></mrow><mi>k</mi></munderover></mstyle><mi>k</mi></mrow><mrow><mi>j</mi><mo>=</mo><mn>1</mn></mrow><mi>n</mi></munderover>")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(S(j;1;n;+(factorial(j);-(factorial(sys_def_i_complex));67;-(S(i;1;k;k)))))", root.toString())
    }

    @Test
    fun testFunctionWithDigitsInNameWithComplex() {
        val expressionTreeParser = ExpressionTreeParser("<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>S</mi><mn>1</mn><mo>(</mo><mi>i</mi><mo>,</mo><mi>j</mi><mo>)</mo></math>")
        expressionTreeParser.parse()
        val root = expressionTreeParser.root
        assertEquals("(S1(sys_def_i_complex;j))", root.toString())
    }
}