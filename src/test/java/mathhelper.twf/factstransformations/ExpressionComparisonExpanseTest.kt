package mathhelper.twf.factstransformations

import mathhelper.twf.config.CompiledConfiguration
import org.junit.Test

import kotlin.test.assertEquals

class ExpressionComparisonExpanseTest {
    val expressionComparisonExpanse = ExpressionComparisonExpanse(CompiledConfiguration())

    @Test
    fun expanseGeneratorUnaryBinary() {
        val result = mutableListOf<ExpressionComparison>()
        val expressionComparison = parseFromFactIdentifier("x+y;ec;=;ec;j/k")!! as ExpressionComparison
        assertEquals("(+(x;y));=;(/(j;k))", expressionComparison.toString())
        expressionComparisonExpanse.expanseGenerator(expressionComparison, result, true)
        val resultString = result.joinToString (separator = "\n")
        assertEquals("" +
                "y;=;+(/(j;k);-(x))\n" +
                "x;=;+(/(j;k);-(y))\n" +
                "j;=;*(+(x;y);k)\n" +
                "k;=;/(j;+(x;y))", resultString)
    }

    @Test
    fun expanseGeneratorNoUnexpressedVariable() {
        val result = mutableListOf<ExpressionComparison>()
        val expressionComparison = parseFromFactIdentifier("x+y;ec;=;ec;j/x")!! as ExpressionComparison
        assertEquals("(+(x;y));=;(/(j;x))", expressionComparison.toString())
        expressionComparisonExpanse.expanseGenerator(expressionComparison, result, true)
        val resultString = result.joinToString (separator = "\n")
        assertEquals("" +
                "y;=;+(/(j;x);-(x))\n" +
                "j;=;*(+(x;y);x)", resultString)
    }

    @Test
    fun expanseGeneratorIneguality() {
        val result = mutableListOf<ExpressionComparison>()
        val expressionComparison = parseFromFactIdentifier("-(x+y);ec;<=;ec;j/x")!! as ExpressionComparison
        assertEquals("(+(-(+(x;y))));<=;(/(j;x))", expressionComparison.toString())
        expressionComparisonExpanse.expanseGenerator(expressionComparison, result, true)
        val resultString = result.joinToString (separator = "\n")
        assertEquals("" +
                "y;>=;+(+(-();/(j;x));-(x))\n" +
                "j;<=;*(+(-(+(x;y)));x)", resultString)
    }


    @Test
    fun expanseGeneratorThinkAboutSimmplification() {
        val result = mutableListOf<ExpressionComparison>()
        val expressionComparison = parseFromFactIdentifier("x+y+z;ec;=;ec;j*k")!! as ExpressionComparison
        assertEquals("(+(x;y;z));=;(*(j;k))", expressionComparison.toString())
        expressionComparisonExpanse.expanseGenerator(expressionComparison, result, true)
        val resultString = result.joinToString (separator = "\n")
        assertEquals("" +
                "z;=;+(+(*(j;k);-(x));-(y))\n" +
                "y;=;+(+(*(j;k);-(x));-(z))\n" +
                "z;=;+(+(*(j;k);-(y));-(x))\n" +
                "x;=;+(+(*(j;k);-(y));-(z))\n" +
                "y;=;+(+(*(j;k);-(z));-(x))\n" +
                "x;=;+(+(*(j;k);-(z));-(y))\n" +
                "k;=;/(+(x;y;z);j)\n" +
                "j;=;/(+(x;y;z);k)", resultString)
    }
}