package mathhelper.twf

import com.google.gson.GsonBuilder
import mathhelper.twf.api.getRulePackCodesFromTree
import mathhelper.twf.config.RulePackITR
import mathhelper.twf.config.TaskSetITR
import mathhelper.twf.defaultcontent.defaultrulepacks.DefaultRulePacks
import mathhelper.twf.defaultcontent.toRulePackFrontInput
import mathhelper.twf.defaultcontent.toTaskSetFrontInput
import java.io.FileDescriptor
import java.io.FileWriter
import kotlin.test.assertTrue

/**
 * Throws an [AssertionError] calculated by [lazyMessage] if the [value] is false
 * and runtime assertions have been enabled on the JVM using the *-ea* JVM option.
 */
inline fun assert(value: Boolean, lazyMessage: () -> Any) {
    if (!value) {
        val assertAction = lazyMessage.invoke()
        if (assertAction is String) {
            assertTrue(value, assertAction)
        }
    }
}

inline fun assert(value: Boolean) {
    if (!value) {
        assertTrue(value)
    }
}

fun getGsonWriter() = GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create()

data class RulePackITRArray(
        val rulePacks: MutableList<RulePackITR> = mutableListOf()
)

fun printJsonRulePacksInITR(namespace: String, outputDirectoryPath: String, rulePacks: List<RulePackITR> = DefaultRulePacks.defaultRulePacks) {
    val gsonWriter = getGsonWriter()
    val rulePackArrayITR = RulePackITRArray()
    for (rulePack in rulePacks) {
        rulePackArrayITR.rulePacks.add(rulePack.cloneBesidesMaps(newNamespaceCode = namespace))
    }
    val writer = FileWriter(outputDirectoryPath + namespace + "__allRulePacks.json")
    gsonWriter.toJson(rulePackArrayITR, writer)
    writer.flush()
    writer.close()
}

fun printJsonRulePacksInFrontInput(namespace: String, outputDirectoryPath: String, rulePacks: List<RulePackITR> = DefaultRulePacks.defaultRulePacks) {
    val gsonWriter = getGsonWriter()
    var counter = 10
    for (rulePack in rulePacks) {
        val modifiedRulePack = rulePack.cloneBesidesMaps(newNamespaceCode = namespace).toRulePackFrontInput()

        val writer = FileWriter(outputDirectoryPath + counter.toString() + "__" + rulePack.code + ".json")
        gsonWriter.toJson(modifiedRulePack, writer)
        writer.flush()
        writer.close()
        counter += 1
    }
}

data class TaskSetITRArray(
        val taskSets: MutableList<TaskSetITR> = mutableListOf()
)

fun printJsonTaskSetsInITR(namespace: String, outputDirectoryPath: String, taskSets: List<TaskSetITR>) {
    val gsonWriter = getGsonWriter()
    val taskSetITRArray = TaskSetITRArray()
    for (taskSet in taskSets) {
        taskSetITRArray.taskSets.add(taskSet.cloneBesidesMaps(newNamespaceCode = namespace))
    }
    val writer = FileWriter(outputDirectoryPath + namespace + "__allTaskSets.json")
    gsonWriter.toJson(taskSetITRArray, writer)
    writer.flush()
    writer.close()
}

fun printJsonTaskSetsInFrontInput(namespace: String, outputDirectoryPath: String, taskSets: List<TaskSetITR>) {
    val gsonWriter = getGsonWriter()
    for (taskSet in taskSets) {
        val modifiedTaskSet = taskSet.cloneBesidesMaps(newNamespaceCode = namespace).toTaskSetFrontInput()

        val writer = FileWriter(outputDirectoryPath + taskSet.code + ".json")
        gsonWriter.toJson(modifiedTaskSet, writer)
        writer.flush()
        writer.close()
    }
}


fun printJsonTaskSets(namespace: String, outputDirectoryPath: String, taskSets: List<TaskSetITR>) {
    val gsonWriter = getGsonWriter()
    for (taskSet in taskSets) {
        val modifiedTaskSet = taskSet.cloneBesidesMaps(newNamespaceCode = namespace)

        val writer = FileWriter(outputDirectoryPath + taskSet.code + ".json")
        gsonWriter.toJson(modifiedTaskSet, writer)
        writer.flush()
        writer.close()
    }
}


fun printJsonTaskSetsToConsole(namespace: String, taskSets: List<TaskSetITR>) {
    val gsonWriter = getGsonWriter()
    for (taskSet in taskSets) {
        val modifiedTaskSet = taskSet.cloneBesidesMaps(newNamespaceCode = namespace).toTaskSetFrontInput()

        val writer = FileWriter(FileDescriptor.out)
        gsonWriter.toJson(modifiedTaskSet, writer)
        writer.flush()
        writer.close()
    }
}


data class TaskSetWithRulePacks(
        val taskSet: TaskSetITR,
        val rulePacks: MutableList<RulePackITR> = mutableListOf()
)

fun printTaskSetWithRulePacksInITR(namespace: String, outputDirectoryPath: String, taskSet: TaskSetITR, rulePacks: List<RulePackITR>) {
    val gsonWriter = getGsonWriter()
    val taskSetWithRulePacks = TaskSetWithRulePacks(taskSet.cloneBesidesMaps(newNamespaceCode = namespace))
    val needRulePacksSet = mutableSetOf<String>()
    for (task in taskSet.tasks!!) {
        for (rulePackLink in task.rulePacks!!){
            needRulePacksSet.add(rulePackLink.rulePackCode!!)
        }
    }
    for (rulePack in rulePacks) {
        if (rulePack.code in needRulePacksSet) {
            needRulePacksSet.addAll(getRulePackCodesFromTree(rulePack, DefaultRulePacks.map()))
        }
    }
    for (rulePack in rulePacks) {
        if (rulePack.code in needRulePacksSet) {
            taskSetWithRulePacks.rulePacks.add(rulePack.cloneBesidesMaps(newNamespaceCode = namespace))
        }
    }
    val writer = FileWriter(outputDirectoryPath + namespace + "__" + taskSet.code + "InITR.json")
    gsonWriter.toJson(taskSetWithRulePacks, writer)
    writer.flush()
    writer.close()
}
