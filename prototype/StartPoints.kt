import config.CompiledConfiguration
import expressiontree.ExpressionTreeParser
import factstransformations.TransformationChainParser
import factstransformations.parseFromFactIdentifier
import logs.MessageType
import logs.log
import mainpoints.checkFactsInMathML
import mainpoints.configSeparator
import visualization.brushMathMl
import visualization.dropPerformedBrushing

@JsName("getParsedExpressionByMathML")
fun getParsedExpressionByMathML (mathML: String): String{
    print("Hello kotlin")
    val expressionTreeParser = ExpressionTreeParser(mathML)
    expressionTreeParser.parse()
    val root = expressionTreeParser.root
    return root.toString()
}

@JsName("checkFactsInMathML")
fun checkFactsInMathML(
        brushedMathML: String,
        wellKnownFunctions: String = "+$configSeparator-1$configSeparator-$configSeparator-1$configSeparator*$configSeparator-1$configSeparator/$configSeparator-1", //functions, which null-weight transformations allowed (if no other transformations), split by configSeparator
        expressionTransformationRules: String = "S(i, a, a, f(i))${configSeparator}f(a)${configSeparator}S(i, a, b, f(i))${configSeparator}S(i, a, b-1, f(i)) + f(b)", //function transformation rules, parts split by configSeparator; if it equals " " then expressions will be checked only by testing
        targetFactIdentifier: String = "", //Fact that learner need to prove should be here
        targetVariablesNames: String = "", //Variables expressions for which learner need to deduce, split by configSeparator
        minNumberOfMultipliersInAnswer: String = "", //For factorization tasks
        maxNumberOfDivisionsInAnswer: String = "", //For fraction reducing tasks
        additionalFactsIdentifiers: String = "", ///Identifiers split by configSeparator - task condition facts should be here
        maxExpressionTransformationWeight: String = "1.0",
        unlimitedWellKnownFunctions: String = wellKnownFunctions, //functions, which null-weight transformations allowed with any other transformations, split by configSeparator
        shortErrorDescription: String = "0", //crop parsed steps from error description
        taskContextExpressionTransformationRules: String = "", //for expression transformation rules based on variables
        allowedVariablesNames: String = "" //Variables expressions for which learner need to deduce, split by configSeparator
): String {
    try {
        return mainpoints.checkFactsInMathML(
                brushedMathML = brushedMathML,
                wellKnownFunctions = wellKnownFunctions,
                expressionTransformationRules = expressionTransformationRules,
                targetFactIdentifier = targetFactIdentifier,
                targetVariablesNames = targetVariablesNames,
                additionalFactsIdentifiers = additionalFactsIdentifiers,
                maxExpressionTransformationWeight = maxExpressionTransformationWeight,
                unlimitedWellKnownFunctions = unlimitedWellKnownFunctions,
                minNumberOfMultipliersInAnswer = minNumberOfMultipliersInAnswer,
                maxNumberOfDivisionsInAnswer = maxNumberOfDivisionsInAnswer,
                shortErrorDescription = shortErrorDescription,
                taskContextExpressionTransformationRules = taskContextExpressionTransformationRules,
                allowedVariablesNames = allowedVariablesNames)
    } catch (e: Exception){
        return (e.message ?: "Unknown error") + "\n" +
                log.getLogInPlainText()
    }
}

@JsName("getUserLogInPlainText")
fun getUserLogInPlainText() = log.getLogInPlainText(MessageType.USER)

@JsName("getUserLogInJson")
fun getUserLogInJson() = log.getLogInJson(MessageType.USER)

@JsName("getAllLogInPlainText")
fun getAllLogInPlainText() = log.getLogInPlainText()

@JsName("getAllLogInJson")
fun getAllLogInJson() = log.getLogInJson()

@JsName("encryptSolution")
fun encryptSolution(solution: String) = "${solution.hashCode()}_${solution.length}"

@JsName("encryptResult")
fun encryptResult(result: String, solution: String) = "$result ${solution.hashCode()}_${solution.length}"