let windowWidth;
let windowHeight;
const minWidth = 570;

function onLoad() {
    windowHeight = window.innerHeight;
    windowWidth = window.innerWidth;
    let height = windowHeight - $("#topCup").attr("clientHeight") - $("#bottomCup").attr("clientHeight");
    let width = windowWidth - $("#menu").outerWidth();
    $("#content").hide();
    $("#mainArea").css("height", height + "px");
    $("#content").css("height", height + "px");
    $("#information").css("height", height + "px");
    if (width < minWidth) {
        $("#content").css("width", minWidth + "px");
        $("#content").css("overflow-x", "scroll");
        $("#information").css("width", minWidth + "px");
        $("#information").css("overflow-x", "scroll");
    } else {
        $("#content").css("width", width + "px");
        $("#information").css("width", width + "px");
    }
    $("#taskPicture").css("maxWidth", (width - 80) + "px");

    initMenu();
    initContent();
    initTooltip();
}

function onResize() {
    windowHeight = window.innerHeight;
    windowWidth = window.innerWidth;
    let height = windowHeight - $("#topCup").attr("clientHeight") - $("#bottomCup").attr("clientHeight");
    let width = windowWidth - $("#menu").outerWidth();
    $("#mainArea").css("height", height + "px");
    $("#content").css("height", height + "px");
    $("#information").css("height", height + "px");
    if (width < minWidth) {
        $("#content").css("width", minWidth + "px");
        $("#content").css("overflow-x", "scroll");
        $("#information").css("width", minWidth + "px");
        $("#information").css("overflow-x", "scroll");
    } else {
        $("#content").css("width", width + "px");
        $("#content").css("overflow-x", "hidden");
        $("#information").css("width", width + "px");
        $("#information").css("overflow-x", "hidden");
    }
    $("#content").css("width", width + "px");
    $("#information").css("width", width + "px");
    $("#taskPicture").css("maxWidth", (width - 80) + "px");
}

let editor;
let curTask = 0;
let curItemID = "AboutMain";
let curTopicItem;

function initMenu() {
    const $about = $('<li class="itemLevel1" id="AboutMain" onclick="onAboutItemClick(this)">About</li>');
    $about.css({'font-weight': "bold"});
	let $aboutMenu = $('<li class="emptyItem"><ul  class="menuLevel2" id="About"></ul></li>');
	curTopicItem = $aboutMenu;
    $("#menuLevel1").append($about);
	$("#menuLevel1").append($aboutMenu);
	const $hiEverybody = $('<li class="itemLevel2"><a href="#HiEverybody" text-decoration="none">Hi Everybody</a></li>');
	$hiEverybody.css({'font-weight': "normal"});
	$("#About").append($hiEverybody);
	const $alreadyDeveloped = $('<li class="itemLevel2"><a href="#AlreadyDeveloped">Already Developed</a></li>');
	$alreadyDeveloped.css({'font-weight': "normal"});
	$("#About").append($alreadyDeveloped);
	const $howDoesItWork = $('<li class="itemLevel2"><a href="#HowDoesItWork">How Does It Work</a></li>');
	$howDoesItWork.css({'font-weight': "normal"});
	$("#About").append($howDoesItWork);
	const $existingSolutions = $('<li class="itemLevel2"><a href="#ExistingSolutions">Existing Solutions</a></li>');
	$existingSolutions.css({'font-weight': "normal"});
	$("#About").append($existingSolutions);
	const $whoWeAre = $('<li class="itemLevel2"><a href="#WhoWeAre">Who We Are</a></li>');
	$whoWeAre.css({'font-weight': "normal"});
	$("#About").append($whoWeAre);
//    const $philosophy = $('<li class="itemLevel1" id="Philosophy" onclick="onPhilosophyItemClick(this)">Philosophy</li>');
//    $("#menuLevel1").append($philosophy);
    let $newTopic;
    let $liNewTopic;
    for (key in TOPICS) {
        let iTopic = key - 0;
        $newTopic = $('<li class="itemLevel1" onclick="onTopicItemClick(this)">' + TOPICS[iTopic] + '</li>');
        $newTopic.css({'background-image': 'url("images/hidden.png")'});
        $liNewTopic = $('<li hidden class="emptyItem"><ul  class="menuLevel2" id="topic' + iTopic + '"></ul></li>');
        $("#menuLevel1").append($newTopic);
        $("#menuLevel1").append($liNewTopic);
    }
    const $whatElse = $('<li class="itemLevel1" id=' + (tasks.length - 1) + ' onclick="onWhatElseItemClick(this)">What else</li>');
    $("#menuLevel1").append($whatElse);

    let $newTask;
    for (let j = 0; j < tasks.length - 1; j++) { //last task in WhatElse
        $newTask = $('<li class="itemLevel2" id=' + j + ' onClick="selectTask(this)">' + tasks[j].tagTitle + '</li>');
        $("#topic" + tasks[j].topic).append($newTask);
    }
}

function onAboutItemClick (item){
	item.nextElementSibling.hidden = !item.nextElementSibling.hidden;
    $("#"+curItemID).css({'font-weight': "normal"});
    curItemID = item.id;
    $(item).css({'font-weight': "bold"});

    $("#content").hide();
    $("#information").show();
}

function onWhatElseItemClick (item){
	var aboutItem = document.getElementById("AboutMain");
	aboutItem.nextElementSibling.hidden = true;
    $("#"+curItemID).css({'font-weight': "normal"});
    curItemID = item.id;
    $(item).css({'font-weight': "bold"});

    $("#content").show();
    $("#information").hide();

    selectTaskByNum(item.id);
}

function initTaskPicture() {
	if (tasks[curTask].condition != ""){
        $("#taskPicture").attr("src", "http://latex.codecogs.com/gif.latex? " + tasks[curTask].condition + "");
	} else {
		$("#taskPicture").attr("src", "");
	}
}

function initContent() {
    $("#taskTitle").attr("textContent", tasks[curTask].title);
	initTaskPicture();
    $("#formules").show();

    editor = com.wiris.jsEditor.JsEditor.newInstance({'language': 'en'});
    editor.insertInto(document.getElementById('editorContainer'));
    editor.setMathML(tasks[curTask].task);
    $("#checkResult").attr("textContent", "Solution is not checked");
    $("#checkResult").css({
        'background-color': "#dae4e9",
        'background-image': 'none',
        'color': "#000000",
    });
    $("#editor").show();

    $("#buttons0").show();
    $("#buttons1").show();
	$("#feedback").show();
}

function initTooltip() {
    let $newFormule;
    for (key in tasks[curTask].hint) {
        $newFormule = $('<li><img id="formule' + key + '" src="" class="latex"></li>');
        $("#formules-list").append($newFormule);
        $("#formule" + key).attr("src", "http://latex.codecogs.com/gif.latex? \\LARGE {" + tasks[curTask].hint[key] + "}");
    }
}

function sendRequest (task, result, solutionEncrypted){
    let name = document.getElementById("nameId");
    let surname = document.getElementById("surnameId");
    let group = document.getElementById("groupId");

    const request = new XMLHttpRequest();
    const url = "http://vikto9494.wallst.ru/cgi-bin/hel.pl?name=" + name.value + "&surname=" + surname.value + "&group=" + group.value + "&task=" + task + "&result=" + result + "&solution_encrypted=" + solutionEncrypted;
    request.open('GET', url);
    request.setRequestHeader('Content-Type', 'application/x-www-form-url');
    request.addEventListener("readystatechange", () => {
        if (request.readyState === 4 && request.status === 200) {
            console.log( request.responseText );
        }
    });
    request.send();
}

function encodeURLparam (string){
	return encodeURIComponent( string ).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");
}

function sendStringFromPosition(string, start, ts, description, task, maxPartLen, number){
	console.log("start:"+start+";number:"+number);
	if (start < string.length && number < 3){
		let end = start + maxPartLen;
		if (end > string.length){
			end = string.length;
		}
		let part = string.substring(start, end);
		let encodedPart = encodeURLparam(part);
		const request = new XMLHttpRequest();
		const url = "http://vikto9494.wallst.ru/cgi-bin/hel.pl?ts=" + ts + "&solution=" + encodedPart + "&description=" + description + "&task=" + task;
		request.open('GET', url);
		request.setRequestHeader('Content-Type', 'application/x-www-form-url');
		var handleFunction = function() {
			if (request.readyState === 4){
				request.removeEventListener("readystatechange", handleFunction);
				if (request.status === 200) {
					sendStringFromPosition(string, start + maxPartLen, ts, description, task, maxPartLen, 0);
				} else {
					sendStringFromPosition(string, start, ts, description, task, maxPartLen, number + 1);
				}
			}
	    }
		request.addEventListener("readystatechange", handleFunction);
		request.send();
	} else {
//		alert("Thank you for the problem report.");
	}
}

function sendSolutionAndText (){
	closeForm();
	
	let ts = Date.now();
	let maxPartLen = 1000;
	
	let mathML = editor.getMathML();
	let encodedMathML = encodeURLparam(mathML);
	let description = encodeURLparam(document.getElementById("problem_description").value);
	
    const request = new XMLHttpRequest();
    const url = "http://vikto9494.wallst.ru/cgi-bin/hel.pl?ts=" + ts + "&solution=" + encodedMathML + "&description=" + description + "&task=" + curTask;
    request.open('GET', url);
    request.setRequestHeader('Content-Type', 'application/x-www-form-url');
	var handleFunction = function() {
		if (request.readyState === 4){
			alert("Thank you for the problem report.");
			request.removeEventListener("readystatechange", handleFunction);
			if (request.readyState === 4 && request.status === 200) {
//				alert("Thank you for the problem report.");
			} else if (mathML.length > maxPartLen) {
				sendStringFromPosition(mathML, 0, ts, description, curTask, maxPartLen, 0);
			} else {
				const request = new XMLHttpRequest();
				request.open('GET', url);
				request.setRequestHeader('Content-Type', 'application/x-www-form-url');
				request.send();
//				alert("Thank you for the problem report.");
			}
		}
    }
    request.addEventListener("readystatechange", handleFunction);
    request.send();
}

function checkFactsInMathML(mathML, taskOptions){
    return TWF_JS.checkFactsInMathML(
        mathML,
        taskOptions.wellKnownFunctions,
        taskOptions.expressionTransformationRules,
        taskOptions.targetFactIdentifier,
        taskOptions.targetVariablesNames,
        taskOptions.minNumberOfMultipliersInAnswer,
        taskOptions.maxNumberOfDivisionsInAnswer,
        taskOptions.additionalFactsIdentifiers,
        taskOptions.maxExpressionTransformationWeight,
        taskOptions.unlimitedWellKnownFunctions,
        taskOptions.shortErrorDescription,
        taskOptions.taskContextExpressionTransformationRules,
		taskOptions.allowedVariablesNames,
        taskOptions.maxDistBetweenDiffSteps,
		taskOptions.forbiddenFunctions
    );
}

function checkCalculations1 (){
    let mathML = editor.getMathML()
    let result = checkFactsInMathML(mathML, tasks[curTask].options);
    editor.setMathML(result);
    console.log( "ok" );
    if (result.indexOf("rror") == -1){
        $("#checkResult").attr("textContent", "Solution is correct");
        $("#checkResult").css({
            'background-color': "#2b574d",
            'background-image': 'url("images/correct.gif")',
            'color': "#f2f5f6",
        });
    } else {
        $("#checkResult").attr("textContent", "Solution is unclear");
        $("#checkResult").css({
            'background-color': "#97354e",
            'background-image': 'none',
            'color': "#f2f5f6",
        });
    }
}

function onTopicItemClick (item){
//    curTopicItemID.nextElementSibling.hidden = !curTopicItemID.nextElementSibling.hidden;
	item.nextElementSibling.hidden = !item.nextElementSibling.hidden;
    if (item.nextElementSibling.hidden) {
        $(item).css({'background-image': 'url("images/hidden.png")'});
    } else {
        $(item).css({'background-image': 'url("images/unhidden.png")'});
    }
	var aboutItem = document.getElementById("AboutMain");
	aboutItem.nextElementSibling.hidden = true;
//	curTopicItemID = item;
}

function selectTaskByNum(taskNum){
    curTask = taskNum - 0;

    let t = $("#taskPicture");
    $("#taskTitle").attr("textContent", tasks[curTask].title);
    initTaskPicture();
    let h = $("#taskPicture").attr("clientHeight");
    editor.setMathML(tasks[curTask].task);

    $("#checkResult").attr("textContent", "Solution is not checked");
    $("#checkResult").css({
        'background-color': "#dae4e9",
        'background-image': 'none',
        'color': "#000000",
    });

    let $newFormule;
    $("#formules-list").empty();
    for (key in tasks[curTask].hint) {
        $newFormule = $('<li><img id="formule' + key + '" src="" class="latex"></li>');
        $("#formules-list").append($newFormule);
        $("#formule" + key).attr("src", "http://latex.codecogs.com/gif.latex? \\LARGE {" + tasks[curTask].hint[key] + "}");
    }
}

function toNextTask (taskNum){
    selectTaskByNum(tasks[curTask].nextNum);
}

function selectTask(item) {
    $("#content").show();
    $("#information").hide();
	
	var aboutItem = document.getElementById("AboutMain");
	aboutItem.nextElementSibling.hidden = true;

    $("#"+curItemID).css({'font-weight': "normal"});
    curItemID = item.id;
    $(item).css({'font-weight': "bold"});

    selectTaskByNum(item.id);
}

function copyTextToClipboard(text, message) {
    let textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
        let successful = document.execCommand('copy');
        alert(message);
    } catch (err) {
        alert(err);
    }

    document.body.removeChild(textArea);
}

function showLog (){
    let logInPlainText = TWF_JS.getAllLogInPlainText();
    copyTextToClipboard(logInPlainText, "log copied to clipboard")
}

function setCorrectSolution (){
    editor.setMathML(tasks[curTask].solution);
}

function showFormules() {
    $("#tooltip").show();
}

function hideFormules() {
    $("#tooltip").hide();
}

function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}