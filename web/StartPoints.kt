import config.CompiledConfiguration
import config.FunctionConfiguration
import expressiontree.*
import factstransformations.TransformationChainParser
import factstransformations.parseFromFactIdentifier
import logs.MessageType
import logs.log
import mainpoints.checkFactsInMathML
import mainpoints.configSeparator
import platformdependent.escapeCharacters
import visualization.brushMathMl
import visualization.dropPerformedBrushing

@JsName("getParsedExpressionByMathML")
fun getParsedExpressionByMathML (mathML: String): String{
    print("Hello kotlin")
    val expressionTreeParser = ExpressionTreeParser(mathML)
    expressionTreeParser.parse()
    val root = expressionTreeParser.root
    return root.toString()
}

@JsName("checkFactsInMathML")
fun checkFactsInMathML(
        brushedMathML: String,
        wellKnownFunctions: String = "+$configSeparator-1$configSeparator-$configSeparator-1$configSeparator*$configSeparator-1$configSeparator/$configSeparator-1", //functions, which null-weight transformations allowed, split by configSeparator
        expressionTransformationRules: String = "S(i, a, a, f(i))${configSeparator}f(a)${configSeparator}S(i, a, b, f(i))${configSeparator}S(i, a, b-1, f(i)) + f(b)", //function transformation rules, parts split by configSeparator
        targetFactIdentifier: String = "", //Fact that learner need to prove should be here
        targetVariablesNames: String = "", //Variables expressions for which learner need to deduce, split by configSeparator
        additionalFactsIdentifiers: String = "", ///Identifiers split by configSeparator - task condition facts should be here
        maxExpressionTransformationWeight: String = "1.0"
) = mainpoints.checkFactsInMathML(brushedMathML, wellKnownFunctions, expressionTransformationRules,
        targetFactIdentifier, targetVariablesNames, additionalFactsIdentifiers, maxExpressionTransformationWeight)

@JsName("getUserLogInPlainText")
fun getUserLogInPlainText() = log.getLogInPlainText(MessageType.USER)

@JsName("getUserLogInJson")
fun getUserLogInJson() = log.getLogInJson(MessageType.USER)

@JsName("getAllLogInPlainText")
fun getAllLogInPlainText() = log.getLogInPlainText()

@JsName("getAllLogInJson")
fun getAllLogInJson() = log.getLogInJson()

@JsName("encryptSolution")
fun encryptSolution(solution: String) = "${solution.hashCode()}_${solution.length}"

@JsName("encryptResult")
fun encryptResult(result: String, solution: String) = "$result ${solution.hashCode()}_${solution.length}"


//expressionAPI
@JsName("stringToExpression")
fun stringToExpression(
        string: String,
        scope: String = "",
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = api.stringToExpression(string, scope, functionConfiguration, compiledConfiguration)

@JsName("structureStringToExpression")
fun structureStringToExpression(
        structureString: String
) = api.structureStringToExpression(structureString)

@JsName("expressionToStructureString")
fun expressionToStructureString(
        expressionNode: ExpressionNode
) = api.expressionToStructureString(expressionNode)

@JsName("expressionToString")
fun expressionToString(
        expressionNode: ExpressionNode,
        characterEscapingDepth: Int = 1
) = api.expressionToString(expressionNode, characterEscapingDepth)


//compare expressions without substitutions
fun compareWithoutSubstitutions(
        left: ExpressionNode,
        right: ExpressionNode,
        scope: Set<String> = setOf(""),
        notChangesOnVariablesFunction: Set<String> = setOf("+", "-", "*", "/", "^"),
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(scope, notChangesOnVariablesFunction),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = api.compareWithoutSubstitutions(left, right, scope, notChangesOnVariablesFunction, functionConfiguration, compiledConfiguration)


//substitutions
@JsName("expressionSubstitutionFromStrings")
fun expressionSubstitutionFromStrings(
        left: String,
        right: String,
        scope: String = "",
        basedOnTaskContext: Boolean = false,
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = api.expressionSubstitutionFromStrings(left, right, scope, basedOnTaskContext, functionConfiguration, compiledConfiguration)

@JsName("findSubstitutionPlacesInExpression")
fun findSubstitutionPlacesInExpression(
        expression: ExpressionNode,
        substitution: ExpressionSubstitution
) = api.findSubstitutionPlacesInExpression(expression, substitution)

@JsName("applySubstitution")
fun applySubstitution(
        expression: ExpressionNode,
        substitution: ExpressionSubstitution,
        substitutionPlaces: List<SubstitutionPlace> //containsPointersOnExpressionPlaces
) = api.applySubstitution(expression, substitution, substitutionPlaces)

@JsName("generateTask")
fun generateTask(
        expressionSubstitutions: List<ExpressionSubstitution>, //if substitution can be applied in both directions, it has to be specified twice
        stepsCount: Int,
        originalExpressions: List<ExpressionNode> //it's better to set original expression manually, to choose correct number of variables
) = api.generateTask(expressionSubstitutions, stepsCount, originalExpressions)


//string api
@JsName("compareWithoutSubstitutions")
fun compareWithoutSubstitutions(
        left: String,
        right: String,
        scope: String = "",
        notChangesOnVariablesFunction: String = "+;-;*;/;^",
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet(),
                notChangesOnVariablesInComparisonFunctionFilter = notChangesOnVariablesFunction.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = api.compareWithoutSubstitutions(left, right, scope, notChangesOnVariablesFunction, functionConfiguration, compiledConfiguration)

@JsName("findSubstitutionPlacesCoordinatesInExpressionJSON")
fun findSubstitutionPlacesCoordinatesInExpressionJSON(
        expression: String,
        substitutionLeft: String,
        substitutionRight: String,
        scope: String = "",
        basedOnTaskContext: Boolean = false,
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = api.findSubstitutionPlacesCoordinatesInExpressionJSON(expression, substitutionLeft, substitutionRight, scope, basedOnTaskContext, functionConfiguration, compiledConfiguration)

@JsName("applyExpressionBySubstitutionPlaceCoordinates")
fun applyExpressionBySubstitutionPlaceCoordinates(
        expression: String,
        substitutionLeft: String,
        substitutionRight: String,
        parentStartPosition: Int,
        parentEndPosition: Int,
        startPosition: Int,
        endPosition: Int,
        scope: String = "",
        basedOnTaskContext: Boolean = false,
        characterEscapingDepth: Int = 1,
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = api.applyExpressionBySubstitutionPlaceCoordinates(expression, substitutionLeft, substitutionRight, parentStartPosition, parentEndPosition, startPosition, endPosition, scope, basedOnTaskContext, characterEscapingDepth, functionConfiguration, compiledConfiguration)

@JsName("generateTaskInJSON")
fun generateTaskInJSON(
        expressionSubstitutions: String, //';' separated equalities
        stepsCount: Int,
        originalExpressions: String, //';' separated
        scope: String = "", //';' separated
        characterEscapingDepth: Int = 1,
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = api.generateTaskInJSON(expressionSubstitutions, stepsCount, originalExpressions, scope, characterEscapingDepth, functionConfiguration, compiledConfiguration)

