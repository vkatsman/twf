const TYPES = {
    0: 'undefined',
    1: 'Equation',
    2: 'Proving an identity',
    3: 'Factorization',
    4: 'Computation',
}

const TOPICS = {
    0: 'Trigonometry',
    1: 'Polynoms',
    2: 'Physics',
    3: 'Equations',
    4: 'Differentiation',
}

class Task {
    constructor(num, nextNum, tagTitle, title, condition, topic, task, solution, hint, options){
        this.num = num;
        this.nextNum = nextNum;
        this.tagTitle = tagTitle || 'no task';
		this.title = title || 'no task';
        this.condition = condition || 0;
        this.topic = topic || 0;
        this.task = task || 'no task';
        this.solution = solution || 'no specified solution';
        this.hint = hint || 'no hint';
        this.options = options;
    }
}

let tasks = [];

trigonometryExpressionTransformationRules = "sin(x)^2;;;1 - cos(x)^2;;;cos(x)^2;;;1 - sin(x)^2;;;cos(x)^2+sin(x)^2;;;1;;;tg(x);;;sin(x)/cos(x);;;ctg(x);;;cos(x)/sin(x);;;ctg(x)tg(x);;;1;;;tg(x)^2+1;;;1/cos(x)^2;;;ctg(x)^2+1;;;1/sin(x)^2;;;sin(x+y);;;sin(x)cos(y)+sin(y)cos(x);;;sin(x-y);;;sin(x)cos(y)-sin(y)cos(x);;;cos(x+y);;;cos(x)cos(y)-sin(x)sin(y);;;cos(x-y);;;cos(x)cos(y)+sin(x)sin(y);;;tg(x+y);;;(tg(x)+tg(y))/(1-tg(x)tg(y));;;tg(x-y);;;(tg(x)-tg(y))/(1+tg(x)tg(y));;;ctg(x+y);;;(ctg(x)ctg(y)-1)/(ctg(x)+ctg(y));;;ctg(x-y);;;(ctg(x)ctg(y)+1)/(ctg(x)-ctg(y));;;sin(2*x);;;2*sin(x)cos(x);;;sin(2*x);;;2*tg(x)/(1+(tg(x))^2);;;cos(2*x);;;cos(x)^2 - sin(x)^2;;;cos(2*x);;;2*cos(x)^2 - 1;;;cos(2*x);;;1 - 2*sin(x)^2;;;cos(2*x);;;(1-(tg(x))^2)/(1+(tg(x))^2);;;sin(x)^2;;;(1 - cos(2*x))/2;;;cos(x)^2;;;(1 + cos(2*x))/2;;;sin(x)+sin(y);;;2*sin((x+y)/2)*cos((x-y)/2);;;sin(x)-sin(y);;;2*sin((x-y)/2)*cos((x+y)/2);;;cos(x)+cos(y);;;2*cos((x+y)/2)*cos((x-y)/2);;;cos(x)-cos(y);;;2*sin((x+y)/2)*sin((x-y)/2);;;sin(x)sin(y);;;(cos(x-y)-cos(x+y))/2;;;sin(x)cos(y);;;(sin(x-y)+sin(x+y))/2;;;cos(x)cos(y);;;(cos(x-y)+cos(x+y))/2;;;sin(0);;;0;;;sin(pi/2);;;1;;;cos(0);;;1;;;cos(pi/2);;;0;;;sin(pi/6);;;1/2;;;sin(pi/4);;;2^0.5/2;;;sin(pi/3);;;3^0.5/2;;;cos(pi/3);;;1/2;;;cos(pi/4);;;2^0.5/2;;;cos(pi/6);;;3^0.5/2;;;cos(pi/6);;;3^0.5/2;;;cos(-x);;;cos(x);;;sin(-x);;;-sin(x);;;sin(pi-x);;;sin(x);;;cos(pi-x);;;-cos(x);;;sin(x+2*pi);;;sin(x);;;cos(x+2*pi);;;cos(x);;;cos(pi/2-x);;;sin(x);;;sin(pi/2-x);;;cos(x)";
noExpressionTransformationRules = " ";
ommLawExpressionTransformationRules = "U;;;I*R;;;A;;;U*q;;;R;;;p*l/S";
pressureExpressionTransformationRules = "P;;;p*g*h;;;P_o;;;P_m;;;p_o*g*h_o;;;p_m*g*h_m;;;P_m;;;p_m*g*h_m;;;P_o;;;p_o*g*h_o;;;p_oil;;;p_o;;;p_mercury;;;p_m;;;h_mercury;;;h_m";
quantumExpressionTransformationRules = "E;;;h*v;;;v;;;c/l;;;lambda;;;l;;;Plank_constant;;;h;;;light_speed;;;c";
wellKnownFunctionsAllArithmetic = ";;;0;;;;;;1;;;+;;;-1;;;-;;;-1;;;*;;;-1;;;/;;;-1;;;^;;;-1";
wellKnownFunctionsAllArithmeticWithoutDegree = ";;;0;;;;;;1;;;+;;;-1;;;-;;;-1;;;*;;;-1;;;/;;;-1";

tasks[0] = new Task(
    num = 0,
    nextNum = 1,
    'Task 1. Lite prove',
	'Task 1. Prove that',
	'\\LARGE {\\textbf{Task 1. Prove that  }3+4\cos2x+\cos4x = 8\cos^4x}',
    0,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo><mo>=</mo><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>=</mo><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>)</mo><mo>+</mo><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><msup><mo>)</mo><mn>2</mn></msup><mo>-</mo><mn>1</mn><mo>)</mo><mo>=</mo><mn>8</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>4</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></math>",
    { 1: "cos(2*x) = 2*(cos(x))^2 - 1" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: trigonometryExpressionTransformationRules,
        targetFactIdentifier: "EXPRESSION_COMPARISON{(+(3;*(4;cos(*(2;x)));cos(*(4;x))))}{=}{(*(8;^(cos(x);4)))}",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmeticWithoutDegree,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);

tasks[1] = new Task(
    num = 1,
    nextNum = 2,
    'Task 2. Lite prove',
	'Task 2. Prove that',
    '\\LARGE {\\textbf{Task 2. Prove that  }\cosx+\cos2x+\cos6x+\cos7x = 4\cos(\\frac{x}{2})\cos(\\frac{5x}{2})\cos(4x)}',
    0,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>6</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>7</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo><mo>=</mo><mo>&#xA0;</mo><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi>x</mi><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mfrac><mrow><mn>5</mn><mo>*</mo><mi>x</mi></mrow><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>6</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>7</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>=</mo><mspace linebreak=\"newline\"/><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>7</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>6</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mo>=</mo><mo>&#xA0;</mo><mspace linebreak=\"newline\"/><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>3</mn><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mo>&#xA0;</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>=</mo><mo>&#xA0;</mo><mspace linebreak=\"newline\"/><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>3</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>+</mo><mo>&#xA0;</mo><mi>cos</mi><mo>&#xA0;</mo><mo>(</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mo>=</mo><mspace linebreak=\"newline\"/><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mn>2</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mn>5</mn><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>x</mi><mo>&#xA0;</mo><mo>/</mo><mo>&#xA0;</mo><mn>2</mn><mo>)</mo><mo>&#xA0;</mo><mo>*</mo><mo>&#xA0;</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>&#xA0;</mo><mo>/</mo><mo>&#xA0;</mo><mn>2</mn><mo>)</mo><mo>&#xA0;</mo><mo>=</mo><mo>&#xA0;</mo><mspace linebreak=\"newline\"/><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi>x</mi><mn>2</mn></mfrac><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mrow><mn>5</mn><mo>*</mo><mi>x</mi></mrow><mn>2</mn></mfrac><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></math>",
    { 1: "cos(x) + cos(y) = 2*cos((x+y)/2)*cos((x-y)/2)" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: trigonometryExpressionTransformationRules,
        targetFactIdentifier: "EXPRESSION_COMPARISON{(+(cos(x);cos(*(2;x));cos(*(6;x));cos(*(7;x))))}{=}{(*(4;cos(/(x;2));cos(/(*(5;x);2));cos(*(4;x))))}",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmeticWithoutDegree,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);

tasks[2] = new Task(
    num = 2,
    nextNum = 3,
    'Task 3. Lite prove',
	'Task 3. Prove that',
    '\\LARGE {\\textbf{Task 3. Prove that  }\\frac{\sinx}{1+\cosx}+\\frac{1+\cosx}{\sinx} = \\frac{2}{\sinx}}',
    0,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo><mo>=</mo><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo>=</mo><mfrac><mrow><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow><mo>^</mo><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mn>2</mn><mo>+</mo><mn>2</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mo>(</mo><mn>1</mn><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>=</mo><mo>&#xA0;</mo><mfrac><mn>2</mn><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
    { 1: "(sin(x))^2 + (cos(x))^2 = 1" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: trigonometryExpressionTransformationRules,
        targetFactIdentifier: "EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{=}{(/(2;sin(x)))}",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmeticWithoutDegree,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);

tasks[3] = new Task(
    num = 3,
    nextNum = 0,
    'Task 4. Prove',
	'Task 4. Prove that',
    '\\LARGE {\\textbf{Task 4. Prove that  }4\cos(\\frac{\pi}{6}-x)\sin(\\frac{\pi}{3}-x) = \\frac{\sin3x}{\sinx}}',
    0,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>6</mn></mfrac><mo>-</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>3</mn></mfrac><mo>-</mo><mi>x</mi><mo>)</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo><mo>=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mn>3</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>4</mn><mo>*</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>6</mn></mfrac><mo>-</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>3</mn></mfrac><mo>-</mo><mi>x</mi><mo>)</mo><mo>=</mo><mrow><mn>2</mn><mo>*</mo><mo>(</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>6</mn></mfrac><mo>)</mo><mo>)</mo><mo>=</mo><mn>2</mn><mo>*</mo><mo>(</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>0</mn><mo>.</mo><mn>5</mn><mo>)</mo></mrow><mo>=</mo><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>0</mn><mo>.</mo><mn>5</mn><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mn>3</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mn>3</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac></math>",
    { 1: "sin(x)*cos(y) = (sin(x+y)+sin(x-y))/2", 2: "sin(pi/2) = 0.5", 3: "sin(pi/2-x) = cos(x)" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: trigonometryExpressionTransformationRules,
        targetFactIdentifier: "EXPRESSION_COMPARISON{(*(4;cos(+(/(pi;6);-(x)));sin(+(/(pi;3);-(x)))))}{=}{(/(sin(*(3;x));sin(x)))}",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmeticWithoutDegree,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[4] = new Task(
    num = 4,
    nextNum = 0,
    'Task 5. Simplification',
	'Task 5. Compute',
    '\\LARGE {\\textbf{Task 5. Compute  }\sin4x+\cos4x*\ctg2x\\textbf{  if  }\{tg}2x = 4}',
    0,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>4</mn><mo>]</mo><mspace linebreak=\"newline\"/><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>4</mn><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mn>1</mn></mfrac><mo>=</mo><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mfrac><mn>1</mn><mrow><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>tg</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>-</mo><mn>1</mn><mo>=</mo><mn>2</mn><mo>*</mo><mfrac><mn>1</mn><mrow><msup><mi>tg</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>1</mn></mrow></mfrac><mo>-</mo><mn>1</mn><mo>=</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mi>sin</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>=</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>/</mo><mn>4</mn><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>/</mo><mn>4</mn><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>+</mo><mi>t</mi><msup><mi>g</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>/</mo><mn>4</mn><mo>=</mo><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mn>4</mn></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>+</mo><mfrac><mrow><mn>1</mn><mo>-</mo><msup><mn>4</mn><mn>2</mn></msup></mrow><mrow><mn>1</mn><mo>+</mo><msup><mn>4</mn><mn>2</mn></msup></mrow></mfrac><mo>/</mo><mn>4</mn><mo>=</mo><mn>1</mn><mo>/</mo><mn>4</mn></math>",
    { 1: "ctg(x) = 1 / tg(x)", 2: "sin(2x) = 2*tg(x)/(1+(tg(x))^2)", 3: "cos(2x) = (1-(tg(x))^2)/(1+(tg(x))^2)" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: trigonometryExpressionTransformationRules,
        targetFactIdentifier: "EXPRESSION_COMPARISON{(+(sin(*(4;x));*(cos(*(4;x));ctg(*(2;x)))))}{=}{(0.25)}",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: "EXPRESSION_COMPARISON{(tg(*(2;x)))}{=}{(4)}",
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmeticWithoutDegree,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[5] = new Task(
    num = 5,
    nextNum = 0,
    'Task 6. Prove',
	'Task 6. Prove that',
    '\\LARGE {\\textbf{Task 6. Prove that  }\{tg}4x-\\frac{1}{\cos4x} = \\frac{\sin2x-\cos2x}{\sin2x+\cos2x}}',
    0,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>t</mi><mi>g</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mfrac><mn>1</mn><mrow><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo>=</mo><mo>&#xA0;</mo><mo>.</mo><mo>.</mo><mo>.</mo><mo>=</mo><mfrac><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>t</mi><mi>g</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mfrac><mn>1</mn><mrow><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo>=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mn>2</mn><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn></mrow><mrow><mi>cos</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>&#xA0;</mo><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn></mrow><mrow><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mn>1</mn></mrow><mrow><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>=</mo><mspace linebreak=\"newline\"/><mfrac><mrow><mn>2</mn><mo>*</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow><mrow><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><msup><mi>sin</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>-</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></mfrac></math>",
    { 1: "tg(x) = sin(x) / cos(x)", 2: "sin(2*x) = 2*sin(x)*cos(x)", 3: "cos(2*x) = (cos(x))^2 - (sin(x))^2", 4: "(sin(x))^2 + (cos(x))^2 = 1" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: trigonometryExpressionTransformationRules,
        targetFactIdentifier: "EXPRESSION_COMPARISON{(+(tg(*(4;x));-(/(1;cos(*(4;x))))))}{=}{(/(+(sin(*(2;x));-(cos(*(2;x))));+(sin(*(2;x));cos(*(2;x)))))}",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmeticWithoutDegree,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[6] = new Task(
    num = 6,
    nextNum = 0,
    'Task 7. Hard prove',
	'Task 7. Prove that',
    '\\LARGE {\\textbf{Task 7. Prove that  }\ctg\\frac{x}{2}+\ctg\\frac{y}{2}+\ctg\\frac{z}{2} = \ctg\\frac{x}{2}\ctg\\frac{y}{2}\ctg\\frac{z}{2}\\textbf{  if  }x + y + z = pi}',
    0,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>]</mo><mspace linebreak=\"newline\"/><mi>c</mi><mi>t</mi><mi>g</mi><mfenced><mfrac><mi>x</mi><mn>2</mn></mfrac></mfenced><mo>+</mo><mi>c</mi><mi>t</mi><mi>g</mi><mfenced><mfrac><mi>y</mi><mn>2</mn></mfrac></mfenced><mo>+</mo><mi>c</mi><mi>t</mi><mi>g</mi><mfenced><mfrac><mi>z</mi><mn>2</mn></mfrac></mfenced><mo>&#xA0;</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo><mo>=</mo><mo>&#xA0;</mo><mi>c</mi><mi>t</mi><mi>g</mi><mfenced><mfrac><mi>x</mi><mn>2</mn></mfrac></mfenced><mi>c</mi><mi>t</mi><mi>g</mi><mfenced><mfrac><mi>y</mi><mn>2</mn></mfrac></mfenced><mi>c</mi><mi>t</mi><mi>g</mi><mfenced><mfrac><mi>z</mi><mn>2</mn></mfrac></mfenced></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>=</mo><mo>&gt;</mo><mo>(</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>+</mo><mi>z</mi><mo>)</mo><mo>-</mo><mi>z</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi><mo>=</mo><mo>&gt;</mo><mi>x</mi><mo>+</mo><mi>y</mi><mo>=</mo><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi><mo>=</mo><mo>&gt;</mo><mfrac><mrow><mi mathvariant=\"normal\">x</mi><mo>+</mo><mi mathvariant=\"normal\">y</mi></mrow><mn>2</mn></mfrac><mo>=</mo><mfrac><mrow><mi mathvariant=\"normal\">&#x3C0;</mi><mo>-</mo><mi mathvariant=\"normal\">z</mi></mrow><mn>2</mn></mfrac><mo>=</mo><mo>&gt;</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>=</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>=</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>=</mo><mo>&gt;</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>=</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>=</mo><mo>&gt;</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>=</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>]</mo><mspace linebreak=\"newline\"/><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>ctg</mi><mfenced><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac></mfenced><mo>+</mo><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>=</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>=</mo><mspace linebreak=\"newline\"/><mo>=</mo><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>=</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>+</mo><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac><mo>=</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mo>=</mo><mspace linebreak=\"newline\"/><mo>=</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">&#x3C0;</mi><mn>2</mn></mfrac><mo>-</mo><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mo>=</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>+</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mo>=</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo><mfenced><mfrac><mrow><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>cos</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac><mo>)</mo><mi>sin</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></mrow></mfrac></mfenced><mo>=</mo><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">x</mi><mn>2</mn></mfrac><mo>)</mo><mi>ctg</mi><mfenced><mfrac><mi mathvariant=\"normal\">y</mi><mn>2</mn></mfrac></mfenced><mi>ctg</mi><mo>(</mo><mfrac><mi mathvariant=\"normal\">z</mi><mn>2</mn></mfrac><mo>)</mo></math>",
    { 1: "sin(x+y) = sin(x)cos(y) + sin(y)cos(x)", 2: "sin(pi/2-x) = cos(x)", 3: "cos(x+y) = cos(x)cos(y) - sin(y)sin(x)", 4: "ctg(x)=cos(x) / sin(x)" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: trigonometryExpressionTransformationRules,
        targetFactIdentifier: "EXPRESSION_COMPARISON{(+(ctg(/(x;2));ctg(/(y;2));ctg(/(z;2))))}{=}{(*(ctg(/(x;2));ctg(/(y;2));ctg(/(z;2))))}",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: "EXPRESSION_COMPARISON{(+(x;y;z))}{=}{(pi)}",
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmeticWithoutDegree,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[7] = new Task(
    num = 7,
    nextNum = 0,
    'Task 1. Lite factorization',
	'Task 1. Factorize',
    '\\LARGE {\\textbf{Task 1. Factorize  }x^2+3x+2}',
    1,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>3</mn><mi>x</mi><mo>+</mo><mn>2</mn><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>3</mn><mi>x</mi><mo>+</mo><mn>2</mn><mo>=</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>2</mn><mo>)</mo></math>",
    { 1: "Viete formula" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: noExpressionTransformationRules,
        targetFactIdentifier: "(+(^(x;2);*(3;x);2))",
        targetVariablesNames: 'x',
        minNumberOfMultipliersInAnswer: '2',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmetic,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[8] = new Task(
    num = 8,
    nextNum = 0,
    'Task 2. Reduce fraction',
	'Task 2. Reduce fraction',
    '\\LARGE {\\textbf{Task 2. Reduce fraction  }\\frac{x^4+x^2+1}{x^2-x+1}}',
    1,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><msup><mi>x</mi><mn>4</mn></msup><mo>+</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>1</mn></mrow><mrow><msup><mi>x</mi><mn>2</mn></msup><mo>-</mo><mi>x</mi><mo>+</mo><mn>1</mn></mrow></mfrac><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><msup><mi>x</mi><mn>4</mn></msup><mo>+</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>1</mn></mrow><mrow><msup><mi>x</mi><mn>2</mn></msup><mo>-</mo><mi>x</mi><mo>+</mo><mn>1</mn></mrow></mfrac><mo>=</mo><mfrac><mstyle displaystyle=\"true\"><msup><mi>x</mi><mn>4</mn></msup><mo>+</mo><mn>2</mn><mo>*</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>1</mn><mo>-</mo><msup><mi>x</mi><mn>2</mn></msup></mstyle><mstyle displaystyle=\"true\"><msup><mi>x</mi><mn>2</mn></msup><mo>-</mo><mi>x</mi><mo>+</mo><mn>1</mn></mstyle></mfrac><mo>=</mo><mfrac><mstyle displaystyle=\"true\"><msup><mrow><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>1</mn><mo>)</mo></mrow><mn>2</mn></msup><mo>-</mo><msup><mi>x</mi><mn>2</mn></msup></mstyle><mstyle displaystyle=\"true\"><msup><mi>x</mi><mn>2</mn></msup><mo>-</mo><mi>x</mi><mo>+</mo><mn>1</mn></mstyle></mfrac><mo>=</mo><mfrac><mstyle displaystyle=\"true\"><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>-</mo><mi>x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mi>x</mi><mo>+</mo><mn>1</mn><mo>)</mo></mstyle><mstyle displaystyle=\"true\"><msup><mi>x</mi><mn>2</mn></msup><mo>-</mo><mi>x</mi><mo>+</mo><mn>1</mn></mstyle></mfrac><mo>=</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mi>x</mi><mo>+</mo><mn>1</mn></math>",
    { 1: "x^2 - y^2 = (x-y)(x+y)", 2: "(x+y)^2 = x^2 + 2xy + y^2" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: noExpressionTransformationRules,
        targetFactIdentifier: "(/(+(^(x;4);^(x;2);1);+(^(x;2);-(x);1)))",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '0',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmetic,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[9] = new Task(
    num = 9,
    nextNum = 0,
    'Task 3. Factorization',
	'Task 3. Factorize',
    '\\LARGE {\\textbf{Task 3. Factorize  }(x+1)(x+2)(x+3)(x+4) - 35}',
    1,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>(</mo><mi>x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>2</mn><mo>)</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>3</mn><mo>)</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>4</mn><mo>)</mo><mo>-</mo><mn>35</mn><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>(</mo><mi>x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>2</mn><mo>)</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>3</mn><mo>)</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>4</mn><mo>)</mo><mo>-</mo><mn>35</mn><mo>=</mo><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>+</mo><mn>4</mn><mo>)</mo><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>+</mo><mn>4</mn><mo>+</mo><mn>2</mn><mo>)</mo><mo>-</mo><mn>35</mn><mo>=</mo><msup><mrow><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>+</mo><mn>4</mn><mo>)</mo></mrow><mn>2</mn></msup><mo>+</mo><mn>2</mn><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>+</mo><mn>4</mn><mo>)</mo><mo>-</mo><mn>35</mn><mo>=</mo><msup><mrow><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>+</mo><mn>4</mn><mo>)</mo></mrow><mn>2</mn></msup><mo>+</mo><mn>2</mn><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>+</mo><mn>4</mn><mo>)</mo><mo>+</mo><mn>1</mn><mo>-</mo><msup><mn>6</mn><mn>2</mn></msup><mo>=</mo><msup><mrow><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>+</mo><mn>4</mn><mo>+</mo><mn>1</mn><mo>)</mo></mrow><mn>2</mn></msup><mo>-</mo><msup><mn>6</mn><mn>2</mn></msup><mo>=</mo><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>+</mo><mn>5</mn><mo>+</mo><mn>6</mn><mo>)</mo><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>+</mo><mn>5</mn><mo>-</mo><mn>6</mn><mo>)</mo><mo>=</mo><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>+</mo><mn>11</mn><mo>)</mo><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>5</mn><mi>x</mi><mo>-</mo><mn>1</mn><mo>)</mo></math>",
    { 1: "x^2 - y^2 = (x-y)(x+y)", 2: "(x+y)^2 = x^2 + 2xy + y^2" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: noExpressionTransformationRules,
        targetFactIdentifier: "(+(*(+(x;1);+(x;2);+(x;3);+(x;4));-(35)))",
        targetVariablesNames: 'x',
        minNumberOfMultipliersInAnswer: '2',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmetic,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[10] = new Task(
    num = 7,
    nextNum = 0,
    'Task 4. Factorization',
	'Task 4. Factorize',
    '\\LARGE {\\textbf{Task 4. Factorize  }x^3+6x^2+11x+6}',
    1,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msup><mi>x</mi><mn>3</mn></msup><mo>+</mo><mn>6</mn><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>11</mn><mi>x</mi><mo>+</mo><mn>6</mn><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msup><mi>x</mi><mn>3</mn></msup><mo>+</mo><mn>6</mn><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>11</mn><mi>x</mi><mo>+</mo><mn>6</mn><mo>=</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>2</mn><mo>)</mo><mo>(</mo><mi>x</mi><mo>+</mo><mn>3</mn><mo>)</mo></math>",
    { 1: "Viete formula for 3rd degree" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: noExpressionTransformationRules,
        targetFactIdentifier: "(+(^(x;3);*(6;^(x;2));*(11;x);6))",
        targetVariablesNames: 'x',
        minNumberOfMultipliersInAnswer: '2',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmetic,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[11] = new Task(
    num = 11,
    nextNum = 0,
    'Task 1. Ohm\'s law',
	'Task 1. A voltage U is applied to a conductor of length l with a resistivity p and cross-sectional area S. Determine the current strength I.',
    '\\LARGE {\\textbf{Task 1. A voltage }U\\textbf{ is applied to a conductor of length }l} \\linebreak \\LARGE {\\textbf{     with a resistivity }p\\textbf{ and cross-sectional area }S\\textbf{.}} \\\\* \\LARGE {\\textbf{       Determine the current strength }I\\textbf{.}}',
    2,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>U</mi><mo>=</mo><mi>U</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>l</mi><mo>=</mo><mi>l</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>p</mi><mo>=</mo><mi>p</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>S</mi><mo>=</mo><mi>S</mi><mo>]</mo><mspace linebreak=\"newline\"/><mi mathvariant=\"normal\">I</mi><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>U</mi><mo>=</mo><mi>U</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>l</mi><mo>=</mo><mi>l</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>p</mi><mo>=</mo><mi>p</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>S</mi><mo>=</mo><mi>S</mi><mo>]</mo><mspace linebreak=\"newline\"/><mi mathvariant=\"normal\">I</mi><mo>=</mo><mi>U</mi><mo>/</mo><mi>R</mi><mo>=</mo><mfrac><mrow><mi>U</mi><mo>*</mo><mi>S</mi></mrow><mrow><mi>l</mi><mo>*</mo><mi>p</mi></mrow></mfrac></math>",
    { 1: "U = I*R", 2: "A = U*q", 3: "R = p*l/S" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: noExpressionTransformationRules,
        targetFactIdentifier: "",
        targetVariablesNames: 'I',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmeticWithoutDegree,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: ommLawExpressionTransformationRules,
		allowedVariablesNames: "U;;;S;;;l;;;p",
    },
);


tasks[12] = new Task(
    num = 12,
    nextNum = 0,
    'Task 2. Pressure',
	'Task 2. What height h_o should have a column of oil in order to balance a column of mercury h_m in communicating vessels? Oil density is p_o, mercury density is p_m',
    '\\LARGE {\\textbf{Task 2. What height h\\_o should have a column of oil in order to balance}} \\linebreak \\LARGE {\\textbf{     a column of mercury h\\_m in communicating vessels? }} \\linebreak \\LARGE {\\textbf{      Oil density is p\\_o, mercury density is p\\_m. }}',
    2,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>p</mi><mi>_</mi><mi>m</mi><mi>e</mi><mi>r</mi><mi>c</mi><mi>u</mi><mi>r</mi><mi>y</mi><mo>=</mo><mi>p</mi><mi>_</mi><mi>m</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>p</mi><mi>_</mi><mi>o</mi><mi>i</mi><mi>l</mi><mo>=</mo><mi>p</mi><mi>_</mi><mi>o</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>h</mi><mi>_</mi><mi>m</mi><mi>e</mi><mi>r</mi><mi>c</mi><mi>u</mi><mi>r</mi><mi>y</mi><mo>=</mo><mi>h</mi><mi>_</mi><mi>m</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>.</mo><mo>.</mo><mo>.</mo><mspace linebreak=\"newline\"/><mi>h</mi><mi>_</mi><mi>o</mi><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>p</mi><mi>_</mi><mi>m</mi><mi>e</mi><mi>r</mi><mi>c</mi><mi>u</mi><mi>r</mi><mi>y</mi><mo>=</mo><mi>p</mi><mi>_</mi><mi>m</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>p</mi><mi>_</mi><mi>o</mi><mi>i</mi><mi>l</mi><mo>=</mo><mi>p</mi><mi>_</mi><mi>o</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>h</mi><mi>_</mi><mi>m</mi><mi>e</mi><mi>r</mi><mi>c</mi><mi>u</mi><mi>r</mi><mi>y</mi><mo>=</mo><mi>h</mi><mi>_</mi><mi>m</mi><mo>]</mo><mspace linebreak=\"newline\"/><mi>P</mi><mi>_</mi><mi>o</mi><mo>=</mo><mi>P</mi><mi>_</mi><mi>m</mi><mspace linebreak=\"newline\"/><mi>h</mi><mi>_</mi><mi>o</mi><mo>*</mo><mi>g</mi><mo>*</mo><mi>p</mi><mi>_</mi><mi>o</mi><mo>&#xA0;</mo><mo>=</mo><mo>&#xA0;</mo><mi>h</mi><mi>_</mi><mi>m</mi><mo>*</mo><mi>g</mi><mo>*</mo><mi>p</mi><mi>_</mi><mi>m</mi><mspace linebreak=\"newline\"/><mi>h</mi><mi>_</mi><mi>o</mi><mo>=</mo><mfrac><mrow><mo>&#xA0;</mo><mi>h</mi><mi>_</mi><mi>m</mi><mo>*</mo><mi>g</mi><mo>*</mo><mi>p</mi><mi>_</mi><mi>m</mi></mrow><mrow><mi>g</mi><mo>*</mo><mi>p</mi><mi>_</mi><mi>o</mi></mrow></mfrac><mspace linebreak=\"newline\"/><mi>h</mi><mi>_</mi><mi>o</mi><mo>=</mo><mfrac><mrow><mo>&#xA0;</mo><mi>h_m</mi><mo>*</mo><mi>p_m</mi></mrow><mi>p_o</mi></mfrac></math>",
    { 1: "P = p*g*h" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: noExpressionTransformationRules,
        targetFactIdentifier: "",
        targetVariablesNames: 'h_o',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmeticWithoutDegree,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: pressureExpressionTransformationRules,
		allowedVariablesNames: "p_m;;;p_o;;;h_m",
    },
);


tasks[13] = new Task(
    num = 13,
    nextNum = 0,
    'Task 3. Quantum',
	'Task 3. The atom emits light wavelength l during transition from one energy state to another. How much has the energy of the atom decreased?',
    '\\LARGE {\\textbf{Task 3. The atom emits light wavelength l during transition from one energy state}} \\linebreak \\LARGE {\\textbf{     to another. How much has the energy of the atom decreased?}}',
    2,
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>l</mi><mi>a</mi><mi>m</mi><mi>b</mi><mi>d</mi><mi>a</mi><mo>=</mo><mi>l</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>P</mi><mi>l</mi><mi>a</mi><mi>n</mi><mi>k</mi><mi>_</mi><mi>c</mi><mi>o</mi><mi>n</mi><mi>s</mi><mi>tan</mi><mi>t</mi><mo>=</mo><mi>h</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>l</mi><mi>i</mi><mi>g</mi><mi>h</mi><mi>t</mi><mi>_</mi><mi>s</mi><mi>p</mi><mi>e</mi><mi>e</mi><mi>d</mi><mo>=</mo><mi>c</mi><mo>]</mo><mspace linebreak=\"newline\"/><mi>E</mi><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mi>l</mi><mi>a</mi><mi>m</mi><mi>b</mi><mi>d</mi><mi>a</mi><mo>=</mo><mi>l</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>P</mi><mi>l</mi><mi>a</mi><mi>n</mi><mi>k</mi><mi>_</mi><mi>c</mi><mi>o</mi><mi>n</mi><mi>s</mi><mi>tan</mi><mi>t</mi><mo>=</mo><mi>h</mi><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mi>l</mi><mi>i</mi><mi>g</mi><mi>h</mi><mi>t</mi><mi>_</mi><mi>s</mi><mi>p</mi><mi>e</mi><mi>e</mi><mi>d</mi><mo>=</mo><mi>c</mi><mo>]</mo><mspace linebreak=\"newline\"/><mi>E</mi><mo>=</mo><mi>h</mi><mo>*</mo><mi>v</mi><mo>=</mo><mfrac><mrow><mi>h</mi><mo>*</mo><mi>c</mi></mrow><mi>l</mi></mfrac></math>",
    { 1: "E = h*v", 2: "v = c/l" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: noExpressionTransformationRules,
        targetFactIdentifier: "",
        targetVariablesNames: 'E',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmeticWithoutDegree,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: quantumExpressionTransformationRules,
		allowedVariablesNames: "h;;;c;;;l",
    },
);


tasks[14] = new Task(
    num = 14,
    nextNum = 0,
    'Task 1. Lite',
	'Task 1. Express \'x\' from',
    '\\LARGE {\\textbf{Task 1. Express \'x\' from  }8x+4a = 5b+4x-4a^2}',
    3,
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>8</mn><mo>*</mo><mi>x</mi><mo>+</mo><mn>4</mn><mo>*</mo><mi>a</mi><mo>=</mo><mn>5</mn><mo>*</mo><mi>b</mi><mo>+</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>4</mn><mo>*</mo><msup><mi>a</mi><mn>2</mn></msup><mspace linebreak=\"newline\"/><mo>.</mo><mo>.</mo><mo>.</mo><mspace linebreak=\"newline\"/><mi>x</mi><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>8</mn><mo>*</mo><mi>x</mi><mo>+</mo><mn>4</mn><mo>*</mo><mi>a</mi><mo>=</mo><mn>5</mn><mo>*</mo><mi>b</mi><mo>+</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>4</mn><mo>*</mo><msup><mi>a</mi><mn>2</mn></msup><mspace linebreak=\"newline\"/><mn>4</mn><mo>*</mo><mi>x</mi><mo>=</mo><mn>5</mn><mo>*</mo><mi>b</mi><mo>-</mo><mn>4</mn><mo>*</mo><mi>a</mi><mo>-</mo><mn>4</mn><mo>*</mo><msup><mi>a</mi><mn>2</mn></msup><mspace linebreak=\"newline\"/><mi>x</mi><mo>=</mo><mn>1</mn><mo>.</mo><mn>25</mn><mo>*</mo><mi>b</mi><mo>-</mo><mi>a</mi><mo>-</mo><msup><mi>a</mi><mn>2</mn></msup></math>",
    { 1: "\\textbf{You know everything without tip}" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: noExpressionTransformationRules,
        targetFactIdentifier: "",
        targetVariablesNames: 'x',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: 'EXPRESSION_COMPARISON{(+(*(8;x);*(4;a)))}{=}{(+(*(5;b);*(4;x);-(*(4;^(a;2)))))}',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmetic,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[15] = new Task(
    num = 15,
    nextNum = 0,
    'Task 2. System',
	'Task 2. Express \'x\' and \'y\' from',
    '\\LARGE {\\textbf{Task 2. Express \'x\' and \'y\' from system of equations: }} \\\\ \\\\ \\LARGE {\\textbf{       }\\begin{cases} 2x + 3y = 7a \\\\ 4x + 2y = 6a \\end{cases}}',
    3,
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfenced open=\"{\" close=\"\"><mtable columnalign=\"left\"><mtr><mtd><mn>2</mn><mo>*</mo><mi>x</mi><mo>+</mo><mn>3</mn><mo>*</mo><mi>y</mi><mo>=</mo><mn>7</mn><mo>*</mo><mi>a</mi></mtd></mtr><mtr><mtd><mn>4</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>2</mn><mo>*</mo><mi>y</mi><mo>=</mo><mn>6</mn><mo>*</mo><mi>a</mi></mtd></mtr></mtable></mfenced><mspace linebreak=\"newline\"/><mo>.</mo><mo>.</mo><mo>.</mo><mspace linebreak=\"newline\"/><mfenced open=\"{\" close=\"\"><mtable columnalign=\"left\"><mtr><mtd><mi>x</mi><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></mtd></mtr><mtr><mtd><mi>y</mi><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></mtd></mtr></mtable></mfenced></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfenced open=\"{\" close=\"\"><mtable columnalign=\"left\"><mtr><mtd><mn>2</mn><mo>*</mo><mi>x</mi><mo>+</mo><mn>3</mn><mo>*</mo><mi>y</mi><mo>=</mo><mn>7</mn><mo>*</mo><mi>a</mi></mtd></mtr><mtr><mtd><mn>4</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>2</mn><mo>*</mo><mi>y</mi><mo>=</mo><mn>6</mn><mo>*</mo><mi>a</mi></mtd></mtr></mtable></mfenced><mo>=</mo><mo>&gt;</mo><mspace linebreak=\"newline\"/><mfenced open=\"{\" close=\"\"><mrow><mtable columnalign=\"left\"><mtr><mtd><mn>2</mn><mo>*</mo><mi>x</mi><mo>+</mo><mn>3</mn><mo>*</mo><mi>y</mi><mo>=</mo><mn>7</mn><mo>*</mo><mi>a</mi></mtd></mtr><mtr><mtd><mn>2</mn><mo>*</mo><mi>x</mi><mo>-</mo><mi>y</mi><mo>=</mo><mn>3</mn><mo>*</mo><mi>a</mi></mtd></mtr></mtable><mo>=</mo><mo>&gt;</mo></mrow></mfenced><mspace linebreak=\"newline\"/><mfenced open=\"{\" close=\"\"><mtable columnalign=\"left\"><mtr><mtd><mn>2</mn><mo>*</mo><mi>x</mi><mo>+</mo><mn>3</mn><mo>*</mo><mi>y</mi><mo>=</mo><mn>7</mn><mo>*</mo><mi>a</mi></mtd></mtr><mtr><mtd><mn>2</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>3</mn><mo>*</mo><mi>a</mi><mo>=</mo><mi>y</mi></mtd></mtr></mtable></mfenced><mo>=</mo><mo>&gt;</mo><mspace linebreak=\"newline\"/><mfenced open=\"{\" close=\"\"><mrow><mtable columnalign=\"left\"><mtr><mtd><mn>2</mn><mo>*</mo><mi>x</mi><mo>+</mo><mn>3</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>3</mn><mo>*</mo><mi>a</mi><mo>)</mo><mo>=</mo><mn>7</mn><mo>*</mo><mi>a</mi></mtd></mtr><mtr><mtd><mi>y</mi><mo>=</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>3</mn><mo>*</mo><mi>a</mi></mtd></mtr></mtable><mo>=</mo><mo>&gt;</mo></mrow></mfenced><mspace linebreak=\"newline\"/><mfenced open=\"{\" close=\"\"><mrow><mtable columnalign=\"left\"><mtr><mtd><mn>8</mn><mo>*</mo><mi>x</mi><mo>=</mo><mn>16</mn><mo>*</mo><mi>a</mi></mtd></mtr><mtr><mtd><mi>y</mi><mo>=</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>3</mn><mo>*</mo><mi>a</mi></mtd></mtr></mtable><mo>=</mo><mo>&gt;</mo></mrow></mfenced><mspace linebreak=\"newline\"/><mfenced open=\"{\" close=\"\"><mrow><mtable columnalign=\"left\"><mtr><mtd><mi>x</mi><mo>=</mo><mn>2</mn><mo>*</mo><mi>a</mi></mtd></mtr><mtr><mtd><mi>y</mi><mo>=</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>3</mn><mo>*</mo><mi>a</mi></mtd></mtr></mtable><mo>=</mo><mo>&gt;</mo></mrow></mfenced><mspace linebreak=\"newline\"/><mfenced open=\"{\" close=\"\"><mtable columnalign=\"left\"><mtr><mtd><mi>x</mi><mo>=</mo><mn>2</mn><mo>*</mo><mi>a</mi></mtd></mtr><mtr><mtd><mi>y</mi><mo>=</mo><mi>a</mi></mtd></mtr></mtable></mfenced></math>",
    { 1: "\\textbf{You know everything without tip}" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: noExpressionTransformationRules,
        targetFactIdentifier: "",
        targetVariablesNames: 'x;;;y',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: 'EXPRESSION_COMPARISON{(+(*(2;x);*(3;y)))}{=}{(*(7;a))};;;EXPRESSION_COMPARISON{(+(*(4;x);-(*(2;y))))}{=}{(*(6;a))}',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmetic,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[16] = new Task(
    num = 16,
    nextNum = 0,
    'Task 3. Combinatoric',
	'Task 3. Express \'x\' from',
    '\\LARGE {\\textbf{Task 3. Express \'x\' from  }C(x+1,x) = k}',
    3,
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi mathvariant=\"normal\">C</mi><mo>(</mo><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>,</mo><mi mathvariant=\"normal\">x</mi><mo>)</mo><mo>=</mo><mi mathvariant=\"normal\">k</mi><mspace linebreak=\"newline\"/><mo>.</mo><mo>.</mo><mo>.</mo><mspace linebreak=\"newline\"/><mi mathvariant=\"normal\">x</mi><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi mathvariant=\"normal\">C</mi><mo>(</mo><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>,</mo><mi mathvariant=\"normal\">x</mi><mo>)</mo><mo>=</mo><mi mathvariant=\"normal\">k</mi><mspace linebreak=\"newline\"/><mo>[</mo><mi mathvariant=\"normal\">C</mi><mo>(</mo><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>,</mo><mi mathvariant=\"normal\">x</mi><mo>)</mo><mo>=</mo><mo>(</mo><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>!</mo><mo>/</mo><mi mathvariant=\"normal\">x</mi><mo>!</mo><mo>/</mo><mo>(</mo><mo>(</mo><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>-</mo><mi mathvariant=\"normal\">x</mi><mo>)</mo><mo>!</mo><mo>=</mo><mo>(</mo><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>!</mo><mo>/</mo><mi mathvariant=\"normal\">x</mi><mo>!</mo><mo>/</mo><mn>1</mn><mo>!</mo><mo>=</mo><mo>(</mo><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>!</mo><mo>/</mo><mi mathvariant=\"normal\">x</mi><mo>!</mo><mo>/</mo><mn>1</mn><mo>=</mo><mi mathvariant=\"normal\">x</mi><mo>!</mo><mo>*</mo><mo>(</mo><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>/</mo><mi mathvariant=\"normal\">x</mi><mo>!</mo><mo>/</mo><mn>1</mn><mo>=</mo><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>]</mo><mspace linebreak=\"newline\"/><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>=</mo><mi mathvariant=\"normal\">k</mi><mspace linebreak=\"newline\"/><mo>(</mo><mi mathvariant=\"normal\">x</mi><mo>+</mo><mn>1</mn><mo>)</mo><mo>-</mo><mn>1</mn><mo>=</mo><mi mathvariant=\"normal\">k</mi><mo>-</mo><mn>1</mn><mo mathvariant=\"bold\">=</mo><mo mathvariant=\"bold\">&gt;</mo><mspace linebreak=\"newline\"/><mi mathvariant=\"normal\">x</mi><mo>=</mo><mi mathvariant=\"normal\">k</mi><mo>-</mo><mn>1</mn></math>",
    { 1: "C(m, n) = m! / (m - n)! / n! \\textbf{  -  the number of combinations  }" },
    {
        wellKnownFunctions: wellKnownFunctionsAllArithmetic,
        expressionTransformationRules: "C(m,n);;;m! / (m - n)! / n!;;;P(i, a, a, f(i));;;f(a);;;P(i, a, b, f(i));;;P(i, a, b-1, f(i)) * f(b);;;P(i, a, b, f(i));;;P(i, a+1, b, f(i)) * f(a);;;P(i, a, c, f(i)) * P(i, c+1, b, f(i));;;P(i, a, b, f(i));;;P(i, a, b, f(i));;;P(i, a, b-2, f(i)) * f(b) * f(b-1);;;P(i, a, b, f(i));;;P(i, a+2, b, f(i)) * f(a) * f(a+1)",
        targetFactIdentifier: "",
        targetVariablesNames: 'x',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: 'EXPRESSION_COMPARISON{(C(+(x;1);x))}{=}{(k)}',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: wellKnownFunctionsAllArithmetic,
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
    },
);


tasks[17] = new Task(
    num = 17,
    nextNum = 0,
    'Task 1. Lite',
	'Differentiate',
    '\\LARGE {\\textbf{Differentiate  }}\\frac{4x-7}{x^2+4}',
    4,
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>(</mo><mfrac><mrow><mn>4</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>7</mn></mrow><mrow><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>4</mn></mrow></mfrac><mo>)</mo><mo>'</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>(</mo><mfrac><mrow><mn>4</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>7</mn></mrow><mrow><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>4</mn></mrow></mfrac><mo>)</mo><mo>'</mo><mo>=</mo><mfrac><mrow><mn>4</mn><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>4</mn><mo>)</mo><mo>-</mo><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>4</mn><mo>)</mo><mo>'</mo><mo>*</mo><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>7</mn><mo>)</mo></mrow><msup><mrow><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>4</mn><mo>)</mo></mrow><mn>2</mn></msup></mfrac><mo>=</mo><mfrac><mrow><mn>4</mn><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>4</mn><mo>)</mo><mo>-</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>*</mo><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>-</mo><mn>7</mn><mo>)</mo></mrow><msup><mrow><mo>(</mo><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo><mn>4</mn><mo>)</mo></mrow><mn>2</mn></msup></mfrac></math>",
    { 1: "\\textbf{You know everything without tip}" },
    {
        wellKnownFunctions: '',
        expressionTransformationRules: ' ',
        targetFactIdentifier: "(d(/(+(*(4;x);-(7));+(^(x;2);4))))",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '1.0',
        unlimitedWellKnownFunctions: '',
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
		maxDistBetweenDiffSteps: "",
		forbiddenFunctions: "d;;;1;;;d;;;2",
    },
);


tasks[18] = new Task(
    num = 18,
    nextNum = 0,
    'Task 2. Lite',
	'Differentiate',
    '\\LARGE {\\textbf{Differentiate  }}\\frac{\cos(x)}{1-x}',
    4,
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfenced><mfrac><mrow><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>-</mo><mi>x</mi></mrow></mfrac></mfenced><mo>'</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfenced><mfrac><mrow><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><mrow><mn>1</mn><mo>-</mo><mi>x</mi></mrow></mfrac></mfenced><mo>'</mo><mo>=</mo><mfrac><mrow><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>'</mo><mo>*</mo><mo>(</mo><mn>1</mn><mo>-</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><msup><mrow><mo>(</mo><mn>1</mn><mo>-</mo><mi>x</mi><mo>)</mo></mrow><mn>2</mn></msup></mfrac><mo>=</mo><mfrac><mrow><mo>-</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>(</mo><mn>1</mn><mo>-</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow><msup><mrow><mo>(</mo><mn>1</mn><mo>-</mo><mi>x</mi><mo>)</mo></mrow><mn>2</mn></msup></mfrac></math>",
    { 1: "\\textbf{You know everything without tip}" },
    {
        wellKnownFunctions: '',
        expressionTransformationRules: ' ',
        targetFactIdentifier: "(d(/(cos(x);+(1;-(x)))))",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '1.0',
        unlimitedWellKnownFunctions: '',
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
		maxDistBetweenDiffSteps: "",
		forbiddenFunctions: "d;;;1;;;d;;;2",
    },
);


tasks[19] = new Task(
    num = 19,
    nextNum = 0,
    'Task 3. Middle',
	'Differentiate',
    '\\LARGE {\\textbf{Differentiate  }}\cos(x)^{\sin(x)}',
    4,
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>(</mo><msup><mrow><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></msup><mo>)</mo><mo>'</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>(</mo><msup><mrow><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></msup><mo>)</mo><mo>'</mo><mo>=</mo><mo>(</mo><msup><mi>e</mi><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>)</mo><mo>'</mo><mo>=</mo><msup><mi>e</mi><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>)</mo><mo>'</mo><mo>=</mo><msup><mi>e</mi><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mfrac><mrow><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>'</mo></mrow><mrow><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>)</mo><mo>=</mo><msup><mi>e</mi><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>-</mo><mfrac><mrow><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mn>2</mn></msup></mrow><mrow><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mo>)</mo></math>",
    { 1: "x^y = e^{y*ln(x)}" },
    {
        wellKnownFunctions: '',
        expressionTransformationRules: ' ',
        targetFactIdentifier: "(d(^(cos(x);sin(x))))",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '1.0',
        unlimitedWellKnownFunctions: '',
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
		maxDistBetweenDiffSteps: "",
		forbiddenFunctions: "d;;;1;;;d;;;2",
    },
);


tasks[20] = new Task(
    num = 20,
    nextNum = 0,
    'Task 4. Hard',
	'Differentiate',
    '\\LARGE {\\textbf{Differentiate  }}\cos(x)^{\sin(x)^{ch(x)}}',
    4,
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>(</mo><msup><mrow><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup></msup><mo>)</mo><mo>'</mo><mo>=</mo><mo>.</mo><mo>.</mo><mo>.</mo></math>",
    "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>[</mo><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>)</mo><mo>'</mo><mrow><mo>=</mo></mrow><mi>s</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mfrac><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>'</mo></mrow><mrow><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow><mo>=</mo></mrow><mi>s</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mo>(</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>)</mo><mo>'</mo><mrow><mo>=</mo></mrow><mo>(</mo><msup><mi>e</mi><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>)</mo><mo>'</mo><mrow><mo>=</mo></mrow><msup><mi>e</mi><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>)</mo><mo>'</mo><mrow><mo>=</mo></mrow><mo>(</mo><mi>s</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><msup><mi>e</mi><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mrow><mo>=</mo></mrow><mo>(</mo><mi>s</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>]</mo><mspace linebreak=\"newline\"/><mo>[</mo><mo>(</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>)</mo><mo>'</mo><mrow><mo>=</mo></mrow><mo>(</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>)</mo><mo>'</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mfrac><mrow><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>'</mo></mrow><mrow><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow><mo>=</mo></mrow><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>*</mo><mo>(</mo><mi>s</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>+</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mfrac><mrow><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>'</mo></mrow><mrow><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo></mrow></mfrac><mrow><mo>=</mo></mrow><mspace linebreak=\"newline\"/><mo>=</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>*</mo><mo>(</mo><mi>s</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>-</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mrow><mo>=</mo></mrow><mo>(</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>*</mo><mo>(</mo><mi>s</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>]</mo><mspace linebreak=\"newline\"/><mo>(</mo><msup><mrow><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup></msup><mo>)</mo><mo>'</mo><mrow><mo>=</mo></mrow><mo>(</mo><msup><mi>e</mi><mrow><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>)</mo><mo>'</mo><mrow><mo>=</mo></mrow><msup><mi>e</mi><mrow><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mo>(</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>)</mo><mo>'</mo><mrow><mo>=</mo></mrow><msup><mrow><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup></msup><mo>*</mo><mo>(</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>)</mo><mo>'</mo><mrow><mo>=</mo></mrow><msup><mrow><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup></msup><mo>*</mo><msup><mrow><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow><mrow><mo>(</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></mrow></msup><mo>*</mo><mo>(</mo><mi>ln</mi><mo>(</mo><mi>cos</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>*</mo><mo>(</mo><mi>s</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>ln</mi><mo>(</mo><mi>sin</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>+</mo><mi>c</mi><mi>h</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>*</mo><mi>c</mi><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo><mo>&#xA0;</mo><mo>-</mo><mo>&#xA0;</mo><mi>t</mi><mi>g</mi><mo>(</mo><mi>x</mi><mo>)</mo><mo>)</mo></math>",
    { 1: "x^y = e^{y*ln(x)}" },
    {
        wellKnownFunctions: '',
        expressionTransformationRules: ' ',
        targetFactIdentifier: "(d(^(cos(x);^(sin(x);ch(x)))))",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '1.0',
        unlimitedWellKnownFunctions: '',
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
		maxDistBetweenDiffSteps: "",
		forbiddenFunctions: "d;;;1;;;d;;;2",
    },
);


tasks[21] = new Task(
    num = 21,
    nextNum = 0,
    'Default settings',
	'Check what you want',
    '\\LARGE {\\textbf{Check what you want. Supported operations: + - * / \\^\\ , trigonometry, logarifm, logical}}',
    5,
	"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>s</mi><mi>a</mi><mi>m</mi><mi>p</mi><mi>l</mi><mi>e</mi><mo>=</mo><mi>s</mi><mi>a</mi><mi>m</mi><mi>p</mi><mi>l</mi><mi>e</mi></math>",
    "",
    { 1: "\\textbf{You know everything without tip}" },
    {
        wellKnownFunctions: '',
        expressionTransformationRules: '',
        targetFactIdentifier: "",
        targetVariablesNames: '',
        minNumberOfMultipliersInAnswer: '',
        maxNumberOfDivisionsInAnswer: '',
        additionalFactsIdentifiers: '',
        maxExpressionTransformationWeight: '',
        unlimitedWellKnownFunctions: '',
        shortErrorDescription: '1',
        taskContextExpressionTransformationRules: '',
		allowedVariablesNames: "",
		maxDistBetweenDiffSteps: "100000",
		forbiddenFunctions: "",
    },
);